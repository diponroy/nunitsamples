﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class StaffAccountPom : WebPageObjectModel, IPageObjectModel
    {
        public StaffAccountPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/StaffAccount";
        }

        public string ExpectedTitle()
        {
            return "Staff Profile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
