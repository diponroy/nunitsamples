﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class ConsultationsPom : WebPageObjectModel, IPageObjectModel
    {
        public ConsultationsPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public bool checkScheduledConsultatoin(string date, string time, string patient, string doctor)
        {
            bool found = false;
            Console.WriteLine("Searching for: " + patient + " " + doctor + " " + date + " " + time);
            ReadOnlyCollection<IWebElement> scheduledConsultations =
                WebDriver.FindElements(By.XPath("//*[@id=\"divScheduledConsultations\"]/div"));

            foreach (IWebElement consultation in scheduledConsultations)
            {
                Console.WriteLine(consultation.Text);
                if (consultation.Text.Contains(date) && consultation.Text.Contains(time) &&
                    consultation.Text.Contains(patient) && consultation.Text.Contains(doctor))
                {
                    consultation.FindElement(By.ClassName("btn-cell")).Click();
                    found = true;
                    break;
                }
            }

            return found;
        }

        public override string Route()
        {
            return "Admin/Consultations";
        }

        public string ExpectedTitle()
        {
            return "Consultations";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
