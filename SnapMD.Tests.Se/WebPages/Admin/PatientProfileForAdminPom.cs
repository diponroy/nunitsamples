﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class PatientProfileForAdminPom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.CssSelector, Using = "ul.commentList > li")]
        public IList<IWebElement> NoteHolders { get; set; }

        [FindsBy(How = How.Id, Using = "txtNotes")]
        public IWebElement TxtAddNote { get; set; }

        [FindsBy(How = How.Id, Using = "btnsubmit")]
        public IWebElement BtnAddNote { get; set; }

        [FindsBy(How = How.Id, Using = "txtPNotes")]
        public IWebElement TxtUpdateNote { get; set; }

        [FindsBy(How = How.Id, Using = "btnUpdatePatientNote")]
        public IWebElement BtnUpdateNote { get; set; }

        public IWebElement FindNote(string note)
        {
            var noteHolder = NoteHolders.FirstOrDefault(x => x.FindElement(By.ClassName("comment")).Text.Trim() == note);
            return noteHolder;
        }

        public void EditNote(IWebElement noteHolderElement, string newNote)
        {
            IWebElement btnShowNoteInPopup = noteHolderElement.FindElement(By.ClassName("editPatientNotes"));
            btnShowNoteInPopup.Click();
            TestingHelpers.WaitUntil(WebDriver, d => btnShowNoteInPopup.Displayed && btnShowNoteInPopup.Enabled);
            
            TxtUpdateNote.Clear();
            TxtUpdateNote.SendKeys(newNote);
            BtnUpdateNote.Click();
        }

        public void DeleteNote(IWebElement noteHolderElement)
        {
            var btnDelete = noteHolderElement.FindElement(By.ClassName("deletePatientNotes"));
            btnDelete.Click();
        }


        public PatientProfileForAdminPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }


        public void SubmitNote(string note)
        {
            TxtAddNote.SendKeys(note);
            BtnAddNote.Click();
        }

        public void editNote(string noteText, string noteTextNew)
        {
            //find created note and click Edit
            ReadOnlyCollection<IWebElement> notes = WebDriver.FindElements(By.ClassName("comment"));
            foreach (IWebElement note in notes)
            {
                if (note.Text == noteText)
                {
                    note.FindElement(By.XPath("..//*[@class='editPatientNotes']")).Click();
                    break;
                }
            }

            //type new text in field
            IWebElement noteField = WebDriver.FindElement(By.Id("txtPNotes"));
            Thread.Sleep(1000);
            noteField.Click();
            noteField.Clear();
            noteField.SendKeys(noteTextNew);

            //click Update
            WebDriver.FindElement(By.Id("btnUpdatePatientNote")).Click();
        }


        public override string Route()
        {
            return "Admin/Patient";
        }

        public string ExpectedTitle()
        {
            return "Patient Profile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
