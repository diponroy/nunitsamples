﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class PatientListPom : WebPageObjectModel, IPageObjectModel
    {
        #region Patient Grid

        [FindsBy(How = How.CssSelector, Using = "div.searchBox > div> input.search")]
        public IWebElement TxtSearchBox { get; set; }

        [FindsBy(How = How.CssSelector, Using = "a.allPatients")]
        public IWebElement BtnAllPatients { get; set; }


        [FindsBy(How = How.CssSelector, Using = "table#accountsGrid tr")]
        public IList<IWebElement> PatientRows { get; set; }

        public IWebElement FindPatient(string patientName)
        {
            var row = PatientRows.FirstOrDefault(x => x.FindElement(By.XPath("./td[3]/span[1]")).Text.Trim() == patientName.Trim());
            return row;
        }

        public void SearchPatientRows(string searchByPatientName)
        {
            TxtSearchBox.Value(searchByPatientName);
            TxtSearchBox.TriggerChange(WebDriver);
            Wait(2);    /*wait to start ajax call*/
            WaitForAjax();
        }

        /*may not work as expected, if multiple patient got same name*/
        public IWebElement SearchAndGetStaffRow(string patientName)
        {
            SearchPatientRows(patientName);
            Wait(2);
            var element = FindPatient(patientName);
            if (element == null)
            {
                throw new NullReferenceException("unable to get patient row with this name");
            }
            return element;
        }

        public IWebElement LinkPatientFile(IWebElement patientRow)
        {
            IWebElement link = patientRow.FindElement(By.CssSelector("a.PatientFile"));
            return link;
        }

        public IWebElement BtnScheduleAppointment(IWebElement patientRow)
        {
            IWebElement btn = patientRow.FindElement(By.CssSelector("a.schedulerbtn"));
            return btn;
        }

        #endregion

        #region Popup
        [FindsBy(How = How.CssSelector, Using = "div#divSchedulerAptPopup.popup")]
        public IWebElement AppointmentPopup { get; set; }
        #endregion

        public PatientListPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/Patients";
        }

        public string ExpectedTitle()
        {
            return "Patient List";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void ShowScheduleAppointment(IWebElement patient)
        {
            IWebElement btn = BtnScheduleAppointment(patient);
            btn.Click();
            if (!IsVisiable(AppointmentPopup))
            {
                throw new Exception("appointment popup is not showing up.");
            }
            WaitForAjax();
        }

        public void ShowPatientFile(IWebElement patient)
        {
            IWebElement link = LinkPatientFile(patient);
            link.Click();
            HasRedirectedFromUrl(); /*no need to use wait*/
        }
    }
}
