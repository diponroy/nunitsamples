﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    /*NEED: route from "Admin/ForgotPasswordAdmin.aspx" at route table*/

    public class AdminForgotPasswordPom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.Id, Using = "txtEmail")]
        public IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "btnSendEmail")]
        public IWebElement BtnSendEmail { get; set; }

        [FindsBy(How = How.LinkText, Using = "Back")]
        public IWebElement LinkBack { get; set; }

        public AdminForgotPasswordPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/ForgotPasswordAdmin.aspx";
        }

        public string ExpectedTitle()
        {
            return "Forgot Password";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void SendRecoveryEmail(string email)
        {
            TxtEmail.SendKeys(email);
            BtnSendEmail.Click();
        }
    }
}
