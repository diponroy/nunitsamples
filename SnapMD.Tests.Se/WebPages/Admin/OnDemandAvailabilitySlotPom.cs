﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    internal class OnDemandAvailabilitySlotPom : WebPageObjectModel, IPageObjectModel
    {
        public OnDemandAvailabilitySlotPom(IWebDriver webDriver) : base(webDriver)
        {
        }


        public void OpenScheduleConsultation(DateTime date)
        {
            String day = date.Day.ToString();
            //Console.WriteLine(day);

            //Get calendar
            IWebElement baseTable = WebDriver.FindElement(By.ClassName("k-scheduler-content"));
            IList<IWebElement> tableRows = baseTable.FindElements(By.TagName("tr"));

            //Go to calendar items
            foreach (IWebElement tableData in tableRows)
            {
                // Console.WriteLine(tableData.Text);

                //Click on current day
                if (tableData.Text.Contains(day))
                {
                    //Doubule click
                    //var action = new Actions(WebDriver);
                    //action.DoubleClick(tableData);
                    //action.Perform();
                    tableData.DoubleClick(WebDriver);

                    break;
                }
            }
        }

        public void ScheduleAvailability(String doctor, DateTime due)
        {
            IWebElement dueDate =
                WebDriver.FindElement(
                    By.CssSelector(
                        "body > div:nth-child(16) > div.k-popup-edit-form.k-scheduler-edit-form.k-window-content.k-content > div > div:nth-child(1) > p:nth-child(3) > span > span > input"));
            dueDate.Clear();
            dueDate.SendKeys(due.ToString("M/dd/yyyy hh:mm tt"));
            Thread.Sleep(5000);
        }


        public override string Route()
        {
            return "Admin/ScheduleCalendar";
        }

        public string ExpectedTitle()
        {
            return "ScheduleCalendar";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
