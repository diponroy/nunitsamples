﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class AdminRxNTPom : WebPageObjectModel, IPageObjectModel
    {
        public AdminRxNTPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "ePrescription/RxNT.aspx";
        }

        public string ExpectedTitle()
        {
            throw new NotImplementedException();
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
