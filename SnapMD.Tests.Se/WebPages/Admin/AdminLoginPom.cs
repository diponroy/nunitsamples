﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class AdminLoginPom : WebPageObjectModel, IPageObjectModel, ILoginPom
    {
        [FindsBy(How = How.Id, Using = "txtloginemail")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.Id, Using = "txtPassword")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement BtnLogin { get; set; }

        [FindsBy(How = How.LinkText, Using = "password")]
        public IWebElement LinkForgotPassword { get; set; }

        public AdminLoginPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/Login";
        }

        public string ExpectedTitle()
        {
            return "Login";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void SubminCredentials(String email, String password)
        {
            Email.SendKeys(email);
            Password.SendKeys(password);
            BtnLogin.Submit();
        }
    }
}
