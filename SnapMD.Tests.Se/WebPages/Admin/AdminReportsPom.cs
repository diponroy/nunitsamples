﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class AdminReportsPom : WebPageObjectModel, IPageObjectModel
    {
        public AdminReportsPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/Reports";
        }

        public string ExpectedTitle()
        {
            return "Reports";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
