﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class ManageRolesPom : WebPageObjectModel, IPageObjectModel
    {
        public ManageRolesPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/CreateRoles";
        }

        public string ExpectedTitle()
        {
            return "CreateRoles";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
