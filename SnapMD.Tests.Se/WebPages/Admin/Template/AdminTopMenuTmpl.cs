﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin.Template
{
    public class AdminTopMenuTmpl
    {
        public readonly IWebDriver WebDriver;

        [FindsBy(How = How.Id, Using = "lidashboard")]
        public IWebElement LinkDashboard { get; set; }

        [FindsBy(How = How.Id, Using = "listaff")]
        public IWebElement LinkStaffAccounts { get; set; }

        [FindsBy(How = How.Id, Using = "lipatients")]
        public IWebElement LinkPatients { get; set; }

        [FindsBy(How = How.Id, Using = "lireports")]
        public IWebElement LinkReports { get; set; }

        [FindsBy(How = How.Id, Using = "liroles")]
        public IWebElement LinkManageRoles { get; set; }


        [FindsBy(How = How.Id, Using = "lifiles")]
        public IWebElement LinkFiles { get; set; }


        [FindsBy(How = How.Id, Using = "liconsults")]
        public IWebElement LinkConsultations { get; set; }


        public AdminTopMenuTmpl(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }
    }
}
