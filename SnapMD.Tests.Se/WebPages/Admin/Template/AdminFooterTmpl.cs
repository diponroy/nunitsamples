﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin.Template
{
    class AdminFooterTmpl
    {
        public readonly IWebDriver WebDriver;

        [FindsBy(How = How.LinkText, Using = "Powered by SnapMD")]
        public IWebElement LinkPoweredBy { get; set; }

        [FindsBy(How = How.LinkText, Using = "End User License Agreement")]
        public IWebElement LinkAgreement { get; set; }

        public AdminFooterTmpl(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }
    }
}
