﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class DashBoardPom : WebPageObjectModel, IPageObjectModel
    {
        public override string Route()
        {
            return "Admin/Dashboard";
        }

        public string ExpectedTitle()
        {
            return "Dashboard";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public DashBoardPom(IWebDriver webDriver) : base(webDriver)
        {
        }
    }
}
