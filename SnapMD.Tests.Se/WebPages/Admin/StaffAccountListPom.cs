﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class StaffAccountListPom : WebPageObjectModel, IPageObjectModel
    {
        #region Staff Grid
        [FindsBy(How = How.CssSelector, Using = "div.searchBox > input.search")]
        public IWebElement TxtSearchBox { get; set; }

        [FindsBy(How = How.CssSelector, Using = "table#accountsGrid tr")]
        public IList<IWebElement> StaffRows { get; set; }

        public IWebElement FindStaff(string staffName)
        {
            var row = StaffRows.FirstOrDefault(x => x.FindElement(By.XPath("./td[3]/span[1]")).Text.Trim() == staffName.Trim());
            return row;
        }

        public void SearchStaffRows(string searchByStaffName)
        {
            TxtSearchBox.Value(searchByStaffName);
            TxtSearchBox.TriggerChange(WebDriver);
            Wait(2);    /*wait to start ajax call*/
            WaitForAjax();
        }

        /*may not work as expected, if multiple staff got same name*/
        public IWebElement SearchAndGetStaffRow(string staffName)
        {
            SearchStaffRows(staffName);
            Wait(2);
            var element = FindStaff(staffName);
            if (element == null)
            {
                throw new NullReferenceException("unable to get staff row with this name");
            }
            return element;
        }

        public IWebElement BtnScheduleAppointment(IWebElement staffRow)
        {
            IWebElement btn = staffRow.FindElement(By.CssSelector("a.schedulerbtn"));
            return btn;
        }
        #endregion

        #region Popup
        [FindsBy(How = How.CssSelector, Using = "div#divSchedulerAptPopup.popup")]
        public IWebElement AppointmentPopup { get; set; }
        #endregion

        public StaffAccountListPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/StaffAccounts";
        }

        public string ExpectedTitle()
        {
            return "Staff List";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void ShowAppointmentPopUp(IWebElement staffRow)
        {
            IWebElement btn = BtnScheduleAppointment(staffRow);
            btn.Click();
            if (!IsVisiable(AppointmentPopup))
            {
                throw new Exception("appointment popup is not showing up.");
            }
            WaitForAjax();
        }
    }
}
