﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Admin
{
    public class HospitalAdminFilesPom : WebPageObjectModel, IPageObjectModel
    {
        public HospitalAdminFilesPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Admin/HospitalAdminFiles";
        }

        public string ExpectedTitle()
        {
            return "HospitalAdminFiles";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
