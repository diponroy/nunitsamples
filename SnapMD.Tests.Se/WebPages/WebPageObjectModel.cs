﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se.WebPages
{
    public abstract class WebPageObjectModel
    {
        public readonly IWebDriver WebDriver;

        [FindsBy(How = How.ClassName, Using = "snapError")]
        public virtual IList<IWebElement> ErrorNotifications { get; set; }

        [FindsBy(How = How.ClassName, Using = "snapSuccess")]
        public virtual IList<IWebElement> SuccessNotifications { get; set; }

        [FindsBy(How = How.ClassName, Using = "k-notification-confirmation")]
        public virtual IList<IWebElement> ConfirmationNotifications { get; set; }

        public virtual bool ConfirmationNotificationContains(out IWebElement confirmationNotification, string message = "")
        {
            Func<IWebElement, bool> qsnFunc = x => x.Displayed && x.FindElement(By.CssSelector("P")).Text.Contains(message);
            
            confirmationNotification = null;
            bool hasNotification = TestingHelpers.WaitUntil(WebDriver, d => ConfirmationNotifications.Any(qsnFunc));
            if (hasNotification)
            {
                confirmationNotification = ConfirmationNotifications.First(qsnFunc);
            }

            return hasNotification;
        }

        public virtual void Confirm(IWebElement confirmationNotification, bool isConfirmed = true)
        {
            IWebElement btnConfirm = confirmationNotification.FindElement(isConfirmed ? By.Id("btnConfirmYes") : By.Id("btnConfirmNo"));
            btnConfirm.Click();
        }

        public virtual string Url
        {
            get
            {
                string baseUrl = TestSettings.Datas.Hospital.DomainName.Trim(new[] { ' ', '/' });
                string route = Route().Trim(new[] { ' ', '/' });
                return String.Format("{0}/{1}", baseUrl, route);
            }
        }

        public abstract string Route();

        protected WebPageObjectModel(IWebDriver webDriver)
        {
            WebDriver = webDriver;
            InitializePageFactory();
        }

        public abstract void InitializePageFactory();

        public virtual void NavigateOrRefreshUrl(string with = "")
        {
            string expectedUrl = Url + with;
            if (!WebDriver.Url.Equals(expectedUrl))
            {
                NavigateToUrlWith(with);
            }
            else
            {
                WebDriver.Navigate().Refresh();
            }
        }

        public virtual void NavigateToUrl()
        {
            NavigateToUrlWith("");
        }

        public virtual void NavigateToUrlWith(string value)
        {
            string expectedUrl = Url + value;
            WebDriver.Navigate().GoToUrl(expectedUrl);
        }

        public virtual bool HasNavigatedAtUrl()
        {
            return HasNavigatedAtUrlWith("");
        }

        public virtual bool HasNavigatedAtUrlWith(string value)
        {
            string expectedUrl = Url + value;
            return TestingHelpers.WaitUntil(WebDriver, driver => driver.Url.ToLower().Equals(expectedUrl.ToLower()));
        }

        public virtual bool HasRedirectedFromUrl()
        {
            return HasRedirectedFromUrlWith("");
        }

        public virtual bool HasRedirectedFromUrlWith(string value)
        {
            string expectedUrl = Url + value;
            return TestingHelpers.WaitUntil(WebDriver, driver => !driver.Url.ToLower().Equals(expectedUrl.ToLower()));
        }

        public virtual bool ErrorNotificationContains(string message = "")
        {
            return TestingHelpers.WaitUntil(WebDriver, d => ErrorNotifications.Any(x => x.Displayed && x.Text.Contains(message)));
        }

        public virtual bool SuccessNotificationContains(string message = "")
        {
            return TestingHelpers.WaitUntil(WebDriver, d => SuccessNotifications.Any(x => x.Displayed && x.Text.Contains(message)));
        }

        public virtual void Wait(uint timeInSeconds)
        {
            TestingHelpers.Wait(new TimeSpan(0, 0, (int)timeInSeconds));
        }

        public virtual void WaitForAjax()
        {
            TestingHelpers.WaitForAjax(WebDriver);
        }

        public virtual bool IsVisiable(IWebElement webElement)
        {
            return TestingHelpers.WaitUntil(WebDriver, d => webElement.Displayed && webElement.Enabled);
        }

    }
}
