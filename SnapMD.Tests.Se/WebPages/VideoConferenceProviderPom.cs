﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages
{
    public class VideoConferenceProviderPom
    {
        public virtual string Url
        {
            get
            {
                return "http://tokbox.com/tools/connectivity";
            }
        }
    }
}
