﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.SnapMDAdmin
{
    /*NEED: a route for "SnapMDAdmin/SnapAdmin.aspx at route table"*/
    internal class SnapMdAdminHomePom : WebPageObjectModel, IPageObjectModel
    {
        public override string Route()
        {
            return "SnapMDAdmin/SnapAdmin.aspx";
        }

        public string ExpectedTitle()
        {
            throw new System.NotImplementedException();
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public SnapMdAdminHomePom(IWebDriver webDriver) : base(webDriver)
        {
        }
    }
}
