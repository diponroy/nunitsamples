﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace SnapMD.Tests.Se.WebPages
{
    public interface IPageObjectModel
    {
        string Url { get; }
        string Route();
        string ExpectedTitle();

        void NavigateOrRefreshUrl(string with = "");
        void NavigateToUrl();
        void NavigateToUrlWith(string value);

        bool HasNavigatedAtUrl();
        bool HasNavigatedAtUrlWith(string value);

        bool HasRedirectedFromUrl();
        bool HasRedirectedFromUrlWith(string value);

        IList<IWebElement> SuccessNotifications { get; set; }
        bool SuccessNotificationContains(string message);

        IList<IWebElement> ErrorNotifications { get; set; }
        bool ErrorNotificationContains(string message);

        IList<IWebElement> ConfirmationNotifications { get; set; }
        bool ConfirmationNotificationContains(out IWebElement confirmationNotification, string message);
        void Confirm(IWebElement confirmationNotification, bool isConfirmed);

        bool IsVisiable(IWebElement webElement);

        void Wait(uint timeInSeconds);
        void WaitForAjax();

        void InitializePageFactory();
    }
}
