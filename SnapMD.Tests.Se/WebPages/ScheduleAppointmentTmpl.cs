﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages
{
    internal class ScheduleAppointmentTmpl
    {
        public readonly IWebDriver WebDriver;

        [FindsBy(How = How.Id, Using = "chkNoChargeConsultation")]
        public IWebElement ChkIsChargeConsultation { get; set; }

        [FindsBy(How = How.Id, Using = "cmbDoctorsList")]
        public IWebElement DdlDoctors { get; set; }

        [FindsBy(How = How.Id, Using = "cmbPatientsList")]
        public IWebElement DdlPatients { get; set; }

        [FindsBy(How = How.Id, Using = "selPrimaryConcern")]
        public IWebElement DdlPrimaryConcerns { get; set; }

        [FindsBy(How = How.Id, Using = "selSecondaryConcern")]
        public IWebElement DdlSecondaryConcerns { get; set; }

        [FindsBy(How = How.Id, Using = "chkNone")]
        public IWebElement ChkSecondaryConcernNone { get; set; }

        [FindsBy(How = How.Id, Using = "txtAppointmentNotes")]
        public IWebElement TxtAreaNote { get; set; }

        [FindsBy(How = How.Id, Using = "dtpScheduleDatepicker")]
        public IWebElement TxtScheduleDate { get; set; }

        [FindsBy(How = How.Id, Using = "dtpScheduleTimepicker")]
        public IWebElement TxtScheduleTime { get; set; }

        [FindsBy(How = How.Id, Using = "btnScheduleAppointment")]
        public IWebElement BntCreateScheduleAppointment { get; set; }

        public ScheduleAppointmentTmpl(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }
    }
}
