﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace SnapMD.Tests.Se.WebPages
{
    public interface ILogoutPom
    {
        IWebElement LinkLogOut { get; }
        void RequestLogout();
    }
}
