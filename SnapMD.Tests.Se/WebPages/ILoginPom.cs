﻿using System;
using OpenQA.Selenium;

namespace SnapMD.Tests.Se.WebPages
{
    public interface ILoginPom
    {
        IWebElement Email { get; set; }

        IWebElement Password { get; set; }

        IWebElement BtnLogin { get; set; }

        void SubminCredentials(String email, String password);
    }
}
