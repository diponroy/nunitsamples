﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientConsultationsPom : WebPageObjectModel, IPageObjectModel
    {
        public PatientConsultationsPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/PatientConsultations";
        }

        public string ExpectedTitle()
        {
            return "PatientConsultations";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
