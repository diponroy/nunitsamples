﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class SignUpPom : WebPageObjectModel, IPageObjectModel
    {
        /*step-1*/
        [FindsBy(How = How.Id, Using = "registerPanel-1")]
        public IWebElement RegisterPanel1 { get; set; }

        [FindsBy(How = How.Id, Using = "txtzipcode")]
        public IWebElement TxtZipCode { get; set; }

        [FindsBy(How = How.CssSelector, Using = "button.slider-next.next.orange.step-2")]
        public IWebElement BtnGotoStep2 { get; set; }

        /*step-2*/
        [FindsBy(How = How.Id, Using = "registerPanel-2")]
        public IWebElement RegisterPanel2 { get; set; }

        [FindsBy(How = How.Id, Using = "txtfirstname")]
        public IWebElement TxtFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "txtlastname")]
        public IWebElement TxtLastName { get; set; }

        [FindsBy(How = How.Id, Using = "txtemail")]
        public IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "txtdob")]
        public IWebElement TxtDateBirth { get; set; }

        [FindsBy(How = How.CssSelector, Using = "button.slider-next.next.orange.step-3")]
        public IWebElement BtnGotoStep3 { get; set; }


        /*step-3*/
        [FindsBy(How = How.Id, Using = "registerPanel-3")]
        public IWebElement RegisterPanel3 { get; set; }

        [FindsBy(How = How.Id, Using = "txtaddress")]
        public IWebElement TxtAddress { get; set; }

        [FindsBy(How = How.Id, Using = "field_country")]
        public IWebElement DdlCountry { get; set; }

        [FindsBy(How = How.CssSelector, Using = "button.slider-next.next.orange.step-4")]
        public IWebElement BtnGotoStep4 { get; set; }


        /*step-4*/
        [FindsBy(How = How.Id, Using = "registerPanel-4")]
        public IWebElement RegisterPanel4 { get; set; }

        [FindsBy(How = How.Id, Using = "txtpassword")]
        public IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "txtconfirmpassword")]
        public IWebElement TxtConfirmPassword { get; set; }

        [FindsBy(How = How.Id, Using = "chkterms")]
        public IWebElement ChkTermsAndConditions { get; set; }

        [FindsBy(How = How.CssSelector, Using = "button.finish.green")]
        public IWebElement BtnRegister { get; set; }



        public SignUpPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        //public void typeZipCode(string zipCode)
        //{
        //    IWebElement zipCodeField = WebDriver.FindElement(By.Id("txtzipcode"));
        //    zipCodeField.SendKeys(zipCode);
        //}

        //public void clickNext()
        //{
        //    IWebElement nextButton = WebDriver.FindElement(By.ClassName("slider-next.next.orange"));
        //    nextButton.Click();
        //}

        //public void typeFirstName(string firstName)
        //{
        //    IWebElement firstNameField = WebDriver.FindElement(By.Id("txtfirstname"));
        //    firstNameField.SendKeys(firstName);
        //}

        //public void typeLastName(string lastName)
        //{
        //    IWebElement lastNameField = WebDriver.FindElement(By.Id("txtlastname"));
        //    lastNameField.SendKeys(lastName);
        //}

        //public void typeEmail(string email)
        //{
        //    IWebElement emailField = WebDriver.FindElement(By.Id("txtemail"));
        //    emailField.SendKeys(email);
        //}

        //public void typeDOB(string dob)
        //{
        //    IWebElement dobField = WebDriver.FindElement(By.Id("txtdob"));
        //    dobField.SendKeys(dob);
        //}

        //public void typeAddress(string address)
        //{
        //    IWebElement addressField = WebDriver.FindElement(By.Id("txtaddress"));
        //    addressField.SendKeys(address);
        //}

        //public void selectCountry()
        //{
        //    //open dropdown
        //    WebDriver.FindElement(By.XPath("//*[@id=\"field_country\"]/../span/span")).Click();

        //    ReadOnlyCollection<IWebElement> countries =
        //        WebDriver.FindElements(By.XPath("//*[@id=\"field_country_listbox\"]/li"));

        //    Thread.Sleep(1000);

        //    //click random country
        //    countries[ Utility.RandomNumber(1, countries.Count() - 1)].Click();
        //}

        //public void typePasswords(string password)
        //{
        //    IWebElement passwordField = WebDriver.FindElement(By.Id("txtpassword"));
        //    passwordField.SendKeys(password);

        //    IWebElement passwordConfirmationField = WebDriver.FindElement(By.Id("txtconfirmpassword"));
        //    passwordConfirmationField.SendKeys(password);
        //}

        //public void checkTermsAndConditions()
        //{
        //    IWebElement termsAndConditions = WebDriver.FindElement(By.ClassName("iCheck-helper"));
        //    termsAndConditions.Click();
        //}

        public override string Route()
        {
            return "Customer/SignUp.aspx";
        }

        public string ExpectedTitle()
        {
            return "Registration | SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
