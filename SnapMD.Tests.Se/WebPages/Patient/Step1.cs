﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class Step1
    {
        private IWebDriver webDriver;

    public Step1(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }



    public void patientPrimaryConcern(String concern)
    {
        try
        {
            IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div > div:nth-child(3) > div:nth-child(1) > span > span > span > span"));
            dropDownListBox.Click();
            Thread.Sleep(2000);
            IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

            foreach (IWebElement item in options)
            {
                if (item.Text.Contains(concern))
                {
                   item.Click();
               }
            }
        }
        
        catch (StaleElementReferenceException)
        {
            
            Console.WriteLine("some errors but I am well");
        }
        
    }

    public void patientSecondaryConcern(String concern)
    {
       // webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //Wait for  5 sec

        try
        {
            IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div > div:nth-child(3) > div:nth-child(2) > span > span > span > span"));
            dropDownListBox.Click();

            Thread.Sleep(2000);

             IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

              foreach (IWebElement item in options)
             {
              if (item.Text.Contains(concern))
             {
               item.Click();
            }
            }

        }
        catch (StaleElementReferenceException)
        {
            
            Console.WriteLine("some errors but I am well");
        }
    }


    public void clickNext()
    {
        var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
        wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_lnkNext"))); //Wait for element to show up
        var nextButton = webDriver.FindElement(By.Id("ContentPlaceHolder1_lnkNext"));
        nextButton.Click();

    }
    public void checkIfClicked()
    {
        webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
        //Check if you are on page
        Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Medical History"));

    }
    }
}
