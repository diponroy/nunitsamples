﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class NewProfilePom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.Id, Using = "inputAddName")]
        public IWebElement TxtName { get; set; }

        [FindsBy(How = How.Id, Using = "inputAddEmail")]
        public IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "btnsubmit")]
        public IWebElement BtnSubmit { get; set; }

        [FindsBy(How = How.Id, Using = "btnCancel")]
        public IWebElement BtnCancel { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_divWarning")]
        public IWebElement DivEmailVerification { get; set; }

        public NewProfilePom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/NewProfile";
        }

        public string ExpectedTitle()
        {
            return "AddNewUserProfile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void AddProfile(string name, string email)
        {
            TxtName.Value(name);
            TxtEmail.Value(email);
            BtnSubmit.Click();
        }
    }
}
