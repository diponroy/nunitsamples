﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class Step2
    {
        private IWebDriver webDriver;
        public Step2(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }


        public void setlistOfChronic(String condition)
        {
            IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_liMedicalCondition1"));
            SelectElement clickThis = new SelectElement(dropDownListBox);
            clickThis.SelectByText(condition);

        }
        public void setlistOfPrior(String surgery)
        {
            var priorsurgery = webDriver.FindElement(By.Id("ContentPlaceHolder1_idSurgery1"));
            priorsurgery.SendKeys(surgery);

            IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_listMonthID1"));
            SelectElement clickThis = new SelectElement(dropDownListBox);
            clickThis.SelectByText("JAN");

            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_listYearID1"))); //Wait for element to show up

            IWebElement dropDownListBox2 = webDriver.FindElement(By.Id("ContentPlaceHolder1_listYearID1"));
            SelectElement clickThis2 = new SelectElement(dropDownListBox2);
            clickThis2.SelectByValue("2000");




        }

        public void clickNext()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_btnNext"))); //Wait for element to show up
            var nextButton = webDriver.FindElement(By.Id("ContentPlaceHolder1_btnNext"));
            nextButton.Click();
        }
        public void checkIfClicked()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Medications"));

        }

        public void chronicMedicalConditionsNone()
        {
            var checkbox = webDriver.FindElement(By.CssSelector("#cb1 > div"));
            checkbox.Click();

        }
        public void listPriorSurgeriesNone()
        {
            var checkbox = webDriver.FindElement(By.CssSelector("#cb2 > div"));
            checkbox.Click();

        }
    
    
    
    
    }
}
