﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientHealthPlansPom : WebPageObjectModel, IPageObjectModel
    {
        public PatientHealthPlansPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/HealthPlan";
        }

        public string ExpectedTitle()
        {
            return "HealthPlan";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
