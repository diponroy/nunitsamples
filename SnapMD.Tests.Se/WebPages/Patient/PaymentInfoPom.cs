﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PaymentInfoPom : WebPageObjectModel, IPageObjectModel
    {
        /*show to add card*/
        [FindsBy(How = How.Id, Using = "ddAddCard")]
        public IWebElement DivAddCardPanel { get; set; }

        [FindsBy(How = How.Id, Using = "idAddCard")]
        public IWebElement BtnShowAddCardPanel { get; set; }

        /*add card info*/

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_card_number")]
        public IWebElement TxtCardNumber { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_ddlMonth")]
        public IWebElement DdlExpirationMonth { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_ddlYear")]
        public IWebElement DdlExpirationYear { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_cscCode")]
        public IWebElement TxtCvvCode { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_firstName")]
        public IWebElement TxtFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_lastName")]
        public IWebElement TxtLastName { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_billingAddress")]
        public IWebElement TxtAddress { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_city")]
        public IWebElement TxtCity { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_state")]
        public IWebElement TxtState { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_zip")]
        public IWebElement TxtZip { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_ctrlAddCard_ddlCountry")]
        public IWebElement DdlCountry { get; set; }

        [FindsBy(How = How.Id, Using = "btnClose")]
        public IWebElement BtnCancelAddNewCard { get; set; }

        [FindsBy(How = How.Id, Using = "btnsubmit")]
        public IWebElement BtnAddNewCard { get; set; }

        public PaymentInfoPom(IWebDriver webDriver) : base(webDriver)
        {
        }


        public void addNewCreditCard()
        {
            IWebElement newCC = WebDriver.FindElement(By.Id("idAddCard"));
            newCC.Click();
        }

        public void enterFirstName(String name)
        {
            IWebElement firstName = WebDriver.FindElement(By.Id("first_name"));
            firstName.SendKeys(name);
        }

        public void enterLastName(String name)
        {
            IWebElement lastName = WebDriver.FindElement(By.Id("last_name"));
            lastName.SendKeys(name);
        }

        public void enterCCNumber(String number)
        {
            IWebElement ccnumber = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_card_number"));
            ccnumber.SendKeys(number);
        }

        public void enterBilingAddress1(String address)
        {
            IWebElement baddress1 = WebDriver.FindElement(By.Id("newaddress1"));
            baddress1.SendKeys(address);
        }

        public void enterBilingAddress2(String address)
        {
            IWebElement baddress2 = WebDriver.FindElement(By.Id("newaddress2"));
            baddress2.SendKeys(address);
        }

        //public void enterCVV(String cvv)

        //public void enterCVV(String cvv)
        //{
        //    var baddress2 = webDriver.FindElement(By.Id("csc_code"));
        //    baddress2.SendKeys(cvv);

        //}
        public void enterCity(String address)
        {
            IWebElement city = WebDriver.FindElement(By.Id("newcity"));
            city.SendKeys(address);
        }

        public void enterState(String address)
        {
            IWebElement state = WebDriver.FindElement(By.Id("newstate"));
            state.SendKeys(address);
        }

        public void enterZIP(String address)
        {
            IWebElement zip = WebDriver.FindElement(By.Id("newzip_code"));
            zip.SendKeys(address);
        }

        //public void enterPhone(String phone)
        //{
        //    var phone1 = webDriver.FindElement(By.Id("phone02"));
        //    phone1.SendKeys(phone);

        //}

        public void expDate(String month)
        {
            IWebElement dropDownListBox =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#ContentPlaceHolder1_ContentPlaceHolder2_addCard > div > span:nth-child(5) > div.left > span > span.k-widget.k-dropdown.k-header.month.custom"));
            dropDownListBox.Click();
            IList<IWebElement> options = WebDriver.FindElements(By.ClassName("k-item"));

            foreach (IWebElement item in options)
            {
                if (item.Text.Contains(month))
                {
                    item.Click();
                }
            }
        }

        public void expYear(String year)
        {
            IWebElement dropDownListBox =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#ContentPlaceHolder1_ContentPlaceHolder2_addCard > div > span:nth-child(5) > div.left > span > span.k-widget.k-dropdown.k-header.year.custom"));
            dropDownListBox.Click();
            // clickThis.SelectByText(date);
            IList<IWebElement> options = WebDriver.FindElements(By.ClassName("k-item"));

            foreach (IWebElement item in options)
            {
                if (item.Text.Contains(year))
                {
                    item.Click();
                }
            }
        }


        public virtual void ccType(String type)
        {
            IWebElement dropDownListBox =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#ContentPlaceHolder1_ContentPlaceHolder2_addCard > div > span:nth-child(2) > div.right > span > span"));
            dropDownListBox.Click();

            IList<IWebElement> options = WebDriver.FindElements(By.ClassName("k-item"));

            foreach (IWebElement item in options)
            {
                if (item.Text.Contains(type))
                {
                    item.Click();
                }
            }
        }

        public void submit()
        {
            IWebElement subbutton = WebDriver.FindElement(By.Id("btnSubmit"));
            subbutton.Click();
        }


        public override string Route()
        {
            return "Customer/AddPaymentInformation";
        }

        public string ExpectedTitle()
        {
            return "AddPaymentInformation";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
