﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class UserProfilesPom : WebPageObjectModel, IPageObjectModel
    {
        public UserProfilesPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/Users";
        }

        public string ExpectedTitle()
        {
            return "SnapMD-UserProfiles";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
