﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class EnterWaitingRoom
    {
        private IWebDriver webDriver;
        public EnterWaitingRoom(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void clickEnter()
        {
            var enterWR = webDriver.FindElement(By.Id("waitingbtn"));
            enterWR.Click();
        }
        public void checkIfClicked()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Please Wait..."));

        }






    }
}
