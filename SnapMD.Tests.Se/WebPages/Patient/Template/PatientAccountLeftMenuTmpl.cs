﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient.Template
{
    public class PatientAccountLeftMenuTmpl
    {
        public readonly IWebDriver WebDriver;

        public PatientAccountLeftMenuTmpl(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        [FindsBy(How = How.Id, Using = "liUserProfile")]
        public IWebElement BtnUserProfiles { get; set; }

        [FindsBy(How = How.Id, Using = "liPatientProfile")]
        public IWebElement BtnDependentProfiles { get; set; }

        [FindsBy(How = How.Id, Using = "liUserFiles")]
        public IWebElement BtnMyFiles { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_liHealthPlans")]
        public IWebElement BtnHealthPlans { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_liPaymentInfo")]
        public IWebElement BtnPaymentInformation { get; set; }

        [FindsBy(How = How.Id, Using = "liConsultation")]
        public IWebElement BtnConsultations { get; set; }

        [FindsBy(How = How.Id, Using = "liAccountSett")]
        public IWebElement BtnAccountSettings { get; set; }
    }
}
