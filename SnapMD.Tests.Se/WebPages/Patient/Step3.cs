﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class Step3
    {
         private IWebDriver webDriver;
        public Step3(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void setAlergicTo(String alergic)
        {
            //IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_ddlMedicationAllergy1"));
            //SelectElement clickThis = new SelectElement(dropDownListBox);
            //clickThis.SelectByText(alergic);
            try
            {
                IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div:nth-child(2) > div.row > ol > li:nth-child(1) > div > span > span > span > span"));
                dropDownListBox.Click();
                Thread.Sleep(2000);
                IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

                foreach (IWebElement item in options)
                {
                    if (item.Text.Contains(alergic))
                    {
                        item.Click();
                    }
                }
            }

            catch (StaleElementReferenceException)
            {

                Console.WriteLine("some errors but I am well");
            }

        }
       
        public void setAlergicTo1(String alergic)
        {
            //IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_ddlMedicationAllergy1"));
            //SelectElement clickThis = new SelectElement(dropDownListBox);
            //clickThis.SelectByText(alergic);
            try
            {
                IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div:nth-child(2) > div.row > ol > li:nth-child(2) > div > span > span > span > span"));
                dropDownListBox.Click();
                Thread.Sleep(2000);
                IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

                foreach (IWebElement item in options)
                {
                    if (item.Text.Contains(alergic))
                    {
                        item.Click();
                    }
                }
            }

            catch (StaleElementReferenceException)
            {

                Console.WriteLine("some errors but I am well");
            }

        }

        public void setAlergicTo2(String alergic)
        {
            try
            {
                IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div:nth-child(2) > div.row > ol > li:nth-child(3) > div > span > span > span > span"));
                dropDownListBox.Click();
                Thread.Sleep(2000);
                IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

                foreach (IWebElement item in options)
                {
                    if (item.Text.Contains(alergic))
                    {
                        item.Click();
                    }
                }
            }

            catch (StaleElementReferenceException)
            {

                Console.WriteLine("some errors but I am well");
            }

        }
        public void setAlergicTo3(String alergic)
        {
            //IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_ddlMedicationAllergy1"));
            //SelectElement clickThis = new SelectElement(dropDownListBox);
            //clickThis.SelectByText(alergic);
            try
            {
                IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div:nth-child(2) > div.row > ol > li:nth-child(4) > div > span > span > span > span"));
                dropDownListBox.Click();
                Thread.Sleep(2000);
                IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

                foreach (IWebElement item in options)
                {
                    if (item.Text.Contains(alergic))
                    {
                        item.Click();
                    }
                }
            }

            catch (StaleElementReferenceException)
            {

                Console.WriteLine("some errors but I am well");
            }

        }
        public void setCurrentlyTaking(String taking)
        {

            //var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            //wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_ddlCurrentMedication1"))); //Wait for element to show up\

            //IWebElement dropDownListBox = webDriver.FindElement(By.Id("ContentPlaceHolder1_ddlCurrentMedication1"));
            //SelectElement clickThis = new SelectElement(dropDownListBox);
            //clickThis.SelectByText(taking);
            try
            {
                IWebElement dropDownListBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.patient-form > div > div.form-content.center > div.row > ol > li:nth-child(1) > div > span > span > span > span"));
                dropDownListBox.Click();
                System.Threading.Thread.Sleep(2000);
                IList<IWebElement> options = webDriver.FindElements(By.ClassName("k-item"));

                foreach (IWebElement item in options)
                {
                    if (item.Text.Contains(taking))
                    {
                        item.Click();
                    }
                }
            }

            catch (StaleElementReferenceException)
            {

                Console.WriteLine("some errors but I am well");
            }

        }

        public void clickNext()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_btnNext"))); //Wait for element to show up
            var nextButton = webDriver.FindElement(By.Id("ContentPlaceHolder1_btnNext"));
            nextButton.Click();
        }
        public void checkIfClicked()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            //Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Consent to Treat"));

        }

        public void listMedicationsAlergicNone()
        {
            var checkbox = webDriver.FindElement(By.XPath("//*[@id=\"content\"]/div[2]/div[2]/div/div[1]/div[2]/div/div"));
            checkbox.Click();

        }
    
        public void listMedicationTakingNone()
        {
            var checkbox = webDriver.FindElement(By.XPath("//*[@id=\"content\"]/div[2]/div[2]/div/div[2]/div[1]/div/div"));
            checkbox.Click();
        }
    
    
    }
}
