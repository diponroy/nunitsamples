﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    /*impt: 
     * To select checkbox and radio btn click its holder, and use real item inupt to check if selected
     *      find more from test, ui using css jcf-hidden
     */

    public class AddDependentPom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.Name, Using = "userfile")]
        public IWebElement BtnUploadPhoto { get; set; }

        [FindsBy(How = How.Id, Using = "btnsubmit")]
        public IWebElement BtnSave { get; set; }

        [FindsBy(How = How.Id, Using = "ancCancelAddDepProfile")]
        public IWebElement BtnCancel { get; set; }


        /*personal info*/

        [FindsBy(How = How.Id, Using = "txtFirstName")]
        public IWebElement TxtFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "txtLastName")]
        public IWebElement TxtLastName { get; set; }

        [FindsBy(How = How.Id, Using = "txtDOB")]
        public IWebElement TxtDateOfBirth { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"field_gendermale\"]/../div")] /*ui using css jcf-hidden*/
        public IWebElement ChkGenderMaleHolder { get; set; }

        [FindsBy(How = How.Id, Using = "field_gendermale")]
        public IWebElement ChkGenderMale { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"field_genderfemale\"]/../div")]
        public IWebElement ChkGenderFemaleHolder { get; set; }

        [FindsBy(How = How.Id, Using = "field_genderfemale")]
        public IWebElement ChkGenderFemale { get; set; }


        /*address*/

        [FindsBy(How = How.Id, Using = "emailaddress")]
        public IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "s5")]
        public IWebElement DdlCountry { get; set; }

        [FindsBy(How = How.Id, Using = "txtCountryCode")]
        public IWebElement TxtMobilePhoneCountryCode { get; set; }

        [FindsBy(How = How.Id, Using = "field_cellphone")]
        public IWebElement TxtMobilePhoneNumber { get; set; }

        [FindsBy(How = How.Id, Using = "txtHomeContact")]
        public IWebElement TxtHomePhoneNumber { get; set; }

        [FindsBy(How = How.Id, Using = "tbHomeAddress")]
        public IWebElement TxtAddress { get; set; }


        /*physical details*/

        [FindsBy(How = How.Id, Using = "drpdwnEthnicity")]
        public IWebElement DdlEthnicity { get; set; }

        [FindsBy(How = How.Id, Using = "drpdwnHairColor")]
        public IWebElement DdlHairColor { get; set; }

        [FindsBy(How = How.Id, Using = "drpdwnEyeColor")]
        public IWebElement DdlEyeColor { get; set; }

        [FindsBy(How = How.Id, Using = "drpdwnBloodType")]
        public IWebElement DdlBloodType { get; set; }

        [FindsBy(How = How.Id, Using = "weight")]
        public IWebElement NTxtBoxWeight { get; set; }

        [FindsBy(How = How.Id, Using = "cmbWeightUnits")]
        public IWebElement DdlWeightUnit { get; set; }

        [FindsBy(How = How.Id, Using = "heightfeet")]
        public IWebElement NTxtBoxHeightFeet { get; set; }

        [FindsBy(How = How.Id, Using = "heightinches")]
        public IWebElement NTxtBoxHeightInche { get; set; }

        [FindsBy(How = How.Id, Using = "cmbHeightUnits")]
        public IWebElement DdlHeightUnit { get; set; }


        /*relations*/

        [FindsBy(How = How.Id, Using = "drpdwnRelationShip")]
        public IWebElement DdlRelationShip { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"field_authorized\"]/../div")]
        public IWebElement ChkAuthorizedYesHolder { get; set; }

        [FindsBy(How = How.Id, Using = "field_authorized")]
        public IWebElement ChkAuthorizedYes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"field_authorizedno\"]/../div")]
        public IWebElement ChkAuthorizedNoHolder { get; set; }

        [FindsBy(How = How.Id, Using = "field_authorizedno")]
        public IWebElement ChkAuthorizedNo { get; set; }


        /*medical conditions*/

        [FindsBy(How = How.Id, Using = "idMCondCheck")]
        public IWebElement ChkMedicalConditionsNone { get; set; }

        /*medical allergies*/

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_idMAllergyCheck")]
        public IWebElement ChkMedicalAllergiesNone { get; set; }


        /*family doctor*/

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_txtPhysician")]
        public IWebElement TxtFamilyDoctorName { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_txtPhysicianContact")]
        public IWebElement TxtFamilyDoctorPhone { get; set; }

        /*clinician cpecialist*/

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_chkSpecialist")]
        public IWebElement ChkClinicianSpecialistNone { get; set; }

        /*preferred pharmacy*/

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_chkPharmacy")]
        public IWebElement ChkPreferredPharmacyNone { get; set; }

        private readonly TestingHelpers helper = new TestingHelpers();

        public AddDependentPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public string enterFirstName()
        {
            //generate random string for first name
            string firstName = Utility.RandomString(Utility.RandomNumber(9, 15));

            helper.typeInTextField(WebDriver, "txtFirstName", firstName);

            return firstName;
        }

        public string enterLastName()
        {
            //generate random string for last name
            string lastName = Utility.RandomString(Utility.RandomNumber(9, 15));

            helper.typeInTextField(WebDriver, "txtLastName", lastName);

            return lastName;
        }


        public string enterDOB()
        {
            //generate new random date of birth
            int years = Utility.RandomNumber(-99, -18);
            int days = Utility.RandomNumber(-364, 0);
            string dateOfBirth = DateTime.Today.AddYears(years)
                .AddDays(days)
                .ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);

            helper.typeInTextField(WebDriver, "txtDOB", dateOfBirth);

            return dateOfBirth;
        }

        public string selectGender()
        {
            if (Utility.RandomNumber(1, 2) == 1)
            {
                WebDriver.FindElement(By.XPath("//*[@id=\"field_gendermale\"]/../div")).Click();
                return "Male";
            }
            WebDriver.FindElement(By.XPath("//*[@id=\"field_genderfemale\"]/../div")).Click();
            return "Female";
        }

        public string setEthnicity()
        {
            string fieldPath = "//*[@id=\"drpdwnEthnicity\"]/../span/span";
            string listPath = "//*[@id=\"drpdwnEthnicity_listbox\"]/li";
            string inputPath = "//*[@id=\"drpdwnEthnicity_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string setHairColor()
        {
            string fieldPath = "//*[@id=\"drpdwnHairColor\"]/../span/span";
            string listPath = "//*[@id=\"drpdwnHairColor_listbox\"]/li";
            string inputPath = "//*[@id=\"drpdwnHairColor_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string setEyeColor()
        {
            string fieldPath = "//*[@id=\"drpdwnEyeColor\"]/../span/span";
            string listPath = "//*[@id=\"drpdwnEyeColor_listbox\"]/li";
            string inputPath = "//*[@id=\"drpdwnEyeColor_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string setBloodType()
        {
            string fieldPath = "//*[@id=\"drpdwnBloodType\"]/../span/span";
            string listPath = "//*[@id=\"drpdwnBloodType_listbox\"]/li";
            string inputPath = "//*[@id=\"drpdwnBloodType_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string setWeight()
        {
            //generate random weight
            string weight = Utility.RandomNumber(1, 450).ToString();

            //click on weight input field
            WebDriver.FindElement(By.XPath("//*[@id=\"weight\"]/..")).Click();

            //clear text from field 
            WebDriver.FindElement(By.Id("weight")).Clear();

            //enter new value
            WebDriver.FindElement(By.Id("weight")).SendKeys(weight);

            return weight;
        }

        public string setWeightUnit()
        {
            string fieldPath = "//*[@id=\"cmbWeightUnits\"]/../span/span";
            string listPath = "//*[@id=\"cmbWeightUnits_listbox\"]/li";
            string inputPath = "//*[@id=\"cmbWeightUnits_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string setHeight()
        {
            //generate random height
            string height = Utility.RandomNumber(1, 10).ToString();

            //click on height ft/m input field
            WebDriver.FindElement(By.XPath("//*[@id=\"heightfeet\"]/..")).Click();

            //clear text from field 
            WebDriver.FindElement(By.Id("heightfeet")).Clear();

            //enter new value
            WebDriver.FindElement(By.Id("heightfeet")).SendKeys(height);

            return height;
        }

        public string setCountry()
        {
            string fieldPath = "//*[@id=\"s5\"]/../span/span";
            string listPath = "//*[@id=\"s5_listbox\"]/li";
            string inputPath = "//*[@id=\"s5_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);
        }

        public string enterCellPhone()
        {
            //generate random cell phone number
            string cellPhone = Utility.RandomString(Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(WebDriver, "field_cellphone", cellPhone);

            return cellPhone;
        }

        public string enterHomePhone()
        {
            //generate random home phone number
            string homePhone = Utility.RandomString(Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(WebDriver, "txtHomeContact", homePhone);

            return homePhone;
        }

        public string setRelationshipAndAuthorize()
        {
            string fieldPath = "//*[@id=\"drpdwnRelationShip\"]/../span/span";
            string listPath = "//*[@id=\"drpdwnRelationShip_listbox\"]/li";
            string inputPath = "//*[@id=\"drpdwnRelationShip_listbox\"]/../span/input";

            string relationship = helper.chooseRandomFromDropdown(WebDriver, fieldPath, listPath, inputPath);

            //click Yes for authorization
            WebDriver.FindElement(By.XPath("//*[@id=\"field_authorized\"]/../div")).Click();

            //click Accept on popup
            Thread.Sleep(1000);
            WebDriver.FindElement(By.Id("snapPopUpOkButton")).Click();

            return relationship;
        }

        public string enterOrganization()
        {
            //generate random string for organization
            string organization = Utility.RandomString(Utility.RandomNumber(9, 20));

            helper.typeInTextField(WebDriver, "txtOrganization", organization);

            return organization;
        }

        public string enterLocation()
        {
            //generate random string for location
            string location = Utility.RandomString(Utility.RandomNumber(9, 20));

            helper.typeInTextField(WebDriver, "txtLocation", location);

            return location;
        }

        public Dictionary<string, string> fillInData()
        {
            //create dictionary with new patient informations
            var newValues = new Dictionary<string, string>();

            //type first name and add value to dictionary
            newValues.Add("firstName", enterFirstName());

            //type last name and add value to dictionary
            newValues.Add("lastName", enterLastName());

            //enter date of birth and add value to dictionary
            newValues.Add("dateOfBirth", enterDOB());

            //select gender and add value to dictionary
            newValues.Add("gender", selectGender());

            //set ethnicity and add value to dictionary
            newValues.Add("ethnicity", setEthnicity());

            //set hair color and add value to dictionary
            newValues.Add("hairColor", setHairColor());

            //set eye color and add value to dictionary
            newValues.Add("eyeColor", setEyeColor());

            //set blood type and add value to dictionary
            newValues.Add("bloodType", setBloodType());

            //set weight and add value to dictionary
            newValues.Add("weight", setWeight());

            //set weight unit and add value to dictionary
            newValues.Add("weightUnit", setWeightUnit());

            //set height and add value to dictionary
            newValues.Add("height", setHeight());

            //set country and add value to dictionary
            newValues.Add("country", setCountry());

            //type cell phone and add value to dictionary
            newValues.Add("cellPhone", enterCellPhone());

            //type home phone and add value to dictionary
            newValues.Add("homePhone", enterHomePhone());

            //set relationship and add value to dictionary
            newValues.Add("relationship", setRelationshipAndAuthorize());

            //type organization and add value to dictionary
            newValues.Add("organization", enterOrganization());

            //type location and add value to dictionary
            newValues.Add("location", enterLocation());
            return newValues;
        }

        public void clickSave()
        {
            IWebElement save = WebDriver.FindElement(By.Id("btnsubmit"));
            save.Click();
        }

        public override string Route()
        {
            return "Customer/AddDependent";
        }

        public string ExpectedTitle()
        {
            return "Add Dependent Profile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
