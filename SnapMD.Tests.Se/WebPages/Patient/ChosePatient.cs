﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class ChosePatient
    {
        private IWebDriver webDriver;
        public ChosePatient(IWebDriver webDriver)
        {
            this.webDriver=webDriver;
        }

    
        public void selectPatient()
        {
            //wait for page to load
            webDriver.FindElement(By.Id("ancAddDepProfile"));
            System.Threading.Thread.Sleep(1000);

            var patients = webDriver.FindElements(By.TagName("label"));

            foreach(var patient in patients)
            {
                if (patient.Text == "Test Acc")
                {
                    patient.Click();
                    break;
                }
            }
            //var patient = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.profile-section > div.profile-gallery.profile-gallery-height > div > ul > li:nth-child(1) > a"));
            //patient.Click();
        }

        public void chekIfPatientIsSelected()
        {
            //Wait 10 seconds for element to show up
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_spnSteps2"))); //Wait for element to show up

            //Check if you are on page
            Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Patient Information"));

        }
    
    }
}
