﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class InsuranceAndPayment
    {
        private IWebDriver webDriver;
        public InsuranceAndPayment(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }


        public void clickSubmit()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_aSubmitPayDis"))); //Wait for element to show up
            var nextButton = webDriver.FindElement(By.Id("ContentPlaceHolder1_aSubmitPayDis"));
            nextButton.Click();
        }
        public void confirm()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            var proceed = webDriver.FindElement(By.Id("aProceedPayDis"));
            proceed.Click();
        }
        public void checkIfClicked()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Payment Success"));

        }

    }
}
