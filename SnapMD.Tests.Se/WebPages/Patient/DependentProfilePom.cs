﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class DependentProfilePom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_anchrEdit")]
        public IWebElement BtnEditProfile { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"ContentPlaceHolder1_ContentPlaceHolder2_anchrEdit\"]/../a[1]")]
        public IWebElement BtnClose { get; set; }

        [FindsBy(How = How.Id, Using = "ContentPlaceHolder1_ContentPlaceHolder2_aRemove")]
        public IWebElement BtnDelete { get; set; }

        public DependentProfilePom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public void checkData (Dictionary<string, string> dependentData)
        {
            //check name (first + last)
            string actualName = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_lblUserName")).Text;
            string expectedName = dependentData["firstName"] + " " + dependentData["lastName"];
            Assert.AreEqual(expectedName, actualName, "Name is not correct");

            //check date of birth
            string actualDateOfBirth = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblDOB")).Text;
            Assert.AreEqual(dependentData["dateOfBirth"], actualDateOfBirth.Substring(2, actualDateOfBirth.Length - 2), "Date of birth is not correct");

            //check sex
            string actualSex = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblGender")).Text;
            Assert.AreEqual(dependentData["gender"], actualSex.Substring(2, actualSex.Length - 2), "Sex is not correct");

            //check ethnicity
            string actualEthnicity = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblEthnicity")).Text;
            Assert.AreEqual(dependentData["ethnicity"], actualEthnicity.Substring(2, actualEthnicity.Length - 2), "Ethnicity is not correct");

            //check hair color
            string actualHairColor = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHairColor")).Text;
            Assert.AreEqual(dependentData["hairColor"], actualHairColor.Substring(2, actualHairColor.Length - 2), "Hair color is not correct");

            //check eye color
            string actualEyeColor = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblEyeColor")).Text;
            Assert.AreEqual(dependentData["eyeColor"], actualEyeColor.Substring(2, actualEyeColor.Length - 2), "Eye color is not correct");

            //check blood type
            string actualBloodType = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblBloodType")).Text;
            Assert.AreEqual(dependentData["bloodType"], actualBloodType.Substring(2, actualBloodType.Length - 2), "Blood type is not correct");

            //check weight
            string actualWeight = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblWeight")).Text;
            Assert.AreEqual(dependentData["weight"] + " " + dependentData["weightUnit"], actualWeight.Substring(2, actualWeight.Length - 2), "Weight and/or weight unit is not correct");

            //check height
            string actualHeight = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHeight")).Text;
            Assert.AreEqual(dependentData["height"] + " ft 0 in", actualHeight.Substring(2, actualHeight.Length - 2), "Height is not correct");

            //check cell phone
            string actualCellPhone = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblCellPhone")).Text;
            actualCellPhone = actualCellPhone.Substring(2, actualCellPhone.Length - 2);
            if (dependentData["country"].ToLower().Equals("united states"))
            {
                Assert.AreEqual("+1" + dependentData["cellPhone"], actualCellPhone, "Cell phone is not correct");
            }
            else if (dependentData["country"].ToLower().Equals("united kingdom"))
            {
                Assert.AreEqual("+44" + dependentData["cellPhone"], actualCellPhone, "Cell phone is not correct");
            }

            //check home phone
            string actualHomePhone = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHomePhone")).Text;
            Assert.AreEqual(dependentData["homePhone"], actualHomePhone.Substring(2, actualHomePhone.Length - 2), "Home phone is not correct");

            //check organization
            string actualOrganization = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblOrganization")).Text;
            Assert.AreEqual(dependentData["organization"], actualOrganization.Substring(2, actualOrganization.Length - 2), "Organization is not correct");

            //check location
            string actualLocation = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblLocation")).Text;
            Assert.AreEqual(dependentData["location"], actualLocation.Substring(2, actualLocation.Length - 2), "Location is not correct");
        }

        public void confirmDelete()
        {
            WebDriver.FindElement(By.Id("btnConfirmYes")).Click();
        }

        public override string Route()
        {
            return "Customer/Dependent";
        }

        public string ExpectedTitle()
        {
            return "View Dependent Profile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
