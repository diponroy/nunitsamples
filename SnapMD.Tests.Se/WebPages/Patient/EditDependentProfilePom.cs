﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class EditDependentProfilePom : WebPageObjectModel, IPageObjectModel
    {
        public EditDependentProfilePom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/EditDependent";
        }

        public string ExpectedTitle()
        {
            return "Edit Dependent Profile";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
