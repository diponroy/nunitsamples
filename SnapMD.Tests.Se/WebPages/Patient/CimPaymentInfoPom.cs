﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class CimPaymentInfoPom : PaymentInfoPom
    {
        private readonly IWebDriver _webDriver;

        public CimPaymentInfoPom(IWebDriver webDriver) : base(webDriver as ChromeDriver)
        {
            _webDriver = webDriver;
        }

        public void AddNewCreditCard()
        {
            var newCC = _webDriver.FindElement(By.Id("idAddCard"));
            newCC.Click();
        }

        public void EnterFirstName(String name)
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15));
            wait.Until(d => d.FindElement(By.Id("first_name")).Displayed);
            var firstName = _webDriver.FindElement(By.Id("first_name"));
            firstName.SendKeys(name);
        }

        public void EnterLastName(String name)
        {
            var lastName = _webDriver.FindElement(By.Id("last_name"));
            lastName.SendKeys(name);
        }

        public void EnterCCNumber(String number)
        {
            var ccnumber = _webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_card_number"));
            ccnumber.SendKeys(number);
        }

        public void EnterBillingAddress(String address)
        {
            var baddress1 = _webDriver.FindElement(By.Id("newaddress1"));
            baddress1.SendKeys(address);
        }
       
        public void EnterCity(String address)
        {
            var city = _webDriver.FindElement(By.Id("newcity"));
            city.SendKeys(address);

        }
        public void EnterState(String address)
        {
            var state = _webDriver.FindElement(By.Id("newstate"));
            state.SendKeys(address);

        }
        public void EnterZip(String address)
        {
            var zip = _webDriver.FindElement(By.Id("newzip_code"));
            zip.SendKeys(address);
        }

        public void ExpMonth(int month)
        {
            IWebElement dropDownListBox = _webDriver.FindElement(
                By.CssSelector(
                    "#ContentPlaceHolder1_ContentPlaceHolder2_addCard > div:nth-child(2) > span:nth-child(2) > div:nth-child(1) > span > span.month"));

            dropDownListBox.Click();

            for (int i = 0; i < month; i++)
            {
                dropDownListBox.SendKeys(Keys.ArrowDown);
            }
        }

        public void ExpYear(String year)
        {
            int targetYear;
            if (!int.TryParse(year, out targetYear))
            {
                Assert.Fail("Bad test input for CimPaymentPage.ExpYear().");
            }
            
            IWebElement dropDownListBox = _webDriver.FindElement(
                By.CssSelector(
                    "#ContentPlaceHolder1_ContentPlaceHolder2_addCard > div:nth-child(2) > span:nth-child(2) > div:nth-child(1) > span > span.year"));
            dropDownListBox.Click();

            for (int i = DateTime.Today.Year; i <= targetYear; i++)
            {
                dropDownListBox.SendKeys(Keys.ArrowDown);
            }
        }

        [Obsolete("Deprecated in 1.9.5.")]
        public override void ccType(String type)
        {
            throw new NotImplementedException("Deprecated in 1.9.5.");
        }
       
        public void Submit()
        {
            var subbutton = _webDriver.FindElement(By.Id("btnSubmit"));
            subbutton.Click();
        }


        public void EnterCvv(string s)
        {
            var field = _webDriver.FindElement(By.Id("csc_code"));
            field.SendKeys(s);
        }
    }
}
