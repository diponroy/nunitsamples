﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientLoginPom : WebPageObjectModel, IPageObjectModel, ILoginPom
    {
        [FindsBy(How = How.Id, Using = "txtloginemail")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.Id, Using = "txtPassword")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement BtnLogin { get; set; }

        //[FindsBy(How = How.XPath, Using = "//a[contains(text(), 'password')]")]
        [FindsBy(How = How.LinkText, Using = "password")]
        public IWebElement LinkForgotPassword { get; set; }

        //[FindsBy(How = How.XPath, Using = "//a[contains(text(), 'Register Here')]")]
        [FindsBy(How = How.LinkText, Using = "Register Here")]
        public IWebElement LinkRegister { get; set; }

        public PatientLoginPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/Login";
        }

        public string ExpectedTitle()
        {
            return "Login | SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void SubminCredentials(String email, String password)
        {
            Email.SendKeys(email);
            Password.SendKeys(password);
            BtnLogin.Submit();
        }
    }
}

