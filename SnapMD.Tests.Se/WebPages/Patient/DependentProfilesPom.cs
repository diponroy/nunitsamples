﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class DependentProfilesPom : WebPageObjectModel, IPageObjectModel
    {

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/form/div[3]/div/div[1]/div/div/div[1]/a")]
        public IWebElement BtnAddNewDependentProfile { get; set; }

        [FindsBy(How = How.CssSelector, Using = "table.profiles.dependentlist > tbody > tr")]
        public IList<IWebElement> DependentRows { get; set; }

        public IList<IWebElement> AuthorizedDependentRows()
        {
            return DependentRows.Where(x => !x.HasCssClass("not-authorized")).ToList();
        }

        public IList<IWebElement> UnauthorizedDependentRows()
        {
            return DependentRows.Where(x => x.HasCssClass("not-authorized")).ToList();
        }

        public bool IsDependentAuthorized(IWebElement dependentRow)
        {
            return dependentRow.HasCssClass("not-authorized");
        }

        public void ViewDependentRow(IWebElement dependentRow)
        {
            var btnView = dependentRow.FindElement(By.XPath("./td[5]/a"));
            btnView.Click();
        }

        private IWebElement Find(IEnumerable<IWebElement> dependentRows, string dependentName)
        {
            return
                dependentRows.FirstOrDefault(
                    x => x.FindElement(By.XPath("./td[2]")).Text.Trim().Equals(dependentName));

        }
        public DependentProfilesPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement FindDependentRow(string dependentName)
        {
            return Find(DependentRows, dependentName);
        }

        public IWebElement FindAuthorizedDependentRow(string dependentName)
        {
            return Find(AuthorizedDependentRows(), dependentName);
        }

        public IWebElement FindUnauthorizedDependentRow(string dependentName)
        {
            return Find(UnauthorizedDependentRows(), dependentName);
        }

        public void viewDependentProfile(string dependentName)
        {
            IWebElement table = WebDriver.FindElement(By.ClassName("profiles.dependentlist"));
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> tableRows = table.FindElements(By.TagName("tr"));

            foreach (IWebElement row in tableRows)
            {
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> rowData = row.FindElements(By.TagName("td"));
                if (rowData[1].Text == dependentName)
                {
                    rowData[4].Click();
                    break;
                }
            }
        }

        public override string Route()
        {
            return "Customer/Dependents";
        }

        public string ExpectedTitle()
        {
            return "Dependent Profile List";
        }

        public override void InitializePageFactory()
        {
           PageFactory.InitElements(WebDriver, this);
        }
    }
}
