﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Helpers;
using System.Threading;
using System.Globalization;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class EditPatientProfile
    {
        private IWebDriver webDriver;
        private TestingHelpers helper = new TestingHelpers();

        public EditPatientProfile(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public string editFirstName()
        {
            //generate random string for first name
            string newFirstName = Utility.RandomString( Utility.RandomNumber(9, 15));

            helper.typeInTextField(webDriver, "field_firstname", newFirstName);

            return newFirstName;
        }

        public string editLastName()
        {
            //generate random string for last name
            string newLastName = Utility.RandomString( Utility.RandomNumber(9, 15));

            helper.typeInTextField(webDriver, "field_lastname", newLastName);

            return newLastName;
        }

        public string editDateOfBirth()
        {
            //generate new random date of birth
            int years =  Utility.RandomNumber(-99, -18);
            int days =  Utility.RandomNumber(-364, 0);
            string dateOfBirth = DateTime.Today.AddYears(years).AddDays(days).ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);

            //set new date of birth using jquery
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElement(By.Id("field_dateofbirth")));
            Thread.Sleep(1000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            string script = "$('#field_dateofbirth').val(\"" + dateOfBirth + "\");";
            js.ExecuteScript(script);

            return dateOfBirth;
        }

        public string editSex()
        {
            string newSex = null;

            //find male and female radio buttons
            var male = webDriver.FindElement(By.XPath("//*[@id=\"field_gendermale\"]/../div"));
            var female = webDriver.FindElement(By.XPath("//*[@id=\"field_genderfemale\"]/../div"));

            Thread.Sleep(1000);
            if (male.GetAttribute("class").Equals("rad-area rad-checked"))
            {
                female.Click();
                newSex = "Female";
            }
            else if (female.GetAttribute("class").Equals("rad-area rad-checked"))
            {
                male.Click();
                newSex = "Male";
            }
            else
            {
                throw new Exception("Sex not found");
            }

            return newSex;
        }

        public string editEthnicity()
        {
            string fieldPath = "//*[@id=\"field_ethnicity\"]/../span/span";
            string listPath = "//*[@id=\"field_ethnicity_listbox\"]/li";
            string inputPath = "//*[@id=\"field_ethnicity_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editHairColor()
        {
            string fieldPath = "//*[@id=\"field_haircolor\"]/../span/span";
            string listPath = "//*[@id=\"field_haircolor_listbox\"]/li";
            string inputPath = "//*[@id=\"field_haircolor_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editEyeColor()
        {
            string fieldPath = "//*[@id=\"field_eyecolor\"]/../span/span";
            string listPath = "//*[@id=\"field_eyecolor_listbox\"]/li";
            string inputPath = "//*[@id=\"field_eyecolor_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editBloodType()
        {
            string fieldPath = "//*[@id=\"field_bloodtype\"]/../span/span";
            string listPath = "//*[@id=\"field_bloodtype_listbox\"]/li";
            string inputPath = "//*[@id=\"field_bloodtype_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editWeight()
        {
            //generate random weight
            string weight =  Utility.RandomNumber(1, 450).ToString();

            //click on weight input field
            webDriver.FindElement(By.XPath("//*[@id=\"field_weight\"]/..")).Click();

            //clear text from field 
            webDriver.FindElement(By.Id("field_weight")).Clear();

            //enter new value
            webDriver.FindElement(By.Id("field_weight")).SendKeys(weight);

            return weight;
        }

        public string editWeightUnit()
        {
            string fieldPath = "//*[@id=\"cmbWeightUnits\"]/../span/span";
            string listPath = "//*[@id=\"cmbWeightUnits_listbox\"]/li";
            string inputPath = "//*[@id=\"cmbWeightUnits_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editHeightUnit()
        {
            string fieldPath = "//*[@id=\"cmbHeightUnits\"]/../span/span";
            string listPath = "//*[@id=\"cmbHeightUnits_listbox\"]/li";
            string inputPath = "//*[@id=\"cmbHeightUnits_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editHeight()
        {
            //generate random height
            string height_FtM =  Utility.RandomNumber(0, 10).ToString();
            string height_InCm = null;
            string currentUnits = webDriver.FindElement(By.XPath("//*[@id=\"cmbHeightUnits\"]/../span/span")).Text;
            if (currentUnits.Equals("ft/in"))
            {
                height_InCm =  Utility.RandomNumber(0, 11).ToString();
            }
            else if (currentUnits.Equals("m/cm"))
            {
                height_InCm =  Utility.RandomNumber(0, 99).ToString();
            }

            //click on height ft/m input field
            webDriver.FindElement(By.XPath("//*[@id=\"field_heightfeet\"]/..")).Click();

            //clear text from field 
            webDriver.FindElement(By.Id("field_heightfeet")).Clear();

            //enter new value
            webDriver.FindElement(By.Id("field_heightfeet")).SendKeys(height_FtM);

            //click on height in/cm input field
            webDriver.FindElement(By.XPath("//*[@id=\"field_heightinches\"]/..")).Click();

            //clear text from field 
            webDriver.FindElement(By.Id("field_heightinches")).Clear();

            //enter new value
            webDriver.FindElement(By.Id("field_heightinches")).SendKeys(height_InCm);

            return height_FtM + "." + height_InCm;
        }

        public string editCountry()
        {
            string fieldPath = "//*[@id=\"s5\"]/../span/span";
            string listPath = "//*[@id=\"s5_listbox\"]/li";
            string inputPath = "//*[@id=\"s5_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editCellPhone()
        {
            //generate random cell phone number
            string cellPhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(webDriver, "field_cellphone", cellPhone);

            return cellPhone;
        }

        public string editHomePhone()
        {
            //generate random home phone number
            string homePhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(webDriver, "field_homephone", homePhone);

            return homePhone;
        }

        public string editOrganization()
        {
            //generate random string for organization
            string organization = Utility.RandomString( Utility.RandomNumber(9, 20));

            helper.typeInTextField(webDriver, "txtOrganization", organization);

            return organization;
        }

        public string editLocation()
        {
            //generate random string for location
            string location = Utility.RandomString( Utility.RandomNumber(9, 20));

            helper.typeInTextField(webDriver, "txtLocation", location);

            return location;
        }

        public string editHomeAddress()
        {
            //generate random string for home address
            string homeAddress = Utility.RandomString( Utility.RandomNumber(15, 35)) + " " +  Utility.RandomNumber(1, 999999);

            helper.typeInTextField(webDriver, "tbHomeAddress", homeAddress);
            
            return homeAddress;
        }

        public string editMedicalCondition1()
        {
            string fieldPath = "//*[@id=\"field_chronicconditions1\"]/../span/span";
            string listPath = "//*[@id=\"field_chronicconditions1_listbox\"]/li";
            string inputPath = "//*[@id=\"field_chronicconditions1_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editMedicalCondition2(List <string> filter)
        {
            string fieldPath = "//*[@id=\"field_chronicconditions2\"]/../span/span";
            string listPath = "//*[@id=\"field_chronicconditions2_listbox\"]/li";
            string inputPath = "//*[@id=\"field_chronicconditions2_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editMedicalCondition3(List<string> filter)
        {
            string fieldPath = "//*[@id=\"field_chronicconditions3\"]/../span/span";
            string listPath = "//*[@id=\"field_chronicconditions3_listbox\"]/li";
            string inputPath = "//*[@id=\"field_chronicconditions3_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editMedicalCondition4(List<string> filter)
        {
            string fieldPath = "//*[@id=\"field_chronicconditions4\"]/../span/span";
            string listPath = "//*[@id=\"field_chronicconditions4_listbox\"]/li";
            string inputPath = "//*[@id=\"field_chronicconditions4_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editAllergy1()
        {
            string fieldPath = "//*[@id=\"field_allergies1\"]/../span/span";
            string listPath = "//*[@id=\"field_allergies1_listbox\"]/li";
            string inputPath = "//*[@id=\"field_allergies1_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath);
        }

        public string editAllergy2(List<string> filter)
        {
            string fieldPath = "//*[@id=\"field_allergies2\"]/../span/span";
            string listPath = "//*[@id=\"field_allergies2_listbox\"]/li";
            string inputPath = "//*[@id=\"field_allergies2_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editAllergy3(List<string> filter)
        {
            string fieldPath = "//*[@id=\"field_allergies3\"]/../span/span";
            string listPath = "//*[@id=\"field_allergies3_listbox\"]/li";
            string inputPath = "//*[@id=\"field_allergies3_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editAllergy4(List<string> filter)
        {
            string fieldPath = "//*[@id=\"field_allergies4\"]/../span/span";
            string listPath = "//*[@id=\"field_allergies4_listbox\"]/li";
            string inputPath = "//*[@id=\"field_allergies4_listbox\"]/../span/input";

            return helper.chooseRandomFromDropdown(webDriver, fieldPath, listPath, inputPath, filter);
        }

        public string editPrimaryPhysicianName()
        {
            //generate random string for primary physician name
            string primaryPhysicianName = Utility.RandomString( Utility.RandomNumber(3, 15)) + " " + Utility.RandomString( Utility.RandomNumber(3, 15));

            helper.typeInTextField(webDriver, "field_physicianname", primaryPhysicianName);

            return primaryPhysicianName;
        }

        public string editPrimaryPhysicianPhone()
        {
            //generate random string for primary physician phone
            string primaryPhysicianPhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(webDriver, "field_physicianphone", primaryPhysicianPhone);

            return primaryPhysicianPhone;
        }

        public string editPhysicianSpecialistName()
        {
            //generate random string for physician specialist name
            string physicianSpecialistName = Utility.RandomString( Utility.RandomNumber(3, 15)) + " " + Utility.RandomString( Utility.RandomNumber(3, 15));

            helper.typeInTextField(webDriver, "field_specialistname", physicianSpecialistName);

            return physicianSpecialistName;
        }

        public string editPhysicianSpecialistPhone()
        {
            //generate random string for physician specialist phone
            string physicianSpecialistPhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(webDriver, "field_specialistnumber", physicianSpecialistPhone);

            return physicianSpecialistPhone;
        }

        public string editPreferredPharmacyName()
        {
            //generate random string for preffered pharmacy specialist name
            string preferredPharmacyName = Utility.RandomString( Utility.RandomNumber(3, 15)) + " " + Utility.RandomString( Utility.RandomNumber(3, 15));

            helper.typeInTextField(webDriver, "field_pharmname", preferredPharmacyName);

            return preferredPharmacyName;
        }

        public string editPreferredPharmacyPhone()
        {
            //generate random string for preferred pharmacy phone
            string preferredPharmacyPhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            helper.typeInTextField(webDriver, "field_pharmphone", preferredPharmacyPhone);
            
            return preferredPharmacyPhone;
        }

        public Dictionary<string, string> editPatientInformations()
        {
            //create dictionary with new patient informations
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            List<string> conditionsSelected = new List<string>();
            List<string> allergiesSelected = new List<string>();

            //edit first name and add new value to dictionary
            //newValues.Add("firstName", editFirstName());

            //edit last name and add new value to dictionary
            //newValues.Add("lastName", editLastName());

            //edit date of birth and add new value to dictionary
            newValues.Add("dateOfBirth", editDateOfBirth());

            //edit sex and save new value to dictionary
            newValues.Add("sex", editSex());

            //edit ethnicity and save new value to dictionary
            newValues.Add("ethnicity", editEthnicity());

            //edit hair color and save new value to dictionary
            newValues.Add("hairColor", editHairColor());

            //edit eye color and save new value to dictionary
            newValues.Add("eyeColor", editEyeColor());

            //edit blood type and save new value to dictionary
            newValues.Add("bloodType", editBloodType());

            //edit weight and save new value to dictionary
            newValues.Add("weight", editWeight());

            //edit weight unit and save new value to dictionary
            newValues.Add("weightUnit", editWeightUnit());

            //edit height unit and save new value to dictionary
            newValues.Add("heightUnit", editHeightUnit());

            //edit height and save new value to dictionary
            newValues.Add("height", editHeight());

            //edit country and save new value to dictionary
            newValues.Add("country", editCountry());

            //edit cell phone and save new value to dictionary
            newValues.Add("cellPhone", editCellPhone());

            //edit home phone and save new value to dictionary
            newValues.Add("homePhone", editHomePhone());

            //edit organization and save new value to dictionary
            newValues.Add("organization", editOrganization());

            //edit location and save new value to dictionary
            newValues.Add("location", editLocation());

            //edit home address and save new value to dictionary
            newValues.Add("homeAddress", editHomeAddress());

            //edit 1. medical condition and save new value to dictoinary
            string medicalCondition1 = editMedicalCondition1();
            newValues.Add("medicalCondition1", medicalCondition1);
            conditionsSelected.Add(medicalCondition1);

            //edit 2. medical condition and save new value to dictoinary
            string medicalCondition2 = editMedicalCondition2(conditionsSelected);
            newValues.Add("medicalCondition2", medicalCondition2);
            conditionsSelected.Add(medicalCondition2);

            //edit 3. medical condition and save new value to dictoinary
            string medicalCondition3 = editMedicalCondition3(conditionsSelected);
            newValues.Add("medicalCondition3", medicalCondition3);
            conditionsSelected.Add(medicalCondition3);

            //edit 4. medical condition and save new value to dictoinary
            string medicalCondition4 = editMedicalCondition4(conditionsSelected);
            newValues.Add("medicalCondition4", medicalCondition4);
            conditionsSelected.Add(medicalCondition4);

            //edit 1. allergy and save new value to dictoinary
            string allergy1 = editAllergy1();
            newValues.Add("allergy1", allergy1);
            allergiesSelected.Add(allergy1);

            //edit 2. allergy and save new value to dictoinary
            string allergy2 = editAllergy2(allergiesSelected);
            newValues.Add("allergy2", allergy2);
            allergiesSelected.Add(allergy2);

            //edit 3. allergy and save new value to dictoinary
            string allergy3 = editAllergy3(allergiesSelected);
            newValues.Add("allergy3", allergy3);
            allergiesSelected.Add(allergy3);

            //edit 4. allergy and save new value to dictoinary
            string allergy4 = editAllergy4(allergiesSelected);
            newValues.Add("allergy4", allergy4);
            allergiesSelected.Add(allergy4);

            //edit primary physician name and save new value to dictionary
            newValues.Add("primaryPhysicianName", editPrimaryPhysicianName());

            //edit primary physician phone and save new value to dictionary
            newValues.Add("primaryPhysicianPhone", editPrimaryPhysicianPhone());

            //edit physician specialist name and save new value to dictionary
            newValues.Add("physicianSpecialistName", editPhysicianSpecialistName());

            //edit physician specialist phone and save new value to dictionary
            newValues.Add("physicianSpecialistPhone", editPhysicianSpecialistPhone());

            //edit preferred pharmacy name and save new value to dictionary
            newValues.Add("preferredPharmacyName", editPreferredPharmacyName());

            //edit preferred pharmacy phone and save new value to dictionary
            newValues.Add("preferredPharmacyPhone", editPreferredPharmacyPhone());


            return newValues;
        }

        public void clickSave()
        {
            webDriver.FindElement(By.Id("btnSaveProfile1")).Click();
        }

        public void checkIsUpdated(Dictionary<string, string> newValues)
        {
            //check name (first + last)
            //string actualName = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_lblUserName")).GetAttribute("value");
            //string name = newValues["firstName"] + " " + newValues["lastName"];
            //Assert.AreEqual(name, actualName, "Name is not correct");

            //check date of birth
            string actualDateOfBirth = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblDOB")).Text;
            Assert.AreEqual(newValues["dateOfBirth"], actualDateOfBirth.Substring(2, actualDateOfBirth.Length - 2), "Date of birth is not correct");

            //check age
            string actualAge = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblAge")).Text;
            int expectedAge = DateTime.Today.Year - Convert.ToDateTime(newValues["dateOfBirth"], CultureInfo.InvariantCulture).Year;
            if (Convert.ToDateTime(newValues["dateOfBirth"], CultureInfo.InvariantCulture) > DateTime.Today.AddYears(-expectedAge))
                expectedAge--;
            Assert.AreEqual(expectedAge.ToString() + " Years", actualAge.Substring(2, actualAge.Length - 2), "Age is not correct");

            //check sex
            string actualSex = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblGender")).Text;
            Assert.AreEqual(newValues["sex"], actualSex.Substring(2, actualSex.Length - 2), "Sex is not correct");

            //check ethnicity
            string actualEthnicity = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblEthnicity")).Text;
            Assert.AreEqual(newValues["ethnicity"], actualEthnicity.Substring(2, actualEthnicity.Length - 2), "Ethnicity is not correct");

            //check hair color
            string actualHairColor = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHairColor")).Text;
            Assert.AreEqual(newValues["hairColor"], actualHairColor.Substring(2, actualHairColor.Length - 2), "Hair color is not correct");

            //check eye color
            string actualEyeColor = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblEyeColor")).Text;
            Assert.AreEqual(newValues["eyeColor"], actualEyeColor.Substring(2, actualEyeColor.Length - 2), "Eye color is not correct");

            //check blood type
            string actualBloodType = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblBloodType")).Text;
            Assert.AreEqual(newValues["bloodType"], actualBloodType.Substring(2, actualBloodType.Length - 2), "Blood type is not correct");

            //check weight
            string actualWeight = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblWeight")).Text;
            Assert.AreEqual(newValues["weight"] + " " + newValues["weightUnit"], actualWeight.Substring(2, actualWeight.Length - 2), "Weight and/or weight unit is not correct");

            //check height
            string actualHeight = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHeight")).Text;
            string[] heightValues = newValues["height"].Split('.');
            string newHeight = null;
            if (newValues["heightUnit"].ToLower().Equals("ft/in"))
            {
                newHeight = heightValues.First() + " ft " + heightValues.Last() + " in";
            }
            else if (newValues["heightUnit"].ToLower().Equals("m/cm"))
            {
                newHeight = heightValues.First() + " m " + heightValues.Last() + " cm";
            }
            Assert.AreEqual(newHeight, actualHeight.Substring(2, actualHeight.Length - 2), "Height and/or height unit is not correct");

            //check cell phone
            string actualCellPhone = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblCellPhone")).Text;
            actualCellPhone = actualCellPhone.Substring(2, actualCellPhone.Length - 2);
            if (newValues["country"].ToLower().Equals("united states"))
            {
                Assert.AreEqual("+1" + newValues["cellPhone"], actualCellPhone, "Cell phone is not correct");
            }
            else if (newValues["country"].ToLower().Equals("united kingdom"))
            {
                Assert.AreEqual("+44" + newValues["cellPhone"], actualCellPhone, "Cell phone is not correct");
            }

            //check home phone
            string actualHomePhone = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHomePhone")).Text;
            Assert.AreEqual(newValues["homePhone"], actualHomePhone.Substring(2, actualHomePhone.Length - 2), "Home phone is not correct");

            //check organization
            string actualOrganization = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblOrganization")).Text;
            Assert.AreEqual(newValues["organization"], actualOrganization.Substring(2, actualOrganization.Length - 2), "Organization is not correct");

            //check location
            string actualLocation = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblLocation")).Text;
            Assert.AreEqual(newValues["location"], actualLocation.Substring(2, actualLocation.Length - 2), "Location is not correct");

            //check home address
            string actualHomeAddress = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblHomeAddress")).Text;
            Assert.AreEqual(newValues["homeAddress"], actualHomeAddress, "Home address is not correct");

            //check medical conditions
            string actualMedicalConditions = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_lblMedicalCondition")).Text;
            string expectedMedicalConditions = newValues["medicalCondition1"] + ", " + newValues["medicalCondition2"] + ", " + newValues["medicalCondition3"] + ", " + newValues["medicalCondition4"];
            Assert.AreEqual(expectedMedicalConditions, actualMedicalConditions, "Medical conditions are not correct");

            //check primary physician name and phone
            string actualPrimaryPhysician = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_addPrimaryPhysician")).Text;
            Assert.IsTrue(actualPrimaryPhysician.Contains("Name : " + newValues["primaryPhysicianName"]), "Primary physician name is not correct");
            Assert.IsTrue(actualPrimaryPhysician.Contains("Contact : " + newValues["primaryPhysicianPhone"]), "Primary physician contact is not correct");

            //check physician specialist name and phone
            string actualPhysicianSpecialist = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_addPhysicianSpecialist")).Text;
            Assert.IsTrue(actualPhysicianSpecialist.Contains("Name : " + newValues["physicianSpecialistName"]), "Physician specialist name is not correct");
            Assert.IsTrue(actualPhysicianSpecialist.Contains("Contact : " + newValues["physicianSpecialistPhone"]), "Physician specialist contact is not correct");

            //check preferred pharmacy name and phone
            string actualPreferredPharmacy = webDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_patientProfile_addPreferedPharmacy")).Text;
            Assert.IsTrue(actualPreferredPharmacy.Contains("Name : " + newValues["preferredPharmacyName"]), "Preferred pharmacy name is not correct");
            Assert.IsTrue(actualPreferredPharmacy.Contains("Contact : " + newValues["preferredPharmacyPhone"]), "Preferred pharmacy contact is not correct");
        }
    }
}
