﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class RegisterConfirmPom : WebPageObjectModel, IPageObjectModel
    {
        public RegisterConfirmPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/RegisterConfirm";
        }

        public string ExpectedTitle()
        {
            return "SnapMD - Registration";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
