﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientForgotPasswordPom : WebPageObjectModel, IPageObjectModel
    {
        [FindsBy(How = How.Id, Using = "txtEmail")]
        public IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "btnSendEmail")]
        public IWebElement BtnSendEmail { get; set; }

        [FindsBy(How = How.LinkText, Using = "Back")]
        public IWebElement LinkBack { get; set; }

        [FindsBy(How = How.LinkText, Using = "Register")]
        public IWebElement LinkRegister { get; set; }


        public PatientForgotPasswordPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/ForgotPassword";
        }

        public string ExpectedTitle()
        {
            return "Forgot Password | SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void SendRecoveryEmail(string email)
        {
            TxtEmail.SendKeys(email);
            BtnSendEmail.Click();
        }
    }
}
