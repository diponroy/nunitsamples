﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.WebPages.Patient.Template;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientHomePom : WebPageObjectModel, IPageObjectModel
    {
        /*session phone number*/
        [FindsBy(How = How.Id, Using = "phone-field")]
        public IWebElement TxtSessionContactNumber { get; set; }

        [FindsBy(How = How.Id, Using = "inpUpdateContact")]
        public IWebElement BtnUpdateSessionContactNumber { get; set; }

        public PatientHomePom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Customer/Home";
        }

        public string ExpectedTitle()
        {
            return "SnapMD - No Consultation Home";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void clickOnNewConsultation()
        {
            //Wait 10 seconds for element to show up
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.CssSelector("#aNewConsultation > a")));
                //Wait for element to show up

            //Click on new Consultation
            Thread.Sleep(5000);
            IWebElement newConsultaitonButton = WebDriver.FindElement(By.CssSelector("#aNewConsultation > a"));
            newConsultaitonButton.Click();
        }

        public void checkIFClickedOnNewConsultation()
        {
            WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            Assert.IsTrue(
                WebDriver.FindElement(
                    By.CssSelector(
                        "#form1 > div.speak-block > div.profile-section > div.title-holder > div.desctop-text > h2"))
                    .Text.Contains("Who is the patient today?"));
        }


        public void openMainMenu()
        {
            //Wait 10 seconds for element to show up
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(
                driver =>
                    WebDriver.FindElement(
                        By.CssSelector("#header > div > div.menu-section > ul > li.menu-links > div > a")));
                //Wait for element to show up

            //Click on Menu
            IWebElement menu =
                WebDriver.FindElement(By.CssSelector("#header > div > div.menu-section > ul > li.menu-links > div > a"));
            menu.Click();
        }

        public void clickOnHome()
        {
            IWebElement home =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#header > div > div.menu-section > ul > li.menu-links > div > div > ul > li:nth-child(1) > a"));
            home.Click();
        }

        public void clickOnDownloadVidyoPlugin()
        {
            IWebElement vidyo =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#header > div > div.menu-section > ul > li.menu-links > div > div > ul > li:nth-child(3) > a"));
            vidyo.Click();
        }

        public void checkIfYouAreOnHomePage()
        {
            Assert.IsTrue(WebDriver.Title.Equals("SnapMD - No Consultation Home"));
        }

        public void clickOnMyAccount()
        {
            IWebElement myAccount =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#header > div > div.menu-section > ul > li.menu-links > div > div > ul > li:nth-child(2) > a"));
            myAccount.Click();
        }

        public void clickOnLogOut()
        {
            IWebElement logOut =
                WebDriver.FindElement(
                    By.CssSelector(
                        "#header > div > div.menu-section > ul > li.menu-links > div > div > ul > li:nth-child(4) > a"));
            logOut.Click();
        }

        public void checkIfContactIsUpdated(string newContact)
        {
            Assert.IsTrue(new TestingHelpers().checkForSnapSuccess(WebDriver, "Phone number for this session is updated."));
            Thread.Sleep(1000);
            Assert.AreEqual(newContact, WebDriver.FindElement(By.Id("phone-field")).GetAttribute("value"));
        }

        public void UpdateSessionContactNumber(string contactNumber)
        {
            TxtSessionContactNumber.Value(contactNumber);
            BtnUpdateSessionContactNumber.Click();
            WaitForAjax();
        }
    }
}
