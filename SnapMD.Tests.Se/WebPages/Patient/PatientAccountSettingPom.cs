﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    public class PatientAccountSettingPom : WebPageObjectModel, IPageObjectModel
    {
        /*personal info*/
        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.Id, Using = "btnUpdatePersonalInfo")]
        public IWebElement BtnUpdatePersonalInfo { get; set; }


        /*change password*/

        [FindsBy(How = How.Id, Using = "currentpassword")]
        public IWebElement TxtCurrentPassword { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement TxtNewPassword { get; set; }

        [FindsBy(How = How.Id, Using = "confirmpassword")]
        public IWebElement TxtConfirmNewPassword { get; set; }

        [FindsBy(How = How.Id, Using = "btnUpdatePassword")]
        public IWebElement BtnUpdatePassword { get; set; }


        public PatientAccountSettingPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public void selectDependentProfiles()
        {
            var dependents = WebDriver.FindElement(By.Id("liPatientProfile"));
            dependents.Click();
        }

        public void selectUserProfiles()
        {
            //profiles in left menu
            var userProfiles = WebDriver.FindElement(By.Id("liUserProfile"));
            userProfiles.Click();
        }

        public void selectMyFiles()
        {
            var myFiles = WebDriver.FindElement(By.Id("liUserFiles"));
            myFiles.Click();
        }

        public void selectHealthPlans()
        {
            var healthPlans = WebDriver.FindElement(By.Id("ContentPlaceHolder1_liHealthPlans"));
            healthPlans.Click();
        }

        public void selectConsultations()
        {
            var consultations = WebDriver.FindElement(By.Id("liConsultation"));
            consultations.Click();
        }

        public void selectAccountSettings()
        {
            var accountSettings = WebDriver.FindElement(By.Id("liAccountSett"));
            accountSettings.Click();
        }

        public void selectUserProfile()
        {
            //profile
            var userprofile = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_rptAdultProfile_aEditProfile_0"));
            userprofile.Click();

        }

        public void clickEditProfile()
        {
            //profile
            var editprofile = WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_anchrEdit"));
            editprofile.Click();

        }

        public void checkIfSaved()
        {
            //click ok
            WebDriver.SwitchTo().Alert().Accept();
            Assert.That(WebDriver.FindElement(By.Id("ContentPlaceHolder1_ContentPlaceHolder2_anchrEdit")).ToString(), Contains.Substring("Edit"));
        }

        public void clickOnAddDependentProfile()
        {
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.CssSelector("#tab7 > div.heading > a"))); //Wait for element to show up

            var dependentsprofile = WebDriver.FindElement(By.LinkText("Add New Profile"));
            dependentsprofile.Click();
        }

        public void selectPaymentInformation()
        {
            var payment = WebDriver.FindElement(By.Id("ContentPlaceHolder1_liPaymentInfo"));
            payment.Click();

        }

        public void findCC(String findCCnumber)
        {
            IList<IWebElement> cclist = WebDriver.FindElements(By.XPath("//input[contains(@id, 'ContentPlaceHolder1_ContentPlaceHolder2_rptCardDetails_hdnCardNumber_')]"));
            Console.WriteLine("Number of CCards: " + cclist.Count());

            foreach (IWebElement ccNumber in cclist)
            {


                Console.WriteLine("CCNumbers: " + ccNumber.GetAttribute("value"));
                Console.WriteLine("CC ID: " + ccNumber.GetAttribute("id"));

                if (ccNumber.GetAttribute("value").Contains(findCCnumber)){

                    //Magic don't tuch!

                    String ccID=ccNumber.GetAttribute("id");
                    char last = ccID[ccID.Length - 1];
                    String deleteID = ("ContentPlaceHolder1_ContentPlaceHolder2_rptCardDetails_btnDelete_" + last);

                    //Crash Kill and Distory
                    var delete = WebDriver.FindElement(By.Id(deleteID));
                    delete.Click();

                    //Acept
                    new TestingHelpers().waitForAlert(WebDriver,10);


                }
            }
        }

        public void confirmDeleteCC()
        {
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.CssSelector("#btnConfirmYes"))); //Wait for element to show up
            IWebElement clickYes = WebDriver.FindElement(By.CssSelector("#btnConfirmYes"));
            clickYes.Click();
        }

        public override string Route()
        {
            return "Customer/AccountSettings";
        }

        public string ExpectedTitle()
        {
            return "AccountSettings";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void UpdatePassword(string currentPassword, string newPassword)
        {
            TxtCurrentPassword.SendKeys(currentPassword);
            TxtNewPassword.SendKeys(newPassword);
            TxtConfirmNewPassword.SendKeys(newPassword);
            BtnUpdatePassword.Click();
        }
    }
}
