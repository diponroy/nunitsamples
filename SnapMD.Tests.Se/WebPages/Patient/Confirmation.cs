﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.WebPages.Patient
{
    class Confirmation
    {
        private IWebDriver webDriver;
        public Confirmation(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void acknowladge()
        {
            var checkBox = webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.check-bar > div.check-item > div"));
            checkBox.Click();
        }
        public void clickSubmit()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.Id("ContentPlaceHolder1_btnSubmit"))); //Wait for element to show up
            var nextButton = webDriver.FindElement(By.Id("ContentPlaceHolder1_btnSubmit"));
            nextButton.Click();
        }
        public void checkIfClicked()
        {
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //Wait for  3 sec
            //Check if you are on page
            Assert.IsTrue(this.webDriver.FindElement(By.CssSelector("#form1 > div.speak-block > div.nav-panel > div > h2")).Text.Contains("Consultation Charge"));

        }


    }


}
