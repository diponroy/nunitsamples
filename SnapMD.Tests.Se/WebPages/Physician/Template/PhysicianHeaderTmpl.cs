﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.WebPages.Physician.Template
{
    public class PhysicianHeaderTmpl : ILogoutPom
    {
        public readonly IWebDriver WebDriver;

        public IWebElement LblUserName { get; set; }

        [FindsBy(How = How.CssSelector,
            Using = "#header > div > div.menu-section > ul.menu > li.menu-links > div.popup-holder")]
        public IWebElement MenuPopUpHolder { get; set; }

        [FindsBy(How = How.CssSelector,
            Using = "#header > div > div.menu-section > ul.menu > li.menu-links > div.popup-holder > a")]
        public IWebElement Menu { get; set; }

        [FindsBy(How = How.CssSelector,
            Using = "#header > div > div.menu-section > ul.menu > li.menu-links > div.popup-holder > div.popup")]
        public IWebElement MenuPopUp { get; set; }

        [FindsBy(How = How.CssSelector,
            Using = "#header > div > div.menu-section > ul.menu > li.menu-links > div.popup-holder ul li > a")]
        public IList<IWebElement> MenuItems { get; set; }

        public IWebElement LinkLogOut
        {
            get { return MenuLink("Log Out"); }
        }

        public bool IsMenuPopupVisible()
        {
            /*popup holder*/
            bool holderIsActive = MenuPopUpHolder.GetAttribute("class").Contains("popup-active");
            /*popup*/
            bool popUpIsDisplayed = MenuPopUp.Displayed && MenuPopUp.Enabled;
            return holderIsActive && popUpIsDisplayed;
        }

        public PhysicianHeaderTmpl(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        public IWebElement MenuLink(string text)
        {
            return MenuItems.FirstOrDefault(x => x.Text.Trim().ToLower().Equals(text.ToLower()));
        }

        public void ShowMenus()
        {
            if (!IsMenuPopupVisible())
            {
                Menu.Click();
            }

            bool popupGotOpened = TestingHelpers.WaitUntil(WebDriver,
                d =>
                {
                    bool isVisible = IsMenuPopupVisible();
                    return isVisible;
                });

            if (!popupGotOpened)
            {
                throw new Exception("Menu popup is not opening.");
            }
        }

        public void HideMenus()
        {
            if (IsMenuPopupVisible())
            {
                Menu.Click();
            }
            bool popupGotClosed = TestingHelpers.WaitUntil(WebDriver,
                d =>
                {
                    bool isVisible = IsMenuPopupVisible() == false;
                    return isVisible;
                });

            if (!popupGotClosed)
            {
                throw new Exception("Menu popup is not closing.");
            }
        }

        public void RequestLogout()
        {
            ShowMenus();
            LinkLogOut.Click();
        }
    }
}
