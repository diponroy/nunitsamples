﻿using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Physician.Template
{
    public class PhysicianAccountLeftMenuTmpl
    {
        public readonly IWebDriver WebDriver;

        [FindsBy(How = How.Id, Using = "liAccountSettings")]
        public IWebElement BtnAccountSettings { get; set; }

        [FindsBy(How = How.Id, Using = "liUserFiles")]
        public IWebElement BtnMyFiles { get; set; }

        [FindsBy(How = How.Id, Using = "liPhysicianConsultations")]
        public IWebElement BtnConsultations { get; set; }

        [FindsBy(How = How.Id, Using = "liPhysicianPatientList")]
        public IWebElement BtnPatientProfiles { get; set; }

        public PhysicianAccountLeftMenuTmpl (IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }
    }
}
