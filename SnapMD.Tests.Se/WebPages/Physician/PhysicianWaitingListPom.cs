﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianWaitingListPom : WebPageObjectModel, IPageObjectModel
    {
        public override string Route()
        {
            return "Physician/WaitingList";
        }

        public string ExpectedTitle()
        {
            throw new NotImplementedException();
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }


        public PhysicianWaitingListPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public void selectConsultation()
        {
            //Wait 10 seconds for element to show up
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.PartialLinkText("View Consultation")));
                //Wait for element to show up


            IWebElement clickOnViewConsultation = WebDriver.FindElement(By.PartialLinkText("View Consultation"));
            clickOnViewConsultation.Click();
        }

        public void chekIfClicked()
        {
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.PartialLinkText("Start Consult")));
                //Wait for element to show up

            Assert.IsTrue(WebDriver.FindElement(By.PartialLinkText("Start Consult")).Text.Contains("Consult"));
        }

        public void clickMenu()
        {
            try
            {
                //Wait 10 seconds for element to show up
                var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
                wait.Until(driver => WebDriver.FindElement(By.ClassName("menu-links"))).Click();
                    //Wait for element to show up and click it
            }
            catch (StaleElementReferenceException)
            {
                //try again
                var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
                wait.Until(driver => WebDriver.FindElement(By.ClassName("menu-links"))).Click();
                    //Wait for element to show up and click it
            }
        }

        public void selectRxNT()
        {
            //open menu
            clickMenu();

            //click E-Prescribing
            WebDriver.FindElement(By.Id("liEPrescription")).Click();
        }

        public void selectMyAccount()
        {
            clickMenu();

            //Wait 10 seconds for element to show up
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => WebDriver.FindElement(By.Id("ancMyAccount"))).Click();
                //Wait for element to show up and click it
        }

        public void clickLogOut()
        {
            WebDriver.FindElement(
                By.CssSelector(
                    "#header > div > div.menu-section > ul > li.menu-links > div > div > ul > li:nth-child(5) > a"))
                .Click();
        }
    }
}
