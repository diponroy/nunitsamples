﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    class Consultation
    {
        private IWebDriver webDriver;

        public Consultation(IWebDriver webDriver)
        {
            // TODO: Complete member initialization
            this.webDriver = webDriver;
        }

        [SetUp]
        public void InitializeDriver()
        {
            
            webDriver.Manage().Window.Maximize();
        }

        public void startConsultation()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(driver => webDriver.FindElement(By.PartialLinkText("Start Consult"))); //Wait for element to show up

            //time out---druging loading of vidyo plugin
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30)); //Wait for  30 sec

            var startConsultation = webDriver.FindElement(By.PartialLinkText("Start Consult"));
            startConsultation.Click();
        }

        public void endConsultation()
        {
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(30));
            wait.Until(driver => webDriver.FindElement(By.PartialLinkText("End Session"))); //Wait for element to show up

            var endConsultation = webDriver.FindElement(By.PartialLinkText("End Session"));
            endConsultation.Click();

            //time out---druging loading of vidyo plugin
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30)); //Wait for  30 sec
            
            //Press ok for END Consultation
            webDriver.SwitchTo().Alert().Accept();
        }
        

        [TearDown]
        public void TearDown()
        {
            webDriver.Dispose();
        }
    }
}
