﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianPatientProfilesPom : WebPageObjectModel, IPageObjectModel
    {
        public PhysicianPatientProfilesPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Physician/PatientsList";
        }

        public string ExpectedTitle()
        {
            return "SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
