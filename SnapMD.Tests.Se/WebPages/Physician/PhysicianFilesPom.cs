﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianFilesPom : WebPageObjectModel, IPageObjectModel
    {
        public PhysicianFilesPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Physician/MyFiles";
        }

        public string ExpectedTitle()
        {
            return "SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
