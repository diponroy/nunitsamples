﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianConsultationsPom : WebPageObjectModel, IPageObjectModel
    {
        public PhysicianConsultationsPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Physician/PhysicianConsultations";
        }

        public string ExpectedTitle()
        {
            return "SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
