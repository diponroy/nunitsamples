﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianLoginPom : WebPageObjectModel, IPageObjectModel, ILoginPom
    {
        [FindsBy(How = How.Id, Using = "txtloginemail")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.Id, Using = "txtPassword")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement BtnLogin { get; set; }

        [FindsBy(How = How.LinkText, Using = "password")]
        public IWebElement LinkForgotPassword { get; set; }

        public PhysicianLoginPom(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public override string Route()
        {
            return "Physician/Login";
        }

        public string ExpectedTitle()
        {
            return "Clinician Login | SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }

        public void SubminCredentials(String email, String password)
        {
            Email.SendKeys(email);
            Password.SendKeys(password);
            BtnLogin.Submit();
        }
    }
}
