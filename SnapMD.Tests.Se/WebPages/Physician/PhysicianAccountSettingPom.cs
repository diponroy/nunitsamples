﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using System.Threading;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.WebPages.Physician
{
    public class PhysicianAccountSettingPom : WebPageObjectModel, IPageObjectModel
    {
        private TestingHelpers helper = new TestingHelpers();

        public PhysicianAccountSettingPom(IWebDriver webDriver) : base(webDriver)
        {
        }

        public string editTimeZone()
        {
            //open time zone dropdown
            IWebElement timeZoneField = WebDriver.FindElements(By.ClassName("k-input")).First();
            timeZoneField.Click();

            //select random time zone
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> timeZones = WebDriver.FindElement(By.Id("field_timezone_listbox")).FindElements(By.ClassName("k-item"));
            int timeZoneInd =  Utility.RandomNumber(0, timeZones.Count - 1);

            IWebElement timeZoneInput = WebDriver.FindElements(By.ClassName("k-textbox")).First();
            timeZoneInput.SendKeys(timeZones[timeZoneInd].Text);
            timeZones[timeZoneInd].Click();

            return timeZones[timeZoneInd].Text;
        }

        public string editFirstName()
        {
            //generate random string for first name
            String newFirstName = Utility.RandomString( Utility.RandomNumber(9, 15));

            //type new value
            helper.typeInTextField(WebDriver, "txtFirstName", newFirstName);
            
            return newFirstName;
        }

        public string editLastName()
        {
            //generate random string for last name
            String newLastName = Utility.RandomString( Utility.RandomNumber(9, 15));

            //type new value
            helper.typeInTextField(WebDriver, "txtLastName", newLastName);

            return newLastName;
        }

        public string editHomePhone()
        {
            //generate random phone number
            String newHomePhone = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            //type new value
            helper.typeInTextField(WebDriver, "txtHomePhone", newHomePhone);

            return newHomePhone;
        }

        public string editMobileCountry()
        {
            //open country code dropdown
            IWebElement mobileCountryField = WebDriver.FindElements(By.ClassName("k-input")).Last();
            mobileCountryField.Click();

            //get all country codes in list
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> countryCodes = WebDriver.FindElement(By.Id("s5_listbox")).FindElements(By.XPath(".//li"));

            int i;
            string country;
            //select random country code different from current country code
            do
            {
                i =  Utility.RandomNumber(0, countryCodes.Count - 1);
                country = countryCodes[i].Text;
            } while (country.Equals(mobileCountryField.Text) || country.ToLower().Equals("choose") || String.IsNullOrEmpty(country));
            Thread.Sleep(1000);
            countryCodes[i].Click();

            return country;
        }

        public string editMobileNumber()
        {
            //generate random mobile number
            String newMobileNumber = Utility.RandomString( Utility.RandomNumber(6, 10), "numbers");

            //type new value
            helper.typeInTextField(WebDriver, "txtCellPhone", newMobileNumber);

            return newMobileNumber;
        }

        public string editYearOfQualification()
        {
            //generate random year greater than 2000 for year od qualification
            String yearOfQualification =  Utility.RandomNumber(2000, DateTime.Now.Year).ToString();

            //type new value
            helper.typeInTextField(WebDriver, "txtPracticingYears", yearOfQualification);
            
            return yearOfQualification;
        }

        public string editDepartment()
        {
            //generate random string for department
            String newDepartment = Utility.RandomString( Utility.RandomNumber(5, 25));

            //type new value
            helper.typeInTextField(WebDriver, "txtDepartment", newDepartment);
            
            return newDepartment;
        }

        public string editMedicalSpeciality()
        {
            //generate random string for medical speciality
            String newMedicalSpeciality = Utility.RandomString( Utility.RandomNumber(10, 50));

            //type new value
            helper.typeInTextField(WebDriver, "txtMedSepecialty", newMedicalSpeciality);
            
            return newMedicalSpeciality;
        }

        public string editSubSpeciality()
        {
            //generate random string for sub speciality
            String newSubSpeciality = Utility.RandomString( Utility.RandomNumber(10, 50));

            //type new value
            helper.typeInTextField(WebDriver, "txtSubSpecialty", newSubSpeciality);
            
            return newSubSpeciality;
        }

        public string editGmcNumber()
        {
            //generate random 7 digit number for GMC number
            String newGmcNumber = Utility.RandomString(7, "numbers");

            //type new value
            helper.typeInTextField(WebDriver, "txtMedLicense", newGmcNumber);
            
            return newGmcNumber;
        }

        public string editStatesLicenesed()
        {
            //generate random string for states licensed
            String newStatesLicensed = Utility.RandomString( Utility.RandomNumber(10, 50));

            //type new value
            helper.typeInTextField(WebDriver, "txtStatesLicensed", newStatesLicensed);
            
            return newStatesLicensed;
        }

        public string editSchoolOfMedicine()
        {
            //generate random string for school of medicine
            String newSchoolOfMedicine = Utility.RandomString( Utility.RandomNumber(10, 30));

            //type new value
            helper.typeInTextField(WebDriver, "txtMedSchool", newSchoolOfMedicine);

            return newSchoolOfMedicine;
        }

        public string editPreMedSchool()
        {
            //generate random string for pre-medical education
            String newPredMedSchool = Utility.RandomString( Utility.RandomNumber(10, 30));

            //type new value
            helper.typeInTextField(WebDriver, "txtPreMedSchool", newPredMedSchool);
            
            return newPredMedSchool;
        }

        public string editInternship()
        {
            //generate random string for internship
            String newInternship = Utility.RandomString( Utility.RandomNumber(10, 30));

            //type new value
            helper.typeInTextField(WebDriver, "txtIntership", newInternship);
            
            return newInternship;
        }

        public string editResidency()
        {
            //generate random string for residency
            String newResidency = Utility.RandomString( Utility.RandomNumber(5, 20));

            //type new value
            helper.typeInTextField(WebDriver, "txtResidency", newResidency);
            
            return newResidency;
        }

        public string editBusinessAddress()
        {
            //generate random business address
            String newBusinessAddress = Utility.RandomString( Utility.RandomNumber(10, 40)) + " " +  Utility.RandomNumber(1, 999999).ToString();

            //type new value
            helper.typeInTextField(WebDriver, "txtBusinessAddresss", newBusinessAddress);
            
            return newBusinessAddress;
        }

        public Dictionary<string, string> editPhysicianInformations()
        {
            //create dictionary with new doctor informations
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //edit time zone and add new value to dictionary
            newValues.Add("timeZone", editTimeZone());

            //edit first name and add new value to dictionary
            //newValues.Add("firstName", editFirstName());

            //edit last name and add new value to dictionary
            //newValues.Add("lastName", editLastName());

            //edit home phone and add new value to dictionary
            newValues.Add("homePhone", editHomePhone());

            //edit mobile country code and add new value to dictionary
            newValues.Add("mobileCountry", editMobileCountry());

            //edit mobile number and add new value to dictionary
            newValues.Add("mobileNumber", editMobileNumber());

            //edit year of qualification and add new value to dictionary
            newValues.Add("yearOfQualification", editYearOfQualification());

            //edit department and add new value to dictionary
            newValues.Add("department", editDepartment());

            //edit medical speciality and add new value to dictionary
            newValues.Add("medicalSpeciality", editMedicalSpeciality());

            //edit sub speciality and add new value to dictionary
            newValues.Add("subSpeciality", editSubSpeciality());

            //edit GMC number and add new value to dictionary
            newValues.Add("gmcNumber", editGmcNumber());

            //edit states licensed and add new value to dictionary
            newValues.Add("statesLicensed", editStatesLicenesed());

            //edit school of medicine and add new value to dictionary
            newValues.Add("schoolOfMedicine", editSchoolOfMedicine());

            //edit pre-medical education and add new value to dictionary
            newValues.Add("preMedSchool", editPreMedSchool());

            //edit internship and add new value to dictionary
            newValues.Add("internship", editInternship());

            //edit residency and add new value to dictionary
            newValues.Add("residency", editResidency());

            //edit business address and add new value to dictionary
            newValues.Add("businessAddress", editBusinessAddress());

            return newValues;
        }

        public void clickSave()
        {
            WebDriver.FindElement(By.Id("btnUpdate")).Click();
        }

        public void checkIsUpdated(Dictionary<string, string> newValues)
        {
            //check time zone
            string actualTimeZone = WebDriver.FindElements(By.ClassName("k-input")).First().Text;
            Assert.AreEqual(newValues["timeZone"], actualTimeZone, "Time zone is not correct");

            //check first name
            //string actualFirstName = WebDriver.FindElement(By.Id("txtFirstName")).GetAttribute("value");
            //Assert.AreEqual(newValues["firstName"], actualFirstName, "First name is not correct");

            //check last name
            //string actualLastName = WebDriver.FindElement(By.Id("txtLastName")).GetAttribute("value");
            //Assert.AreEqual(newValues["lastName"], actualLastName, "Last name is not correct");

            //check home phone
            string actualHomePhone = WebDriver.FindElement(By.Id("txtHomePhone")).GetAttribute("value");
            Assert.AreEqual(newValues["homePhone"], actualHomePhone, "Home phone is not correct");

            //check mobile country code
            string actualMobileCountry = WebDriver.FindElements(By.ClassName("k-input")).Last().Text;
            Assert.AreEqual(newValues["mobileCountry"], actualMobileCountry, "Mobile country code is not correct");

            //check mobile number
            string actualMobileNumber = WebDriver.FindElement(By.Id("txtCellPhone")).GetAttribute("value");
            Assert.AreEqual(newValues["mobileNumber"], actualMobileNumber, "Mobile number is not correct");

            //check year of qualification
            string actualYearOfQualification = WebDriver.FindElement(By.Id("txtPracticingYears")).GetAttribute("value");
            Assert.AreEqual(newValues["yearOfQualification"], actualYearOfQualification, "Year of qualification is not correct");

            //check department
            string actualDepartment = WebDriver.FindElement(By.Id("txtDepartment")).GetAttribute("value");
            Assert.AreEqual(newValues["department"], actualDepartment, "Department is not correct");

            //check medical speciality
            string actualMedicalSpeciality = WebDriver.FindElement(By.Id("txtMedSepecialty")).GetAttribute("value");
            Assert.AreEqual(newValues["medicalSpeciality"], actualMedicalSpeciality, "Medical speciality is not correct");

            //check sub speciality
            string actualSubSpeciality = WebDriver.FindElement(By.Id("txtSubSpecialty")).GetAttribute("value");
            Assert.AreEqual(newValues["subSpeciality"], actualSubSpeciality, "Sub speciality is not correct");

            //check GMC number
            string actualGmcNumber = WebDriver.FindElement(By.Id("txtMedLicense")).GetAttribute("value");
            Assert.AreEqual(newValues["gmcNumber"], actualGmcNumber, "GMC number is not correct");

            //check states licensed
            string actualStatesLicensed = WebDriver.FindElement(By.Id("txtStatesLicensed")).GetAttribute("value");
            Assert.AreEqual(newValues["statesLicensed"], actualStatesLicensed, "States licensed are not correct");

            //check school of medicine
            string actualSchoolOfMedicine = WebDriver.FindElement(By.Id("txtMedSchool")).GetAttribute("value");
            Assert.AreEqual(newValues["schoolOfMedicine"], actualSchoolOfMedicine, "School of medicine is not correct");

            //check pre-medical education
            string actualPreMedSchool = WebDriver.FindElement(By.Id("txtPreMedSchool")).GetAttribute("value");
            Assert.AreEqual(newValues["preMedSchool"], actualPreMedSchool, "Pre-Medical education is not correct");

            //check intersnhip
            string actualInternship = WebDriver.FindElement(By.Id("txtIntership")).GetAttribute("value");
            Assert.AreEqual(newValues["internship"], actualInternship, "Internship is not correct");

            //check residency
            string actualResidency = WebDriver.FindElement(By.Id("txtResidency")).GetAttribute("value");
            Assert.AreEqual(newValues["residency"], actualResidency, "Residency is not correct");

            //check business address
            string actualBusinessAddress = WebDriver.FindElement(By.Id("txtBusinessAddresss")).GetAttribute("value");
            Assert.AreEqual(newValues["businessAddress"], actualBusinessAddress, "Business address is not correct");
        }

        public override string Route()
        {
            return "Physician/EditPhysicianProfile";
        }

        public string ExpectedTitle()
        {
            return "SnapMD";
        }

        public override void InitializePageFactory()
        {
            PageFactory.InitElements(WebDriver, this);
        }
    }
}
