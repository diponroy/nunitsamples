﻿using System.Threading;
using OpenQA.Selenium;
using SnapMD.Tests.Se.JsHelpers;

namespace SnapMD.Tests.Se.Utilities
{
    public class SeleniumUtility
    {
        /*
         * http://stackoverflow.com/questions/6201425/wait-for-an-ajax-call-to-complete-with-selenium-2-web-driver
         */
        public static void WaitForAjax(IWebDriver driver)
        {
            while (true)
            {
                bool ajaxIsComplete = !JqueryHelper.HasAjax(driver);
                if (ajaxIsComplete)
                {
                    break;
                }
                Thread.Sleep(500); /*selenium by default checks expected condition every 500 milliseconds*/
            }
        }

        public static void WaitUntilDocumentReady(IWebDriver driver)
        {
            while (true)
            {
                bool isDocReady = JavascriptHelper.IsDocumentReady(driver);
                if (isDocReady)
                {
                    break;
                }
                Thread.Sleep(500); /*selenium by default checks expected condition every 500 milliseconds*/
            }
        }
    }
}
