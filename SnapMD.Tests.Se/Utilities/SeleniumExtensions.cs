﻿using System;
using System.Threading;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.JsHelpers;

namespace SnapMD.Tests.Se.Utilities
{
    /*
     * few extensions 
     * http://www.vcskicks.com/selenium-extensions.php
     */

    public static class SeleniumExtensions
    {
        /*impt: make sure you are using the 'return' word
         * http://stackoverflow.com/questions/18520892/driver-executescript-returns-nullpointerexception-for-simple-javascript
         */

        public static TSource ExecuteScriptForData<TSource>(this IJavaScriptExecutor javaScriptExecutor,
            string script,
            params object[] args)
        {
            if (!script.Contains("return"))
            {
                throw new Exception("script should have the word 'return'.");
            }

            object result = javaScriptExecutor.ExecuteScript(script, args);
            TSource source;
            if (typeof (TSource) == typeof (string))
            {
                var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                string resultString = JsonConvert.SerializeObject(result, settings);
                source = JsonConvert.DeserializeObject<TSource>(resultString, settings);
            }
            else
            {
                string resultString = (result is string) ? result as string : JsonConvert.SerializeObject(result);
                source = JsonConvert.DeserializeObject<TSource>(resultString);
            }

            return source;
        }

        public static void Value(this IWebElement webElement, string value, IWebDriver webDriver)
        {
            JavascriptHelper.Value(webElement, value, webDriver);
        }

        public static void SetAttribute(this IWebElement webElement,
            string attributeName,
            string value,
            IWebDriver webDriver)
        {
            JavascriptHelper.SetAttribute(webElement,
                attributeName,
                value,
                webDriver);
        }

        public static void Html(this IWebElement webElement, string html, IWebDriver webDriver)
        {
            JavascriptHelper.Html(webElement, html, webDriver);
        }

        public static void Trigger(this IWebElement webElement, string eventName, IWebDriver webDriver)
        {
            JqueryHelper.Trigger(webElement, eventName, webDriver);
        }

        public static void TriggerChange(this IWebElement webElement, IWebDriver webDriver)
        {
            webElement.Trigger("change", webDriver);
        }
    }
}
