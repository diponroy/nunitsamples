﻿using NUnit.Framework;

namespace SnapMD.Tests.Se.Utilities
{
    public class NunitUtility
    {
        public static bool LastTestFailed()
        {
            return TestContext.CurrentContext.Result.Status == TestStatus.Failed;
        }
    }
}
