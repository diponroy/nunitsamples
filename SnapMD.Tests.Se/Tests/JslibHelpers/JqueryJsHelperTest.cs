﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.JslibHelpers
{
    [TestFixture]
    public class JqueryJsHelperTest
    {
        public IWebDriver Driver { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void Val()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //get
            page.Email.SendKeys("dipon");
            string email = JqueryHelper.Val(page.Email, Driver);
            Assert.AreEqual("dipon", email, "get Jquery val() from argument[0] not working.");

            //set
            JqueryHelper.Val(page.Password, "123", Driver);
            Assert.AreEqual("123", page.Password.GetAttribute("value"), "set Jquery val() from argument[0] not working.");
        }
    }
}
