﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Physician;

namespace SnapMD.Tests.Se.Tests.WebPages.Physician
{
    [TestFixture]
    public class LoginPageTest : ISingleUserTest<PhysicianLoginPom>
    {
        public IWebDriver Driver { get; set; }

        public PhysicianLoginPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new PhysicianLoginPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateToUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }
        /*
         * Story: Login
         * Hospital staff wants to login from "/Physician/Login"
         *  enters email
         *  enters password
         *  submits credencial
         * 
         * Success: user has Physician role function and credencials are valid
         *  redirect to "/Physician/WaitingList"
         * 
         * Fail: user has no Physician role function or credencials invalid
         *  show error message
         *  stay on "/Physician/Login"
         */

        [Test]
        public void Login_Success()
        {
            Page.SubminCredentials(TestSettings.Datas.Physician.Email, TestSettings.Datas.Physician.Password);
            Assert.IsTrue(new PhysicianWaitingListPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Login_Fail()
        {
            Page.SubminCredentials(TestSettings.Datas.Physician.Email, "invalid password");
            Assert.IsTrue(Page.ErrorNotificationContains("Email and Password combination failed."));
            Page.Wait(5);   /*wait to see if rederected*/
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url)); /*still at login page*/
        }

        /*
        * Story: Navigation
        * ForgetPassword: User clicks on Forget Password
        *      goto "Admin/ForgotPasswordAdmin.aspx"
        */
        [Test]
        public void Navigation_ForgetPassword()
        {
            Page.LinkForgotPassword.Click();
            Assert.IsTrue(new PhysicianForgotPasswordPom(Driver).HasNavigatedAtUrl());
        }
    }
}
