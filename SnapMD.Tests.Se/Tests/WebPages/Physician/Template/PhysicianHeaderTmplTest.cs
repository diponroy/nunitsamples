﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages;
using SnapMD.Tests.Se.WebPages.Physician;
using SnapMD.Tests.Se.WebPages.Physician.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Physician.Template
{
    [TestFixture]
    public class PhysicianHeaderTmplTest : ISessionPageTest<PhysicianWaitingListPom>, ISingleUserTest<PhysicianWaitingListPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PhysicianWaitingListPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PhysicianHelper(Driver);
            Page = new PhysicianWaitingListPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Physician);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            /*Todo: Test physician name here*/
        }

        private PhysicianHeaderTmpl HeaderMenuTmpl()
        {
            return PageFactory.InitElements<PhysicianHeaderTmpl>(Driver);
        }

        /*
         * Story: Menu at the header section
         * Physician user logs in
         * 
         * Menu:
         *  by defaul the menu items is hidden
         *  clicks on menu buttom, menu items shows up
         *  clicks again on menu buttom, it should hide the items
         *  toggle menu
         */

        [Test]
        public void Menu_Toggle()
        {
            var tmpl = HeaderMenuTmpl();

            Assert.AreEqual(false, tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        [Test]
        public void Menu_Show_Hide_Methods()
        {
            var tmpl = HeaderMenuTmpl();

            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        /*
         * Story: Menu items at the header section
         * Physician user logs in
         * 
         * Menu Items: total 4 menu items, clicks on the menu items
         * 
         *  Patient Waiting List    - redirected at "Physician/WaitingList"
         *  My Account              - redirected at "Physician/EditPhysicianProfile"
         *  Test Connectivity       - Opens a new tab for "tokbox.com/tools/connectivity"
         *  Log Out                 - redirected at "/Physician/Login?lo=true"
         */

        [Test]
        public void Logout()
        {
            /*Log Out*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[3].Text.Equals("Log Out"));
            tmpl.MenuLink("Log Out").Click();
            Assert.DoesNotThrow(SessionTest);

            /*impt to continue other tests*/
            Login();
        }

        [Test]
        public void Goto_PatientWaitingList()
        {
            /*Patient Waiting List*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[0].Text.Equals("Patient Waiting List"));
            tmpl.MenuLink("Patient Waiting List").Click();
            Page.Wait(3);   /*current page and goto page same, so wait and see if still at the page*/
            Assert.IsTrue(new PhysicianWaitingListPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_MyAccount()
        {
            /*My Account*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[1].Text.Equals("My Account"));
            tmpl.MenuLink("My Account").Click();
            Assert.IsTrue(new PhysicianAccountSettingPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Open_Tab_TestConnectivity()
        {
            var currentWindow = Driver.CurrentWindowHandle;

            /*Test Connectivity*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[2].Text.Equals("Test Connectivity"));
            tmpl.MenuLink("Test Connectivity").Click();

            /*new tab*/
            Assert.IsTrue(TestingHelpers.WaitUntil(Driver, d => d.WindowHandles.Count == 2)); //opens a new tab
            Driver = Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new VideoConferenceProviderPom().Url));

            /*impt: back to old tab*/
            Driver.ColseAllTab(expectedWindowHandle: currentWindow);
        }


        /*
         * Story: logout
         * Physician user logged in
         *      clicks on "Log Out" from header menu
         * 
         * redirected to login page.
         * after logout, user cann't access session pages, the url request will be redirected to the login page
         * 
         */

        public void SessionTest()
        {
            var loginPage = new PhysicianLoginPom(Driver);
            var sessionPage = new PhysicianWaitingListPom(Driver);

            Assert.IsTrue(loginPage.HasNavigatedAtUrlWith("?lo=true"));
            sessionPage.NavigateToUrl();                //trying to go at home page after logout
            sessionPage.Wait(5);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, loginPage.Url)); /*still at login page*/
        }


        [Test]
        public void Logout_Method_Test()
        {
            var tmpl = HeaderMenuTmpl();
            tmpl.RequestLogout();
            Assert.DoesNotThrow(SessionTest);

            /*impt to continue other tests*/
            Login();
        }
    }
}
