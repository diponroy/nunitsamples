﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Physician;
using SnapMD.Tests.Se.WebPages.Physician.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Physician.Template
{
    [TestFixture]
    public class PhysicianAccountLeftMenuTmplTest : ISessionPageTest<PhysicianAccountSettingPom>, ISingleUserTest<PhysicianAccountSettingPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PhysicianAccountSettingPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PhysicianHelper(Driver);
            Page = new PhysicianAccountSettingPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
           Helper.Login(TestSettings.Datas.Physician);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void SessionTest()
        {
            /*Todo: check if login, loadTestPage method not throwing error*/
        }

        [Test]
        public void TitleTest()
        {
            /*Todo: Test client name here*/
        }

        /*
         * physician log's in, and goes to his account page
         * 
         * got 4 buttons on left
         *      Account Settings    -- Goto "Physician/EditPhysicianProfile"
         *      My Files            -- Goto "Physician/MyFiles"
         *      Consultations       -- Goto  "Physician/PhysicianConsultations#scheduled"
         *      Patient Profiles    -- Goto "Physician/PatientsList"
         */

        private PhysicianAccountLeftMenuTmpl LeftMenuTmpl()
        {
            return PageFactory.InitElements<PhysicianAccountLeftMenuTmpl>(Driver);
        }

        [Test]
        public void Goto_AccountSettings()
        {
            /*Account Settings*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Account Settings", menu.BtnAccountSettings.Text.Trim());
            menu.BtnAccountSettings.Click();
            Assert.IsTrue(new PhysicianAccountSettingPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_PatientProfiles()
        {
            /*Patient Profiles*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Patient Profiles", menu.BtnPatientProfiles.Text.Trim());
            menu.BtnPatientProfiles.Click();
            Assert.IsTrue(new PhysicianPatientProfilesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Consultations()
        {
            /*Consultations*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Consultations", menu.BtnConsultations.Text.Trim());
            menu.BtnConsultations.Click();
            Assert.IsTrue(new PhysicianConsultationsPom(Driver).HasNavigatedAtUrlWith("#scheduled"));
        }

        [Test]
        public void Goto_MyFiles()
        {
            /*My Files*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("My Files", menu.BtnMyFiles.Text.Trim());
            menu.BtnMyFiles.Click();
            Assert.IsTrue(new PhysicianFilesPom(Driver).HasNavigatedAtUrl());
        }
    }
}
