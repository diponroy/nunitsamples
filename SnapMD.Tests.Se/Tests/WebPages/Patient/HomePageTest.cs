﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class HomePageTest : ISessionPageTest<PatientHomePom>, ISingleUserTest<PatientHomePom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PatientHomePom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new PatientHomePom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new PatientLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }

        /*
         * UpdateSessionContactNumber
         *      udates patient contact number for consultaion usages
         */
        [Test]
        public void UpdateSessionContactNumber_Success()
        {
            string contactNumber = "+1" + Utility.RandomString(10, "numbers");

            /*update*/
            Page.UpdateSessionContactNumber(contactNumber);
            Assert.AreEqual(Utility.DigitString(contactNumber), Utility.DigitString(Page.TxtSessionContactNumber.Value()));

            /*contact is at session*/
            new PatientAccountSettingPom(Driver).NavigateToUrl();
            Page.WaitForAjax(); //goto another page
            LoadTestPage();     //back to home page
            Assert.AreEqual(Utility.DigitString(contactNumber), Utility.DigitString(Page.TxtSessionContactNumber.Value())); /*number still here*/
        }

        [Test]
        public void UpdateSessionContactNumber_Fail()
        {
            /*emply number*/
            Page.UpdateSessionContactNumber("");
            Assert.IsTrue(Page.ErrorNotificationContains("Please enter a Contact Number"));

            /*less than 10 digit*/
            Page.UpdateSessionContactNumber(Utility.RandomString(Utility.RandomNumber(3, 9), "numbers"));
            Assert.IsTrue(Page.ErrorNotificationContains("Please enter at least 10-digit phone number"));
        }
    }
}
