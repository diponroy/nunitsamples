﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;
using SnapMD.Tests.Se.WebPages.Patient.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient.Template
{
    [TestFixture]
    public class PatientAccountLeftMenuTmplTest : ISessionPageTest<PatientAccountSettingPom>, ISingleUserTest<PatientAccountSettingPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PatientAccountSettingPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new PatientAccountSettingPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void SessionTest()
        {
            /*Todo: check if login, loadTestPage method not throwing error*/
        }

        [Test]
        public void TitleTest()
        {
            /*Todo: Test client name here*/
        }

        private PatientAccountLeftMenuTmpl LeftMenuTmpl()
        {
            return PageFactory.InitElements<PatientAccountLeftMenuTmpl>(Driver);
        }

        /*
         * Login as patient, go to his account setting page
         * 
         * On Left got 7 btn. (hide/show depends on hospital setting, like 'Payment Information')
         * User Profiles        -- Goto "Customer/Users"
         * Dependent Profiles   -- Goto "Customer/Dependents"
         * My Files             -- Goto "Customer/MyFiles"
         * Health Plans         -- Goto "Customer/HealthPlan"   (pore)
         * Payment Information  -- Goto "Customer/AddPaymentInformation"
         * Consultations        -- Goto "Customer/PatientConsultations#scheduled"
         * Account Settings     -- Goto "Customer/AccountSettings"
         */

        [Test]
        public void Goto_AccountSettings()
        {
            /*Account Settings*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Account Settings", menu.BtnAccountSettings.Text.Trim());
            menu.BtnAccountSettings.Click();
            Page.Wait(3);   /*current page and goto page same, so wait and see if still at the page*/
            Assert.IsTrue(new PatientAccountSettingPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Consultations()
        {
            /*Consultations*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Consultations", menu.BtnConsultations.Text.Trim());
            menu.BtnConsultations.Click();
            Assert.IsTrue(new PatientConsultationsPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_PaymentInformation()
        {
            /*Payment Information*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("Payment Information", menu.BtnPaymentInformation.Text.Trim());
            menu.BtnPaymentInformation.Click();
            Assert.IsTrue(new PaymentInfoPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_HealthPlans()
        {
            PatientAccountLeftMenuTmpl menu;
            /*Health Plans*/
            menu = LeftMenuTmpl();
            Assert.IsTrue(menu.BtnHealthPlans.Text.Trim().Contains("Health Plans")); /*has a hidden "Manage" word*/
            menu.BtnHealthPlans.Click();
            Assert.IsTrue(new PatientHealthPlansPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_MyFiles()
        {
            /*My Files*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("My Files", menu.BtnMyFiles.Text.Trim());
            menu.BtnMyFiles.Click();
            Assert.IsTrue(new PatientFilesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_DependentProfiles()
        {
            PatientAccountLeftMenuTmpl menu;
            /*Dependent Profiles*/
            menu = LeftMenuTmpl();
            Assert.AreEqual("Dependent Profiles", menu.BtnDependentProfiles.Text.Trim());
            menu.BtnDependentProfiles.Click();
            Assert.IsTrue(new DependentProfilesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_UserProfiles()
        {
            /*User Profiles*/
            var menu = LeftMenuTmpl();
            Assert.AreEqual("User Profiles", menu.BtnUserProfiles.Text.Trim());
            menu.BtnUserProfiles.Click();
            Assert.IsTrue(new UserProfilesPom(Driver).HasNavigatedAtUrl());
        }
    }
}