﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages;
using SnapMD.Tests.Se.WebPages.Patient;
using SnapMD.Tests.Se.WebPages.Patient.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient.Template
{
    [TestFixture]
    public class PatientHeaderTmplTest : ISessionPageTest<PatientHomePom>, ISingleUserTest<PatientHomePom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PatientHomePom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new PatientHomePom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            /*Todo: Test physician name here*/
        }

        private PatientHeaderTmpl HeaderMenuTmpl()
        {
            return PageFactory.InitElements<PatientHeaderTmpl>(Driver);
        }

        /*
         * Story: Menu at the header section
         * Patient user logs in
         * 
         * Menu:
         *  by defaul the menu items is hidden
         *  clicks on menu buttom, menu items shows up
         *  clicks again on menu buttom, it should hide the items
         *  toggle menu
         */

        [Test]
        public void Menu_Toggle()
        {
            var tmpl = HeaderMenuTmpl();
            Assert.AreEqual(false, tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        [Test]
        public void Menu_Show_Hide_Methods()
        {
            var tmpl = HeaderMenuTmpl();

            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        /*
         * Story: Menu items at the header section
         * Patient user logs in and clicks on the menu items
         * 
         * Menu Items: total 4 menu items
         *  Home                - redirected at "Customer/Home#home"
         *  My Account          - redirected at "Customer/Users"
         *  Test Connectivity   - Opens a new tab for "tokbox.com/tools/connectivity"
         *  Log Out             - redirected at "Customer/Login?lo=true"
         */

        [Test]
        public void Open_Tab_TestConnectivity()
        {
            var currentWindow = Driver.CurrentWindowHandle;

            /*Test Connectivity*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[2].Text.Equals("Test Connectivity"));
            tmpl.MenuLink("Test Connectivity").Click();

            /*new tab*/
            Assert.IsTrue(TestingHelpers.WaitUntil(Driver, d => d.WindowHandles.Count == 2)); //opens a new tab
            Driver = Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new VideoConferenceProviderPom().Url));

            /*impt: back to old tab*/
            Driver.ColseAllTab(expectedWindowHandle: currentWindow);
        }

        [Test]
        public void Goto_MyAccount()
        {
            /*My Account*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[1].Text.Equals("My Account"));
            tmpl.MenuLink("My Account").Click();
            Assert.IsTrue(new UserProfilesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Home()
        {
            /*Home*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[0].Text.Equals("Home"));
            tmpl.MenuLink("Home").Click();
            Page.Wait(3);   /*current page and goto page same, so wait and see if still at the page*/
            Assert.IsTrue(new PatientHomePom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void LogOut()
        {
            /*Log Out*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[3].Text.Equals("Log Out"));
            tmpl.MenuLink("Log Out").Click();
            Assert.DoesNotThrow(SessionTest);

            /*impt: to run other tests*/
            Login();
        }


        /*
         * Story: logout
         * Patient user logged in
         *      clicks on "Log Out" from header menu
         * 
         * redirected to login page.
         * after logout, user cann't access session pages, the url request will be redirected to the login page
         * 
         */
        public void SessionTest()
        {
            var loginPage = new PatientLoginPom(Driver);
            var sessionPage = new PatientHomePom(Driver);

            Assert.IsTrue(loginPage.HasNavigatedAtUrlWith("?lo=true"));
            sessionPage.NavigateToUrl();                //trying to go at home page after logout
            sessionPage.Wait(5);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, loginPage.Url)); /*still at login page*/
        }

        [Test]
        public void Logout_Method()
        {
            var tmpl = HeaderMenuTmpl();
            tmpl.RequestLogout();
            Assert.DoesNotThrow(SessionTest);

            /*impt: to run other tests*/
            Login();
        }
    }
}
