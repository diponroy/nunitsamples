﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class LoginPageTest : ISingleUserTest<PatientLoginPom>
    {
        public IWebDriver Driver { get; set; }

        public PatientLoginPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new PatientLoginPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateToUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        /*
         * Page title "Login | SnapMD"
         */
        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        /*
         * Story: Login
         * User wants to login from "/Customer/Login"
         *  enters email
         *  enters password
         *  submits credencial
         * 
         * Success: user type is Customer and credencials are valid
         *  redirect to "/Customer/Home#home"
         * 
         * Fail: user type not Customer or credencials invalid
         *  show error message
         *  stay on "/Customer/Login"
         */

        [Test]
        public void Login_Success()
        {
            Page.SubminCredentials(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);
            Assert.IsTrue(new PatientHomePom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Login_Fail()
        {
            Page.SubminCredentials(TestSettings.Datas.Patient.Email, "invalid password");
            Assert.IsTrue(Page.ErrorNotificationContains("Email and Password combination failed."));
            Page.Wait(5);   /*wait to see if rederected*/
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url)); /*still at login page*/
        }

        [Test]
        public void Goto_ForgotPassword()
        {
            Page.LinkForgotPassword.Click();
            Assert.IsTrue(new PatientForgotPasswordPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Register()
        {
            Page.LinkRegister.Click();
            var signUpPage = new SignUpPom(Driver);
            Assert.IsTrue(signUpPage.HasNavigatedAtUrl());
        }
    }
}