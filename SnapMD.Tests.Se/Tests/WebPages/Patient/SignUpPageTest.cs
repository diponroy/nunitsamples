﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class SignUpPageTest : ISingleUserTest<SignUpPom>
    {
        public IWebDriver Driver { get; set; }

        public SignUpPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new SignUpPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }
        [Test]
        public void Registration_Success()
        {
            /*Step-1*/
            Assert.IsTrue(Page.IsVisiable(Page.RegisterPanel1));
            Page.TxtZipCode.SendKeys(Utility.RandomNumber(10000, 99999).ToString());
            Page.BtnGotoStep2.Click();

            /*Setp-2*/
            Assert.IsTrue(Page.IsVisiable(Page.RegisterPanel2));
            Page.TxtFirstName.SendKeys(Utility.RandomString(Utility.RandomNumber(3, 10)));
            Page.TxtLastName.SendKeys(Utility.RandomString(Utility.RandomNumber(3, 10)));
            Page.TxtEmail.SendKeys(TestSettings.Datas.Patient.NewAlliancedEmail());
            Page.TxtDateBirth.SendKeys("01/13/1990"); /*MM/dd/yyyy and age > 18*/
            Page.BtnGotoStep3.Click();

            /*Setp-3*/
            Assert.IsTrue(Page.IsVisiable(Page.RegisterPanel3));
            Page.TxtAddress.SendKeys(Utility.RandomString(Utility.RandomNumber(10, 25)));
            new KendoDropdown(Page.DdlCountry, Driver).SelectByIndex(1); //country usa
            Page.BtnGotoStep4.Click();

            /*Setp-4*/
            Assert.IsTrue(Page.IsVisiable(Page.RegisterPanel4));
            Page.TxtPassword.SendKeys(TestSettings.Datas.Patient.Password);
            Page.TxtConfirmPassword.SendKeys(TestSettings.Datas.Patient.Password);
            Page.ChkTermsAndConditions.Click();

            /*submit*/
            Page.BtnRegister.Click();
            Assert.IsTrue(new RegisterConfirmPom(Driver).HasNavigatedAtUrl());
        }
    }
}
