﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class AddDependentPageTest : ISessionPageTest<AddDependentPom>, ISingleUserTest<AddDependentPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public AddDependentPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new AddDependentPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new PatientLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }

        /*
         * adds adult dependent user where age > 18 and has own email
         *      on success moves to dependent list
         */

        /*TODO: need to work with pom, the page got new MVVM*/
        [Test, Ignore]
        public void Add_Adult_Dependent_Success()
        {
            string firstName = Utility.RandomString(9, 15);
            string lastName = Utility.RandomString(9, 15);

            /*form data*/
            Page.TxtFirstName.Value(firstName);
            Page.TxtLastName.Value(lastName);
            Page.TxtDateOfBirth.Value(TestingHelpers.DateToString(DateTime.Now.AddYears(-25))); //25 years old
            Page.ChkGenderMaleHolder.Click();
            Page.TxtDateOfBirth.TriggerChange(Driver); //impt: not to do just after setting datepicker value
            Assert.IsTrue(Page.IsVisiable(Page.TxtEmail));

            Page.TxtEmail.Value(TestSettings.Datas.Patient.NewAlliancedEmail());
            new KendoDropdown(Page.DdlCountry, Driver).SelectByIndex(1);
            new KendoDropdown(Page.DdlEthnicity, Driver).SelectByIndex(1);
            new KendoDropdown(Page.DdlHairColor, Driver).SelectByIndex(1);
            new KendoDropdown(Page.DdlEyeColor, Driver).SelectByIndex(1);
            new KendoDropdown(Page.DdlBloodType, Driver).SelectByIndex(1);
            Page.TxtMobilePhoneNumber.Value(Utility.RandomString(6, 10, "numbers"));
            Page.TxtHomePhoneNumber.Value(Utility.RandomString(6, 10, "numbers"));
            new KendoNumericTextBox(Page.NTxtBoxWeight, Driver).Value(35.ToString());
            new KendoDropdown(Page.DdlWeightUnit, Driver).SelectByIndex(1);
            new KendoNumericTextBox(Page.NTxtBoxHeightFeet, Driver).Value(5.ToString());
            new KendoNumericTextBox(Page.NTxtBoxHeightInche, Driver).Value(2.ToString());
            new KendoDropdown(Page.DdlHeightUnit, Driver).SelectByIndex(1);
            new KendoDropdown(Page.DdlRelationShip, Driver).SelectByIndex(1);
            Page.ChkAuthorizedNoHolder.Click();
            Page.TxtAddress.Value(Utility.RandomString(9, 15));
            Page.TxtFamilyDoctorName.Value(Utility.RandomString(9, 15));
            Page.TxtFamilyDoctorPhone.Value(Utility.RandomString(6, 10, "numbers"));

            /*default values*/
            Assert.IsTrue(Page.ChkGenderMale.Selected); /*if gender male checked*/
            Assert.IsFalse(Page.ChkGenderFemale.Selected);
            Assert.IsTrue(Page.ChkAuthorizedNo.Selected); /*if authorized no checked*/
            Assert.IsFalse(Page.ChkAuthorizedYes.Selected);
            Assert.IsFalse(String.IsNullOrEmpty(Page.TxtMobilePhoneCountryCode.Value().Trim())); /*from ddl country*/
            Assert.IsTrue(Page.ChkAuthorizedNo.Selected); /*all default checked*/
            Assert.IsTrue(Page.ChkMedicalConditionsNone.Selected);
            Assert.IsTrue(Page.ChkMedicalAllergiesNone.Selected);
            Assert.IsTrue(Page.ChkClinicianSpecialistNone.Selected);
            Assert.IsTrue(Page.ChkPreferredPharmacyNone.Selected);

            /*submit button*/
            Page.BtnSave.Click();
            Page.WaitForAjax();
            //Assert.IsTrue(page.SuccessNotificationContains("Profille has been created successfully."));
            var dependentListPage = new DependentProfilesPom(Driver);
            Assert.IsTrue(dependentListPage.HasNavigatedAtUrl());
            Assert.IsNotNull(dependentListPage.FindUnauthorizedDependentRow(Utility.FullName(firstName, lastName)));/*list got new user*/
        }


        //[Test, Explicit]
        //public void CreateAndDeleteDependent()
        //{
        //    //Login
        //    var webDriver = DriverSetup.Driver;
        //    new TestingHelpers().doLogin(webDriver, "/Customer/Login", Settings.Default.PatientUser, "Password@123");

        //    // wait until home page loads
        //    var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
        //    wait.Until((d) => { return !d.Title.Contains("Login"); });

        //    //Click on Main Manu and open My Account 
        //    PatientHomePom home = new PatientHomePom(webDriver);
        //    home.openMainMenu();
        //    home.clickOnMyAccount();

        //    //Slect Dependets profile
        //    PatientAccountSettingPom myPatientAccount = new PatientAccountSettingPom(webDriver);
        //    myPatientAccount.selectDependentProfiles();
        //    myPatientAccount.clickOnAddDependentProfile();

        //    //Fill dependent details
        //    AddDependentPom addDependentPom = new AddDependentPom(webDriver);
        //    var dependentData = addDependentPom.fillInData();

        //    //Click save
        //    addDependentPom.clickSave();

        //    //view profile of created dependent
        //    DependentProfilesPom dependents = new DependentProfilesPom(webDriver);
        //    dependents.viewDependentProfile(dependentData["firstName"] + " " + dependentData["lastName"]);

        //    //check data on dependents profile
        //    DependentProfilePom dependent = new DependentProfilePom(webDriver);
        //    dependent.checkData(dependentData);

        //    //delete dependent profile
        //    dependent.BtnDelete.Click();
        //    IWebElement confirmNotification;
        //    Assert.IsTrue(dependent.ConfirmationNotificationContains(out confirmNotification, "Are you sure that you want to delete this Dependent?"));
        //    dependent.Confirm(confirmNotification);


        //    //check for notification about successful deleting
        //    Assert.IsTrue(dependent.SuccessNotificationContains("Dependent has been deleted successfully"));

        //    //check that deleted profile is not on list of dependents anymore
        //    Assert.IsNotNull(dependents.FindDependentRow(dependentData["firstName"] + " " + dependentData["lastName"]));
        //}

    }
}
