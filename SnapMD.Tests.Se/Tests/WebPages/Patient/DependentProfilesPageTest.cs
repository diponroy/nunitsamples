﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class DependentProfilesPageTest : ISessionPageTest<DependentProfilesPom>, ISingleUserTest<DependentProfilesPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public DependentProfilesPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new DependentProfilesPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new PatientLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }

        /*Todo: need defaul data*/
        [Test]
        public void Find()
        {
            var authorizedDependent = "lo lo";
            var unauthorizedDependent = "ko ko";
            var unknownName = Utility.RandomString(10);

            Assert.IsNotNull(Page.FindDependentRow(authorizedDependent));
            Assert.IsNotNull(Page.FindDependentRow(unauthorizedDependent));
            Assert.IsNull(Page.FindDependentRow(unknownName));

            Assert.IsNotNull(Page.FindAuthorizedDependentRow(authorizedDependent));
            Assert.IsNull(Page.FindAuthorizedDependentRow(unauthorizedDependent));
            Assert.IsNull(Page.FindAuthorizedDependentRow(unknownName));

            Assert.IsNull(Page.FindUnauthorizedDependentRow(authorizedDependent));
            Assert.IsNotNull(Page.FindUnauthorizedDependentRow(unauthorizedDependent));
            Assert.IsNull(Page.FindUnauthorizedDependentRow(unknownName));
        }

        [Test]
        public void Goto_AddNewDependent()
        {
            Page.BtnAddNewDependentProfile.Click();
            Assert.IsTrue(new AddDependentPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_View_Authorized_Dependent()
        {
            TestingHelpers.WaitUntil(Driver,
                d =>
                {
                    var hasRow = Page.AuthorizedDependentRows().Count > 1;
                    return hasRow;
                });
            Assert.GreaterOrEqual(Page.AuthorizedDependentRows().Count, 1);
            IWebElement dependent = Page.AuthorizedDependentRows().First();
            Page.ViewDependentRow(dependent);
            Assert.IsTrue(new DependentProfilePom(Driver).HasNavigatedAtUrl());
        }
    }
}
