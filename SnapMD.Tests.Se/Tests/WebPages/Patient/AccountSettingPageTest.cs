﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.WebPages.Patient
{
    [TestFixture]
    public class AccountSettingPageTest : ISessionPageTest<PatientAccountSettingPom>, ISingleUserTest<PatientAccountSettingPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PatientAccountSettingPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
            Page = new PatientAccountSettingPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Patient);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new PatientLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }

        [Test, Ignore]
        public void UpdateEmail()
        {
            //Page.Email.Value("test@test.com");
            //Page.BtnUpdatePersonalInfo.Click(); /*send email to old email address to confirm change*/
            //Assert.IsTrue(Page.SuccessNotificationContains(""));
            //Assert.AreEqual(Page.Email.Value(), TestSettings.Datas.Patient.Email);  /*still old email, unless confirming froming by email*/

            ///*need to check popup at login page*/
            //Helper.Logout();
            //Assert.DoesNotThrow(() =>
            //{
            //    Login();
            //    LoadTestPage();
            //});
        }

        /*
         * TODO: if changing or restoring password faills, other test will fail if depends on patient login
         * need another patient, or restoring default data
         */
        [Test, Explicit]
        public void UpdatePassword_Success()
        {
            /*update new passoword*/
            string newPassword = "Password@1234";
            Page.UpdatePassword(TestSettings.Datas.Patient.Password, newPassword);
            Assert.IsTrue(Page.SuccessNotificationContains("Password has been changed successfully."));

            /*login with new password*/
            Helper.Logout();
            Assert.DoesNotThrow(() => Helper.Login(TestSettings.Datas.Patient.Email, newPassword));

            /*restore password to default*/
            LoadTestPage();
            Page.UpdatePassword(newPassword, TestSettings.Datas.Patient.Password);
            Assert.IsTrue(Page.SuccessNotificationContains("Password has been changed successfully."));
        }


    }
}
