﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    [TestFixture]
    public class LoginUserTest : ISingleUserTest<AdminLoginPom>
    {
        public IWebDriver Driver { get; set; }

        public AdminLoginPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new AdminLoginPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Dispose();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }


        /*
         * Story: Login
         * Hospital staff wants to login from "/Admin/Login"
         *  enters email
         *  enters password
         *  submits credencial
         * 
         * Success: user has Admin role function and credencials are valid
         *  redirect to "/Admin/Dashboard"
         * 
         * Fail: user has no Admin role function or credencials invalid
         *  show error message
         *  stay on "/Admin/Login"
         */

        [Test]
        public void Login_Success()
        {
            Page.SubminCredentials(TestSettings.Datas.Admin.Email, TestSettings.Datas.Admin.Password);
            Assert.IsTrue(new DashBoardPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Login_Fail()
        {
            Page.SubminCredentials(TestSettings.Datas.Admin.Email, "invalid password");
            Assert.IsTrue(Page.ErrorNotificationContains("Email and Password combination failed."));
            Page.Wait(5);   /*wait to see if rederected*/
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url)); /*still at login page*/
        }

        /*
         * Story: Navigation
         * ForgetPassword: User clicks on Forget Password
         *      goto "Admin/ForgotPasswordAdmin.aspx"
         */
        [Test]
        public void Goto_ForgetPassword()
        {
            Page.LinkForgotPassword.Click();
            Assert.IsTrue(new AdminForgotPasswordPom(Driver).HasNavigatedAtUrl());
        }

    }
}
