﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    [TestFixture]
    public class DashboardUserTest : ISessionPageTest<DashBoardPom>, ISingleUserTest<DashBoardPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public DashBoardPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Page = new DashBoardPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }


        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        /*
         * Story: logout
         * Admin user logged in
         *      clicks on "Log Out" from header menu
         * 
         * redirected to login page.
         * after logout, user cann't access session pages, the url request will be redirected to the login page
         * 
         */

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new AdminLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }
    }
}
