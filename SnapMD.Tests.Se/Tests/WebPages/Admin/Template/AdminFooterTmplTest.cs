﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;
using SnapMD.Tests.Se.WebPages.Admin.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin.Template
{
    [TestFixture]
    public class AdminFooterTmplTest : ISessionPageTest<DashBoardPom>, ISingleUserTest<DashBoardPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public DashBoardPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Page = new DashBoardPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        public void SessionTest()
        {
        }

        private AdminFooterTmpl FooterTmpl()
        {
            return PageFactory.InitElements<AdminFooterTmpl>(Driver);
        }

        /*Todo: onclick the pdf opend at new tab, but testing that way is not working.
         * Driver.WindowHandles was throwing error
         */
        [Test]
        public void Download_TermsAndConditionsPdf()
        {
            var currentWindow = Driver.CurrentWindowHandle;
            var tmpl = FooterTmpl();
            Assert.DoesNotThrow(() => Driver.Download(tmpl.LinkAgreement.GetAttribute("href"))); //download pdf
            Assert.IsTrue(tmpl.LinkAgreement.GetAttribute("href").Contains("TermsandConditions.pdf"));          
        }

        [Test]
        public void Navigation_PoweredBy()
        {
            var currentWindow = Driver.CurrentWindowHandle;
            var tmpl = FooterTmpl();
            tmpl.LinkPoweredBy.Click();

            /*new tab*/
            Assert.IsTrue(TestingHelpers.WaitUntil(Driver, d => d.WindowHandles.Count == 2)); //opens a new tab
            Driver = Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            Assert.IsTrue(TestingHelpers.HasNavigatedAtUrl(Driver, "http://snap.md/"));

            /*impt: back to the started tab*/
            Driver.ColseAllTab(currentWindow);
        }
    }
}
