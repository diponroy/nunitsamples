﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages;
using SnapMD.Tests.Se.WebPages.Admin;
using SnapMD.Tests.Se.WebPages.Admin.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin.Template
{
    [TestFixture]
    public class AdminHeaderTmplTest : ISessionPageTest<DashBoardPom>, ISingleUserTest<DashBoardPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public DashBoardPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Page = new DashBoardPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            /*Todo: Test physician name here*/
        }

        public void SessionTest()
        {
            var loginPage = new AdminLoginPom(Driver);
            var sessionPage = new DashBoardPom(Driver);
            Assert.IsTrue(loginPage.HasNavigatedAtUrlWith("?lo=true"));

            sessionPage.NavigateToUrl();                //trying to go at home page after logout
            sessionPage.Wait(5);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, loginPage.Url)); /*still at login page*/
        }

        private AdminHeaderTmpl HeaderMenuTmpl()
        {
            return PageFactory.InitElements<AdminHeaderTmpl>(Driver);
        }

        /*
         * Story: Menu at the header section
         * Admin user logs in
         * 
         * Menu:
         *  by defaul the menu items is hidden
         *  clicks on menu buttom, menu items shows up
         *  clicks again on menu buttom, it should hide the items
         *  toggle menu
         */

        [Test]
        public void Menu_Toggle()
        {
            var tmpl = HeaderMenuTmpl();
            Assert.AreEqual(false, tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        [Test]
        public void Menu_Show_Hide_Methods()
        {
            var tmpl = HeaderMenuTmpl();

            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.ShowMenus();
            Assert.IsTrue(tmpl.IsMenuPopupVisible());

            tmpl.Menu.Click();
            tmpl.HideMenus();
            Assert.IsFalse(tmpl.IsMenuPopupVisible());
        }

        /*
         * Story: Menu items at the header section
         * Admin user logs in and clicks on the menu items
         * 
         * Menu Items: total 5 menu items
         *  Home                - redirected at "Admin/Dashboard"
         *  My Account          - redirected at "Admin/StaffAccount"
         *  Test Connectivity   - Opens a new tab for "tokbox.com/tools/connectivity"
         *  E-Prescribing       - Opens a new tab for "ePrescription/RxNT.aspx"
         *  Log Out             - redirected at "Admin/Login?lo=true"
         */

        [Test]
        public void Goto_MyAccount()
        {
            /*My Account*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[1].Text.Equals("My Account"));
            tmpl.MenuLink("My Account").Click();
            Assert.IsTrue(new StaffAccountPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Home()
        {
            /*Home*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[0].Text.Equals("Home"));
            tmpl.MenuLink("Home").Click();
            Page.Wait(3); /*current page and goto page same, so wait and see if still at the page*/
            Assert.IsTrue(new DashBoardPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Open_Tab_EPrescribing()
        {
            var currentWindow = Driver.CurrentWindowHandle;

            /*E-Prescribing*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[3].Text.Equals("E-Prescribing"));
            tmpl.MenuLink("E-Prescribing").Click();

            /*new tab*/
            Assert.IsTrue(TestingHelpers.WaitUntil(Driver, d => d.WindowHandles.Count == 2)); //opens a new tab
            Driver = Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            Assert.IsTrue(new AdminRxNTPom(Driver).HasNavigatedAtUrl());

            /*impt: back to old tab*/
            Driver.ColseAllTab(expectedWindowHandle: currentWindow);
        }

        [Test]
        public void Open_Tab_TestConnectivity()
        {
            var currentWindow = Driver.CurrentWindowHandle;

            /*Test Connectivity*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[2].Text.Equals("Test Connectivity"));
            tmpl.MenuLink("Test Connectivity").Click();

            /*new tab*/
            Assert.IsTrue(TestingHelpers.WaitUntil(Driver, d => d.WindowHandles.Count == 2)); //opens a new tab
            Driver = Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new VideoConferenceProviderPom().Url));

            /*impt: back to old tab*/
            Driver.ColseAllTab(expectedWindowHandle: currentWindow);
        }

        [Test]
        public void Logout()
        {
            /*Log Out*/
            var tmpl = HeaderMenuTmpl();
            tmpl.Menu.Click();
            Assert.IsTrue(tmpl.MenuItems[4].Text.Equals("Log Out"));
            tmpl.MenuLink("Log Out").Click();
            Assert.DoesNotThrow(SessionTest);

            /*impt to continue other tests*/
            Login();
        }

        /*
         * Story: logout
         * Admin user logged in
         *      clicks on "Log Out" from header menu
         * 
         * redirected to login page.
         * after logout, user cann't access session pages, the url request will be redirected to the login page
         * 
         */

        [Test]
        public void Logout_Method()
        {
            var tmpl = HeaderMenuTmpl();
            tmpl.RequestLogout();
            Assert.DoesNotThrow(SessionTest);

            /*impt: for other tests*/
            Login();
        }
    }
}
