﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;
using SnapMD.Tests.Se.WebPages.Admin.Template;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin.Template
{
    [TestFixture]
    public class AdminTopMenuTmplTest : ISessionPageTest<DashBoardPom>, ISingleUserTest<DashBoardPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public DashBoardPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Page = new DashBoardPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        public void SessionTest()
        {
        }

        private AdminTopMenuTmpl TopMenuTmpl()
        {
            return PageFactory.InitElements<AdminTopMenuTmpl>(Driver);
        }

        [Test]
        public void Goto_Dashboard()
        {
            /*Dashboard*/
            var menu = TopMenuTmpl();
            menu.LinkDashboard.Click();
            Page.Wait(5);   /*impt: goto page and current page same, so wait and check if still at same page*/
            Assert.IsTrue(Page.HasNavigatedAtUrl());
        }

        /*asking for permission, may be missing some sql update*/
        [Test]
        public void Goto_Files()
        {
            /*Files*/
            var menu = TopMenuTmpl();
            menu.LinkFiles.Click();
            Assert.IsTrue(new HospitalAdminFilesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_ManageRoles()
        {
            /*Manage Roles*/
            var menu = TopMenuTmpl();
            menu.LinkManageRoles.Click();
            Assert.IsTrue(new ManageRolesPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Consultations()
        {
            /*Consultations*/
            var menu = TopMenuTmpl();
            menu.LinkConsultations.Click();
            Assert.IsTrue(new ConsultationsPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Reports()
        {
            /*Reports*/
            var menu = TopMenuTmpl();
            menu.LinkReports.Click();
            Assert.IsTrue(new AdminReportsPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_Patients()
        {
            /*Patients*/
            var menu = TopMenuTmpl();
            menu.LinkPatients.Click();
            Assert.IsTrue(new PatientListPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Goto_StaffAccounts()
        {
            /*Staff Accounts*/
            var menu = TopMenuTmpl();
            menu.LinkStaffAccounts.Click();
            Assert.IsTrue(new StaffAccountListPom(Driver).HasNavigatedAtUrl());
        }
    }
}
