﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    [TestFixture]
    public class ForgotPasswordUserTest : ISingleUserTest<AdminForgotPasswordPom>
    {
        public IWebDriver Driver { get; set; }

        public AdminForgotPasswordPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new AdminForgotPasswordPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }


        /*
         * Story: Password recovery
         *  user enters his email
         *  and clicks at "Email me a link"
         *  
         * Success: if email exists and user is active hospital staff
         *  sends email
         *  and shows success message
         *  
         */
        [Test]
        public void PasswordRecovery_Success_Shows_Message()
        {
            Page.SendRecoveryEmail(TestSettings.Datas.Admin.Email);
            Page.WaitForAjax();
            Assert.IsTrue(Page.SuccessNotificationContains("Your password reset email has been sent."));
        }


        /*
         * Story: Navigation
         * 
         * Back: User clicks on "Back" link
         *      goto the previous visited page
         */
        [Test]
        public void Goto_Back()
        {
            var loginPage = new AdminLoginPom(Driver);
            loginPage.NavigateToUrl();                  //first time at loginPage
            loginPage.LinkForgotPassword.Click();

            Page.HasNavigatedAtUrl();
            Page.LinkBack.Click();                      //should goto back to previously visited loginPage
            Assert.IsTrue(loginPage.HasNavigatedAtUrl());
        }
    }
}
