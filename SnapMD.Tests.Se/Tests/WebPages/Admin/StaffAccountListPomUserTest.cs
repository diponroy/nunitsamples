﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.TestSetting.Data.Model;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    /*
     * most of the tests depends on search and get staff row
     */

    [TestFixture]
    public class StaffAccountListPomUserTest : ISessionPageTest<StaffAccountListPom>, ISingleUserTest<StaffAccountListPom>
    {
        public IWebDriver Driver { get; set; }
        public IUserHelper Helper { get; set; }
        public StaffAccountListPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new StaffAccountListPom(Driver);
            Helper = new AdminHelper(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new AdminLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url));

            /*impt: login, loadTestPage again is important for other tests*/
        }

        [Test]
        public void Find_StaffRow_By_FullName()
        {
            HospitalStaffUser physicianUser = TestSettings.Datas.Physician;
            Page.SearchStaffRows(physicianUser.FirstName);
            Page.Wait(2); /*wait for data binding*/
            Assert.IsNotNull(Page.FindStaff(physicianUser.FullName));
            Assert.GreaterOrEqual(Page.StaffRows.Count, 1); /*at least Datas.Physician should be at the grid*/
        }

        [Test]
        public void Search_And_Get_StaffRow_ByFullName()
        {
            string staffFullName;

            /*found staff*/
            staffFullName = TestSettings.Datas.Physician.FullName;
            Assert.IsNotNull(Page.SearchAndGetStaffRow(staffFullName));
            Assert.GreaterOrEqual(Page.StaffRows.Count, 1);

            /*not found staff*/
            staffFullName = Utility.RandomString(10, 20);
            Assert.Catch<Exception>(() => Page.SearchAndGetStaffRow(staffFullName));
        }

        /*
         * imt: popup only appears for the physician roles
         */
        [Test]
        public void Show_Appointment_PopUp()
        {
            IWebElement staffRow = Page.SearchAndGetStaffRow(TestSettings.Datas.Physician.FullName);
            Assert.DoesNotThrow(() => Page.ShowAppointmentPopUp(staffRow));
            Assert.IsTrue(Page.AppointmentPopup.Displayed && Page.AppointmentPopup.Enabled);
        }

        [Test]
        public void Create_NoCharge_ScheduleApspointment_Success()
        {
            /*select specific staff, for DdlDoctors*/
            IWebElement staffRow = Page.SearchAndGetStaffRow(TestSettings.Datas.Physician.FullName);
            Page.ShowAppointmentPopUp(staffRow);

            /*schedule info*/
            var tmpl = PageFactory.InitElements<ScheduleAppointmentTmpl>(Driver);
            new KendoDropdown(tmpl.DdlPatients, Driver).SelectByIndex(1); //patient
            new KendoDropdown(tmpl.DdlPrimaryConcerns, Driver).SelectByIndex(1); //primary prob
            new KendoDropdown(tmpl.DdlSecondaryConcerns, Driver).SelectByIndex(2);
            //secondary prob, it shouldn't be as primary one
            DateTime randomDateTime = Utility.RandomDateTime(DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));
            tmpl.TxtScheduleDate.Value(TestingHelpers.DateToString(randomDateTime));
            tmpl.TxtScheduleTime.Value(TestingHelpers.TimeToString(randomDateTime));
            tmpl.BntCreateScheduleAppointment.Click();

            Assert.IsTrue(Page.SuccessNotificationContains("New Appointment Scheduled Successfully"));
        }

        /*
         * Double click on staff row, Goto "Admin/StaffAccount"
         */
        [Test]
        public void DoubleClick_OnRow_Show_StaffDetail()
        {
            IWebElement staffRow = Page.SearchAndGetStaffRow(TestSettings.Datas.Physician.FullName);
            staffRow.DoubleClick(Driver);
            staffRow.FindElement(By.XPath("./td[3]")).DoubleClick(Driver);    /*impt: click at staff name*/
            Assert.IsTrue(new StaffAccountPom(Driver).HasNavigatedAtUrl());
        }
    }
}
