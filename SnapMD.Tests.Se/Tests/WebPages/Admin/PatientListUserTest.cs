﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.TestSetting.Data.Model;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    [TestFixture]
    public class PatientListUserTest : ISessionPageTest<PatientListPom>, ISingleUserTest<PatientListPom>
    {
        public IWebDriver Driver { get; set; }

        public IUserHelper Helper { get; set; }

        public PatientListPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Page = new PatientListPom(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Dispose();
            Helper = null;
            Page = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            Page.NavigateOrRefreshUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new AdminLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(Page.HasNavigatedAtUrl());

            /*impt: login, loadTestPage again is important for other tests*/
        }

        [Test]
        public void Find_PatientRow_By_FullName()
        {
            PatientUser user = TestSettings.Datas.Patient;
            Page.SearchPatientRows(user.FirstName);
            Page.Wait(2); /*wait for data binding*/
            Assert.IsNotNull(Page.FindPatient(user.FullName));
            Assert.GreaterOrEqual(Page.PatientRows.Count, 1); /*at least  Datas.Physician should be at the grid*/
        }

        [Test]
        public void Search_And_Get_PatientRow_ByFullName()
        {
            string patientFullName;

            /*found patient*/
            patientFullName = TestSettings.Datas.Patient.FullName;
            Assert.IsNotNull(Page.SearchAndGetStaffRow(patientFullName));
            Assert.GreaterOrEqual(Page.PatientRows.Count, 1);

            /*not found patient*/
            patientFullName = Utility.RandomString(10, 20);
            Assert.Catch<Exception>(() => Page.SearchAndGetStaffRow(patientFullName));
        }


        [Test]
        public void Show_Patient_File_Method()
        {
            var patientFullName = TestSettings.Datas.Patient.FullName;
            IWebElement patientRow = Page.SearchAndGetStaffRow(patientFullName);
            Assert.DoesNotThrow(() => Page.ShowPatientFile(patientRow));
            Assert.IsTrue(new PatientProfileForAdminPom(Driver).HasNavigatedAtUrl());
        }

        [Test]
        public void Show_Appointment_Popup()
        {
            var patientFullName = TestSettings.Datas.Patient.FullName;
            IWebElement patientRow = Page.SearchAndGetStaffRow(patientFullName);
            Assert.DoesNotThrow(() => Page.ShowScheduleAppointment(patientRow));
            Assert.IsTrue(Page.AppointmentPopup.Displayed && Page.AppointmentPopup.Enabled);
        }

        [Test]
        public void Create_ScheduleApspointment_Success()
        {
            var patientFullName = TestSettings.Datas.Patient.FullName;
            IWebElement patientRow = Page.SearchAndGetStaffRow(patientFullName);
            Page.ShowScheduleAppointment(patientRow);

            //Enter Schedule information
            var tmpl = PageFactory.InitElements<ScheduleAppointmentTmpl>(Driver);
            new KendoDropdown(tmpl.DdlDoctors, Driver).SelectByIndex(1);            //doctor
            new KendoDropdown(tmpl.DdlPrimaryConcerns, Driver).SelectByIndex(1);    //primary prob
            new KendoDropdown(tmpl.DdlSecondaryConcerns, Driver).SelectByIndex(2);  //secondary prob, it shouldn't be as primary one
            tmpl.TxtScheduleDate.SendKeys(TestingHelpers.DateToString(DateTime.UtcNow.AddDays(2)));
            tmpl.TxtScheduleTime.SendKeys(TestingHelpers.TimeToString(DateTime.UtcNow));
            tmpl.BntCreateScheduleAppointment.Click();

            //Assert
            Assert.IsTrue(Page.SuccessNotificationContains("New Appointment Scheduled Successfully"));
        }

        /*
         * Double click on patient row, Goto "Admin/Patient"
         */
        [Test]
        public void DoubleClick_OnRow_Show_PatientDetail()
        {
            var patientFullName = TestSettings.Datas.Patient.FullName;
            IWebElement patientRow = Page.SearchAndGetStaffRow(patientFullName);
            patientRow.FindElement(By.XPath("./td[3]")).DoubleClick(Driver);    /*impt: patientRow.DoubleClick(Driver), is clicking at file link*/

            Assert.IsTrue(new PatientProfileForAdminPom(Driver).HasNavigatedAtUrl());
        }

    }
}
