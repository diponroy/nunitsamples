﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.WebPages.Admin
{
    /*
     * Impt: we cann't access the page by just url
     * by selecting a patient row from patient list we need to go to page, thats why PageInstance at LoadTestPage (startup) 
     */
    [TestFixture]
    public class PatientProfileForAdminUserTest : ISessionPageTest<PatientProfileForAdminPom>, ISingleUserTest<PatientProfileForAdminPom>
    {
        public IWebDriver Driver { get; set; }
        public IUserHelper Helper { get; set; }
        public PatientProfileForAdminPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
            Login();
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            Page = null;
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
        }

        public void Login()
        {
            Helper.Login(TestSettings.Datas.Admin);
        }

        public void LoadTestPage()
        {
            /*goto patient list page*/
            var patientsList = new PatientListPom(Driver);
            patientsList.NavigateToUrl();
            patientsList.WaitForAjax();
            patientsList.ShowPatientFile(patientsList.SearchAndGetStaffRow(TestSettings.Datas.Patient.FullName)); /*click a patient file*/

            /*now at the test page*/
            Page = new PatientProfileForAdminPom(Driver);
            Assert.IsTrue(Page.HasNavigatedAtUrl());
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        [Test]
        public void SessionTest()
        {
            /*trying to go to page, without login. Redirected to login page*/
            Helper.Logout();
            Assert.Catch<Exception>(LoadTestPage);  /*we will n't even able to goto list page, how to click patient file*/
            Page = new PatientProfileForAdminPom(Driver);
            Page.NavigateToUrl();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, new AdminLoginPom(Driver).Url));

            /*login and goto page*/
            Login();
            LoadTestPage();
            TestingHelpers.Wait(new TimeSpan(0, 0, 5));
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url));

            /*impt: login, loadTestPage again is important for other tests*/
        }

        /*TODO: 
         * need test to check date time, using admins timezone
         * note pop up test
         */
        /*
         * Creare new note
         * Edit existing note
         *      show in popup
         * Delete existing note
         */
        [Test]
        public void CreateEditDeleteNote()
        {
            string noteText = string.Empty;
            CreateNote(Driver, out noteText); //added new note
            EditNote(Driver, noteText, out noteText); //edit added note
            CancelDleteNote(Driver, noteText); //cancle to delete edited note
            DeleteNote(Driver, noteText); //delete edited note
        }

        private void CreateNote(IWebDriver driver, out string addedNote)
        {
            var page = new PatientProfileForAdminPom(driver);
            addedNote = Utility.RandomString(Utility.RandomNumber(10, 25));
            Assert.IsNull(page.FindNote(addedNote));        //new note not exists
            int oldNotesCount = page.NoteHolders.Count;

            page.SubmitNote(addedNote);
            page.WaitForAjax();
            Assert.IsTrue(page.SuccessNotificationContains("Note has been saved successfully"));
            Assert.AreEqual(++oldNotesCount, page.NoteHolders.Count);
            Assert.IsNotNull(page.FindNote(addedNote));
        }

        private void EditNote(IWebDriver driver, string oldNote, out string noteEditedAs)
        {
            var page = new PatientProfileForAdminPom(driver);
            int oldNotesCount = page.NoteHolders.Count;
            IWebElement noteHolder = page.FindNote(oldNote);
            Assert.IsNotNull(noteHolder);                       //old note exists

            noteEditedAs = Utility.RandomString(Utility.RandomNumber(12, 15));
            Assert.IsNull(page.FindNote(noteEditedAs));         //new note not exists

            page.EditNote(noteHolder, noteEditedAs);
            page.WaitForAjax();
            Assert.IsTrue(page.SuccessNotificationContains("Note has been updated successfully"));
            Assert.AreEqual(oldNotesCount, page.NoteHolders.Count);
            Assert.IsNotNull(page.FindNote(noteEditedAs));
            Assert.IsNull(page.FindNote(oldNote));
        }

        private void CancelDleteNote(IWebDriver driver, string note)
        {
            var page = new PatientProfileForAdminPom(driver);
            int oldNotesCount = page.NoteHolders.Count;
            IWebElement noteHolder = page.FindNote(note);
            Assert.IsNotNull(noteHolder);       //old note exists 

            page.DeleteNote(noteHolder);
            IWebElement confirmationNotification = null;
            Assert.IsTrue(page.ConfirmationNotificationContains(out confirmationNotification,
                "Do you really want to delete this note?"));
            page.Confirm(confirmationNotification, false);

            page.WaitForAjax();
            Assert.AreEqual(oldNotesCount, page.NoteHolders.Count);
            Assert.IsNotNull(page.FindNote(note));
        }

        private void DeleteNote(IWebDriver driver, string note)
        {
            var page = new PatientProfileForAdminPom(driver);
            int oldNotesCount = page.NoteHolders.Count;
            IWebElement noteHolder = page.FindNote(note);
            Assert.IsNotNull(noteHolder);           //old note exists

            page.DeleteNote(noteHolder);
            IWebElement confirmationNotification = null;
            Assert.IsTrue(page.ConfirmationNotificationContains(out confirmationNotification,
                "Do you really want to delete this note?"));
            page.Confirm(confirmationNotification);

            page.WaitForAjax();
            Assert.IsTrue(page.SuccessNotificationContains("Note has been deleted successfully"));
            Assert.AreEqual(--oldNotesCount, page.NoteHolders.Count);
            Assert.IsNull(page.FindNote(note));
        }
    }
}
