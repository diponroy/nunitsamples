﻿using OpenQA.Selenium;

namespace SnapMD.Tests.Se.Tests.WebPages
{
    public interface ISingleUserTest<TPage>
    {
        IWebDriver Driver { get; set; }

        TPage Page { get; set; }

        void FixtureSetUp();
        void Setup();
        void TearDown();
        void FixtureTearDown();
        void LoadTestPage();
        void TestErrorCleanup();

        void TitleTest();
    }
}
