﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;
using SnapMD.Tests.Se.WebPages.SnapMDAdmin;

namespace SnapMD.Tests.Se.Tests.WebPages.SnapMdAdmin
{
    [TestFixture]
    public class LoginPageTest : ISingleUserTest<SnapMdAdminLoginPom>
    {
        public IWebDriver Driver { get; set; }

        public SnapMdAdminLoginPom Page { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Page = new SnapMdAdminLoginPom(Driver);
        }

        [SetUp]
        public void Setup()
        {
            LoadTestPage();
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Page = null;
        }

        public void LoadTestPage()
        {
            Page.NavigateToUrl();
            Page.WaitForAjax();
        }

        public void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void TitleTest()
        {
            Assert.AreEqual(Page.ExpectedTitle(), Driver.Title.Trim());
        }

        /*
         * Story: Login
         * User wants to login from "/SnapMdAdmin/Login"
         *  enters email
         *  enters password
         *  submits credencial
         * 
         * Success: user type is SnapMdAdmin and credencials are valid
         *  redirect to "SnapMDAdmin/SnapAdmin.aspx#Client"
         * 
         * Fail: user type not SnapMdAdmin or credencials invalid
         *  show error message
         *  stay on "/SnapMdAdmin/Login"
         */

        [Test]
        public void Login_Success()
        {
            Page.SubminCredentials(TestSettings.Datas.SnapMdAdmin.Email, TestSettings.Datas.SnapMdAdmin.Password);
            Assert.IsTrue(new SnapMdAdminHomePom(Driver).HasNavigatedAtUrlWith("#Client"));
        }

        [Test]
        public void Login_Fail()
        {
            Page.SubminCredentials(TestSettings.Datas.SnapMdAdmin.Email, "invalid password");
            Assert.IsTrue(Page.ErrorNotificationContains("Email and Password combination failed."));
            Page.Wait(5);   /*wait to see if rederected*/
            Assert.IsTrue(TestingHelpers.NavigatedUrlStartsWith(Driver, Page.Url)); /*still at login page*/
        }

        /*Bug Todo: snapMdAdmin forget password link should not goto "Customer/ForgetPassword"*/
        [Test]
        public void Navigation_ForgetPassword()
        {
            Page.LinkForgotPassword.Click();
            Assert.IsTrue(new PatientForgotPasswordPom(Driver).HasNavigatedAtUrl());
        }

    }
}
