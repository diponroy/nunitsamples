﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.Tests.WebPages
{
    public interface ISessionPageTest<TSessionPage>
    {
        IWebDriver Driver { get; set; }
        IUserHelper Helper { get; set; }
        TSessionPage Page { get; set; }

        void FixtureSetUp();
        void Setup();
        void TearDown();
        void FixtureTearDown();

        void Login();
        void LoadTestPage();
        void TestErrorCleanup();

        void SessionTest();
    }
}
