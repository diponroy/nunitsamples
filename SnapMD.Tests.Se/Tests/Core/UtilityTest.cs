﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NUnit.Framework;
using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.Tests.Core
{
    [TestFixture]
    public class UtilityTest
    {
        [Test]
        [TestCase(" Dipon ", "Roy", "Dipon Roy")] //firstName trimmed
        [TestCase("Dipon", " Roy ", "Dipon Roy")] //lastName trimmed
        [TestCase("", "Roy", "Roy")] //empty firstName  
        [TestCase("Dipon", "", "Dipon")] //empty lastName
        [TestCase(null, "Roy", "Roy")] //null firstName  
        [TestCase("Dipon", null, "Dipon")] //null lastName
        [TestCase("Dipon", "Roy", "Dipon Roy")]
        [TestCase("", "", "")]
        [TestCase(null, null, "")]
        public void FullName(string firstName, string lastName, string expectedFullName)
        {
            Assert.AreEqual(expectedFullName, Utility.FullName(firstName, lastName));
        }

        [Test]
        [TestCase("emailUser@gmail.com", "emailUser")]
        [TestCase("emailUser+12@gmail.com", "emailUser")]
        [TestCase("emailUser.local@gmail.com", "emailUser")]
        public void AlliancedEmail(string email, string expectedEmailUser)
        {
            string allianceEmail = Utility.AlliancedEmail(email);
            var oldEmailDetail = new MailAddress(email);
            var newEmailDetail = new MailAddress(allianceEmail);

            Assert.IsNotNullOrEmpty(allianceEmail);
            Assert.IsTrue(newEmailDetail.User.StartsWith(expectedEmailUser));
            Assert.AreNotEqual(oldEmailDetail.User, newEmailDetail.User);
            Assert.AreEqual(oldEmailDetail.Host, newEmailDetail.Host);
            Assert.IsNotNull(newEmailDetail.User.ToCharArray().Single(x => x == '+'));
            Assert.IsNotNull(allianceEmail.ToCharArray().Single(x => x == '@'));
        }

        [Test]
        public void AlliancedEmail_Always_Unique()
        {
            string email = "emailUser@gmail.com";
            var emails = new List<string>();
            emails.Add(email);

            for (int i = 0; i < 10000; i++)
            {
                string allianceEmail = Utility.AlliancedEmail(email);
                if (emails.Contains(allianceEmail))
                {
                    Assert.Fail("alliance email reproduced.");
                }
                emails.Add(allianceEmail);
            }
        }

        [Test]
        [TestCase(null, "")]
        [TestCase("", "")]
        [TestCase("    ", "")]
        [TestCase("+123", "123")]
        [TestCase("+1a2?3_", "123")]
        public void DigitString(string inputString, string expectedDigitString)
        {
            Assert.AreEqual(expectedDigitString, Utility.DigitString(inputString));
        }

        [Test]
        [TestCase(10, 100)]
        [TestCase(-100, -10)]
        [TestCase(-10, 10)]
        public void RandomNumber(int min, int max)
        {
            int number = Utility.RandomNumber(min, max);
            Assert.IsTrue(min <= number && number <= max );
        }

        [Test]
        public void RandomDateTime()
        {
            DateTime? random = null;
            DateTime min = DateTime.Now;
            DateTime max = DateTime.Now.AddDays(10);

            /*only min*/
            random = Utility.RandomDateTime(min, max);
            Assert.LessOrEqual(min, random);

            /*min and max provided*/
            random = Utility.RandomDateTime(min, max);
            Assert.LessOrEqual(min, random);
            Assert.GreaterOrEqual(max, random);
        }
    }
}
