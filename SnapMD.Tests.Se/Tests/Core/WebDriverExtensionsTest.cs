﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.Tests.Core
{
    [TestFixture]
    public class WebDriverExtensionsTest
    {
        public IWebDriver Driver { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void AsJsExecutor()
        {
            var error = Assert.Catch<NullReferenceException>(() => WebDriverExtensions.AsJsExecutor(null));
            Assert.AreEqual("webDriver is null, cann't convert as IJavaScriptExecutor.", error.Message);
            Assert.IsInstanceOf<IJavaScriptExecutor>(Driver.AsJsExecutor());
        }

        [Test]
        public void Destroy_And_IsDestroyed()
        {
            Driver.Navigate().GoToUrl("https://www.google.com");
            Assert.IsFalse(Driver.IsDestoryed());

            Driver.Destroy();
            Assert.IsNotNull(Driver);
            Assert.IsTrue(Driver.IsDestoryed());

            Driver = null;
            var error = Assert.Catch<Exception>(() => Driver.IsDestoryed());
            Assert.AreEqual("web driver is null, unable to find if destoryed or not.", error.Message);
        }
    }
}
