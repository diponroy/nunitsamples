﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.Core
{
    [TestFixture]
    public class WebElementExtensionsTest
    {
        public IWebDriver Driver { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void Value()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //get
            page.Email.SendKeys("dipon");
            string email = page.Email.Value();
            Assert.AreEqual("dipon", email, "get value from argument[0] not working.");

            //set
            page.Password.Value("123");
            Assert.AreEqual("123", page.Password.Value(), "set value from argument[0] not working.");
        }

        [Test]
        public void Html()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //get
            Assert.AreEqual("Login", page.BtnLogin.Html(), "get html from argument[0] not working.");
        }
    }
}
