﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.Tests.Helpers
{
    [TestFixture]
    public class PatientHelperTest
    {
        public IWebDriver Driver { get; set; }

        public PatientHelper Helper { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PatientHelper(Driver);
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        /*
         * login as patient
         */
        [Test]
        public void Login_By_Email_Password()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => Helper.Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password), "Patient Helper's login with email && password not working");
            Assert.IsTrue(new PatientHomePom(Driver).HasNavigatedAtUrl());
        }

        /*
         * login as patient
         */
        [Test]
        public void Login_By_Patient_User()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => Helper.Login(TestSettings.Datas.Patient), "Patient Helper's login with user not working");
            Assert.IsTrue(new PatientHomePom(Driver).HasNavigatedAtUrl());
        }

        /*
         * logout as patient
         */
        [Test]
        public void Logout()
        {
            Helper.Login(TestSettings.Datas.Patient);
            Assert.DoesNotThrow(Helper.Logout, "Patient Helper's log out working");
            Assert.IsTrue(new PatientLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));
        }

        /*
         * logout as patient
         */
        [Test]
        public void TryToLogout()
        {
            /*logout*/
            Helper.Login(TestSettings.Datas.Patient);
            Assert.DoesNotThrow(Helper.TryToLogout, "Patient Helper's log out working");
            Assert.IsTrue(new PatientLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));

            /*current page doesn't have logout menu*/
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(Helper.TryToLogout, "Patient Helper's log out working");
            Assert.IsTrue(new PatientLoginPom(Driver).HasNavigatedAtUrl());
        }
    }
}
