﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Tests.WebPages;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.Helpers
{
    [TestFixture]
    public class AdminHelperTest
    {
        public IWebDriver Driver { get; set; }

        public AdminHelper Helper { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new AdminHelper(Driver);
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        /*
         * login as admin
         */
        [Test]
        public void Login_By_Email_Password()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => Helper.Login(TestSettings.Datas.Admin.Email, TestSettings.Datas.Admin.Password), "Admin Helper's login by email & password not working");
            Assert.IsTrue(new DashBoardPom(Driver).HasNavigatedAtUrl());
        }

        /*
         * login as admin
         */
        [Test]
        public void Login_By_Admin_User()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => Helper.Login(TestSettings.Datas.Admin), "Admin Helper's login by user not working");
            Assert.IsTrue(new DashBoardPom(Driver).HasNavigatedAtUrl());
        }

        /*
         * logout as admin
         */
        [Test]
        public void Logout()
        {
            Helper.Login(TestSettings.Datas.Admin);
            Assert.DoesNotThrow(Helper.Logout, "Admin Helper's log out working.");
            Assert.IsTrue(new AdminLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));
        }

        /*
         * try to logout as admin
         */
        [Test]
        public void TryToLogout()
        {
            /*logout*/
            Helper.Login(TestSettings.Datas.Admin);
            Assert.DoesNotThrow(Helper.TryToLogout, "Admin Helper's try log out working.");
            Assert.IsTrue(new AdminLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));

            /*current page doesn't have logout menu*/
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(Helper.TryToLogout, "Admin Helper's try log out working.");
            Assert.IsTrue(new AdminLoginPom(Driver).HasNavigatedAtUrl());
        }
    }
}
