﻿using System;
using System.Globalization;
using NUnit.Framework;
using SnapMD.Tests.Se.Helpers;

namespace SnapMD.Tests.Se.Tests.Helpers
{
    [TestFixture]
    public class TestingHelpersTest
    {
        [Test]
        public void DateToString()
        {
            /*formate MM/dd/yyyy*/
            DateTime date = DateTime.ParseExact("09.01.2015", "MM.dd.yyyy", CultureInfo.InvariantCulture);
            Assert.AreEqual("09/01/2015", TestingHelpers.DateToString(date));  
        }

        [Test]
        public void TimeToString()
        {            
            /*Formate hh:mm tt*/
            /*hour, minute to formate*/
            Assert.AreEqual("02:00 AM", TestingHelpers.TimeToString(2));
            Assert.AreEqual("02:10 AM", TestingHelpers.TimeToString(2, 10));
            Assert.AreEqual("01:00 PM", TestingHelpers.TimeToString(13));

            /*datetime to formate*/
            DateTime date = DateTime.ParseExact("09.01.2015 13:10:30", "MM.dd.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            Assert.AreEqual("01:10 PM", TestingHelpers.TimeToString(date));
        }
    }
}
