﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;
using SnapMD.Tests.Se.WebPages.Physician;

namespace SnapMD.Tests.Se.Tests.Helpers
{
    [TestFixture]
    public class PhysicianHelperTest
    {
        public IWebDriver Driver { get; set; }

        public PhysicianHelper Helper { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
            Helper = new PhysicianHelper(Driver);
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
            Helper = null;
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }


        /*
         * login as physician
         */
        [Test]
        public void Login_By_Email_Password()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => new PhysicianHelper(Driver).Login(TestSettings.Datas.Physician.Email, TestSettings.Datas.Physician.Password), "Physician Helper's login by email & password not working");
            Assert.IsTrue(new PhysicianWaitingListPom(Driver).HasNavigatedAtUrl());
        }

        /*
         * login as physician
         */
        [Test]
        public void Login_By_Physician_User()
        {
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(() => new PhysicianHelper(Driver).Login(TestSettings.Datas.Physician), "Physician Helper's login by user not working");
            Assert.IsTrue(new PhysicianWaitingListPom(Driver).HasNavigatedAtUrl());
        }

        /*
         * logout as physician
         */

        [Test]
        public void Logout()
        {
            Helper.Login(TestSettings.Datas.Physician);
            Assert.DoesNotThrow(Helper.Logout, "Physician Helper's log out working");
            Assert.IsTrue(new PhysicianLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));
        }

        /*
         * try to logout as physician
         */
        [Test]
        public void TryToLogout()
        {
            /*logout*/
            Helper.Login(TestSettings.Datas.Physician);
            Assert.DoesNotThrow(Helper.TryToLogout, "Physician Helper's log out working");
            Assert.IsTrue(new PhysicianLoginPom(Driver).HasNavigatedAtUrlWith("?lo=true"));

            /*current page doesn't have logout menu*/
            Driver.Navigate().GoToUrl("http://snap.md/");
            Assert.DoesNotThrow(Helper.TryToLogout, "Physician Helper's log out working");
            Assert.IsTrue(new PhysicianLoginPom(Driver).HasNavigatedAtUrl());
        }
    }
}
