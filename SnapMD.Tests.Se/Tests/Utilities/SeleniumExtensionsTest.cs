﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.JsHelpers.Model;
using SnapMD.Tests.Se.Utilities;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.Tests.Utilities
{
    [TestFixture]
    public class SeleniumExtensionsTest
    {
        public IWebDriver Driver { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Driver = DriverSetup.Driver;
        }

        [TearDown]
        public void TearDown()
        {
            TestErrorCleanup();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Destroy();
        }

        private void TestErrorCleanup()
        {
            if (NunitUtility.LastTestFailed())
            {
                FixtureTearDown();
                FixtureSetUp();
            }
        }

        [Test]
        public void ExecuteScriptForData()
        {
            /*return word*/
            var error = Assert.Catch<Exception>(() => Driver.AsJsExecutor().ExecuteScriptForData<String>("alert('value');"));
            Assert.AreEqual("script should have the word 'return'.", error.Message);
            Assert.DoesNotThrow(() => Driver.AsJsExecutor().ExecuteScriptForData<String>("   return 'value';"));

            /*was not working*/
            Assert.AreEqual("value", Driver.AsJsExecutor().ExecuteScriptForData<String>("return 'value';"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<String>("return '';"));
            Assert.AreEqual("null", Driver.AsJsExecutor().ExecuteScriptForData<String>("return 'null';"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<String>("return null;"));

            Assert.AreEqual("value", Driver.AsJsExecutor().ExecuteScriptForData<string>("return 'value';"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<string>("return '';"));
            Assert.AreEqual("null", Driver.AsJsExecutor().ExecuteScriptForData<string>("return 'null';"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<string>("return null;"));

            /*bool*/
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 1=='1';"));
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 1;"));
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return '1';"));
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return true;"));
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 'true';"));

            Assert.IsFalse(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 1==='1';"));
            Assert.IsFalse(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 0;"));
            Assert.IsFalse(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return '0';"));
            Assert.IsFalse(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return false;"));
            Assert.IsFalse(Driver.AsJsExecutor().ExecuteScriptForData<bool>("return 'false';"));

            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<bool?>("return null;"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<bool?>("return 'null';"));

            /*string and list*/
            Assert.AreEqual("value", Driver.AsJsExecutor().ExecuteScriptForData<string>("return 'value';"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<string>("return '';"));
            Assert.AreEqual("null", Driver.AsJsExecutor().ExecuteScriptForData<string>("return 'null';"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<string>("return null;"));

            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<List<string>>("return ['value1', 'value2'];").Count == 2);
            Assert.IsTrue(
                Driver.AsJsExecutor()
                    .ExecuteScriptForData<List<string>>("return JSON.stringify(['value1', 'value2']);")
                    .Count == 2);
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<List<string>>("return [];"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<List<string>>("return JSON.stringify([]);"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<string>>("return null;"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<string>>("return 'null'"));

            /*int and list*/
            Assert.AreEqual(2, Driver.AsJsExecutor().ExecuteScriptForData<int>("return 2;"));
            Assert.AreEqual(2, Driver.AsJsExecutor().ExecuteScriptForData<int>("return '2';"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<int?>("return null;"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<int?>("return 'null'"));

            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return [1, 2];").Count == 2);
            Assert.IsTrue(
                Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return JSON.stringify([1, 2]);").Count == 2);
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<List<int?>>("return [1, null];").Count == 2);
            Assert.IsTrue(
                Driver.AsJsExecutor().ExecuteScriptForData<List<int?>>("return JSON.stringify([1, null]);").Count == 2);

            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return [];"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return JSON.stringify([]);"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return null;"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<int>>("return 'null'"));

            /*object list*/
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<List<DropdownOption>>(@"
                            return (function () {
                                return [
                                    {
                                        text: 'Option1',
                                        value: 'Value1'
                                    },
                                    {
                                        text: 'Option2',
                                        value: 'Value2'
                                    }
                                ];
                            })();
                        ").Count() == 2);
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<ReadOnlyCollection<DropdownOption>>(@"
                            return (function () {
                                return JSON.stringify([
                                    {
                                        text: 'Option1',
                                        value: 'Value1'
                                    },
                                    {
                                        text: 'Option2',
                                        value: 'Value2'
                                    }          
                                ]);
                            })();
                        ").Count == 2);
            Assert.IsTrue(Driver.AsJsExecutor().ExecuteScriptForData<IReadOnlyCollection<dynamic>>(@"
                            return (function () {
                                return [
                                    {
                                        text: 'Option1',
                                        value: 'Value1'
                                    },
                                    {
                                        text: 'Option2',
                                        value: 'Value2'
                                    }
                                ];
                            })();
                        ").Count() == 2);
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<IEnumerable<DropdownOption>>("return [];"));
            Assert.IsEmpty(Driver.AsJsExecutor().ExecuteScriptForData<IList<DropdownOption>>("return JSON.stringify([]);"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<DropdownOption>>("return null;"));
            Assert.IsNull(Driver.AsJsExecutor().ExecuteScriptForData<List<DropdownOption>>("return 'null'"));


            /*arguments*/
            Assert.AreEqual("test", Driver.AsJsExecutor().ExecuteScriptForData<string>("return arguments[0];", "test"));
            Assert.AreEqual(2, Driver.AsJsExecutor().ExecuteScriptForData<int>(@"
                                                            function test_selenium(value) {
                                                                return value + 1;
                                                            };
                                                            return test_selenium(arguments[0]);", 1));
            Assert.AreEqual(10, Driver.AsJsExecutor().ExecuteScriptForData<int>(@"
                                                            function test_selenium(number1, number2) {
                                                                return number1 + number2;
                                                            };
                                                            return test_selenium(arguments[0], arguments[1]);", 6, 4));


        }

        [Test]
        public void Value()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //get
            page.Email.SendKeys("dipon");
            string email = page.Email.Value();
            Assert.AreEqual("dipon", email, "get value from argument[0] not working.");

            //set
            page.Password.Value("123", Driver);
            Assert.AreEqual("123", page.Password.GetAttribute("value"), "set value from argument[0] not working.");
        }

        [Test]
        public void Attribute()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //Set
            page.Email.SetAttribute("data-name", "dipon", Driver);
            string email = page.Email.GetAttribute("data-name");
            Assert.AreEqual("dipon", email, "get value from argument[0] not working.");
        }

        [Test]
        public void Html()
        {
            var page = new AdminLoginPom(Driver);
            page.NavigateToUrl();

            //set
            page.BtnLogin.Html("dipon", Driver);
            Assert.AreEqual("dipon", page.BtnLogin.GetAttribute("innerHTML"), "set html from argument[0] not working.");
        }
    }
}
