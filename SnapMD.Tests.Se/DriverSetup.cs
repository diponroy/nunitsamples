﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se
{
    /*TODO: need to make sure one driver instance in at used, once at a time
     */
    [SetUpFixture]
    public class DriverSetup
    {
        private static IWebDriver _driver;

        public static IWebDriver Driver
        {
            get
            {
                if (_driver != null && !_driver.IsDestoryed())
                {
                    throw new Exception("User the old driver.");
                }
                _driver = ChromeDriver();
                //_driver = PhantomJSDriver();
                return _driver;
            }
        }

        private static IWebDriver ChromeDriver()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized"); /*maximized window*/
            var driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitlyWait(TestSettings.Timeouts.Implicit); /*strange, if not used this one, test don't even get started*/
            return driver;
        }

        /*Photon Driver: http://www.andykelk.net/tech/headless-browser-testing-with-phantomjs-selenium-webdriver-c-nunit-and-mono*/
        private static IWebDriver PhantomJSDriver()
        {
            var driver = new PhantomJSDriver();
            driver.Manage().Timeouts().ImplicitlyWait(TestSettings.Timeouts.Implicit); /*strange, if not used this one, test don't even get started*/
            return driver;
        }


        [TearDown]
        public void TearDown()
        {
            if (_driver != null && !_driver.IsDestoryed())
            {
                _driver.Destroy();        
            }
        }
    }
}