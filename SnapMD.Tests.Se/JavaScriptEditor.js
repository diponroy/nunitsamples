﻿/*Tmpl: when need return type execution*/
function selenium() {
    return (function() {
        //write your code here to run on selenium js executor
        //and copy the "return" section with "return" it self
        return 'result';
    })();
}

/*Example*/
//return (function () {
//    return [1, 2];
//})();

/*If need to use selenium arguments*/
//function test_selenium(value) {
//    return value + 1;
//};
//return test_selenium(arguments[0]);


/*Tmpl: when need void type execution*/
(function() {
    //write your code here
})();

/*If need to use selenium arguments*/
//function test_selenium(value) {
//    var totla = value + 1;
//};
//test_selenium(arguments[0]);


/*workig with string type*/
//try to use ' instated ", as c# is using "
var name = 'Han';


alert(JSON.stringify(returnAtConsole()));