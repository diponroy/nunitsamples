﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.CouldWork.QA.Patient
{
    [TestFixture, Explicit]
    class DownloadVidyoPlugin
    {
        //ChromeDriver webDriver = new ChromeDriver();
        [Test]
        public void downloadVidyo()
        {
            using (ChromeDriver webDriver = new ChromeDriver())
            {
                //Login
                new PatientHelper(webDriver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

                //Click on Main Manu 
                PatientHomePom patientHome = new PatientHomePom(webDriver);
                patientHome.openMainMenu();
                patientHome.clickOnDownloadVidyoPlugin();
                patientHome.checkIfYouAreOnHomePage();

            }
        }

    }
}
