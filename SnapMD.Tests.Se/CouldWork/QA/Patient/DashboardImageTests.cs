﻿using System.Collections.ObjectModel;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se.CouldWork.QA.Patient
{
    [TestFixture, Explicit]
    public class DashboardImageTest
    {
        #region Public Methods and Operators

        [Test]
        public void TestImages()
        {
            IWebDriver webDriver = DriverSetup.Driver;
            LoginAndLoadDashboard(webDriver);
            TestImages(webDriver);
        }

        #endregion

        #region Methods

        private static void LoginAndLoadDashboard(IWebDriver webDriver)
        {
            //login
            IWebDriver driver = webDriver;
            var helper = new PatientHelper(driver);
            helper.Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

            // Check for error messages
            ReadOnlyCollection<IWebElement> warnings = webDriver.FindElements(By.ClassName("warning"));
            Assert.IsEmpty(warnings);

            string title = webDriver.Title;
            Assert.IsFalse(title.Contains("Login"));

            webDriver.Url = Settings.Default.DomainToTest + "/Customer/Home";
            webDriver.Navigate();

            Assert.IsFalse(webDriver.Url.Contains("Vidyo"));
        }

        private void TestImages(IWebDriver webDriver)
        {
            ReadOnlyCollection<IWebElement> allImages = webDriver.FindElements(By.TagName("img"));
            Assert.IsNotEmpty(allImages);

            foreach (IWebElement image in allImages)
            {
                var loaded =
                    (bool)((IJavaScriptExecutor)webDriver).ExecuteScript(
                        "return (typeof arguments[0].naturalWidth!=\"undefined\"" +
                        " && arguments[0].naturalWidth > 0)",
                        image);
                if (!loaded)
                {
                    Assert.Fail("Broken images on customer dashboard: " + image.GetAttribute("src"));
                }
            }

            Thread.Sleep(4000);
        }

        #endregion
    }
}