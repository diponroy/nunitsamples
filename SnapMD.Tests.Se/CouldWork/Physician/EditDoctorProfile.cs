﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.WebPages.Physician;

namespace SnapMD.Tests.Se.CouldWork.Physician
{
    [TestFixture, Explicit]
    public class EditDoctorProfile
    {
        [Test]
        public void TestEditDoctorProfile()
        {
            IWebDriver driver = DriverSetup.Driver;

            //login
            var physicianLoginPom = new PhysicianLoginPom(driver);
            physicianLoginPom.SubminCredentials(Settings.Default.AdminUser, "Password@123");

            //open my account
            var doctorHomePage = new PhysicianWaitingListPom(driver);
            doctorHomePage.HasNavigatedAtUrl();
            doctorHomePage.selectMyAccount();

            //edit doctor informations
            var editPhysicianPage = new PhysicianAccountSettingPom(driver);
            Dictionary<string, string> newValues = editPhysicianPage.editPhysicianInformations();

            //click on save
            editPhysicianPage.clickSave();

            //wait 2 seconds for popup to open and close it
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until(d => d.FindElement(By.ClassName("snapSuccess")).Displayed);

            //open my account again
            doctorHomePage.selectMyAccount();

            //check if profile is realy updated with new values
            editPhysicianPage.checkIsUpdated(newValues);
        }
    }
}
