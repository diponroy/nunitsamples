﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.WebPages.Physician;

namespace SnapMD.Tests.Se.CouldWork.Physician
{
    [TestFixture, Explicit]
    public class EditDoctorProfileQa
    {
        TestingHelpers helper = new TestingHelpers();

        [Test]
        public void TestEditDoctorProfile ()
        {
            //login
            var webDriver = DriverSetup.Driver;
            helper.doLogin(webDriver, "/Physician/Login", Settings.Default.AdminUser, "Password@123");

            //open my account
            var doctorHomePage = new PhysicianWaitingListPom(webDriver);
            doctorHomePage.selectMyAccount();

            //edit doctor informations
            PhysicianAccountSettingPom physicianPage = new PhysicianAccountSettingPom(webDriver);
            var newValues = physicianPage.editPhysicianInformations();

            //click on save
            physicianPage.clickSave();

            //check for success notification
            var message = webDriver.FindElement(By.ClassName("snapSuccess"));

            //open my account again
            doctorHomePage.selectMyAccount();

            //check if profile is realy updated with new values
            physicianPage.checkIsUpdated(newValues); 
        }
    }
}
