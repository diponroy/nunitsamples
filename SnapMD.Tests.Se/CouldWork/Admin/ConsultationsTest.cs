﻿using NUnit.Framework;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Properties;

namespace SnapMD.Tests.Se.CouldWork.Admin
{
    [TestFixture, Explicit]
    class ConsultationsTests
    {
        TestingHelpers helper = new TestingHelpers();

        [Test]
        public void UpdateScheduledConsultationTest()
        {
            var webDriver = Setup.Driver;
            helper.doLogin(webDriver, "/Admin/Login", Settings.Default.AdminUser, "Password@123");

            string patient = "Test Acc";
            string doctor = "Steve Garcia";

            //Enter Patients in menu
            DashBoardPage dashboard = new DashBoardPage(webDriver);
            dashboard.enterPatients();

            //Enter specific patient
            PatientsListPage patientlist = new PatientsListPage(webDriver);

            //patientlist.enterPatientFile("Test Acc");
            patientlist.scheduleConsultationFor(patient);

            //Enter Schedule information
            SchedulePage schedule = new SchedulePage(webDriver);
            schedule.selectDoctor(doctor);

            //select random date
            string date = schedule.selectDatePatient(6);

            //select random time
            string time = schedule.selectTime();

            //Concerns
            schedule.patientPrimaryConcern("Vomiting");
            schedule.patientSecondaryConcern("Cough");

            //Schedule
            schedule.clickSchedulePatient();

            //Assert
            schedule.check();

            TopMenu topMenu = new TopMenu(webDriver);

            //click Consultations in top menu
            topMenu.clickConsultations();

            //check if created consultation is there and if it is, click View Details
            ConsultationsPage consultationsPage = new ConsultationsPage(webDriver);
            Assert.IsTrue(consultationsPage.checkScheduledConsultatoin(date, time, patient, doctor));
        }
    }
}
