﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.WebPages.Admin;

namespace SnapMD.Tests.Se.CouldWork.Admin
{
    [TestFixture, Explicit]
    public class OnDemandAvailabilitySlotPageTest
    {
        [Test]
        public void OnDemandAvailabilityTest()
        {
            //Login as Admin
            IWebDriver driver = DriverSetup.Driver;
            new AdminHelper(driver).Login(TestSettings.Datas.Admin.Email, TestSettings.Datas.Admin.Password);

            //Schedule consultation for doctor
            var onDemandScheduleCalenderPage = new OnDemandAvailabilitySlotPom(driver);
            onDemandScheduleCalenderPage.NavigateToUrl();
            onDemandScheduleCalenderPage.OpenScheduleConsultation(DateTime.UtcNow);

            //Select Doctor and Time
            //onDemandScheduleCalenderPage.ScheduleAvailability("Charles", DateTime.Now.AddDays(30));
        }
    }
}
