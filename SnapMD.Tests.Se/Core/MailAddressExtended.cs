﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace SnapMD.Tests.Se.Core
{
    public class MailAddressExtended : MailAddress
    {
        public readonly bool IsAlliancedEmail;

        public static IReadOnlyCollection<char> AllianceChars { get; private set; }

        static MailAddressExtended()
        {
            AllianceChars = new[] { '+', '.' };
        }

        public MailAddressExtended(string address) : base(address)
        {
            IsAlliancedEmail = User.ToCharArray().Any(x => AllianceChars.Contains(x));
        }

        public string UserWithOutAlliance()
        {
            if (!IsAlliancedEmail)
            {
                return User;
            }

            var allianceItems =
                User.ToCharArray()
                    .Select((x, i) => new { Item = x, Index = i })
                    .Where(l => AllianceChars.Contains(l.Item))
                    .ToList();
            if (allianceItems.Count > 1)
            {
                throw new AmbiguousMatchException("Found more than one alliance char at User section of the email.");
            }

            return User.Substring(0, allianceItems.First().Index);
        }

        /*short guid 
         * http://stackoverflow.com/questions/2259692/show-a-guid-in-36-letters-format
         * http://www.singular.co.nz/2007/12/shortguid-a-shorter-and-url-friendly-guid-in-c-sharp/ 
         */
        public string AlliancedEmail(char allianceChar)
        {
            if (allianceChar == Char.MinValue)
            {
                throw new NullReferenceException("need allianceChar to create alliance email");
            }

            string user = UserWithOutAlliance();
            string host = Host;

            /*url token working like this*/
            var sha = SHA256.Create();
            var hash = sha.ComputeHash(Guid.NewGuid().ToByteArray());
            var builder = new StringBuilder(Convert.ToBase64String(hash));
            builder.Replace("/", "");
            builder.Replace("+", "");
            builder.Replace("=", "");
            string allianceString = builder.ToString(0, 25);

            string alliancedEmail = String.Format("{0}{1}{2}@{3}", user, allianceChar, allianceString, host);
            return alliancedEmail;
        }
    }
}
