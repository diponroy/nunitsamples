﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using Cookie = OpenQA.Selenium.Cookie;

namespace SnapMD.Tests.Se.Core
{
    public static class WebDriverExtensions
    {
        public static IJavaScriptExecutor AsJsExecutor(this IWebDriver webDriver)
        {
            if (webDriver == null)
            {
                throw new NullReferenceException("webDriver is null, cann't convert as IJavaScriptExecutor.");
            }
            return (IJavaScriptExecutor)webDriver;
        }

        public static byte[] Download(this IWebDriver webDriver, string downloadUrl)
        {
            byte[] downloadedData;
            ReadOnlyCollection<Cookie> cookies = webDriver.Manage().Cookies.AllCookies;
            using (var wc = new WebClient())
            {
                foreach (Cookie cookie in cookies)
                {
                    string cookieText = cookie.Name + "=" + cookie.Value;
                    wc.Headers.Add(HttpRequestHeader.Cookie, cookieText);
                }
                downloadedData = wc.DownloadData(new Uri(downloadUrl));
            }

            return downloadedData;
        }

        public static bool TryToDownload(this IWebDriver webDriver, string downloadUrl, out byte[] downloadedData)
        {
            bool isDownloaded = false;
            try
            {
                downloadedData = Download(webDriver, downloadUrl);
                isDownloaded = true;
            }
            catch
            {
                downloadedData = null;
                isDownloaded = false;
            }

            return isDownloaded;
        }

        //private static void ColseTabs(IWebDriver webDriver)
        //{
        //    if (webDriver != null)
        //    {
        //        ReadOnlyCollection<string> tabs = webDriver.WindowHandles;
        //        if (tabs.Count > 0)
        //        {
        //            for (int tabIndex = tabs.Count - 1; tabIndex >= 0; tabIndex--)
        //            {
        //                IWebDriver tab = webDriver.SwitchTo().Window(tabs[tabIndex]);
        //                tab.Close();
        //            }
        //        }
        //    }
        //}

        //public static void ColseAllTab(this IWebDriver webDriver, string expectedWindowHandle = "")
        //{
        //    if (String.IsNullOrEmpty(expectedWindowHandle))
        //    {
        //        ColseTabs(webDriver);
        //        return;
        //    }

        //    if (webDriver != null)
        //    {
        //        ReadOnlyCollection<string> tabs = webDriver.WindowHandles;
        //        if (!tabs.Any(x => x.Equals(expectedWindowHandle)))
        //        {
        //            throw new AmbiguousMatchException("expected window handle not found.");
        //        }

        //        for (int tabIndex = 0; tabIndex < tabs.Count; tabIndex++)
        //        {
        //            var windowHandle = tabs[tabIndex];
        //            if (!windowHandle.Equals(expectedWindowHandle))
        //            {
        //                IWebDriver tab = webDriver.SwitchTo().Window(windowHandle);
        //                tab.Close();                    
        //            }
        //        }

        //        webDriver.SwitchTo().Window(expectedWindowHandle);
        //    }
        //}

        public static void ColseAllTab(this IWebDriver webDriver, string expectedWindowHandle = "")
        {
            if (webDriver == null)
            {
                return;
            }

            ReadOnlyCollection<string> tabs = webDriver.WindowHandles;
            if (!String.IsNullOrEmpty(expectedWindowHandle) && !tabs.Any(x => x.Equals(expectedWindowHandle)))
            {
                throw new AmbiguousMatchException("expected window handle or tab not found.");
            }

            for (int tabIndex = 0; tabIndex < tabs.Count; tabIndex++)
            {
                var windowHandle = tabs[tabIndex];
                if (windowHandle.Equals(expectedWindowHandle))
                {
                    continue;
                }
                IWebDriver tab = webDriver.SwitchTo().Window(windowHandle);
                tab.Close();
            }

            if (!String.IsNullOrEmpty(expectedWindowHandle))
            {
                webDriver.SwitchTo().Window(expectedWindowHandle);
            }
        }

        /* 
         * http://www.dahuatu.com/oKWkoZdW35.html 
         * using reflection to get protected SessionId
         */
        public static bool IsDestoryed(this IWebDriver webDriver)
        {
            if (webDriver == null)
            {
                throw new NullReferenceException("web driver is null, unable to find if destoryed or not.");
            }

            const string propName = "SessionId";
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty;
            var sessionIdProperty = typeof(RemoteWebDriver).GetProperty(propName, flags); /*impt flags*/

            SessionId sessionId = null;
            if (sessionIdProperty != null)
            {
                sessionId = sessionIdProperty.GetValue(webDriver, null) as SessionId;
            }

            return sessionId == null;
        }

        public static void Destroy<TWebDriver>(this TWebDriver webDriver)
            where TWebDriver : class, IWebDriver
        {
            if (webDriver != null)
            {
                webDriver.ColseAllTab();
                webDriver.Quit();
                webDriver.Dispose();
            }
        }

        public static bool WaitUntil(this IWebDriver webDriver,
    Func<IWebDriver, bool> prediction,
    TimeSpan waitingTimeSpan)
        {
            bool wasAsPredicted;
            try
            {
                new WebDriverWait(webDriver, waitingTimeSpan).Until(prediction);
                wasAsPredicted = true;
            }
            catch
            {
                wasAsPredicted = false;
            }
            return wasAsPredicted;
        }
    }
}
