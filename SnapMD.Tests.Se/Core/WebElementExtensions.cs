﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SnapMD.Tests.Se.Core
{
    public static class WebElementExtensions
    {
        public static void Value(this IWebElement webElement, string value)
        {
            webElement.Clear();
            webElement.SendKeys(value);
        }

        public static string Value(this IWebElement webElement)
        {
            return webElement.GetAttribute("value");
        }

        public static string Html(this IWebElement webElement)
        {
            return webElement.GetAttribute("innerHTML");
        }

        public static bool HasCssClass(this IWebElement webElement, string cssClassName)
        {
            string cssClassListString = webElement.GetAttribute("class");
            if (String.IsNullOrEmpty(cssClassListString))
            {
                return false;
            }

            List<string> cssClassList = cssClassListString.Split(' ').Select(x => x.Trim()).ToList();
            return cssClassList.Any(x => x.Equals(cssClassName.Trim()));
        }

        public static void OpenLinkAtNewTab(this IWebElement linkWebElement, IWebDriver webDriver)
        {
            var action = new Actions(webDriver);
            action.KeyDown(Keys.Control)
                .KeyDown(Keys.Shift)
                .Click(linkWebElement)
                .KeyUp(Keys.Control)
                .KeyUp(Keys.Shift)
                .Build()
                .Perform();
        }

        public static void DoubleClick(this IWebElement webElement, IWebDriver webDriver)
        {
            var action = new Actions(webDriver);
            action.DoubleClick(webElement);
            action.Perform();
        }

        public static void Hover(this IWebElement webElement, IWebDriver webDriver)
        {
            Actions builder = new Actions(webDriver);
            builder.MoveToElement(webElement).Build().Perform();
        }
    }
}
