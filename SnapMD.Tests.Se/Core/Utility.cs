﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SnapMD.Tests.Se.Core
{
    public class Utility
    {
        public static string FullName(string firstName, string lastName)
        {
            firstName = string.IsNullOrEmpty(firstName) ? "" : firstName.Trim();
            lastName = string.IsNullOrEmpty(lastName) ? "" : lastName.Trim();
            return string.Format("{0} {1}", firstName, lastName).Trim();
        }

        /*
         * moved form testing helper
         * but can have a look at http://stackoverflow.com/questions/730268/unique-random-string-generation
         */

        public static string RandomString(int length, string stringType = "letters")
        {
            const string letters = "abcefghijklmnopqrstuvwxyz";
            const string internationalLetterrs = "čćdđšž";
            const string numbers = "0123456789";

            string result = null;
            var random = new Random(Guid.NewGuid().GetHashCode());
            if (stringType.Equals("letters"))
            {
                result = new string(Enumerable.Repeat(letters, length).Select(s => s[random.Next(s.Length)]).ToArray());
            }
            else if (stringType.Equals("numbers"))
            {
                result = new string(Enumerable.Repeat(numbers, length).Select(s => s[random.Next(s.Length)]).ToArray());
            }
            else if (stringType.Equals("international"))
            {
                result =
                    new string(
                        Enumerable.Repeat(letters + internationalLetterrs, length)
                            .Select(s => s[random.Next(s.Length)])
                            .ToArray());
            }
            else
            {
                throw new ArgumentException("Invalid value for type parameter", "stringType");
            }

            //add whitespaces to simulate words if string is longer than 15 characters
            var temp = new StringBuilder(result);
            if (result.Length > 15)
            {
                int div = result.Length/7;
                for (int i = 1; i <= div; i++)
                {
                    temp[i*7 - 1] = ' ';
                }
            }
            result = temp.ToString().Trim();

            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(result);
        }

        public static string RandomString(int minLength, int maxLength, string stringType = "letters")
        {
            return RandomString(RandomNumber(minLength, maxLength), stringType);
        }

        /*moved form testing helper*/
        public static int RandomNumber(int min, int max)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            return random.Next(min, max + 1);
        }

        public static string DigitString(string inputString)
        {
            if (String.IsNullOrEmpty(inputString))
            {
                inputString = "";
            }
            string digitString = Regex.Replace(inputString, "[^0-9]+", string.Empty);
            return digitString;
        }

        /*http://stackoverflow.com/questions/14505932/random-datetime-between-range-not-unified-output*/
        public static DateTime RandomDateTime(DateTime min, DateTime? max = null)
        {
            Random rnd = new Random();
            max = max ?? new DateTime(9999, 12, 31);

            var range = max.Value - min;
            var randomUpperBound = (Int32)range.TotalSeconds;
            if (randomUpperBound <= 0)
                randomUpperBound = rnd.Next(1, Int32.MaxValue);

            var randTimeSpan = TimeSpan.FromSeconds((Int64)(range.TotalSeconds - rnd.Next(0, randomUpperBound)));
            return min.Add(randTimeSpan);
        }

        public static string AlliancedEmail(string email, char allianceChar = '+')      
        {

            return new MailAddressExtended(email).AlliancedEmail(allianceChar);
        }
    }
}
