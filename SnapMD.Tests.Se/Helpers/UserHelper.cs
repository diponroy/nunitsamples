﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.TestSetting.Data.Model;
using SnapMD.Tests.Se.WebPages;

namespace SnapMD.Tests.Se.Helpers
{
    public abstract class UserHelper
    {
        public readonly IWebDriver WebDriver;
        protected UserHelper(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        protected bool Login<TLoginPage, THomePage>(
            string email,
            string password,
            IWebDriver currentWebDriver,
            TLoginPage loginPage,
            THomePage homePage,
            string homePageUrlWith = ""
            )
            where TLoginPage : class, IPageObjectModel, ILoginPom
            where THomePage : class, IPageObjectModel
        {
            loginPage.NavigateOrRefreshUrl();
            loginPage.SubminCredentials(email, password);
            bool isAtExpectedPage = homePage.HasNavigatedAtUrlWith(homePageUrlWith);
            return isAtExpectedPage;
        }

        protected bool Logout<TLogoutPom, TLoginPage>(TLoginPage loginPage, string loginPageUrlWith = "")
            where TLogoutPom : class, ILogoutPom
            where TLoginPage : class, IPageObjectModel, ILoginPom
        {
            var tmpl = PageFactory.InitElements<TLogoutPom>(WebDriver);
            tmpl.RequestLogout();
            bool isAtLoginPage = loginPage.HasNavigatedAtUrlWith(loginPageUrlWith);
            return isAtLoginPage;
        }

        protected void TryToLogout<TLoginPage>(TLoginPage loginPage, string loginPageUrlWith = "")
            where TLoginPage : class, IPageObjectModel, ILoginPom
        {
            try
            {
                Logout();
            }
            catch
            {
                loginPage.NavigateToUrlWith("");
            }
        }

        public virtual void Login(IUser user)
        {
            if (String.IsNullOrEmpty(user.Email))
            {
                throw new NullReferenceException("Need user email to login.");
            }
            if (String.IsNullOrEmpty(user.Password))
            {
                throw new NullReferenceException("Need user password to login.");
            }

            Login(user.Email, user.Password);
        }

        public abstract void Login(string email, string password);
        public abstract void Logout();
    }
}
