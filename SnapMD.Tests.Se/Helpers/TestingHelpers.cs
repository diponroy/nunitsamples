﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.Utilities;
using Cookie = OpenQA.Selenium.Cookie;

namespace SnapMD.Tests.Se.Helpers
{
    internal class TestingHelpers
    {
        public void doLogin(IWebDriver webDriver, string loginPath, string username, string password)
        {
            //go to login page
            webDriver.Url = Settings.Default.DomainToTest + loginPath;
            webDriver.Navigate();

            //type username
            IWebElement usernameField = webDriver.FindElement(By.Id("txtloginemail"));
            Thread.Sleep(500);
            usernameField.SendKeys(username);

            //type password
            IWebElement passwordField = webDriver.FindElement(By.Id("txtPassword"));
            Thread.Sleep(500);
            passwordField.SendKeys(password);

            //click Login
            webDriver.FindElement(By.Id("btnLogin")).Click();
        }

        //public string generateRandomString(int length, string stringType = "letters")
        //{
        //    return Utility.RandomString(length, stringType);
        //}

        //public int generateRandomNumber(int min, int max)
        //{

        //    return Utility.RandomNumber(min, max);
        //}

        public String RandomDay()
        {
            //in next 60 days          
            return DateTime.Today.AddDays(Utility.RandomNumber(1, 60)).ToString();
        }

        public int UID() //for unique ID of user
        {
            var rnd = new Random();
            int number = rnd.Next(1522); // creates a number between 0 and 1522
            return number;
        }

        /// <summary>
        ///     Waits for popup alert and then close it
        /// </summary>
        /// <param name="webDriver"></param>
        /// <param name="maxSeconds">Maximum number of seconds to wait for alert</param>
        public void waitForAlert(IWebDriver webDriver, int maxSeconds)
        {
            int i = 0;

            while (i++ < maxSeconds*2)
            {
                try
                {
                    webDriver.SwitchTo().Alert().Accept();
                    break;
                }
                catch (NoAlertPresentException)
                {
                    Thread.Sleep(500);
                }
            }
        }

        public void typeInTextField(IWebDriver webDriver, string fieldID, string text)
        {
            Thread.Sleep(100);

            //clear text from field 
            webDriver.FindElement(By.Id(fieldID)).Clear();

            //enter new value
            webDriver.FindElement(By.Id(fieldID)).SendKeys(text);
        }

        public string chooseRandomFromDropdown(IWebDriver webDriver,
            string fieldPath,
            string listPath,
            string inputPath,
            List<string> filter = null)
        {
            //open drop down
            IWebElement field = webDriver.FindElement(By.XPath(fieldPath));
            string currentSelected = field.Text;
            field.Click();

            //get all elements in list
            ReadOnlyCollection<IWebElement> elements = webDriver.FindElements(By.XPath(listPath));

            IWebElement input = webDriver.FindElement(By.XPath(inputPath));

            int i;
            string selected;

            //select random element
            if (filter == null)
            {
                do
                {
                    i = Utility.RandomNumber(0, elements.Count - 1);
                    selected = elements[i].Text;
                } while (selected.Equals(currentSelected) || selected.ToLower().Equals("choose") ||
                         String.IsNullOrEmpty(selected));
            }
            else
            {
                do
                {
                    i = Utility.RandomNumber(0, elements.Count - 1);
                    selected = elements[i].Text;
                } while (selected.Equals(currentSelected) || selected.ToLower().Equals("choose") ||
                         String.IsNullOrEmpty(selected) || filter.Contains(selected));
            }

            //type new value in textbox
            input.SendKeys(selected);

            //select new value
            Thread.Sleep(1000);
            ReadOnlyCollection<IWebElement> filteredValues = webDriver.FindElements(By.XPath(listPath));
            foreach (IWebElement x in filteredValues)
            {
                if (x.Text.Equals(selected))
                {
                    x.Click();
                    break;
                }
            }

            return selected;
        }

        public void chooseSpecificFromDropdown(IWebDriver webDriver,
            string fieldPath,
            string listPath,
            string inputPath,
            string wantedValue)
        {
            //open drop down
            IWebElement field = webDriver.FindElement(By.XPath(fieldPath));
            string currentSelected = field.Text;
            field.Click();

            //get all elements in list
            ReadOnlyCollection<IWebElement> elements = webDriver.FindElements(By.XPath(listPath));

            IWebElement input = webDriver.FindElement(By.XPath(inputPath));

            //type new value in textbox
            input.SendKeys(wantedValue);

            //select new value
            Thread.Sleep(1000);
            ReadOnlyCollection<IWebElement> filteredValues = webDriver.FindElements(By.XPath(listPath));
            foreach (IWebElement x in filteredValues)
            {
                if (x.Text.Equals(wantedValue))
                {
                    x.Click();
                    break;
                }
            }
        }

        public void selectFromSimpleDropdown(IWebDriver webDriver, string dropdownPath, string listPath, string value)
        {
            IWebElement dropDownListBox = webDriver.FindElement(By.XPath(dropdownPath));
            dropDownListBox.Click();
            ReadOnlyCollection<IWebElement> options = webDriver.FindElements(By.XPath(listPath));

            foreach (IWebElement item in options)
            {
                if (item.Text.Contains(value))
                {
                    item.Click();
                    break;
                }
            }
        }

        public bool checkIfTitleContains(IWebDriver webDriver, string title)
        {
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
                wait.Until(d => { return d.Title.Contains(title); });
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                Console.WriteLine(webDriver.Title);
                return false;
            }
        }

        public bool checkIfUrlContains(IWebDriver webDriver, string url)
        {
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
                wait.Until(d => { return d.Url.Contains(url); });
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                Console.WriteLine(webDriver.Url);
                return false;
            }
        }


        public bool checkForSnapSuccess(IWebDriver webDriver, string message)
        {
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
                wait.Until(d => { return d.FindElement(By.ClassName("snapSuccess")).Text.Contains(message); });
                webDriver.FindElement(By.ClassName("snapSuccess")).Click();
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public static bool HasNavigatedAtUrl(IWebDriver webDriver, string url)
        {
            return WaitUntil(webDriver, driver => driver.Url.ToLower().Equals(url.ToLower()));
        }

        public static bool HasNavigatedAtUrlContains(IWebDriver webDriver, string value)
        {
            return WaitUntil(webDriver, driver => driver.Url.ToLower().Contains(value.ToLower()));
        }

        public static bool NavigatedUrlStartsWith(IWebDriver webDriver, string value)
        {
            return WaitUntil(webDriver, driver => driver.Url.ToLower().StartsWith(value.ToLower()));
        }

        public static void Wait(TimeSpan timeSpan)
        {
            Thread.Sleep(timeSpan);
        }

        public static bool WaitUntil(IWebDriver webDriver, Func<IWebDriver, bool> prediction)
        {
            return webDriver.WaitUntil(prediction, TestSettings.Timeouts.Explicit);
        }

        public static void WaitForAjax(IWebDriver webDriver)
        {
            SeleniumUtility.WaitForAjax(webDriver);
        }

        public static string DateTimeToString(DateTime dateTime)
        {
            /*datepickers using this formate*/
            return dateTime.ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
        }

        public static string DateToString(DateTime dateTime)
        {
            /*datepickers using this formate*/
            return dateTime.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
        }

        public static string TimeToString(DateTime dateTime)
        {
            /*timepickers using this formate*/
            return dateTime.ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }

        public static string TimeToString(ushort hour = 0, ushort minute = 0)
        {
            DateTime dateTime = DateTime.Today.Add(new TimeSpan(hour, minute, 0));
            return TimeToString(dateTime);
        }
    }
}
