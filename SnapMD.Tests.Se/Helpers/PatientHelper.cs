﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.WebPages.Patient;
using SnapMD.Tests.Se.WebPages.Patient.Template;

namespace SnapMD.Tests.Se.Helpers
{
    public class PatientHelper : UserHelper, IUserHelper
    {
        public PatientHelper(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override void Login(string email, string password)
        {
            bool isAbleToLogin = Login(
                currentWebDriver: WebDriver,
                email: email,
                password: password,
                loginPage: new PatientLoginPom(WebDriver),
                homePage: new PatientHomePom(WebDriver)
                );

            if (!isAbleToLogin)
            {
                throw new Exception("Unable to login as patient with expected page.");
            }
        }

        public override void Logout()
        {
            bool isAbleToLogout = Logout<PatientHeaderTmpl, PatientLoginPom>(
                loginPageUrlWith: "?lo=true",
                loginPage: new PatientLoginPom(WebDriver));
            if (!isAbleToLogout)
            {
                throw new Exception("Unable to log out from the patient session page.");
            }
        }

        public void TryToLogout()
        {
            TryToLogout(new PatientLoginPom(WebDriver));
        }
    }
}
