﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.WebPages.Physician;
using SnapMD.Tests.Se.WebPages.Physician.Template;

namespace SnapMD.Tests.Se.Helpers
{
    public class PhysicianHelper : UserHelper, IUserHelper
    {
        public PhysicianHelper(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override void Login(string email, string password)
        {
            bool isAbleToLogin = Login(
                currentWebDriver: WebDriver,
                email: email,
                password: password,
                loginPage: new PhysicianLoginPom(WebDriver),
                homePage: new PhysicianWaitingListPom(WebDriver)
                );

            if (!isAbleToLogin)
            {
                throw new Exception("Unable to login as physician with expected page.");
            }
        }

        public override void Logout()
        {
            bool isAbleToLogout = Logout<PhysicianHeaderTmpl, PhysicianLoginPom>(
                loginPageUrlWith: "?lo=true",
                loginPage: new PhysicianLoginPom(WebDriver));
            if (!isAbleToLogout)
            {
                throw new Exception("Unable to log out from the physician session page.");
            }
        }

        public void TryToLogout()
        {
            TryToLogout(new PhysicianLoginPom(WebDriver));
        }
    }
}
