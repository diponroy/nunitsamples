﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.WebPages.Admin;
using SnapMD.Tests.Se.WebPages.Admin.Template;

namespace SnapMD.Tests.Se.Helpers
{
    public class AdminHelper : UserHelper, IUserHelper
    {
        public AdminHelper(IWebDriver webDriver) : base(webDriver)
        {
        }

        public override void Login(string email, string password)
        {
            bool isAbleToLogin = Login(
                currentWebDriver: WebDriver,
                email: email,
                password: password,
                loginPage: new AdminLoginPom(WebDriver),
                homePage: new DashBoardPom(WebDriver)
                );

            if (!isAbleToLogin)
            {
                throw new Exception("Unable to login as admin with expected page.");
            }
        }

        public override void Logout()
        {
            bool isAbleToLogout = Logout<AdminHeaderTmpl, AdminLoginPom>(
                loginPageUrlWith: "?lo=true",
                loginPage: new AdminLoginPom(WebDriver)
                );
            if (!isAbleToLogout)
            {
                throw new Exception("Unable to log out from the admin session page.");
            }
        }

        public void TryToLogout()
        {
            TryToLogout(new AdminLoginPom(WebDriver));
        }
    }
}
