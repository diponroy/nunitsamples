﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapMD.Tests.Se.TestSetting.Data.Model;

namespace SnapMD.Tests.Se.Helpers
{
    public interface IUserHelper
    {
        void Login(string email, string password);
        void Login(IUser user);
        void Logout();
        void TryToLogout();
    }
}
