﻿namespace SnapMD.Tests.Se.JsHelpers.Model
{
    public struct DropdownOption
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
