﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.JsHelpers
{
    public class JavascriptHelper
    {
        /*http://stackoverflow.com/questions/15122864/selenium-wait-until-document-is-ready*/
        public static bool IsDocumentReady(IWebDriver webDriver)
        {
            return (bool)webDriver.AsJsExecutor().ExecuteScript("return document.readyState === 'complete';");
        }

        public static void Value(IWebElement webElement, string value, IWebDriver webDriver)
        {
            webDriver.AsJsExecutor().ExecuteScript(String.Format("arguments[0].value = '{0}'", value), webElement);
        }

        public static void SetAttribute(IWebElement webElement,
            string attributeName,
            string value,
            IWebDriver webDriver)
        {
            webDriver.AsJsExecutor()
                .ExecuteScript(String.Format("arguments[0].setAttribute('{0}', '{1}')", attributeName, value),
                    webElement);
        }

        public static void Html(IWebElement webElement, string html, IWebDriver webDriver)
        {
            webDriver.AsJsExecutor().ExecuteScript(String.Format("arguments[0].innerHTML = '{0}'", html), webElement);
        }
    }
}
