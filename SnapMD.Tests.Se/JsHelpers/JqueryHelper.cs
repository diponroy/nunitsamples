﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.JsHelpers
{
    public class JqueryHelper : JavascriptHelper
    {
        public static void Val(IWebElement webElement, string value, IWebDriver webDriver)
        {
            webDriver.AsJsExecutor().ExecuteScript(String.Format("$(arguments[0]).val('{0}')", value), webElement);
        }

        public static string Val(IWebElement webElement, IWebDriver webDriver)
        {
            return (string)webDriver.AsJsExecutor().ExecuteScript("return $(arguments[0]).val()", webElement);
        }

        public static void Trigger(IWebElement webElement, string eventName, IWebDriver webDriver)
        {
            webDriver.AsJsExecutor().ExecuteScript(String.Format("$(arguments[0]).trigger('{0}')", eventName), webElement);
        }

        /*
         * this could not be same when using angular
         * http://stackoverflow.com/questions/6201425/wait-for-an-ajax-call-to-complete-with-selenium-2-web-driver
         */
        public static bool HasAjax(IWebDriver webDriver)
        {
            return (bool)webDriver.AsJsExecutor().ExecuteScript("return jQuery.active != 0;");   /*zero means no active ajax*/
        }
    }
}
