﻿using System;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.JsHelpers
{
    public class KendoNumericTextBox
    {
        private readonly IWebDriver _webDriver;
        private readonly IWebElement _txtNumeric;

        public KendoNumericTextBox(IWebElement txtNumeric, IWebDriver webDriver)
        {
            _webDriver = webDriver;
            _txtNumeric = txtNumeric;
        }

        public string Value()
        {
            return
                (string)
                    _webDriver.AsJsExecutor()
                        .ExecuteScript("return $(arguments[0]).data('kendoNumericTextBox').value();", _txtNumeric);
        }

        public void Value(string value)
        {
            _webDriver.AsJsExecutor()
                .ExecuteScript(String.Format("$(arguments[0]).data('kendoNumericTextBox').value('{0}');", value),
                    _txtNumeric);
        }
    }
}

/*
 * http://demos.telerik.com/kendo-ui/numerictextbox/api
 * $(arguments[0]).data('kendoNumericTextBox').value();
 * $(arguments[0]).data('kendoNumericTextBox').value('value');
 */