﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.JsHelpers.Model;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.JsHelpers
{
    /*
     * this one is like the SelectElement for selenium
     */

    public class KendoDropdown
    {
        private readonly IWebElement _dropdownElement;

        private readonly IWebDriver _webDriver;

        public IReadOnlyCollection<DropdownOption> Options()
        {
            var data = _webDriver.AsJsExecutor().ExecuteScriptForData<IReadOnlyCollection<DropdownOption>>(@"
                                                        function kendoOptions_selenium(webElement) {
	                                                        var ddl = $(webElement).data('kendoDropDownList');
	                                                        var textField = ddl.options.dataTextField;
	                                                        var valueField = ddl.options.dataValueField;
	                                                        var options = []; /*arry of object{ text:string , value:string }*/

	                                                        /*default option*/
	                                                        var defaultOption = ddl.options.optionLabel;
	                                                        if(defaultOption !== undefined){
		                                                        if(defaultOption[textField] !== undefined && defaultOption[valueField] !== undefined){
			                                                        options.push({ text: defaultOption[textField], value: defaultOption[valueField] });
		                                                        }else if(typeof defaultOption === 'string'){
			                                                        options.push({ text: defaultOption, value: '' });
		                                                        }
	                                                        }

	                                                        /*data source options*/
	                                                        var data = ddl.dataSource.data();
	                                                        for (var i = 0; i < data.length; i++) {
		                                                        options.push({ text: (data[i])[textField], value: (data[i])[valueField] });
	                                                        }

	                                                        return options;
                                                        };
                                                        return kendoOptions_selenium(arguments[0]);
                                                    ", _dropdownElement);
            return data;
        }

        public KendoDropdown(IWebElement dropdownElement, IWebDriver webDriver)
        {
            _dropdownElement = dropdownElement;
            _webDriver = webDriver;
        }

        public void TriggerChange()
        {
            _webDriver.AsJsExecutor()
                .ExecuteScript("$(arguments[0]).data('kendoDropDownList').trigger('change')", _dropdownElement);
        }

        public string SelectedText()
        {
            return
                (string)
                    _webDriver.AsJsExecutor()
                        .ExecuteScript("return $(arguments[0]).data('kendoDropDownList').text();", _dropdownElement);
        }

        public void SelectByText(string text)
        {
            _webDriver.AsJsExecutor()
                .ExecuteScript(String.Format("$(arguments[0]).data('kendoDropDownList').text('{0}');", text),
                    _dropdownElement);
            TriggerChange();
        }

        public string SelectedValue()
        {
            return
                (string)
                    _webDriver.AsJsExecutor()
                        .ExecuteScript("return $(arguments[0]).data('kendoDropDownList').value();", _dropdownElement);
        }

        public void SelectByValue(string value)
        {
            _webDriver.AsJsExecutor()
                .ExecuteScript(String.Format("$(arguments[0]).data('kendoDropDownList').value('{0}')", value),
                    _dropdownElement);
            TriggerChange();
        }

        public int SelectedIndex()
        {
            return
                (int)
                    _webDriver.AsJsExecutor()
                        .ExecuteScript("return $(arguments[0]).data('kendoDropDownList').select();", _dropdownElement);
        }

        public void SelectByIndex(int index)
        {
            _webDriver.AsJsExecutor()
                .ExecuteScript(String.Format("$(arguments[0]).data('kendoDropDownList').select({0});", index),
                    _dropdownElement);
            TriggerChange();
        }
    }
}

/*
    alert(JSON.stringify($('#cmbDoctorsList').data('kendoDropDownList').dataSource.data()));

    alert(JSON.stringify($('#cmbDoctorsList').data('kendoDropDownList').options));

    alert($('#cmbDoctorsList').data('kendoDropDownList').select());
    $('#cmbDoctorsList').data('kendoDropDownList').select(2);

    alert($('#cmbDoctorsList').data('kendoDropDownList').value());
    $('#cmbDoctorsList').data('kendoDropDownList').value("");

    alert($('#cmbDoctorsList').data('kendoDropDownList').text());
    $('#cmbDoctorsList').data('kendoDropDownList').text("Select Doctor..");
 
 
 
    function kendoOptions_selenium(webElement) {
	    var ddl = $(webElement).data('kendoDropDownList');
	    var textField = ddl.options.dataTextField;
	    var valueField = ddl.options.dataValueField;
	    var options = []; //arry of object{ text:string , value:string }

	    //default option
	    var defaultOption = ddl.options.optionLabel;
	    if(defaultOption !== undefined){
		    if(defaultOption[textField] !== undefined && defaultOption[valueField] !== undefined){
			    options.push({ text: defaultOption[textField], value: defaultOption[valueField] });
		    }else if(typeof defaultOption === 'string'){
			    options.push({ text: defaultOption, value: '' });
		    }
	    }

	    //data source options
	    var data = ddl.dataSource.data();
	    for (var i = 0; i < data.length; i++) {
		    options.push({ text: (data[i])[textField], value: (data[i])[valueField] });
	    }

	    return options;
    };
    kendoOptions_selenium($('#s5'));
 */