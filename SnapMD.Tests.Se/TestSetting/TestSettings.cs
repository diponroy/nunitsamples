﻿//#define TEST_SERVER
#define DEV_SERVER
//#define LOCAL_SERVER

using SnapMD.Tests.Se.TestSetting.Data;
using SnapMD.Tests.Se.TestSetting.Timeout;

namespace SnapMD.Tests.Se.TestSetting
{
    public static class TestSettings
    {
        public static readonly IDataContainer Datas;
        public static readonly IWaitingTimeout Timeouts;

        static TestSettings()
        {
#if TEST_SERVER
            Datas = new TestServerDataContainer();
            Timeouts = new WaitingTimeout();
#elif DEV_SERVER
            Datas = new DevServerDataContainer();
            Timeouts = new WaitingTimeout();
#elif LOCAL_SERVER
            Datas = new LocalServerDataContainer();
            Timeouts = new WaitingTimeout();
#endif
        }
    }
}
