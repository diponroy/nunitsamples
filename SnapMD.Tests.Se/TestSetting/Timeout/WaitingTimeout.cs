﻿using System;

namespace SnapMD.Tests.Se.TestSetting.Timeout
{
    public class WaitingTimeout : IWaitingTimeout
    {
        public TimeSpan Explicit { get; private set; }
        public TimeSpan Implicit { get; private set; }
        public TimeSpan PageLoad { get; private set; }
        public TimeSpan ScriptLoad { get; private set; }


        public WaitingTimeout(double timeOutInSeconds = 10)
        {
            Implicit = Explicit = PageLoad = ScriptLoad = TimeSpan.FromSeconds(timeOutInSeconds);
        }

    }
}
