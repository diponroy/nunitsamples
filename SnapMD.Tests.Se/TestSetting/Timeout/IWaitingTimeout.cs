﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.TestSetting.Timeout
{
    public interface IWaitingTimeout
    {
        TimeSpan Explicit { get; }
        TimeSpan Implicit { get; }
        TimeSpan PageLoad { get; }
        TimeSpan ScriptLoad { get; }
    }
}
