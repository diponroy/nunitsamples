﻿using SnapMD.Tests.Se.TestSetting.Data.Model;

namespace SnapMD.Tests.Se.TestSetting.Data
{
    public interface IDataContainer
    {
        //string BaseUrl { get; }
        Hospital Hospital { get; }
        HospitalStaffUser Admin { get; }
        HospitalStaffUser SnapMdAdmin { get; }
        HospitalStaffUser Physician { get; }
        PatientUser Patient { get; }
    }
}