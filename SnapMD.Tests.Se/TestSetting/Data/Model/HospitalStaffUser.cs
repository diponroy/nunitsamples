﻿namespace SnapMD.Tests.Se.TestSetting.Data.Model
{
    public class HospitalStaffUser : User, IUser
    {
        public HospitalStaffProfile Profile { get; set; }

        public override string FirstName
        {
            get { return Profile.Name; }
        }

        public override string LastName
        {
            get { return Profile.LastName; }
        }

        public override string FullName
        {
            get { return Profile.FullName; }
        }
    }
}
