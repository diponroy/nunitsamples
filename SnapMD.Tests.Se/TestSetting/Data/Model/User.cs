﻿using System;
using SnapMD.Tests.Se.Core;
using SnapMD.Tests.Se.Utilities;

namespace SnapMD.Tests.Se.TestSetting.Data.Model
{
    /*
     * TODO better to user struct
     */
    public abstract class User
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public abstract string FirstName { get; }
        public abstract string LastName { get; }
        public abstract string FullName { get; }

        public string NewAlliancedEmail()
        {
            return Utility.AlliancedEmail(Email);
        }
    }
}