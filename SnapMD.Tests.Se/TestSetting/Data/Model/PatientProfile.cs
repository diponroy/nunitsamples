﻿using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.TestSetting.Data.Model
{
    public class PatientProfile
    {
        public string PatientName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return Utility.FullName(PatientName, LastName);
            }
        }
    }
}
