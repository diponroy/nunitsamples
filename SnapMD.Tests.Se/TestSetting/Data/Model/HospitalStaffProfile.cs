﻿using SnapMD.Tests.Se.Core;

namespace SnapMD.Tests.Se.TestSetting.Data.Model
{
    public class HospitalStaffProfile
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return Utility.FullName(Name, LastName);
            }
        }
    }
}
