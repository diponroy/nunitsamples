﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Tests.Se.TestSetting.Data.Model
{
    public interface IUser
    {
        string Email { get; set; }
        string Password { get; set; }
    }
}
