﻿using SnapMD.Tests.Se.TestSetting.Data.Model;

namespace SnapMD.Tests.Se.TestSetting.Data
{
    public class DevServerDataContainer : IDataContainer
    {
        public Hospital Hospital
        {
            get
            {
                var hospital = new Hospital()
                {
                    DomainName = @"https://emerald.snap-dev.com"
                };
                return hospital;
            }
        }

        public HospitalStaffUser Admin
        {
            get
            {
                var admin = new HospitalStaffUser()
                {
                    Email = "snapmdstestingema.iladdress@gmail.com",
                    Password = "Password@123",
                    Profile = new HospitalStaffProfile()
                    {
                        Name = "Charles",
                        LastName = "Masterson"
                    }
                };
                return admin;
            }
        }

        public HospitalStaffUser SnapMdAdmin
        {
            get
            {
                var snapMdAdmin = new HospitalStaffUser()
                {
                    Email = "hi@example.com",
                    Password = "Password@123"
                };
                return snapMdAdmin;
            }
        }

        public HospitalStaffUser Physician
        {
            get
            {
                var physician = new HospitalStaffUser()
                {
                    Email = "snapmdstestingema.iladdress@gmail.com",
                    Password = "Password@123",
                    Profile = new HospitalStaffProfile()
                    {
                        Name = "Charles",
                        LastName = "Masterson"
                    }
                };
                return physician;
            }
        }

        public PatientUser Patient
        {
            get
            {
                var patient = new PatientUser()
                {
                    Email = "diponsust@gmail.com",
                    Password = "Password@123",
                    Profile = new PatientProfile()
                    {
                        PatientName = "dipon patient",
                        LastName = "roy"
                    }
                };
                return patient;
            }
        }
    }

}