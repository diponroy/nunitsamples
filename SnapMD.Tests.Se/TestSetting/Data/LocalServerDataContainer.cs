﻿using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.TestSetting.Data.Model;

namespace SnapMD.Tests.Se.TestSetting.Data
{
    /*
     * ToDo: add some merge sql
     *  to setup/update default admin, physician, patient(email, password, first name, last name)
     *  admin and physician need active hospital staff
     *  patient need active patient profile
     *  user same timezone for all
     *  make all active
     *  
     */
    public class LocalServerDataContainer : IDataContainer
    {
        public Hospital Hospital
        {
            get
            {
                var hospital = new Hospital()
                {
                    DomainName =  @"http://snap.local"
                };
                return hospital;
            }
        }

        public HospitalStaffUser Admin
        {
            get
            {
                var admin = new HospitalStaffUser()
                {
                    Email = "contracts@phdlabs.com",
                    Password = "Password@123",
                    Profile = new HospitalStaffProfile()
                    {
                        Name = "Kiki",
                        LastName = "Threlkeld"
                    }
                };
                return admin;
            }
        }

        public HospitalStaffUser SnapMdAdmin
        {
            get
            {
                var snapMdAdmin = new HospitalStaffUser()
                {
                    Email = "hi@phdlabs.com",
                    Password = "Password@123"
                };
                return snapMdAdmin;
            }
        }

        public HospitalStaffUser Physician
        {
            get
            {
                var physician = new HospitalStaffUser()
                {
                    Email = "contracts@phdlabs.com",
                    Password = "Password@123",
                    Profile = new HospitalStaffProfile()
                    {
                        Name = "Kiki",
                        LastName = "Threlkeld"
                    }
                };
                return physician;
            }
        }

        public PatientUser Patient
        {
            get
            {
                var patient = new PatientUser()
                {
                    Email = "todd@phdlabs.com",
                    Password = "Password@123",
                    Profile = new PatientProfile()
                    {
                        PatientName = "Todd",
                        LastName = "Goulding"
                    }
                };
                return patient;
            }
        }
    }

}