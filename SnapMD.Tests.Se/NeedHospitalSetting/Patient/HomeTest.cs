﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.TestSetting;

namespace SnapMD.Tests.Se.NeedHospitalSetting.Patient
{
    [TestFixture, Explicit]
    public class HomeTest
    {
        #region Public Methods and Operators

        [Test]
        public void TestPluginCheck()
        {
            IWebDriver driver = DriverSetup.Driver;
            //Login
            new PatientHelper(driver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

            // Check for error messages
            ReadOnlyCollection<IWebElement> warnings = driver.FindElements(By.ClassName("warning"));
            Assert.IsEmpty(warnings);

            string title = driver.Title;
            Assert.IsFalse(title.Contains("Login"));

            Assert.IsTrue(driver.Url.Contains("Vidyo"));
        }

        [Test]
        public void TestBypassPluginCheck()
        {
            IWebDriver driver = DriverSetup.Driver;
            //Login
            new PatientHelper(driver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

            // Check for error messages
            var warnings = driver.FindElements(By.ClassName("warning"));
            Assert.IsEmpty(warnings);

            string title = driver.Title;
            Assert.IsFalse(title.Contains("Login"));

            driver.Url = Settings.Default.DomainToTest + "/Customer/Home";
            driver.Navigate();

            Assert.IsFalse(driver.Url.Contains("Vidyo"));
        }

        #endregion
    }
}