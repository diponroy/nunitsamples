﻿using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.JsHelpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.NeedHospitalSetting.Patient
{
    /*
     * payment card setup form account, maximum 10
     * need hospital settings "Show Payment Info" ON
     */
    [TestFixture, Explicit]
    class AccountPaymentInfoTest
    {
        public static string ccNumber = "4007000000027";
        public static string ccType = "Visa";

        [Test]
        public void AddPaymentTest()
        {
            IWebDriver driver = DriverSetup.Driver;
            new PatientHelper(driver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);
            var page = new PaymentInfoPom(driver);
            page.NavigateToUrl();
            page.WaitForAjax();

            /*show add card panel*/
            page.BtnShowAddCardPanel.Click();
            Assert.IsTrue(page.IsVisiable(page.DivAddCardPanel));
            page.WaitForAjax();

            /*enter data*/
            page.TxtCardNumber.SendKeys(ccNumber);
            var ddlExpMonth = new KendoDropdown(page.DdlExpirationMonth, driver);
            ddlExpMonth.SelectByValue(ddlExpMonth.Options().Last().Value);
            var ddlExpYear = new KendoDropdown(page.DdlExpirationYear, driver);
            ddlExpYear.SelectByValue(ddlExpYear.Options().Last().Value);
            page.TxtCvvCode.SendKeys("123");
            page.TxtFirstName.SendKeys("Test");
            page.TxtLastName.SendKeys("Acc");
            page.TxtAddress.SendKeys("Street 123");
            page.TxtCity.SendKeys("NY");
            page.TxtState.SendKeys("NY");
            page.TxtZip.SendKeys("12345");
            new KendoDropdown(page.DdlCountry, driver).SelectByIndex(2);

            /*submit*/
            page.BtnAddNewCard.Click();
            page.WaitForAjax();
            Assert.IsTrue(page.SuccessNotificationContains("Payment Details Added Successfully."));
        }

        [Test]
        public void Delete_Card()
        {
            
        }

        //[Test]
        //public void DeletePaymentTest()
        //{
        //    IWebDriver webDriver = DriverSetup.Driver;
        //    //Login
        //    new PatientHelper(webDriver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

        //    // wait until home page loads
        //    var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
        //    wait.Until((d) => { return !d.Title.Contains("Login"); });

        //    //Click on Main Manu and open My Account 
        //    PatientHomePom patientHome = new PatientHomePom(webDriver);
        //    patientHome.openMainMenu();
        //    patientHome.clickOnMyAccount();

        //    //Select Payment Information
        //    MyAccount myAccount = new MyAccount(webDriver);
        //    myAccount.selectPaymentInformation();

        //    //Find CC and delete it
        //    myAccount.findCC(ccNumber);


        //    //confirmation
        //    myAccount.confirmDeleteCC();
        //}
    }
}

