﻿using NUnit.Framework;
using OpenQA.Selenium;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.TestSetting;
using SnapMD.Tests.Se.WebPages.Patient;

namespace SnapMD.Tests.Se.NeedHospitalSetting.Patient
{
    [TestFixture, Explicit]
    public class OnDemandConsultation
    {
        //ChromeDriver webDriver = new ChromeDriver();

        TestingHelpers helper = new TestingHelpers();
  
        [Test]
        public void TestCreateConsultation()
        {
            IWebDriver webDriver = DriverSetup.Driver;
            //Login
            new PatientHelper(webDriver).Login(TestSettings.Datas.Patient.Email, TestSettings.Datas.Patient.Password);

            //Click on New Consultation
            PatientHomePom patientHome = new PatientHomePom(webDriver);
            patientHome.clickOnNewConsultation();
            patientHome.checkIFClickedOnNewConsultation();

            //Select Patient
            ChosePatient patient = new ChosePatient(webDriver);
            patient.selectPatient();
            patient.chekIfPatientIsSelected();

            //Step1 Patient Condition
            Step1 concerns = new Step1(webDriver);
            concerns.patientPrimaryConcern("Cough");
            concerns.patientSecondaryConcern("Vomiting");
                
            concerns.clickNext();
            concerns.checkIfClicked();

            //Step2 Medical History
            Step2 conditions = new Step2(webDriver);

            //conditions.setlistOfChronic("Asthma");
            //conditions.setlistOfPrior("AUTOMATED TEST SURGERY");

            conditions.chronicMedicalConditionsNone();
            conditions.listPriorSurgeriesNone();
            conditions.clickNext();
            conditions.checkIfClicked();

            //Step3 Medications and Vaccinations

            Step3 medications = new Step3(webDriver);
            medications.setAlergicTo("Antibiotics");
            medications.setAlergicTo("Antibiotics");
            medications.setAlergicTo1("Aspirin");
            medications.setAlergicTo2("Insulin");
            medications.setAlergicTo3("Anticonvulsants");

            medications.setCurrentlyTaking("Pain Medications");
            medications.clickNext();
            medications.checkIfClicked();

            //Confirmation
            Confirmation confirmAndAck = new Confirmation(webDriver);
            confirmAndAck.acknowladge();
            confirmAndAck.clickSubmit();
            confirmAndAck.checkIfClicked();

            //Insurance
            Insurance insurance = new Insurance(webDriver);
            insurance.clickNext();
            insurance.checkIfClicked();

            //Insurance and Payment

            InsuranceAndPayment insuranceAndPayment = new InsuranceAndPayment(webDriver);
            insuranceAndPayment.clickSubmit();
            insuranceAndPayment.confirm();
            insuranceAndPayment.checkIfClicked();

            //Enter Waiting Room

            EnterWaitingRoom enterWaiting = new EnterWaitingRoom(webDriver);
            enterWaiting.clickEnter();
            enterWaiting.checkIfClicked();
        }
    }   
}
