﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using SnapMD.Tests.Se.Helpers;
using SnapMD.Tests.Se.Properties;
using SnapMD.Tests.Se.WebPages.Patient;
using SnapMD.Tests.Se.WebPages.Physician;

namespace SnapMD.Tests.Se.NeedHospitalSetting.Patient
{
    [TestFixture, Explicit]
    public class ConsultationTest
    {
        private readonly TestingHelpers helper = new TestingHelpers();

        [Test]
        public void TestCreateConsultation()
        {
            //////USER SIDE OF THE TEST

            //Login as USER
            var webDriver = DriverSetup.Driver;
            helper.doLogin(webDriver, "/Customer/Login", Settings.Default.PatientUser, "Password@123");

            // wait until home page loads
            var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(d => { return !d.Title.Contains("Login"); });

            //Click on New Consultation
            var home = new PatientHomePom(webDriver);
            home.clickOnNewConsultation();
            home.checkIFClickedOnNewConsultation();

            //Select Patient
            var patient = new ChosePatient(webDriver);
            patient.selectPatient();
            patient.chekIfPatientIsSelected();

            //Step1 Patient Condition
            var concerns = new Step1(webDriver);
            concerns.patientPrimaryConcern("Fever (100+)");
            concerns.patientSecondaryConcern("Vomiting");
            concerns.clickNext();
            concerns.checkIfClicked();

            //Step2 Medical History
            var conditions = new Step2(webDriver);
            conditions.listPriorSurgeriesNone();
            conditions.chronicMedicalConditionsNone();
            //conditions.setlistOfChronic("Asthma");
            //conditions.setlistOfPrior("AUTOMATED TEST SURGERY");
            conditions.clickNext();
            conditions.checkIfClicked();

            //Step3 Medications and Vaccinations

            var medications = new Step3(webDriver);
            medications.setAlergicTo("Antibiotics");
            medications.setCurrentlyTaking("Pain Medications");
            medications.clickNext();
            medications.checkIfClicked();

            //Confirmation
            var confirmAndAck = new Confirmation(webDriver);
            confirmAndAck.acknowladge();
            confirmAndAck.clickSubmit();
            confirmAndAck.checkIfClicked();

            //Insurance
            var insurance = new Insurance(webDriver);
            insurance.clickNext();
            insurance.checkIfClicked();

            //Insurance and Payment
            var insuranceAndPayment = new InsuranceAndPayment(webDriver);
            insuranceAndPayment.clickSubmit();
            insuranceAndPayment.confirm();
            insuranceAndPayment.checkIfClicked();

            //Enter Waiting Room
            var enterWaiting = new EnterWaitingRoom(webDriver);
            enterWaiting.clickEnter();
            enterWaiting.checkIfClicked();

            /////END OF USER SIDE OF THE TEST
            /////BEGIN OF PHYSICIAN SIDE OF THE TEST

            //Dcotor login
            helper.doLogin(webDriver, "/Physician/Login", Settings.Default.AdminUser, "Password@123");

            // wait until home page loads
            wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            wait.Until(d => { return !d.Title.Contains("Login"); });

            //Pickup consultation
            var consultation = new PhysicianWaitingListPom(webDriver);
            consultation.selectConsultation();
            consultation.chekIfClicked();

            //In conltations
            var inconsultation = new Consultation(webDriver);
            inconsultation.startConsultation();
            inconsultation.endConsultation();
        }
    }
}
