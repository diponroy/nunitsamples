﻿using System.Collections.Generic;
using NUnit.Framework;
using SnapMD.Core.Utilities;

namespace SnapMD.Core.Tests.Utilities
{
    [TestFixture]
    public class FullNameUtiltiyTest
    {
        [Test]
        [TestCase(" Dipon ", "Roy", "Dipon Roy")]   //firstName trimmed
        [TestCase("Dipon", " Roy ", "Dipon Roy")]   //lastName trimmed
        [TestCase("", "Roy", "Roy")]                //empty firstName  
        [TestCase("Dipon", "", "Dipon")]            //empty lastName
        [TestCase(null, "Roy", "Roy")]              //null firstName  
        [TestCase("Dipon", null, "Dipon")]          //null lastName
        [TestCase("Dipon", "Roy", "Dipon Roy")]
        [TestCase("", "", "")]
        [TestCase(null, null, "")]
        public void Name(string firstName, string lastName, string expectedFullName)
        {
            string fullName = FullNameUtiltiy.Name(firstName, lastName);
            Assert.AreEqual(expectedFullName, fullName);
        }
    }
}
