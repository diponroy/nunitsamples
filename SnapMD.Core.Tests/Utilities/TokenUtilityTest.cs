﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SnapMD.Core.Utilities;

namespace SnapMD.Core.Tests.Utilities
{
    [TestFixture]
    public class TokenUtilityTest
    {
        [Test]
        [TestCase(25)]
        [TestCase(30)]
        public void Token_Unique_EachTime(int legnth)
        {
            List<String> tokens = new List<string>();
            for (int i = 0; i < 1000; i++)
            {
                var token = TokenUtility.Token((uint?)legnth);
                string failMsg = String.Format("Test failled at iteration No. {0}, legnth = {1}, for token = {2}", i, legnth, token);

                Assert.AreEqual(legnth, token.Length, failMsg);
                Assert.True(token.All(char.IsLetterOrDigit), failMsg);
                Assert.IsFalse(token.Any(char.IsLower), failMsg);
                Assert.IsFalse(tokens.Contains(token), failMsg);
                tokens.Add(token);
            }
        }

        [Test]
        public void Token_Expected_Length()
        {
            Assert.DoesNotThrow(() => TokenUtility.Token(25));
            Assert.DoesNotThrow(() => TokenUtility.Token(null));
            Assert.DoesNotThrow(() => TokenUtility.Token());

            var error = Assert.Catch<Exception>(() => TokenUtility.Token(500));
            Assert.AreEqual(String.Format("Unable to create toke of expected length {0}.", 500), error.Message);
        }

    }
}
