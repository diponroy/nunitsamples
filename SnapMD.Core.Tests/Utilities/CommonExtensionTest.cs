﻿using NUnit.Framework;
using SnapMD.Utilities;

namespace SnapMD.Core.Tests.Utilities
{
    [TestFixture]
    public class CommonExtensionTest
    {
        [Test]
        [TestCase("test@test.com", true)]
        [TestCase("test123@test.com", true)]
        [TestCase("test@test123.com", true)]
        [TestCase("test.123@test.com", true)]
        [TestCase("test+123@test.com", true)]
        [TestCase("", false)]
        [TestCase(null, false)]
        [TestCase("test 20@test.com", false)]       //space
        [TestCase("test%40test.com", false)]        //url encoded 'test@test.com'
        [TestCase("test%2B12%40test.com", false)]   //url encoded 'test+12@test.com'
        [TestCase("test@test+123.com", false)]
        public void IsValidEmail(string email, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, CommonExtension.IsValidEmail(email));   //static method
            Assert.AreEqual(expectedResult, email.IsValidEmail());                  //extension method
        }
    }
}
