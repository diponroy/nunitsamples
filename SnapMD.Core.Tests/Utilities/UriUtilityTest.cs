﻿using System;
using NUnit.Framework;
using SnapMD.Core.Utilities;

namespace SnapMD.Core.Tests.Utilities
{
    [TestFixture]
    public class UriUtilityTest
    {
        [Test]
        [TestCase("https", "snap.local.com", "https://snap.local.com")]
        [TestCase("http", "snap.local", "http://snap.local")]
        [TestCase(" http ", " snap.local ", "http://snap.local")]   //trims scheme and domainName
        public void Url(string schemeName, string domainName, string expectedUrl)
        {
            var url = UriUtility.Url(schemeName, domainName);
            Assert.AreEqual(expectedUrl, url);
        }

        [Test]
        [TestCase("", "snap.local.com")]
        [TestCase(null, "snap.local")]
        [TestCase("unknownSchemeName", "snap.local")]
        public void Url_SchemeName_NullOrEmptry_OrUnknown_Throws_Error(string schemeName, string domainName)
        {
            var error = Assert.Catch<UriFormatException>(() => UriUtility.Url(schemeName, domainName));
            Assert.AreEqual("Scheme name is null/empty or unknown. Known schema names are [https, http].", error.Message);
        }

        [Test]
        [TestCase("https", "")]
        [TestCase("https", null)]
        public void Url_DomainName_NullOrEmptry_Throws_Error(string schemeName, string domainName)
        {
            var error = Assert.Catch<UriFormatException>(() => UriUtility.Url(schemeName, domainName));
            Assert.AreEqual("Domain name is null/empty.", error.Message);
        }
    }
}
