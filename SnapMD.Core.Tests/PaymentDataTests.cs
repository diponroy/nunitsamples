﻿using System;
using NUnit.Framework;
using SnapMD.Core.Billing;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class PaymentDataTests
    {
        [Test]
        public void TestIsValidFail()
        {
            var data = new PaymentData();
            Assert.IsFalse(data.IsValid());

            data.CardNumber = "4356";
            Assert.IsFalse(data.IsValid());

            data.CardNumber = "4111111111111111";
            data.ExpiryMonth = 21;

            Assert.IsFalse(data.IsValid());
            data.ExpiryMonth = 12;
            data.ExpiryYear = 2014;
            Assert.IsFalse(data.IsValid());

            data.CardNumber = "asdf-xxxx-xxxx-1111";
            Assert.IsFalse(data.IsValid());
        }

        [Test]
        public void TestIsValid()
        {
            var data = new PaymentData
            {
                CardNumber = "4111111111111111", ExpiryMonth = 12, 
                ExpiryYear = DateTime.Now.Year + 1,
                Cvv = "123"
            };
            Assert.IsTrue(data.IsValid());

            data.CardNumber = "xxxx-xxxx-xxxx-1111";
            Assert.IsTrue(data.IsValid());
        }
    }
}
