﻿using NUnit.Framework;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class HospitalSettingsKeysTest
    {
        [Test]
        [TestCase(HospitalSettingsKeys.CustomerSso, "CustomerSSO")]
        public void Keys(string actualValue, string expectedValue)
        {
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
