﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Interfaces.Repositories;

namespace SnapMD.Core.Tests
{
    public class MedicalProfileFacadeTests
    {
        [Test]
        public void TestMedicalProfileFacadeUpdate()
        {
            var repo1 = new Mock<IMedicalHistoryRepository>();
            var repo2 = new Mock<IPatientMetadataRepository>();
            var facade = new MedicalProfileFacade(repo1.Object, repo2.Object);
            facade.Upsert(1, new MedicalHistoryProfile(), new Dictionary<int, string>());
            Assert.Pass();
        }
    }
}
