﻿using NUnit.Framework;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class SnapModulesTests
    {
        protected readonly string FileSharingModule = "mFileSharing";

        [Test]
        public void TestGetModule()
        {
            var moduleName = SnapModules.GetKey(SnapModule.FileSharing);

            Assert.IsTrue(moduleName == FileSharingModule, "The module names are not pulling correctly");
        }

        [Test]
        public void TestGet()
        {
            var moduleName = SnapModules.GetKey((int) SnapModule.FileSharing);

            Assert.IsTrue(moduleName == FileSharingModule, "The module names are not pulling correctly");
        }

    }
}
