﻿using System;
using NUnit.Framework;
using SnapMD.Core.Loggly;

namespace SnapMD.Core.Tests.Loggly
{
    [TestFixture]
    public class LogglyUriHelperTests
    {
        public const string Token = "0000-0000-0000-0000";

        [TestCase("x")]
        [TestCase("server1")]
        [TestCase("us-east")]
        [TestCase("my_server")]
        [TestCase("1234_zone")]
        [TestCase("www.loggly.com")]
        [TestCase("apache")]
        public void AddTag_GivenValidTag_TagAddedToUri(string tag)
        {
            var logglyUriHelper = new LogglyUriHelper(Token);

            logglyUriHelper.AddTag(tag);

            var resultUri = logglyUriHelper.GetUri();
            var expectedUri = string.Format("http://logs-01.loggly.com/inputs/{0}/tag/{1}", Token, tag);

            Assert.That(resultUri, Is.EqualTo(expectedUri));
        }

        [TestCase("")]
        [TestCase("   ")]
        [TestCase(null)]
        [TestCase("Long_tag_more_that_64_characters_oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo")]
        [TestCase("_my_little_pony")]
        [TestCase("-us")]
        [TestCase("Room 1")]
        [TestCase("apache$")]
        public void AddTag_GivenInvalidTag_ArgumentexceptionThrown(string tag)
        {
            var logglyUriHelper = new LogglyUriHelper(Token);

            Assert.Throws<ArgumentException>(() => logglyUriHelper.AddTag(tag));
        }

        [Test]
        public void AddTag_GivenTags_TagsAddedToUri()
        {
            var logglyUriHelper = new LogglyUriHelper(Token);

            string tag1 = "tag1";
            string tag2 = "tag2";

            logglyUriHelper.AddTag(tag1);
            logglyUriHelper.AddTag(tag2);

            var resultUri = logglyUriHelper.GetUri();
            var expectedUri = string.Format("http://logs-01.loggly.com/inputs/{0}/tag/{1},{2}", Token, tag1, tag2);

            Assert.That(resultUri, Is.EqualTo(expectedUri));
        }
    }
}
