﻿using NUnit.Framework;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class HospitalSettingsValidatorTest
    {
        [Test]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("mandatory")]
        [TestCase("optional")]
        [TestCase("no")]
        [TestCase(" Mandatory")]
        [TestCase("Mandatory ")]
        [TestCase(" Mandatory ")]
        public void CustomerSSO_Invalid_Optaions(string option)
        {
            Assert.False(HospitalSettingsValidator.IsValidCustomerSsoOption(option));
        }

        [Test]
        [TestCase("Mandatory")]
        [TestCase("Optional")]
        [TestCase("None")]
        public void CustomerSSO_Valid_Options(string option)
        {
            Assert.IsTrue(HospitalSettingsValidator.IsValidCustomerSsoOption(option));
        }
    }
}
