﻿using System;
using System.Collections.Generic;
using AuthorizeNet;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Billing;
using SnapMD.Core.Interfaces;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class AuthorizeNetTests
    {
        [Test]
        public void TestGatewayConstructorFailsWithNullEmail()
        {
            var test = new CimCustomerGateway(null, null);
            Assert.Throws<InvalidOperationException>(() => test.CreateCustomer(null, "desc"));
        }

        [Test]
        public void TestDictionary()
        {
            var target = new LegacyCopayTransaction(null, null);

            var input = new Dictionary<string, string>
            {
                {"AuthorizeNet_PostURL", "test1"},
                {"AuthorizeNet_LoginID", "test2"},
                {"AuthorizeNet_TransactionKey", "test3"},
                {"AuthorizeNet_DelimData", "test4"},
                {"AuthorizeNet_DelimChar", "test5"},
                {"AuthorizeNet_RelayResponse", "test6"},
                {"AuthorizeNet_PaymentType", "test7"},
                {"AuthorizeNet_PaymentMethod", "test8"}
            };

            target.SetGatewayDictionary(input);

            Assert.AreEqual("test1", target.TransactionDetails["post_url"]);
        }

        [Test]
        public void TestTransactionDictionary()
        {
            var mock = Builder<PaymentData>.CreateNew().With(p => p.CardNumber = "4111111111111111").Build();

            var target = new LegacyCopayTransaction(null, null, mock);
            target.SetTransactionDictionary(2.00M);
            Assert.AreEqual("4111111111111111", target.TransactionDetails["x_card_num"]);
        }

        [Test]
        public void TestPaymentProfile()
        {
            long customerId = 14;
            var card = Builder<PaymentData>.CreateNew()
                .With(p => p.CardNumber = "4111111111111111")
                .With(p => p.ExpiryYear = DateTime.Now.Year + 1)
                .With(p => p.Cvv = "123")
                .Build();
            var gtw = new Mock<IPaymentProfileGateway>();
            gtw.Setup(m => m.AddCreditCard(customerId.ToString(), card)).Returns("Pass");
            IPaymentProfileApi target = new CimPaymentProfileApi(customerId, gtw.Object);
            var actual = target.AddPaymentMethod(card);
            Assert.AreEqual("Pass", actual.Id);
        }
        

        [Test]
        public void TestGatewayAdapter()
        {
            var mock = new Mock<ICustomerGateway>();
            mock.Setup(m => m.AddCreditCard("1", "4111111111111111", 1, 15, null)).Returns("hi");

            var adapter = new GatewayAdapter(mock.Object);
            var actual = adapter.AddCreditCard("1", "4111111111111111", 1, 15);
            Assert.AreEqual("hi", actual);
        }


        [Test, Explicit]
        public void TestJsonPaymentSubmit()
        {
            var gateway = new CimJsonGateway("https://apitest.authorize.net", MakeSettingsMock().Object,
                cimProfileId: "31863844",
                paymentProfileId: "28801196");

            var response = gateway.DoPayment(0.02M);

            Assert.AreEqual(response.ResponseCode, "1");
        }

        private static Mock<IAuthorizeNetSettings> MakeSettingsMock()
        {
            var mockSettings = new Mock<IAuthorizeNetSettings>();
            mockSettings.SetupGet(s => s.LoginId).Returns("867hTNWkm8");
            mockSettings.SetupGet(s => s.TransactionKey).Returns("7824m2DxJw3gGx3k");
            return mockSettings;
        }
    }
}