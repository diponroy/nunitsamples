﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Core.Models;
using SnapMD.Utilities;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class EncounterMetadataTests
    {
        [Test]
        public void TestEncounterMetadata()
        {
            var medicalHistory = new Mock<IMedicalHistory>();
            
            medicalHistory.SetupGet(h => h.MedicalCondition1).Returns(1);
            medicalHistory.SetupGet(h => h.MedicalCondition2).Returns(2);
            medicalHistory.SetupGet(h => h.AllergicMedication1).Returns(3);
            medicalHistory.SetupGet(h => h.TakingMedication1).Returns(55);

            var dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "I am a condition");
            dictionary.Add(2, "I am something, too");
            dictionary.Add(3, "Allergic to a med");

            var convertedMetadata = ConvertToMetadata(medicalHistory.Object, new EncounterConcern[0], dictionary);
            convertedMetadata.Wait();
            var result = convertedMetadata.Result.Metadata;
            //Assert.AreEqual(

            var expected = @"<intakeQuestionnaire xmlns=""https://snap.md/api/v2/xml/encounters"">
  <medicationAllergies xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <customCode code=""3"">
      <description>Allergic to a med</description>
    </customCode>
  </medicationAllergies>
  <surgeries xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <medicalConditions xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <customCode code=""1"">
      <description>I am a condition</description>
    </customCode>
    <customCode code=""2"">
      <description>I am something, too</description>
    </customCode>
  </medicalConditions>
  <medications xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <encounterConcerns />
</intakeQuestionnaire>".Replace("\r\n", "\n");

            /* <isVaccinationUpToDate />
              <isOneYearBelowChild />
              <isChildBornFullTerm />
              <isChildBornVaginally />
              <isChildDischargeMother />
            </medicalHistory>",*/
            var actual = result.Replace("\r\n", "\n");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public async void TestEncounterMetadataWithInfantAndConcerns()
        {
            var medicalHistory = new Mock<IMedicalHistory>();
            medicalHistory.SetupGet(h => h.IsChildBornVaginally).Returns("Y");
            medicalHistory.SetupGet(h => h.IsOneYearBelowChild).Returns("Y");

            var dictionary = new Dictionary<int, string>();
            var concerns = new []
            {
                new EncounterConcern {IsPrimary = true, CustomCode = new CustomCode { Description = "Concern Description" }},
                new EncounterConcern { CustomCode = new CustomCode {Description = "None"}}
            };

            var convertedMetadata = await ConvertToMetadata(medicalHistory.Object, concerns, dictionary);
            var result = convertedMetadata.Metadata;
            //Assert.AreEqual(

            var expected = @"<intakeQuestionnaire xmlns=""https://snap.md/api/v2/xml/encounters"">
  <medicationAllergies xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <surgeries xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <medicalConditions xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <medications xmlns=""https://snap.md/api/v2/xml/medicalhistory"" />
  <infantData xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <patientAgeUnderOneYear>Y</patientAgeUnderOneYear>
    <vaginalBirth>Y</vaginalBirth>
  </infantData>
  <encounterConcerns>
    <concern isPrimary=""true"">
      <customCode code=""0"">
        <description>Concern Description</description>
      </customCode>
    </concern>
    <concern isPrimary=""false"">
      <customCode code=""0"">
        <description>None</description>
      </customCode>
    </concern>
  </encounterConcerns>
</intakeQuestionnaire>".Replace("\r\n", "\n");

            var actual = result.Replace("\r\n", "\n");
            Assert.AreEqual(expected, actual);
        }

        [Test, Explicit]
        public void TestEncounterDynamic()
        {
            var medicalHistory = new
            {
                MedicalConditions = new[]
                {
                    new {cond = 1},
                    new {cond = 2}
                },
                MedicalCondition2 = 3,
                AllergicMedication1 = 3,
                TakingMedication1 = 55
            };

            var result = Extensions.ToXml(medicalHistory, "mh");
            // var result = convertedMetadata.Metadata;

            var expected = @"<mh>
  <medicalConditions>
    <medicalCondition>
      <cond>1</cond>
    </medicalCondition>
    <medicalCondition>
      <cond>2</cond>
    </medicalCondition>
  </medicalConditions>
  <medicalCondition2>3</medicalCondition2>
  <allergicMedication1>3</allergicMedication1>
  <takingMedication1>55</takingMedication1>
</mh>".Replace("\r\n", "\n");
            var actual = result.ToString().Replace("\r\n", "\n");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestDeserializeEncounterMetadata()
        {
            var target = @"<?xml version=""1.0"" encoding=""utf-8""?>
  <intakeQuestionnaire xmlns=""https://snap.md/api/v2/xml/encounters"">
  <medicationAllergies xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <customCode code=""3"">
      <description>Allergic to a med</description>
    </customCode>
  </medicationAllergies>
  <surgeries />
  <medicalConditions xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <customCode code=""1"">
      <description>I am a condition</description>
    </customCode>
    <customCode code=""2"">
      <description>I am something, too</description>
    </customCode>
  </medicalConditions>
  <medications />
  <encounterConcerns />
</intakeQuestionnaire>";

            var actual = new IntakeQuestionnaire().FromXml(target, XConversionOptions.FromCamelCase);
            Assert.AreEqual("I am something, too", actual.MedicalConditions.ElementAt(1).Description);
        }

        [Test]
        public async void TestDeserializeEncounterMetadataWithInfantAndConcerns()
        {
            var target = @"<intakeQuestionnaire xmlns=""https://snap.md/api/v2/xml/encounters"">
  <medicationAllergies xmlns=""https://snap.md/api/v2/xml/medicalhistory""/>
  <surgeries />
  <medicalConditions />
  <medications />
  <infantData xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
    <vaginalBirth>Y</vaginalBirth>
  </infantData>
  <encounterConcerns>
    <concern isPrimary=""true"">
        <customCode>
            <description>Concern Description</description>
        </customCode>
    </concern>
    <concern isPrimary=""false"">
        <customCode>
            <description>None</description>
        </customCode>
    </concern>
  </encounterConcerns>
</intakeQuestionnaire>";

            var actual = await new IntakeQuestionnaire().FromXmlAsync(target, XConversionOptions.FromCamelCase);
            Assert.AreEqual("Y", actual.InfantData.VaginalBirth);
            Assert.AreEqual("Concern Description", actual.Concerns.SingleOrDefault(c => c.IsPrimary).CustomCode.Description);
        }
        

        private async Task<EncounterMetadata> ConvertToMetadata(
            IMedicalHistory medicalHistory, EncounterConcern[] concerns, IDictionary<int, string> dictionary)
        {
            var metadata = new EncounterMetadata();
            var adapter = new IntakeQuestionnaireAdapter(medicalHistory, concerns, dictionary, null);
            var x = await adapter.ToXmlAsync(XConversionOptions.ToCamelCase);
            metadata.Metadata = x;
            return metadata;
        }
    }


    public static class Extensions
    {
        /// <summary>
        /// When serializing an anonymous type, this has to reside in the same assembly as the type that wants to use it, 
        /// otherwise reflection will fail.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="rootName"></param>
        /// <param name="avoidNestedTypes"></param>
        /// <returns></returns>
        public static XElement ToXml<T>(this T source, string rootName, bool avoidNestedTypes = false)
        {
            Type type = typeof(T);
            var root = new XElement(rootName);

            foreach (var property in type.GetProperties())
            {
                var value = (dynamic)property.GetValue(source, null);
                var name = property.Name;
                name = name.Substring(0, 1).ToLower() + name.Substring(1);

                var items = value as IEnumerable;
                if (items != null)
                {
                    var child = ParseCollection<T>(name, items);
                    root.Add(child);
                }
                else
                {
                    if (property.PropertyType.IsPrimitive ||
                        property.PropertyType == typeof(string))
                    {
                        root.Add(
                            new XElement(name, value));
                    }
                    else if (value == null)
                    {
                        root.Add(new XElement(name));
                    }
                    else if (!avoidNestedTypes)
                    {
                        // Avoid StackOverflow exceptions when types have backreferences.
                        root.Add(Extensions.ToXml(value, name));
                    }
                }
            }

            return root;
        }

        private static XElement ParseCollection<T>(string name, IEnumerable items)
        {
            var child = new XElement(name);
            var enumerator = items.GetEnumerator();
            while (enumerator.MoveNext())
            {
                child.Add(Extensions.ToXml((dynamic)enumerator.Current, name.TrimEnd('s')));
            }
            return child;
        }
    }
}