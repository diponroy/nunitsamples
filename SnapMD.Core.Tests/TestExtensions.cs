﻿using System.Collections;
using System.Xml.Linq;

namespace SnapMD.Core.Tests
{
    public static class TestExtensions
    {
        /// <summary>
        ///     This has to reside in the same assembly as the type that wants to use it, otherwise reflection will fail.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="rootName"></param>
        /// <param name="avoidNestedTypes"></param>
        /// <returns></returns>
        public static XElement ToXml<T>(this T source, string rootName, bool avoidNestedTypes = false)
        {
            var type = typeof (T);
            var root = new XElement(rootName);

            foreach (var property in type.GetProperties())
            {
                var value = (dynamic) property.GetValue(source, null);
                var name = property.Name;
                name = name.Substring(0, 1).ToLower() + name.Substring(1);

                var items = value as IEnumerable;
                if (items != null)
                {
                    var child = ParseCollection<T>(name, items);
                    root.Add(child);
                }
                else
                {
                    if (property.PropertyType.IsPrimitive ||
                        property.PropertyType == typeof (string))
                    {
                        root.Add(
                            new XElement(name, value));
                    }
                    else if (value == null)
                    {
                        root.Add(new XElement(name));
                    }
                    else if (!avoidNestedTypes)
                    {
                        // Avoid StackOverflow exceptions when types have backreferences.
                        root.Add(ToXml(value, name));
                    }
                }
            }

            return root;
        }

        private static XElement ParseCollection<T>(string name, IEnumerable items)
        {
            var child = new XElement(name);
            var enumerator = items.GetEnumerator();
            while (enumerator.MoveNext())
            {
                child.Add(ToXml((dynamic) enumerator.Current, name.TrimEnd('s')));
            }
            return child;
        }
    }
}