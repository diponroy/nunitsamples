﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Core.Models;
using SnapMD.Utilities;

namespace SnapMD.Core.Tests
{
    [TestFixture]
    public class PatientMetadataTests
    {
        [Test]
        public void TestMetadataSmall()
        {
            var medicalHistory = new Mock<IMedicalHistory>();
            
            medicalHistory.SetupGet(h => h.MedicalCondition1).Returns(1);
            medicalHistory.SetupGet(h => h.MedicalCondition2).Returns(2);
            medicalHistory.SetupGet(h => h.AllergicMedication1).Returns(3);
            medicalHistory.SetupGet(h => h.TakingMedication1).Returns(55);

            var dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "I am a condition");
            dictionary.Add(2, "I am something, too");
            dictionary.Add(3, "Allergic to a med");

            var convertedMetadata = ConvertToMetadata(medicalHistory.Object, dictionary);
            convertedMetadata.Wait();
            var result = convertedMetadata.Result.Metadata;
            //Assert.AreEqual(

            var expected = @"<medicalHistory xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
  <medicationAllergies>
    <customCode code=""3"">
      <description>Allergic to a med</description>
    </customCode>
  </medicationAllergies>
  <surgeries />
  <medicalConditions>
    <customCode code=""1"">
      <description>I am a condition</description>
    </customCode>
    <customCode code=""2"">
      <description>I am something, too</description>
    </customCode>
  </medicalConditions>
  <medications />
</medicalHistory>".Replace("\r\n", "\n");

            /* <isVaccinationUpToDate />
              <isOneYearBelowChild />
              <isChildBornFullTerm />
              <isChildBornVaginally />
              <isChildDischargeMother />
            </medicalHistory>",*/
            var actual = result.Replace("\r\n", "\n");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public async void TestMetadataWithInfant()
        {
            var medicalHistory = new Mock<IMedicalHistory>();
            medicalHistory.SetupGet(h => h.IsChildBornVaginally).Returns("Y");
            medicalHistory.SetupGet(h => h.IsOneYearBelowChild).Returns("Y");

            var dictionary = new Dictionary<int, string>();

            var convertedMetadata = await ConvertToMetadata(medicalHistory.Object, dictionary);
            var result = convertedMetadata.Metadata;
            //Assert.AreEqual(

            var expected = @"<medicalHistory xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
  <medicationAllergies />
  <surgeries />
  <medicalConditions />
  <medications />
  <infantData>
    <patientAgeUnderOneYear>Y</patientAgeUnderOneYear>
    <vaginalBirth>Y</vaginalBirth>
  </infantData>
</medicalHistory>".Replace("\r\n", "\n");
            
            var actual = result.Replace("\r\n", "\n");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestDeserialize()
        {
            var target = @"<?xml version=""1.0"" encoding=""utf-8""?>
  <medicalHistory xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
  <medicationAllergies>
    <customCode code=""3"">
      <description>Allergic to a med</description>
    </customCode>
  </medicationAllergies>
  <surgeries />
  <medicalConditions>
    <customCode code=""1"">
      <description>I am a condition</description>
    </customCode>
    <customCode code=""2"">
      <description>I am something, too</description>
    </customCode>
  </medicalConditions>
  <medications />
</medicalHistory>";

            var actual = new MedicalHistoryProfile().FromXml(target, XConversionOptions.FromCamelCase);
            Assert.AreEqual("I am something, too", actual.MedicalConditions.ElementAt(1).Description);
        }

        [Test]
        public async void TestDeserializeWithInfantData()
        {
            var target = @"<medicalHistory xmlns=""https://snap.md/api/v2/xml/medicalhistory"">
  <medicationAllergies />
  <surgeries />
  <medicalConditions />
  <medications />
  <infantData>
    <vaginalBirth>Y</vaginalBirth>
  </infantData>
</medicalHistory>";

            var actual = await new MedicalHistoryProfile().FromXmlAsync(target, XConversionOptions.FromCamelCase);
            Assert.AreEqual("Y", actual.InfantData.VaginalBirth);
        }
        

        private async Task<PatientMetadata> ConvertToMetadata(
            IMedicalHistory medicalHistory, IDictionary<int, string> dictionary)
        {
            var metadata = new PatientMetadata();
            var adapter = new MedicalProfileAdapter(medicalHistory, dictionary);
            var x = await adapter.ToXmlAsync(XConversionOptions.ToCamelCase);
            metadata.Metadata = x;
            return metadata;
        }
    }
}