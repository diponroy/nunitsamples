﻿using System;
using System.Net.Mail;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Core.Mail;
using SnapMD.Core.Mail.Templates;

namespace SnapMD.Core.Tests
{
    [TestFixture, Explicit]
    public class EmailTests
    {  //[Test]
        public void FailsWithNoRecipient()
        {
            var target = new SnapMail();
            Assert.Throws(typeof(InvalidOperationException), target.SendMail);
        }

        [Test]
        public void TestAppointmentEmail()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.PatientAppointment, "http://snap.local/");

            const string patientName = "patientName";
            const string physicianName = "physicianName";
            const string patientEmail = "aaron.lord@snap.md";
            
            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");
            
            var mail = new AppointmentNotification { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(
                () =>
                    mail.SendEmail(patientName, physicianName, patientEmail, DateTime.UtcNow));
            Assert.Pass();
        }

        [Test]
        public void TestStaffAppointmentEmail()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.StaffAppointment, "http://snap.local/");

            const string patientName = "patientName";
            const string physicianName = "physicianName";
            const string patientEmail = "aaron.lord@snap.md";


            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");

            var mail = new AppointmentNotification { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(
                () =>
                    mail.SendEmail(patientName, physicianName, patientEmail, DateTime.UtcNow));
            Assert.Pass();
        }

        [Test]
        public void TestActivationEmail()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.Activation, "http://snap.local/");

            const string displayName = "displayName";
            const string email = "aaron.lord@snap.md";
            const string token = "token1";

            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");

            var mail = new ActivationEmail { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(() => mail.SendEmail(displayName, email, token));

            Assert.Pass();
        }

        [Test]
        public void TestCoUserEmail()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.CoUserInvite, "http://snap.local/");

            const string email = "aaron.lord@snap.md";
            const string token = "token1";

            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");

            var mail = new NewUserInvitation { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(() => mail.SendEmail(email, token));

            Assert.Pass();
        }

        [Test]
        public void PatientPasswordResetEmail_Success()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.PasswordReset, "http://snap.local/");

            const string email = "aaron.lord@snap.md";
            const string token = "token1";

            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");

            var mail = new PasswordResetEmail { MessageFrom = new MailAddress("aaron.lord+fromtest@snap.md"), Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(() => mail.SendEmail("Aaron Lord", email, token));
        }
        [Test]
        public void TestSettingsUpdateEmail()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.UserSettingsUpdate, "http://snap.local/");

            const string email = "aaron.lord@snap.md";
            const string token = "token1";

            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");

            var mail = new SettingsUpdateConfirmation { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(() => mail.SendEmail(email, token));

            Assert.Pass();
        }

        [Test]
        public void TestAdminPasswordReset()
        {
            var factory = new MailTemplateFactory();
            var template = factory.GetMailTemplate(MailTemplates.AdminPasswordReset, "http://snap.local/");

            const string email = "aaron.lord@snap.md";
            const string token = "token1";

            var hospital = new Mock<IHospital>();
            hospital.SetupGet(h => h.HospitalName).Returns("Hello Test");
            var mail = new AdminPasswordResetEmail { Template = template, Hospital = hospital.Object };
            Assert.DoesNotThrow(() => mail.SendEmail(email, token));

            Assert.Pass();
        }
    }
}