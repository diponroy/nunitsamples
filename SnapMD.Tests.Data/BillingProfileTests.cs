﻿using System;
using System.Collections.Generic;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Billing;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class BillingProfileTests
    {
        [Test]
        public void TestGet()
        {
            IList<BillingProfile> set = Builder<BillingProfile>.CreateListOfSize(20).Build();
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.BillingProfiles).Returns(set.ToDbSet());
            mock.SetupGet(x => x.PatientProfiles).Returns(new List<PatientProfile> { patient2 }.ToDbSet	());
            var repo = new BillingProfileRepository(mock.Object);
            BillingProfile actual = repo.Get(14, 14);
            Assert.IsNotNull(actual);
            Assert.AreEqual(14, actual.CreatedBy);
        }

        /// <summary>
        /// This repository does not implement the Get method from the interface because we are calling by user ID.
        /// </summary>
        [Test]
        public void TestGetNotImplemented()
        {
            var mock = new Mock<ISnapContext>();
            var repo = new BillingProfileRepository(mock.Object);
            Assert.Throws<NotImplementedException>(() =>
                repo.Get(1));
        }

        [Test]
        public void TestInsert()
        {
            IList<BillingProfile> set = new List<BillingProfile>();
            BillingProfile @new = Builder<BillingProfile>.CreateNew().Build();

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.BillingProfiles).Returns(set.ToDbSet());

            var repo = new BillingProfileRepository(mock.Object);
            BillingProfile actual = repo.Add(@new);
            Assert.IsNotNull(actual);
        }
    }
}