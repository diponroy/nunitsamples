﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class SnapSystemLogTest
    {
        private readonly Mock<ISnapContext> _context = new Mock<ISnapContext>();

        [SetUp]
        public void SetupTestData()
        {
            var service = Builder<ServiceLog>.CreateListOfSize(3).Build();

            _context.Setup(c => c.ServiceLogs).Returns(service.ToDbSet());
        }

        [Test]
        public void TestGetAllServiceLog()
        {
            var repo = new ServiceLogRepository(_context.Object);
            Assert.AreEqual(3, repo.GetServiceLogList().Count(), "Should Return All");
        }

        [Test]
        public void TestGetAllServiceWithCondition()
        {
            var repo = new ServiceLogRepository(_context.Object);
            Assert.AreEqual(1, repo.GetServiceLogList(c => c.ServiceId == 1).Count(), "Should Return Only 1");
        }
    }
}