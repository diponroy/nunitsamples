﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class CalendarTests
    {
        [Test]
        public void TestScheduleSlot()
        {
            var builder = Builder<ScheduleSlotCalendar>.CreateListOfSize(5).Build();
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.ScheduleSlotCalendars).Returns(builder.ToDbSet());
            var repo = new ScheduleSlotCalendarRepository(mock.Object);
            var calendar = repo.GetDoctorSchedule(5);
            var val = calendar.Any(s => s.SlotStartTime <= DateTime.UtcNow
                                            && s.SlotEndTime >= DateTime.UtcNow);
            Assert.Inconclusive();
        }
    }
}
