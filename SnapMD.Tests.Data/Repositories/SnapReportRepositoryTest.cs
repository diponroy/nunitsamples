﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Consultations;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class SnapReportRepositoryTest
    {
        protected Mock<ISnapContext> DbContext;
        protected SnapReportRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        private void InitializeRepo()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Repo = new SnapReportRepository(DbContext.Object);
        }

        private IDbSet<ChatMessage> BuildChatMessage(int sampleNumber)
        {
            return Builder<ChatMessage>.CreateListOfSize(sampleNumber).Build().ToDbSet();
        }

        private IDbSet<Consultation> BuildConsultation(params Consultation[] data)
        {
            var sampleNumber = data.Count();
            //var _data = Builder<Consultation>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            var _data = Builder<Consultation>.CreateListOfSize(sampleNumber).All().With(x => x.ScheduledConsultationTemps = new List<ScheduledConsultationTemp>() { new ScheduledConsultationTemp() }).Build().ToDbSet();
            var index = 0;
            foreach (var item in _data)
            {
                var info = data[index];
                index++;
                item.ConsultationId = info.ConsultationId;
                item.PatientId = info.PatientId;
                item.AssignedDoctorId = info.AssignedDoctorId;
                item.ConsultantUserId = info.ConsultantUserId;
            }
            return _data;
        }


        private IDbSet<Code> BuildCodes(params Code[] data)
        {
            var sampleNumber = data.Count();
            var _data = Builder<Code>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            var index = 0;
            foreach (var item in _data)
            {
                var info = data[index];
                index++;
                item.HospitalId = info.HospitalId;
                item.CodeId = info.CodeId;
            }
            return _data;
        }

        private IDbSet<HospitalStaffProfile> BuildHospitalStaffProfile(params HospitalStaffProfile[] data)
        {
            var sampleNumber = data.Count();
            var _data = Builder<HospitalStaffProfile>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            var index = 0;
            foreach (var item in _data)
            {
                var info = data[index];
                index++;
                item.HospitalId = info.HospitalId;
                item.UserId = info.UserId;
                item.StaffId = info.StaffId;
            }
            return _data;
        }

        private IDbSet<PatientProfile> BuildPatient(params PatientProfile[] data)
        {
            var sampleNumber = data.Count();
            var _data = Builder<PatientProfile>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            var index = 0;
            foreach (var item in _data)
            {
                var info = data[index];
                index++;
                item.PatientId = info.PatientId;
                item.UserId = info.UserId;
                item.HeightUnit = info.HeightUnit;
            }
            return _data;
        }

        [Test]
        public void RepoInitialization()
        {
            Assert.DoesNotThrow(() => new ClinicianSettingsRepository(DbContext.Object));
        }

        [Test]
        public void GetChatNoteTest()
        {
            var chatMessage = BuildChatMessage(4);
            chatMessage.FirstOrDefault().ConsultationId = 1;
            chatMessage.FirstOrDefault().PatientId = 3;
            var consultationList = BuildConsultationData();
            var patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6
            },
                new PatientProfile
                {
                    PatientId = 4,
                    UserId = 8
                });

            var hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            },
                new HospitalStaffProfile
                {
                    HospitalId = 1,
                    UserId = 2
                },
                new HospitalStaffProfile
                {
                    HospitalId = 1,
                    UserId = 3
                });
            DbContext.Setup(x => x.ChatMessages).Returns(chatMessage);
            DbContext.Setup(x => x.Consultations).Returns(consultationList);
            DbContext.Setup(x => x.PatientProfiles).Returns(patientList);
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaff);


            InitializeRepo();
            var chatInfo = Repo.GetChatNote(1);
            Assert.AreEqual(chatInfo.Count(), 1);
        }

        [Test]
        public void GetChatNoteNoDataTest()
        {
            var chatMessage = BuildChatMessage(4);
            var consultationList = BuildConsultationData();
            var patientList = BuildPatient(new PatientProfile
                {
                    PatientId = 3,
                    UserId = 6
                },
                new PatientProfile
                {
                    PatientId = 4,
                    UserId = 8
                });

            var hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
                {
                    HospitalId = 1,
                    UserId = 1
                },
                new HospitalStaffProfile
                {
                    HospitalId = 1,
                    UserId = 2
                },
                new HospitalStaffProfile
                {
                    HospitalId = 1,
                    UserId = 3
                });

            DbContext.Setup(x => x.ChatMessages).Returns(chatMessage);
            DbContext.Setup(x => x.Consultations).Returns(consultationList);
            DbContext.Setup(x => x.PatientProfiles).Returns(patientList);
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaff);

            InitializeRepo();
            var chatInfo = Repo.GetChatNote(10);
            Assert.AreEqual(chatInfo.Count(), 0);
        }

        [Test]
        public void GetConsultationReportTest()
        {
            SetUpData();
            var xdoc = new IntakeQuestionnaire().ToXml(XConversionOptions.None);

            var metadata = new List<EncounterMetadata>
            {
                new EncounterMetadata
                {
                    ConsultationId = 1,
                    Metadata = xdoc
                }
            };

            DbContext.SetupGet(c => c.EncounterMetadata).Returns(metadata.ToDbSet());

            DbContext.Setup(c => c.ConsultationSoapNotesCptCodes).Returns(new List<ConsultationSoapNotesCptCode>
                {
                    new ConsultationSoapNotesCptCode {
                        Id = 1,
                        ConsultationId = 1,
                        CptCode = "0001",
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = 0
                        
                    }
                }.ToDbSet());

            DbContext.Setup(c => c.CptCodes).Returns(new List<CptCode>
                {
                    new CptCode {
                        CptCodeId = 22,
                        CptCode_ = "0001",
                         Description = string.Empty,
                        IsActive = "A",
                        CptcodeMasterId = 5
                    }
                }.ToDbSet());

            DbContext.SetupGet(c => c.ConsultationMedicalCodes).Returns(new List<ConsultationMedicalCode>
            {
                new ConsultationMedicalCode()
                {
                    ConsultationId = 1,
                    MedicalCodeId = 1,
                    MedicalCode = new MedicalCode
                    {
                        MedicalCodeID = 1,
                        CodeValue = "AA345",
                        ShortDescription = "ShortDescription",
                        MedicalCodingSystem = new MedicalCodingSystem
                        {
                            Name = "ICD-10-DX"
                        }
                        //Other fields were skipped for brevity
                    }
                }
            }.ToDbSet());
            

            var reportInfo = Repo.GetConsultationReport(1);
            var info = reportInfo.FirstOrDefault();
            Assert.AreEqual(reportInfo.Count(), 1);

            Assert.AreEqual(info.PatientId, 3);
            Assert.AreEqual(info.HospitalId, 1);
        }

        [Test]
        public void GetConsultationReportNoRecordTest()
        {
            SetUpData();
            var reportInfo = Repo.GetConsultationReport(20);

            Assert.AreEqual(reportInfo.Count(), 0);
        }

        [Test]
        public void GetConsultationReportDetailsNODataTest()
        {
            SetUpData();
            DbContext.Setup(c => c.PrescriptionInfoes).Returns(new List<PrescriptionInfo>
            {
                new PrescriptionInfo
                {
                    PatConsultationId = 1,
                    DrugName = string.Empty,
                    DrugDosage = "",
                    Quality = "1",
                    NoRefills = 1
                }
            }.ToDbSet());
            var reportInfo = Repo.GetConsultationReportDetails(20);

            Assert.AreEqual(reportInfo.Rows.Count, 0);
        }

        [Test]
        public void GetConsultationReportDetailsTest()
        {
            SetUpData();

            var xdoc = new IntakeQuestionnaire().ToXml(XConversionOptions.None);

            var metadata = new List<EncounterMetadata>
            {
                new EncounterMetadata
                {
                    ConsultationId = 1,
                    Metadata = xdoc
                }
            };

            DbContext.Setup(c => c.PrescriptionInfoes).Returns(new List<PrescriptionInfo>
            {
                new PrescriptionInfo
                {
                    PatConsultationId = 1,
                    DrugName = string.Empty,
                    DrugDosage = "",
                    Quality = "1",
                    NoRefills = 1
                }
            }.ToDbSet());

            DbContext.Setup(c => c.ConsultationSoapNotesCptCodes).Returns(new List<ConsultationSoapNotesCptCode>
                {
                    new ConsultationSoapNotesCptCode {
                        Id = 1,
                        ConsultationId = 1,
                        CptCode = "0001",
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = 0
                        
                    }
                }.ToDbSet());

            DbContext.Setup(c => c.ConsultationMedicalCodes).Returns(new List<ConsultationMedicalCode>
                {
                    new ConsultationMedicalCode {
                        Id = 1,
                        ConsultationId = 1,
                        MedicalCodeId = 1
                    }
                }.ToDbSet());

            DbContext.Setup(c => c.CptCodes).Returns(new List<CptCode>
                {
                    new CptCode {
                        CptCodeId = 22,
                        CptCode_ = "0001",
                         Description = string.Empty,
                        IsActive = "A",
                        CptcodeMasterId = 5
                    }
                }.ToDbSet());

            DbContext.SetupGet(c => c.ConsultationMedicalCodes).Returns(new List<ConsultationMedicalCode>
            {
                new ConsultationMedicalCode()
                {
                    ConsultationId = 1,
                    MedicalCodeId = 1,
                    MedicalCode = new MedicalCode
                    {
                        MedicalCodeID = 1,
                        CodeValue = "AA345",
                        ShortDescription = "ShortDescription",
                        MedicalCodingSystem = new MedicalCodingSystem
                        {
                            Name = "ICD-10-DX"
                        }
                        //Other fields were skipped for brevity
                    }
                }
            }.ToDbSet());
            


            DbContext.SetupGet(c => c.EncounterMetadata).Returns(metadata.ToDbSet());

            var reportInfo = Repo.GetConsultationReportDetails(1);
            Assert.AreEqual(reportInfo.Rows.Count, 1);
        }

        private void SetUpData()
        {
            var chatMessage = BuildChatMessage(4);
            chatMessage.FirstOrDefault().ConsultationId = 1;
            chatMessage.FirstOrDefault().PatientId = 3;
            var consultationList = BuildConsultationData();
            var patientList = BuildPatient(new PatientProfile
                {
                    PatientId = 3,
                    UserId = 6,
                    HeightUnit = 1
                },
                new PatientProfile
                {
                    PatientId = 4,
                    UserId = 8,
                    HeightUnit = 1,
                    Organization = new Organization { Name = "Test Org" },
                    Location = new Location { Name = "Test Loc" }
                });
            var codeList = BuildCodes(new Code
            {
                HospitalId = 1,
                CodeId = 1
            });
            var hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            },
                new HospitalStaffProfile
                {
                    MedicalSpecialityId = 1,
                    HospitalId = 1,
                    UserId = 2
                },
                new HospitalStaffProfile
                {
                    MedicalSpecialityId = 1,
                    HospitalId = 1,
                    UserId = 3
                });
            DbContext.Setup(x => x.ChatMessages).Returns(chatMessage);
            DbContext.Setup(x => x.Consultations).Returns(consultationList);
            DbContext.Setup(x => x.PatientProfiles).Returns(patientList);
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaff);
            DbContext.Setup(x => x.Codes).Returns(codeList);

            BuildHospital();
            BuildMedicalHistory();
            DbContext.Setup(x => x.DoctorSpecialities).Returns(new List<DoctorSpeciality>
            {
                new DoctorSpeciality
                {
                    HospitalId = 1,
                    SpecialityId = 1
                }
            }.ToDbSet());

            DbContext.Setup(x => x.PatientConsultationReports).Returns(new List<PatientConsultationReport>
            {
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 108,
                    ConsultationDescription = "Test Data 1"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 109,
                    ConsultationDescription = "Test Data 2"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 110,
                    ConsultationDescription = "Test Data 3"
                }
            }.ToDbSet());

            InitializeRepo();
        }

        private void BuildHospital()
        {
            var hospitalId = new List<Hospital>
            {
                new Hospital
                {
                    HospitalId = 1,
                    IsActive = "Y",
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            IsActive = true,
                            AddressText = "Test Address"
                        }
                    }
                }
            };

            DbContext.Setup(c => c.Hospitals).Returns(hospitalId.ToDbSet());
        }

        private void BuildMedicalHistory()
        {
            var _data = Builder<PatientMedicalHistory>.CreateListOfSize(2).Build();
            var info = _data.FirstOrDefault();
            info.PatientId = 3;
            info.HistoryId = 1;
            info.MedicalCondition1 = 94;
            info.MedicalCondition2 = 94;
            info.MedicalCondition3 = 94;
            info.MedicalCondition4 = 94;
            info.TakingMedication1 = 94;
            info.TakingMedication2 = 94;
            info.TakingMedication3 = 94;
            info.TakingMedication4 = "";
            info.AllergicMedication1 = 94;
            info.AllergicMedication2 = 94;
            info.AllergicMedication3 = 94;
            info.AllergicMedication4 = 94;

            var info1 = _data.Skip(1).FirstOrDefault();

            info1.PatientId = 4;
            info1.HistoryId = 1;
            info1.MedicalCondition1 = 94;
            info1.MedicalCondition2 = 94;
            info1.MedicalCondition3 = 94;
            info1.MedicalCondition4 = 94;
            info1.TakingMedication1 = 94;
            info1.TakingMedication2 = 94;
            info1.TakingMedication3 = 94;
            info1.TakingMedication4 = "";
            info1.AllergicMedication1 = 94;
            info1.AllergicMedication2 = 94;
            info1.AllergicMedication3 = 94;
            info1.AllergicMedication4 = 94;

            DbContext.Setup(c => c.PatientMedicalHistories).Returns(_data.ToDbSet());
        }

        private IDbSet<Consultation> BuildConsultationData()
        {
            return BuildConsultation(new Consultation
            {
                ConsultationId = 1,
                PatientId = 3,
                AssignedDoctorId = 1,
                ConsultantUserId = 6
            },
                new Consultation
                {
                    ConsultationId = 2,
                    PatientId = 3,
                    AssignedDoctorId = 1,
                    ConsultantUserId = 6
                },
                new Consultation
                {
                    ConsultationId = 3,
                    PatientId = 4,
                    AssignedDoctorId = 2,
                    ConsultantUserId = 8
                },
                new Consultation
                {
                    ConsultationId = 4,
                    PatientId = 4,
                    AssignedDoctorId = 3,
                    ConsultantUserId = 8
                });
        }
    }
}
