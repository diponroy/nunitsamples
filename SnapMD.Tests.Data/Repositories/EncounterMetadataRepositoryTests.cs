﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories.Consultations;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class EncounterMetadataRepositoryTests
    {
        [Test]
        public void TestMetadataRepositoryGet()
        {
            var guid = Guid.NewGuid();
            var mockData = new Mock<ISnapContext>();
            var list = new EncounterMetadata { Id = guid, ConsultationId = 2 };
            mockData.SetupGet(c => c.EncounterMetadata).Returns((new List<EncounterMetadata> { list }).ToDbSet());
            var repository = new EncounterMetadataRepository(mockData.Object);
            var result = repository.GetMetadata(2);

            Assert.AreEqual(guid, result.Id);
        }

        [Test]
        public void TestUpsertException()
        {
            var mock = new Mock<IPatientMetadata>();
            var target = new EncounterMetadataRepository(null);
            Assert.Throws<NotImplementedException>(() => target.Upsert(mock.Object));
        }
    }
}
