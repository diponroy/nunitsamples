﻿using System;
using System.Collections.Generic;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class RoleFunctionRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected RoleFunctionRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new RoleFunctionRepository(MockedDb.Object);
        }

        [Test]
        [TestCase("A", "A", "A", "A", true)]
        [TestCase("I", "A", "A", "A", false)]
        [TestCase("A", "I", "A", "A", false)]
        [TestCase("A", "A", "I", "A", false)]
        [TestCase("A", "A", "A", "I", false)]
        public void HasPatientInteraction_Permission(string functionStatus, string roleStatus, string roleOnOff, string roleFunctionStatus, bool expected)
        {
            var function = Builder<SnapFunction>.CreateNew()
                .With(x => x.FunctionId = 4)
                .And(x => x.Description = "Patient Facing")
                .And(x => x.IsActive = functionStatus)
                .Build();
            var role = Builder<Role>.CreateNew()
                .With(x => x.IsActive = roleStatus)
                .And(x => x.OnOff = roleOnOff)
                .Build();
            var roleFunction = Builder<RoleFunction>.CreateNew()
                .With(x => x.RoleId = role.RoleId)
                .And(x => x.FunctionId = function.FunctionId)
                .And(x => x.IsActive = roleFunctionStatus)
                .Build();

            MockedDb.Setup(x => x.Roles).Returns(new List<Role>() { role }.ToDbSet());
            MockedDb.Setup(x => x.SnapFunctions).Returns(new List<SnapFunction>() { function }.ToDbSet());
            MockedDb.Setup(x => x.RoleFunctions).Returns(new List<RoleFunction>() { roleFunction }.ToDbSet());
            InitializeRepo();

            var result = Repo.CheckHasPatientInteraction(new List<int>() { role.RoleId });
            Assert.AreEqual(expected, result);
        }

    }
}
