﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Consultations;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class PatientMedicalHistoryTests
    {
        [Test]
        public void TestUpsert()
        {
            var context = new Mock<ISnapContext>();
            var list = new List<PatientMedicalHistory>().ToDbSet();
            context.SetupGet(c => c.PatientMedicalHistories).Returns(list);
            
            var target = new PatientMedicalHistoryRepository(context.Object);
            DateTime created = DateTime.UtcNow;
            var source = new PatientMedicalHistory();
            target.Upsert(source);
            Assert.GreaterOrEqual(source.CreateDate, created);
        }

    }
}
