﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class ConsultationPaymentsRepositoryTest
    {
        protected Mock<ISnapContext> DbContextMock;
        protected ConsultationPaymentsRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContextMock = new Mock<ISnapContext>();
        }

        private void InitializeRepo()
        {
            if (DbContextMock == null)
            {
                throw new NullReferenceException("DbContextMock");
            }
            Repo = new ConsultationPaymentsRepository(DbContextMock.Object);
        }

        protected IList<TEntity> BuildList<TEntity>(int size)
        {
            return Builder<TEntity>.CreateListOfSize(size).Build();
        }
            
        public void RepoInitialization()
        {
            Assert.DoesNotThrow(() => new ConsultationPaymentsRepository(DbContextMock.Object));
        }

        /*
         * finds payment detail by consultaionId
         */
        [Test]
        public void FindByConsultationId_Fields()
        {
            //data
            var consultaionId = 100;
            var paidDetail = Builder<ConsultationPaymentsDetail>.CreateNew()
                                .With(x => x.ConsultationId = consultaionId)
                                .Build();
            var data = BuildList<ConsultationPaymentsDetail>(10);
            data.Add(paidDetail);

            //mock
            DbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(data.ToDbSet);
            InitializeRepo();

            //tests
            ConsultationPaymentsDetail result = Repo.FindByConsultationId(consultaionId);
            Assert.IsNotNull(result);
            Assert.AreEqual(paidDetail.PaymentId, result.PaymentId);
            Assert.AreEqual(paidDetail.PaymentStatusCode, result.PaymentStatusCode);
            Assert.AreEqual(paidDetail.PaymentStatusDescription, result.PaymentStatusDescription);
            Assert.AreEqual(paidDetail.PaymentStatusTransactionId, result.PaymentStatusTransactionId);
            Assert.AreEqual(paidDetail.CreatedDate, result.CreatedDate);
            Assert.AreEqual(paidDetail.CreatedBy, result.CreatedBy);
            Assert.AreEqual(consultaionId, result.ConsultationId);
            Assert.AreEqual(paidDetail.PaymentAmount, result.PaymentAmount);
        }

        [Test]
        public void FindByConsultationId_NotFound_Retuns_Null()
        {
            var consultaionId = 1;
            DbContextMock.Setup(x => x.ConsultationPaymentsDetails)
                .Returns(new List<ConsultationPaymentsDetail>().ToDbSet);
            InitializeRepo();

            Assert.IsNull(Repo.FindByConsultationId(consultaionId));
        }

    }
}
