﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class UserTokenRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected UserTokenRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new UserTokenRepository(MockedDb.Object);
        }

        /*
         * Gets user token by token id
         *     if not found returns null
         *     if found more than one throws error
         */
        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void Get_By_TokenId_Found_Returns_Object(string entityStatus)
        {
            UserToken aUserToken = Builder<UserToken>.CreateNew().With(x => x.TokenStatus = entityStatus).Build();
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { aUserToken }.ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.Get(aUserToken.TokenId));
            var result = Repo.Get(aUserToken.TokenId);
            Assert.IsInstanceOf<UserToken>(result);
            Assert.AreEqual(aUserToken.TokenId, result.TokenId);
        }

        [Test]
        public void Get_By_TokenId_NotFound_Returns_Null()
        {
            int TokenId = 100;
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>().ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.Get(TokenId));
            var result = Repo.Get(TokenId);
            Assert.IsNull(result);
        }

        [Test]
        public void Get_By_TokenId_Found_MoreThanOne_Throws_Error()
        {
            int TokenId = 100;
            List<UserToken> userTokens = Builder<UserToken>.CreateListOfSize(2).All()
                                .With(x => x.TokenId = TokenId)
                                .Build().ToList();
            MockedDb.Setup(x => x.UserTokens).Returns(userTokens.ToDbSet());
            InitializeRepo();

            Assert.Catch<Exception>(() => Repo.Get(TokenId));
        }

        /*
         * FindActiveOnboardToken by token
         *      where entity is active (status 'A')
         *      codeset id CodeSetEnum.NewUserOnboardToken
         */

        [Test]
        [TestCase("my token")]
        [TestCase("My Token")]
        [TestCase("MY TOKEN")]
        public void FindActiveOnboardToken_Found_Returns_Entity(string token)
        {
            UserToken entityToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token = "my token")
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "A")
                .Build();

            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { entityToken }.ToDbSet());
            InitializeRepo();

            var result = Repo.FindActiveOnboardToken(token);
            Assert.IsNotNull(result);
            Assert.IsTrue(token.ToLower().Equals(result.Token.ToLower()));
            Assert.AreEqual("A", result.TokenStatus);
            Assert.AreEqual(entityToken.UserId, result.UserId);
            Assert.AreEqual(entityToken.TokenId, result.TokenId);
        }

        [Test]
        [TestCase(CodeSetEnum.NewUserOnboardToken, "E")]     //status not active
        [TestCase(CodeSetEnum.NewUserOnboardToken, "")]
        [TestCase(CodeSetEnum.NewUserOnboardToken, null)]
        [TestCase(CodeSetEnum.New_User_Registation_Token, "A")] // code set not for on boarding
        [TestCase(CodeSetEnum.Reset_Password, "A")]
        public void FindActiveOnboardToken_NotFound_Returns_Null(CodeSetEnum codeSet, string entityStatus)
        {
            string token = "asfdsdfssfsdfsd";
            UserToken entityToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token == token)
                .And(x => x.CodeSetId = (int)codeSet)
                .And(x => x.TokenStatus = entityStatus)
                .Build();

            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { entityToken }.ToDbSet());
            InitializeRepo();

            Assert.IsNull(Repo.FindActiveOnboardToken(token));
        }

        [Test]
        public void Token()
        {
            var expectedLength = 30;
            List<String> tokens = new List<string>();
            for (int i = 0; i < 1000; i++)
            {
                var token = UserTokenRepository.Token();
                string failMsg = String.Format("Test failled at iteration No. {0}, legnth = , for token = {1}", i, expectedLength, token);

                Assert.AreEqual(expectedLength, token.Length, failMsg);
                Assert.True(token.All(char.IsLetterOrDigit), failMsg);
                Assert.IsFalse(token.Any(char.IsLower), failMsg);
                Assert.IsFalse(tokens.Contains(token), failMsg);
                tokens.Add(token);
            }
        }
    }
}
