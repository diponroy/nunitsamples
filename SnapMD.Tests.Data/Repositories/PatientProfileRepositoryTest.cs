﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class PatientProfileRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected PatientProfileRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new PatientProfileRepository(MockedDb.Object);
        }

        [Test]
        public void Get()
        {
            const int patientId = 1;

            List<PatientProfile> list = Builder<PatientProfile>.CreateListOfSize(5).Build().ToList();
            MockedDb.SetupGet(c => c.PatientProfiles).Returns(list.ToDbSet());
            InitializeRepo();

            PatientProfile result = Repo.Get(patientId);
            Assert.IsNotNull(result);
            Assert.AreEqual(patientId, result.PatientId);
        }

        [Test]
        public void GetAll()
        {
            List<PatientProfile> list = Builder<PatientProfile>.CreateListOfSize(5).Build().ToList();
            MockedDb.SetupGet(c => c.PatientProfiles).Returns(list.ToDbSet());
            InitializeRepo();

            IEnumerable<PatientProfile> result = Repo.GetAll();
            Assert.AreEqual(result.Count(), list.Count);
        }

        [Test]
        public void CodeEyeColor()
        {
            const int patientId = 1;

            var cdBt = new Code { CodeId = 5, Description = "Blue", IsActive = "A", HospitalId = 1 };
            List<PatientProfile> list =
                Builder<PatientProfile>.CreateListOfSize(5).All().With(x => x.Code_EyeColor = cdBt).Build().ToList();
            MockedDb.SetupGet(c => c.PatientProfiles).Returns(list.ToDbSet());
            InitializeRepo();

            PatientProfile result = Repo.Get(patientId); //fails on get userid
            Assert.AreEqual(result.Code_EyeColor.Description, "Blue");
            Assert.AreEqual(result.Code_EyeColor.CodeId, 5);
        }

        [Test]
        public void GetPatientProfileDetailsByConsultationId()
        {
            const int consultationId = 1;
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Codes).Returns(new List<Code>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.GetPatientProfileDetailsByConsultationId(consultationId));
        }

        [Test]
        public void GetPatientDepedentProfileDetailsByConsultationId()
        {
            const int consultationId = 1;

            var consultation = Builder<Consultation>.CreateNew().With(x => x.ConsultantUserId = consultationId).Build();


            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Codes).Returns(new List<Code>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            MockedDb.Setup(x => x.Consultations).Returns(new List<Consultation>() {consultation}.ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.GetPatientDepedentProfileDetailsByConsultationId(consultationId));
        }

        [Test]
        public void TestHeightUnitAndWeightUnit()
        {
            const int consultationId = 1;

            var patientProfiles = Builder<PatientProfile>.CreateListOfSize(1).All().Build();
            patientProfiles.ForEach(p =>
            {
                p.HeightUnit = 1;
                p.WeightUnit = 2;
            });
            MockedDb.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());

            var codes = Builder<Code>.CreateListOfSize(2).All().Build();
            MockedDb.Setup(x => x.Codes).Returns(codes.ToDbSet());

            var patientAddresses = Builder<PatientAddress>.CreateListOfSize(1).All().Build();
            MockedDb.Setup(x => x.PatientAddresses).Returns(patientAddresses.ToDbSet());

            var familyProfiles = Builder<FamilyProfile>.CreateListOfSize(1).All().Build();
            MockedDb.Setup(x => x.FamilyProfiles).Returns(familyProfiles.ToDbSet());

            var users = Builder<User>.CreateListOfSize(1).All().Build();
            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());

            var addresses = Builder<Address>.CreateListOfSize(1).All().Build();
            MockedDb.Setup(x => x.Addresses).Returns(addresses.ToDbSet());

            var consultations = Builder<Consultation>.CreateListOfSize(1).All().Build();
            MockedDb.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            InitializeRepo();

            var result = Repo.GetPatientProfileDetailsByConsultationId(consultationId);
            Assert.AreEqual("Description1", result.HeightUnit);
            Assert.AreEqual("Description2", result.WeightUnit); 
        }


        /*
         * Find patient Id by user id 
         *      from PatientProfile tbl
         */
        [Test]
        public void GetPatientId_FoundPatient_Returns_PatientId()
        {
            const int userId = 100;
            var patient = Builder<PatientProfile>.CreateNew().With(x => x.UserId = userId).Build();
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>(){ patient }.ToDbSet());
            InitializeRepo();
            Assert.AreEqual(patient.PatientId, Repo.GetPatientId(userId));
        }

        [Test]
        public void GetPatientId_NotFound_Returns_Null()
        {
            const int userId = 100;
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            InitializeRepo();
            Assert.IsNull(Repo.FindByUserId(userId));
        }


        /*
         * Find patient profile entity by user id 
         *      from PatientProfile tbl
         */
        [Test]
        public void FindByUserId_FoundPatient_Returns_PatientProfile()
        {
            int userId = 100;
            List<PatientProfile> patientProfiles = Builder<PatientProfile>.CreateListOfSize(2).Build().ToList();
            var patient = Builder<PatientProfile>.CreateNew().With(x => x.PatientId = 10).And(x => x.UserId = userId).Build();
            patientProfiles.Add(patient);

            MockedDb.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            InitializeRepo();

            var result = Repo.FindByUserId(userId);
            Assert.IsInstanceOf<PatientProfile>(result);
            Assert.AreEqual(patient.PatientId, result.PatientId);
        }

        [Test]
        public void FindByUserId_NotFound_Returns_Null()
        {
            int userId = 100;
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            InitializeRepo();
            Assert.IsNull(Repo.FindByUserId(userId));
        }
    }
}