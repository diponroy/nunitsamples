﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class TimeZoneRepositoryTests
    {
        [Test]
        public void TestUserTime()
        {
            var users = Builder<User>.CreateListOfSize(50).All().With(u => u.TimeZoneId = 24).Build();
            var times = Builder<StandardTimeZone>.CreateListOfSize(24).Build();
            times.ElementAt(23).TimeZoneName = "Pacific Standard Time";

            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.StandardTimeZones).Returns(times.ToDbSet());
            context.SetupGet(c => c.Users).Returns(users.ToDbSet());
            var timeRepo = new TimeZoneRepository(context.Object);

            var target = new DateTime(1, 1, 1, 9, 0, 0);
            var expected = new DateTime(1, 1, 1, 1, 0, 0);

            var actual = timeRepo.GetUserTime(target, 1);
            Assert.AreEqual(expected, actual);

        }
    }
}
