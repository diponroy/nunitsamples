﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class HospitalStaffRepositoryTests
    {
        private ISnapContext MockContext(int cliniciansCount, bool use2Adms = false)
        {
            var mockContext = new Mock<ISnapContext>();
            var userRoles = new List<UserRole>
            {
                new UserRole
                {
                    RoleId = 1,
                    UserId = 1,
                    IsActive = "A"
                }
            };
            var users = new List<User>
            {
                new User
                {
                    HospitalId = 1,
                    IsActive = "A",
                    UserId = 1,
                    UserRoles = new List<UserRole>
                    {
                        userRoles[0]
                    }
                },
                new User
                {
                    HospitalId = 1,
                    IsActive = "A",
                    UserId = 2
                }
            };

            if (use2Adms)
            {
                userRoles.Add(new UserRole
                {
                    RoleId = 1,
                    UserId = 2,
                    IsActive = "A"
                });
                users[1].UserRoles = new List<UserRole> { userRoles[1] };
            }

            mockContext.SetupGet(x => x.Hospitals).Returns(new List<Hospital>
            {
                new Hospital
                {
                    HospitalId = 1,
                    IsActive = "A",
                    CliniciansCount = cliniciansCount
                }
            }.ToDbSet());
            mockContext.SetupGet(x => x.RoleFunctions).Returns(new List<RoleFunction>
            {
                new RoleFunction
                {
                    FunctionId = 1,
                    RoleId = 1,
                    IsActive = "A"
                },
                new RoleFunction
                {
                    FunctionId = 11,
                    RoleId = 2,
                    IsActive = "A"
                }
            }.ToDbSet());

            mockContext.SetupGet(x => x.UserRoles).Returns(userRoles.ToDbSet());
            mockContext.SetupGet(x => x.Roles).Returns(new List<Role>
            {
                new Role
                {
                    HospitalId = 1,
                    IsActive = "A",
                    RoleId = 1,
                    RoleCode = "HADM",
                    RoleFunctions = new List<RoleFunction>
                    {
                        new RoleFunction
                        {
                            FunctionId = 1,
                            RoleId = 1,
                            IsActive = "A"
                        }
                    }
                }
            }.ToDbSet());

            mockContext.SetupGet(x => x.Users).Returns(users.ToDbSet());

            mockContext.SetupGet(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile>
            {
                new HospitalStaffProfile
                {
                    UserId = 1,
                    IsActive = "A",
                    HospitalId = 1
                }
            }.ToDbSet());

            return mockContext.Object;
        }

        [Test]
        public void ValidateCliniciansLimit_LimitNotReached_Returns_1()
        {
            var context = MockContext(2);

            var repo = new HospitalStaffRepository(context);

            var result = repo.ValidateCliniciansLimit(1, new List<int> { 1 }, 2);
            Assert.AreEqual(result, 1);
        }

        [Test]
        public void ValidateCliniciansLimit_AlreadyClinitian_Returns_1()
        {
            var context = MockContext(1);

            var repo = new HospitalStaffRepository(context);

            var result = repo.ValidateCliniciansLimit(1, new List<int> { 1 }, 1);
            Assert.AreEqual(result, 1);
        }

        [Test]
        public void ValidateCliniciansLimit_NotClinitianAsignment_Returns_1()
        {
            var context = MockContext(1);

            var repo = new HospitalStaffRepository(context);

            var result = repo.ValidateCliniciansLimit(1, new List<int> { 2 }, 1);
            Assert.AreEqual(result, 1);
        }

        [Test]
        public void ValidateCliniciansLimit_LimitReached_Returns_0()
        {
            var context = MockContext(1);

            var repo = new HospitalStaffRepository(context);

            var result = repo.ValidateCliniciansLimit(1, new List<int> { 1 }, 2);
            Assert.AreEqual(result, 0);
        }

        [Test]
        public void IsHAdmUserEditable_No_ReturnsFalse()
        {
            var context = MockContext(1);

            var repo = new HospitalStaffRepository(context);

            var result = repo.IsHAdmUserEditable(1, 1);
        }

        [Test]
        public void IsHAdmUserEditable_NotHadm_ReturnsTrue()
        {
            var context = MockContext(1);

            var repo = new HospitalStaffRepository(context);

            var result = repo.IsHAdmUserEditable(2, 1);
        }

        [Test]
        public void IsHAdmUserEditable_MoreHadm_ReturnsTrue()
        {
            var context = MockContext(1, true);


            var repo = new HospitalStaffRepository(context);

            var result = repo.IsHAdmUserEditable(1, 1);
        }
    }
}
