﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class PatientMetadataRepositoryTests
    {
        [Test]
        public void TestMetadataRepositoryGet()
        {
            var guid = Guid.NewGuid();
            var mockData = new Mock<ISnapContext>();
            var list = new PatientMetadata { Id = guid, PatientId = 2 };
            mockData.SetupGet(c => c.PatientMetadata).Returns((new List<PatientMetadata> { list }).ToDbSet());
            var repository = new PatientMetadataRepository(mockData.Object);
            var result = repository.GetMetadata(2);

            Assert.AreEqual(guid, result.Id);
        }

        [Test]
        public void TestUpsertException()
        {
            var mock = new Mock<IPatientMetadata>();
            var target = new PatientMetadataRepository(null);
            Assert.Throws<NotImplementedException>(() => target.Upsert(mock.Object));
        }

        [Test]
        public void TestAdd()
        {
            var xml = new MedicalHistoryProfile().ToXml(XConversionOptions.ToCamelCase);
            Console.WriteLine(xml);
            var fake = new PatientMetadata { Metadata = xml };
            var mockDbSet = new List<PatientMetadata>().ToDbSet();
            var mockContext = new Mock<ISnapContext>();
            mockContext.SetupGet(c => c.PatientMetadata).Returns(mockDbSet);
            var target = new PatientMetadataRepository(mockContext.Object);
            Assert.DoesNotThrow(() => target.Add(fake));
        }
    }
}
