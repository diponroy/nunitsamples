﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class ClinicianSettingsRepositoryTest
    {
        protected Mock<ISnapContext> DbContext;
        protected ClinicianSettingsRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        private void InitializeRepo()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Repo = new ClinicianSettingsRepository(DbContext.Object);
        }

        private List<DoctorSetting> BuildList(int sampleNumber)
        {
            return Builder<DoctorSetting>.CreateListOfSize(sampleNumber).Build().ToList();
        }


        [Test]
        public void RepoInitialization()
        {
            Assert.DoesNotThrow(() => new ClinicianSettingsRepository(DbContext.Object));
        }

        /*
         * find by settingid from Db using DbSets Add method
         * using DoctorSetting tbl
         */

        [Test]
        public void Get_NotFound_ReturnsNull()
        {
            List<DoctorSetting> settings = BuildList(4);
            DbContext.Setup(x => x.DoctorSettings).Returns(settings.ToDbSet());
            DbContext.Setup(x => x.DoctorSettings.Find(It.IsAny<object[]>()))
                .Returns((object[] objs) => settings.Find(x => x.SettingId == (int) objs[0]));
            InitializeRepo();

            Assert.IsNull(Repo.Get(9));
        }

        [Test]
        public void Get_ReturnType()
        {
            List<DoctorSetting> settings = BuildList(4);
            DbContext.Setup(x => x.DoctorSettings).Returns(settings.ToDbSet());
            DbContext.Setup(x => x.DoctorSettings.Find(It.IsAny<object[]>()))
                .Returns((object[] objs) => settings.Find(x => x.SettingId == (int) objs[0]));
            InitializeRepo();

            Assert.IsInstanceOf<DoctorSetting>(Repo.Get(1));
        }

        [Test]
        public void Get_Found_Returns()
        {
            List<DoctorSetting> settings = BuildList(4);
            DbContext.Setup(x => x.DoctorSettings).Returns(settings.ToDbSet());
            DbContext.Setup(x => x.DoctorSettings.Find(It.IsAny<object[]>()))
                .Returns((object[] objs) => settings.Find(x => x.SettingId == (int) objs[0]));
            InitializeRepo();

            DoctorSetting setting = Repo.Get(1);
            Assert.IsNotNull(setting);
            Assert.AreEqual(1, setting.SettingId);
        }

        /*
         * adds assign object to Db using DbSets Add method 
         * using DoctorSetting tbl
         */

        [Test]
        public void Add_ReturnType()
        {
            var settingToAdd = new DoctorSetting {DoctorId = 100, Key = "TheKey", Value = "TheValue"};
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(9).ToDbSet);
            DbContext.Setup(x => x.DoctorSettings.Add(It.IsAny<DoctorSetting>()))
                .Returns((DoctorSetting setting) => setting);
            InitializeRepo();

            DoctorSetting result = Repo.Add(settingToAdd);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<DoctorSetting>(result);
        }

        [Test]
        public void Add_UsingDbSets_AddMethod()
        {
            var settingToAdd = new DoctorSetting {DoctorId = 100, Key = "TheKey", Value = "TheValue"};
            DoctorSetting settingAdded = null;
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(9).ToDbSet);
            DbContext.Setup(x => x.DoctorSettings.Add(It.IsAny<DoctorSetting>()))
                .Returns((DoctorSetting setting) => setting);
            InitializeRepo();

            settingAdded = Repo.Add(settingToAdd);
            Assert.AreEqual(settingToAdd.DoctorId, settingAdded.DoctorId);
            Assert.AreEqual(settingToAdd.Key, settingAdded.Key);
            Assert.AreEqual(settingToAdd.Value, settingAdded.Value);
        }

        [Test]
        public void Add_NotYetSaved_ToDb()
        {
            bool isSavedToDb = false;
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(9).ToDbSet);
            DbContext.Setup(x => x.SaveChanges()).Returns(() =>
            {
                isSavedToDb = true;
                return 1;
            });
            InitializeRepo();

            Repo.Add(new DoctorSetting());
            Assert.AreEqual(false, isSavedToDb);
        }


        [Test]
        public void Where_IfNotFound_ReturnsEmptyIEnumerable()
        {
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(5).ToDbSet);
            InitializeRepo();

            IEnumerable<DoctorSetting> result = Repo.Where(x => x.SettingId == 10);
            Assert.AreEqual(0, result.Count());
        }


        [Test]
        public void Where_Found_ReturnsIEnumerableWithItem()
        {
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(5).ToDbSet);
            InitializeRepo();

            IEnumerable<DoctorSetting> result = Repo.Where(x => x.SettingId == 1);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<IEnumerable<DoctorSetting>>(result);
            Assert.AreEqual(1, result.Count());
        }


        /*
         * add/update doctorSettings for a clinician
         * uning DoctorSetting tbl
         */

        [Test]
        public void SetRxntCredentials_NoKeyContainsRxnt_FortheClinician_Add_KeyRxntUname_ValueLogin()
        {
            int clinicianId = 10;
            string login = "TheLogIn";
            string secret = "TheScerect";

            List<DoctorSetting> addedList = new List<DoctorSetting>();
            DbContext.Setup(x => x.DoctorSettings.Add(It.IsAny<DoctorSetting>()))
                .Returns((DoctorSetting setting) => setting)
                .Callback((DoctorSetting setting) =>
                {
                    addedList.Add(setting);     //need to know what is he adding, was working fine for add test
                });
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(2).ToDbSet());
            InitializeRepo();

            Repo.SetRxntCredentials(clinicianId, login, secret);

            //string key = "RXNT_UNAME";
            //Assert.IsNotNull(addedList.Find(x => x.DoctorId == clinicianId && x.Key == key && x.Value == login));
            /*two setup for DoctorSettings is doing some problem*/
            Assert.Inconclusive("need to find some way.");
        }

        [Test]
        public void SetRxntCredentials_NoKeyContainsRxnt_FortheClinician_Add_KeyRxntPwd_ValueSecret()
        {
            Assert.Inconclusive("need to find some way.");
        }

        [Test]
        public void SetRxntCredentials_NoKeyContainsRxnt_FortheClinician_SetValueToLoging_ForKeyRxntUname()
        {
            int clinicianId = 10;
            string login = "TheLogIn";
            string secret = "TheScerect";
            string key = "RXNT_UNAME";

            var doctorSettings = new List<DoctorSetting>
            {
                new DoctorSetting {DoctorId = 10, Key = key, Value = ""}
            };
            DbContext.Setup(x => x.DoctorSettings).Returns(doctorSettings.ToDbSet());
            InitializeRepo();

            Repo.SetRxntCredentials(clinicianId, login, secret);
            Assert.AreEqual(login, doctorSettings[0].Value);
        }

        [Test]
        public void SetRxntCredentials_NoKeyContainsRxnt_FortheClinician_SetValueToSecret_ForKeyRxntPwd()
        {
            int clinicianId = 10;
            string login = "TheLogIn";
            string secret = "TheScerect";
            string key = "RXNT_PWD";

            var doctorSettings = new List<DoctorSetting>
            {
                new DoctorSetting {DoctorId = 10, Key = key, Value = ""}
            };
            DbContext.Setup(x => x.DoctorSettings).Returns(doctorSettings.ToDbSet());
            InitializeRepo();

            Repo.SetRxntCredentials(clinicianId, login, secret);
            Assert.AreEqual(secret, doctorSettings[0].Value);
        }

        [Test]
        public void SetRxntCredentials_SavedUsingSaveChanges_ToDb()
        {
            int clinicianId = 10;
            string login = "TheLogIn";
            string secret = "TheScerect";

            bool isSavedToDb = false;
            var addedList = new List<DoctorSetting>();
            DbContext.Setup(x => x.DoctorSettings).Returns(BuildList(2).ToDbSet());
            DbContext.Setup(x => x.SaveChanges()).Returns(() =>
            {
                isSavedToDb = true;
                return 1;
            });
            InitializeRepo();

            Repo.SetRxntCredentials(clinicianId, login, secret);
            Assert.IsTrue(isSavedToDb);
        }


        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Repo = null;
        }
    }
}
