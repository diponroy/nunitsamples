﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class UserRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected UserRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new UserRepository(MockedDb.Object);
        }

        /*
         * Gets user by user id
         *     if not found returns null
         *     if found more than one throws error
         */

        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void Get_By_UserId_Found_Returns_Object(string entityStatus)
        {
            User aUser = Builder<User>.CreateNew().With(x => x.IsActive = entityStatus).Build();
            MockedDb.Setup(x => x.Users).Returns(new List<User>() { aUser }.ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.Get(aUser.UserId));
            var result = Repo.Get(aUser.UserId);
            Assert.IsInstanceOf<User>(result);
            Assert.AreEqual(aUser.UserId, result.UserId);
        }

        [Test]
        public void Get_By_UserId_NotFound_Returns_Null()
        {
            int userId = 100;
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            InitializeRepo();

            Assert.DoesNotThrow(() => Repo.Get(userId));
            var result = Repo.Get(userId);
            Assert.IsNull(result);
        }

        [Test]
        public void Get_By_UserId_Found_MoreThanOne_Throws_Error()
        {
            int userId = 100;
            List<User> users = Builder<User>.CreateListOfSize(2).All()
                                .With(x => x.UserId = userId)
                                .Build().ToList();
            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            InitializeRepo();

            Assert.Catch<Exception>(() => Repo.Get(userId));
        }


        /*Find user by email, hospital and user type
         *  user can be active or inactive
         */

        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void Find_By_Email_And_Hospital_And_UserType(string entityStatus)
        {

            var userType = UserTypeEnum.Customer;
            User user = Builder<User>.CreateNew()
                .With(x => x.Email = "Test@gmail.com")
                .And(x => x.HospitalId = 5)
                .And(x => x.UserTypeId = (int)userType)
                .And(x => x.IsActive = entityStatus)
                .Build();
            List<User> users = Builder<User>.CreateListOfSize(3).Build().ToList();
            users.Add(user);

            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            InitializeRepo();

            var result = Repo.Find(user.Email, user.HospitalId, userType);
            Assert.IsInstanceOf<User>(result);
            Assert.AreEqual(user.Email, result.Email);
            Assert.AreEqual(user.HospitalId, result.HospitalId);
            Assert.AreEqual(user.UserTypeId, result.UserTypeId);
            Assert.AreEqual(user.IsActive, result.IsActive);
        }


        [Test]
        [TestCase("test@out.com", 5, UserTypeEnum.Customer, false)]         //email not matched
        [TestCase("Test@gmail.com", 50, UserTypeEnum.Customer, false)]      //hospital not matched
        [TestCase("Test@gmail.com", 5, UserTypeEnum.HospitalStaff, false)]  //usertype not matched
        [TestCase("Test@gmail.com", 5, UserTypeEnum.Customer, true)]        //able to find
        [TestCase("TEST@GMAIL.COM", 5, UserTypeEnum.Customer, true)]        //able to find
        [TestCase("test@gmail.com", 5, UserTypeEnum.Customer, true)]        //able to find
        public void Find_By_Email_And_Hospital_And_UserType(string email, int hospitalId, UserTypeEnum userType, bool ableToFindUser)
        {
            User aUser = Builder<User>.CreateNew()
                .With(x => x.Email = "Test@gmail.com")
                .And(x => x.HospitalId = 5)
                .And(x => x.UserTypeId = (int)UserTypeEnum.Customer)
                .And(x => x.IsActive = "A")
                .Build();
            List<User> users = new List<User>();
            users.Add(aUser);

            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            InitializeRepo();

            var result = Repo.Find(email, hospitalId, userType);
            Assert.AreEqual((result != null), ableToFindUser);
        }

    }
}
