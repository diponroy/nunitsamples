﻿using System;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class LogRepositoryTests
    {
        [Test]
        public void TestDispose()
        {
            var mockContext = new Mock<ISnapContext>();
            var repository = new LogRepository(mockContext.Object);
            Assert.DoesNotThrow(() => repository.Dispose());
        }

        [Test]
        public void TestWhere()
        {
            var mockContext = new Mock<ISnapContext>();
            var list = Builder<LogMessage>.CreateListOfSize(10);
            mockContext.SetupGet(m => m.LogMessages).Returns(list.Build().ToDbSet());

            var target = new LogRepository(mockContext.Object);
            var actual = target.Where(t => t.Message == "Message10").ToList();
            Assert.AreEqual(1, actual.Count);
        }

        [Test]
        public void TestGetDeveloperLogs()
        {
            var developerId1 = Guid.NewGuid();
            var developerId2 = Guid.NewGuid();
            var mockContext = new Mock<ISnapContext>();
            var list = Builder<ApiAccessLog>.CreateListOfSize(50)
                .Random(10)
                .With(d => d.DeveloperId = developerId1)
                .And(d => d.DateStamp = new DateTime(2015, 11, 1))
                .Build().ToList();
            var list2 = Builder<ApiAccessLog>.CreateListOfSize(10).Build();
            var list3 =
                Builder<ApiAccessLog>.CreateListOfSize(10).Random(2).With(d => d.DeveloperId = developerId2).Build();
            list.AddRange(list2);
            list.AddRange(list3);

            mockContext.SetupGet(m => m.ApiAccessLogs).Returns(list.ToDbSet());
            mockContext.SetupGet(m => m.LogMessages).Returns(list.ConvertAll(c => (LogMessage)c).ToDbSet());

            var target = new LogRepository(mockContext.Object);
            var actual =
                target.GetDeveloperLogs(developerId1, new DateTime(2015, 10, 31), new DateTime(2015, 11, 2)).ToList();
            Assert.GreaterOrEqual(actual.Count, 4);
            Assert.LessOrEqual(actual.Count, 10);
        }
    }
}
