﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class ScheduledConsultationsRepositoryTest
    {
        protected Mock<ISnapContext> DbContext;
        protected ScheduledConsultationsRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
            DbContext.Setup(x => x.SaveChanges()).Returns(1);
        }

        protected List<TSource> BuildList<TSource>(int sampleNumber) where TSource : class
        {
            return Builder<TSource>.CreateListOfSize(sampleNumber).Build().ToList();
        }

        protected void InitializeRepo()
        {
            Repo = new ScheduledConsultationsRepository(DbContext.Object);
        }

        [Test]
        public void ScheduledConsultationsRepository_Initialization()
        {
            Assert.DoesNotThrow(() => new ScheduledConsultationsRepository(DbContext.Object));
        }

        /*
         * get own scheduled consultations by Consultant(customer) userId, Dnawindow, OrderBy Schedule Time when 
         *  1. paient is active
         *  2. where shedule time utc is > DateTime.UtcNow - Distortion (in DnaWindow)
         *  3. consultation statis is not null, ended=72, canceld=79, or paymentDone=68 
         *  4. converts the schedule time to injected userTimeZone
         */

        [Test]
        public void GetPatientConsultations_ReturnType()
        {
            var userId = 1;
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>{patient2}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile>().ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            InitializeRepo();

            Assert.IsInstanceOf<IQueryable<ScheduledConsultationResult>>(Repo.GetPatientConsultations(userId, TimeSpan.FromMinutes(30)));
        }

        [Test]
        public void GetPatientConsultations_Fields()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();
            ScheduledConsultationResult result = results.First();

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patient.PatientId, result.PatientId);
            Assert.AreEqual(consultation.ConsultantUserId, result.ConsultantUserId);
            Assert.AreEqual(scheduledConsultationTemp.ShcheduledId, result.ScheduledId);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.AreEqual(patient.FullName, result.PatientName);
            Assert.AreEqual(patient.UserId, result.PatientUserId);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, result.ScheduledTime.DateTime);
            Assert.AreEqual(hospitalStaffProfile.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaffProfile.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(hospitalStaffProfile.FullName, result.AssignedDoctorName);
            Assert.AreEqual(consultation.ConsultationAmount, result.ConsultationAmount);
            Assert.AreEqual(consultation.CopayAmount, result.CopayAmount);
            Assert.AreEqual(consultation.ConsultationStatus, result.ConsultationStatus);
        }

        [Test]
        public void GetPatientConsultations_OrderBy_ScheduleTime()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(2)   //should appear last in list
            };

            var consultation1 = new Consultation
            {
                ConsultationId = 2,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp1 = new ScheduledConsultationTemp
            {
                ConsultationId = consultation1.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(10)  //should appear first in list
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>() { consultation, consultation1 };
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>() { scheduledConsultationTemp, scheduledConsultationTemp1 };

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();

            Assert.AreEqual(2, results.Count);
            Assert.AreEqual(scheduledConsultationTemp1.ScheduledTime, results.First().ScheduledTime.DateTime);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, results.Last().ScheduledTime.DateTime);
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 0)]
        public void GetPatientConsultations_By_UserId(int consultationUserId, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient3 = new PatientProfile
            {
                PatientId = 3,
                UserId = 10,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2, patient3};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(consultationUserId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetPatientConsultations_Returns_ScheduleConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp();  //not schedule consultaion

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        [TestCase("A", 1)]
        [TestCase("I", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetPatientConsultations_If_Paient_Active(string patientStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = patientStatus //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(30, 1, 1)]
        [TestCase(30, 20, 1)]
        [TestCase(30, 29, 1)]       //testing with [TestCase(30, 30, 1)] will fail, as it would take some time to test
        [TestCase(30, 35, 0)]
        [TestCase(30, 40, 0)]
        public void GetPatientConsultations_If_SceduleTime_In_DnaWindow(double dnaWindowInMinutes, double utcDistortionInMinutes, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan dnaWindow = TimeSpan.FromMinutes(dnaWindowInMinutes);
            TimeSpan utcDistortion = TimeSpan.FromMinutes(utcDistortionInMinutes);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - utcDistortion
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, dnaWindow).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorAssigned, 1)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, 1)]
        [TestCase(ConsultationStatus.StartedConsultation, 1)]
        [TestCase(ConsultationStatus.InProgress, 1)]
        [TestCase(ConsultationStatus.DroppedConsultation, 0)]
        [TestCase(ConsultationStatus.CustomerInWaiting, 1)]
        [TestCase(ConsultationStatus.EndedConsultation, 0)]
        [TestCase(ConsultationStatus.CancelConsultaion, 0)]
        [TestCase(ConsultationStatus.PaymentDone, 1)]
        [TestCase(null, 0)]
        public void GetPatientConsultations_For_Proper_ConsultationStatus(ConsultationStatus? consultationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationStatus = (int?)consultationStatus
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetPatientConsultations_Ignors_DependentConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,                     //user it not the patient
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A"                  //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetPatientConsultations(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        /*
         * get dependent patient's scheduled consultations by Consultant(customer) userId, Dnawindow, Orderby Schedule time when 
         *  1. dependent patients are authorized 
         *  2. dependent patients are active
         *  5. where shedule time utc is > DateTime.UtcNow - Distortion (in DnaWindow)
         *  6. consultation statis is not null, ended=72, canceld=79, or paymentDone=68 
         *  7. converts the schedule time to injected userTimeZone
         */

        [Test]
        public void GetDependentConsultations_ReturnType()
        {
            var userId = 1;

            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>{patient2}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile>().ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            Assert.IsInstanceOf<IQueryable<ScheduledConsultationResult>>(Repo.GetDependentConsultations(userId, TimeSpan.FromMinutes(30)));
        }

        [Test]
        public void GetDependentConsultations_Fields()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            ScheduledConsultationResult result = results.First();

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patient.PatientId, result.PatientId);
            Assert.AreEqual(consultation.ConsultantUserId, result.ConsultantUserId);
            Assert.AreEqual(scheduledConsultationTemp.ShcheduledId, result.ScheduledId);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.AreEqual(patient.FullName, result.PatientName);
            Assert.AreEqual(patient.UserId, result.PatientUserId);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, result.ScheduledTime.DateTime);
            Assert.AreEqual(hospitalStaffProfile.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaffProfile.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(hospitalStaffProfile.FullName, result.AssignedDoctorName);
            Assert.AreEqual(consultation.ConsultationAmount, result.ConsultationAmount);
            Assert.AreEqual(consultation.CopayAmount, result.CopayAmount);
            Assert.AreEqual(consultation.ConsultationStatus, result.ConsultationStatus);
        }

        [Test]
        public void GetDependentConsultations_OrderBy_ScheduleTime()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(5)       //should appear last on list
            };

            var consultation1 = new Consultation
            {
                ConsultationId = 2,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp1 = new ScheduledConsultationTemp
            {
                ConsultationId = consultation1.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(10)       //should appear first on list
            };

            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>() { consultation, consultation1 };
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>() { scheduledConsultationTemp, scheduledConsultationTemp1 };
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();

            Assert.AreEqual(2, results.Count);
            Assert.AreEqual(scheduledConsultationTemp1.ScheduledTime, results.First().ScheduledTime.DateTime);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, results.Last().ScheduledTime.DateTime);
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 0)]
        public void GetDependentConsultations_By_UserId(int consultationUserId, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient3 = new PatientProfile
            {
                PatientId = 3,
                UserId = 10,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient,patient2,patient3};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(consultationUserId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetDependentConsultations_Returns_ScheduleConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp(); //not shcedule consultations

            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        [TestCase("A", 1)]
        [TestCase("I", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetDependentConsultations_If_Paient_Active(string patientStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = patientStatus //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase("Y", 1)]
        [TestCase("N", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetDependentConsultations_If_Paient_Authoried(string patientAuthorizationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = patientAuthorizationStatus //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorAssigned, 1)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, 1)]
        [TestCase(ConsultationStatus.StartedConsultation, 1)]
        [TestCase(ConsultationStatus.InProgress, 1)]
        [TestCase(ConsultationStatus.DroppedConsultation, 0)]
        [TestCase(ConsultationStatus.CustomerInWaiting, 1)]
        [TestCase(ConsultationStatus.EndedConsultation, 0)]
        [TestCase(ConsultationStatus.CancelConsultaion, 0)]
        [TestCase(ConsultationStatus.PaymentDone, 1)]
        [TestCase(null, 0)]
        public void GetDependentConsultations_For_Proper_ConsultationStatus(ConsultationStatus? consultationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)consultationStatus
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(30, 1, 1)]
        [TestCase(30, 20, 1)]
        [TestCase(30, 29, 1)]       //testing with [TestCase(30, 30, 1)] will fail, as it would take some time to test
        [TestCase(30, 35, 0)]
        [TestCase(30, 40, 0)]
        public void GetDependentConsultations_If_SceduleTime_In_DnaWindow(double dnaWindowInMinutes, double utcDistortionInMinutes, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan dnaWindow = TimeSpan.FromMinutes(dnaWindowInMinutes);
            TimeSpan utcDistortion = TimeSpan.FromMinutes(utcDistortionInMinutes);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - utcDistortion
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, dnaWindow).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetDependentConsultations_Ignors_OwnConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,                //user it self patient       
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A"                  //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetDependentConsultations(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        /*
         * get own scheduled consultations by Consultant(customer) userId, Dnawindow, OrderBy Schedule Time when 
         *  1. paient is active
         *  2. where shedule time utc is > DateTime.UtcNow - Distortion (in DnaWindow)
         *  3. consultation statis is not null, ended=72, canceld=79, or paymentDone=68 
         *  4. converts the schedule time to injected userTimeZone
         *  
         * UNION
         * 
         * get dependent patient's scheduled consultations by Consultant(customer) userId, Dnawindow, Orderby Schedule time when 
         *  1. dependent patients are authorized 
         *  2. dependent patients are active
         *  5. where shedule time utc is > DateTime.UtcNow - Distortion (in DnaWindow)
         *  6. consultation statis is not null, ended=72, canceld=79, or paymentDone=68 
         *  7. converts the schedule time to injected userTimeZone
         */

        [Test]
        public void GetConsultationList_ReturnType()
        {
            var userId = 1;

            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>{patient2}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile>().ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            Assert.IsInstanceOf<IEnumerable<ScheduledConsultationResult>>(Repo.GetConsultationList(userId, TimeSpan.FromMinutes(30)));
        }

        //Own
        [Test]
        public void GetConsultationList_Own_Fields()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            ScheduledConsultationResult result = results.First();

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patient.PatientId, result.PatientId);
            Assert.AreEqual(consultation.ConsultantUserId, result.ConsultantUserId);
            Assert.AreEqual(scheduledConsultationTemp.ShcheduledId, result.ScheduledId);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.AreEqual(patient.FullName, result.PatientName);
            Assert.AreEqual(patient.UserId, result.PatientUserId);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, result.ScheduledTime.DateTime);
            Assert.AreEqual(hospitalStaffProfile.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaffProfile.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(hospitalStaffProfile.FullName, result.AssignedDoctorName);
            Assert.AreEqual(consultation.ConsultationAmount, result.ConsultationAmount);
            Assert.AreEqual(consultation.CopayAmount, result.CopayAmount);
            Assert.AreEqual(consultation.ConsultationStatus, result.ConsultationStatus);
        }

        [Test]
        public void GetConsultationList_Own_OrderBy_ScheduleTime()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(2)   //should appear last in list
            };

            var consultation1 = new Consultation
            {
                ConsultationId = 2,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp1 = new ScheduledConsultationTemp
            {
                ConsultationId = consultation1.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(10)  //should appear first in list
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>() { consultation, consultation1 };
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>() { scheduledConsultationTemp, scheduledConsultationTemp1 };

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();

            Assert.AreEqual(2, results.Count);
            Assert.AreEqual(scheduledConsultationTemp1.ScheduledTime, results.First().ScheduledTime.DateTime);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, results.Last().ScheduledTime.DateTime);
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 0)]
        public void GetConsultationList_Own_By_UserId(int consultationUserId, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient3 = new PatientProfile
            {
                PatientId = 3,
                UserId = 10,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2, patient3};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(consultationUserId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetConsultationList_Own_Returns_ScheduleConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp();  //not schedule consultaion

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        [TestCase("A", 1)]
        [TestCase("I", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetConsultationList_Own_If_Paient_Active(string patientStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = patientStatus //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(30, 1, 1)]
        [TestCase(30, 20, 1)]
        [TestCase(30, 29, 1)]       //testing with [TestCase(30, 30, 1)] will fail, as it would take some time to test
        [TestCase(30, 35, 0)]
        [TestCase(30, 40, 0)]
        public void GetConsultationList_Own_If_SceduleTime_In_DnaWindow(double dnaWindowInMinutes, double utcDistortionInMinutes, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan dnaWindow = TimeSpan.FromMinutes(dnaWindowInMinutes);
            TimeSpan utcDistortion = TimeSpan.FromMinutes(utcDistortionInMinutes);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - utcDistortion
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, dnaWindow).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorAssigned, 1)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, 1)]
        [TestCase(ConsultationStatus.StartedConsultation, 1)]
        [TestCase(ConsultationStatus.InProgress, 1)]
        [TestCase(ConsultationStatus.DroppedConsultation, 0)]
        [TestCase(ConsultationStatus.CustomerInWaiting, 1)]
        [TestCase(ConsultationStatus.EndedConsultation, 0)]
        [TestCase(ConsultationStatus.CancelConsultaion, 0)]
        [TestCase(ConsultationStatus.PaymentDone, 1)]
        [TestCase(null, 0)]
        public void GetConsultationList_Own_For_Proper_ConsultationStatus(ConsultationStatus? consultationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = userId,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationStatus = (int?)consultationStatus
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };

            var patientProfiles = new List<PatientProfile>();
            patientProfiles.Add(patient);
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        //Dependent patients
        [Test]
        public void GetConsultationList_Dependent_Fields()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>
            {
                patient,
                patient2
            };
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            ScheduledConsultationResult result = results.First();

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patient.PatientId, result.PatientId);
            Assert.AreEqual(consultation.ConsultantUserId, result.ConsultantUserId);
            Assert.AreEqual(scheduledConsultationTemp.ShcheduledId, result.ScheduledId);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.AreEqual(patient.FullName, result.PatientName);
            Assert.AreEqual(patient.UserId, result.PatientUserId);
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, result.ScheduledTime.DateTime);
            Assert.AreEqual(hospitalStaffProfile.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaffProfile.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(hospitalStaffProfile.FullName, result.AssignedDoctorName);
            Assert.AreEqual(consultation.ConsultationAmount, result.ConsultationAmount);
            Assert.AreEqual(consultation.CopayAmount, result.CopayAmount);
            Assert.AreEqual(consultation.ConsultationStatus, result.ConsultationStatus);
        }

        [Test]
        public void GetConsultationList_Dependent_OrderBy_ScheduleTime()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(5)       //should appear last on list
            };

            var consultation1 = new Consultation
            {
                ConsultationId = 2,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp1 = new ScheduledConsultationTemp
            {
                ConsultationId = consultation1.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - TimeSpan.FromMinutes(10)       //should appear first on list
            };

            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>() { consultation, consultation1 };
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>() { scheduledConsultationTemp, scheduledConsultationTemp1 };
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();

            Assert.AreEqual(2, results.Count);
            Assert.AreEqual(scheduledConsultationTemp1.ScheduledTime.ToString(), results.First().ScheduledTime.ToString("G"));
            Assert.AreEqual(scheduledConsultationTemp.ScheduledTime, results.Last().ScheduledTime.DateTime);
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 0)]
        public void GetConsultationList_Dependent_By_UserId(int consultationUserId, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A",
                IsDependent = "N"//important
            };
            var patient3 = new PatientProfile
            {
                PatientId = 3,
                UserId = 10,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A",
                IsDependent = "N"//important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>
            {
                patient,
                patient2,
                patient3
            };

            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(consultationUserId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        public void GetConsultationList_Dependent_Returns_ScheduleConsultations()
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp(); //not shcedule consultations

            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(0, results.Count);
        }

        [Test]
        [TestCase("A", 1)]
        [TestCase("I", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetConsultationList_Dependent_If_Paient_Active(string patientStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = patientStatus //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = patientStatus //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase("Y", 1)]
        [TestCase("N", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetConsultationList_Dependent_If_Paient_Authoried(string patientAuthorizationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = patientAuthorizationStatus //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorAssigned, 1)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, 1)]
        [TestCase(ConsultationStatus.StartedConsultation, 1)]
        [TestCase(ConsultationStatus.InProgress, 1)]
        [TestCase(ConsultationStatus.DroppedConsultation, 0)]
        [TestCase(ConsultationStatus.CustomerInWaiting, 1)]
        [TestCase(ConsultationStatus.EndedConsultation, 0)]
        [TestCase(ConsultationStatus.CancelConsultaion, 0)]
        [TestCase(ConsultationStatus.PaymentDone, 1)]
        [TestCase(null, 0)]
        public void GetConsultationList_Dependent_For_Proper_ConsultationStatus(ConsultationStatus? consultationStatus, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan distortion = TimeSpan.FromMinutes(30);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)consultationStatus
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>
            {
                patient,
                patient2
            };
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, distortion).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [Test]
        [TestCase(30, 1, 1)]
        [TestCase(30, 20, 1)]
        [TestCase(30, 29, 1)]       //testing with [TestCase(30, 30, 1)] will fail, as it would take some time to test
        [TestCase(30, 35, 0)]
        [TestCase(30, 40, 0)]
        public void GetConsultationList_Dependent_If_SceduleTime_In_DnaWindow(double dnaWindowInMinutes, double utcDistortionInMinutes, int returnedItemCount)
        {
            int userId = 1;
            TimeSpan dnaWindow = TimeSpan.FromMinutes(dnaWindowInMinutes);
            TimeSpan utcDistortion = TimeSpan.FromMinutes(utcDistortionInMinutes);


            /*see the relation carefully*/
            var patient = new PatientProfile
            {
                PatientId = 1,
                UserId = 3,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var patient2 = new PatientProfile
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            };
            var hospitalStaffProfile = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "StaffName",
                LastName = "StaffLastName"
            };
            var consultation = new Consultation
            {
                ConsultationId = 1,
                PatientId = patient.PatientId,
                ConsultantUserId = userId, //userid
                AssignedDoctorId = hospitalStaffProfile.UserId,
                AssignedHospitalUserId = hospitalStaffProfile.UserId,
                ConsultationAmount = 100,
                CopayAmount = 200,
                ConsultationStatus = (int?)ConsultationStatus.DoctorAssigned
            };
            var scheduledConsultationTemp = new ScheduledConsultationTemp
            {
                ConsultationId = consultation.ConsultationId,
                PatientId = patient.PatientId,
                AssignedDoctorId = hospitalStaffProfile.UserId, //userid
                ScheduledTime = DateTime.UtcNow - utcDistortion
            };
            var authorization = new UserDependentRelationAuthorization
            {
                UserId = userId,
                PatientId = patient.PatientId,
                IsAuthorized = "Y" //important
            };

            var patientProfiles = new List<PatientProfile>{patient, patient2};
            var hospitalStaffProfiles = new List<HospitalStaffProfile>();
            hospitalStaffProfiles.Add(hospitalStaffProfile);
            var consultations = new List<Consultation>();
            consultations.Add(consultation);
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            scheduledConsultationTemps.Add(scheduledConsultationTemp);
            var authorizations = new List<UserDependentRelationAuthorization>();
            authorizations.Add(authorization);

            /*Moq*/
            DbContext.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(hospitalStaffProfiles.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            DbContext.Setup(x => x.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());
            InitializeRepo();

            /*result*/
            List<ScheduledConsultationResult> results = Repo.GetConsultationList(userId, dnaWindow).ToList();
            Assert.AreEqual(returnedItemCount, results.Count);
        }

        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Repo = null;
        }
    }
}
