﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class HospitalRepositoryTests
    {
        private Mock<ISnapContext> MockContext;
        private HospitalRepository repository;
        private OperatingHours testHour;

        [SetUp]
        public void Setup()
        {
            MockContext = new Mock<ISnapContext>();
            repository = new HospitalRepository(MockContext.Object);
            testHour = new OperatingHours()
            {
                OperatingHoursId = 1,
                StartTime = new TimeSpan(1,1,1),
                EndTime = new TimeSpan(2,2,2),
                StartDayOfWeek = 1,
                EndDayOfWeek = 7,
                HospitalId = 1
            };
        }

        [Test]
        public void GetHospitalHours_HospitalExists_ReturnHours()
        {
            MockContext.Setup(x => x.Hospitals)
                .Returns(() =>new[]{ new Hospital { HospitalId = 1, OperatingHours = new[] { testHour }.ToList() }}.ToDbSet());

            var hour = repository.GetOperatingHours(1);

            Assert.IsNotNull(hour);
            Assert.AreEqual(1, hour.Count());
            Assert.AreEqual(1, hour.First().OperatingHoursId);
        }

        [Test]
        public void CreateHours_NoHours_ReturnsCreated()
        {
            var list = new List<OperatingHours> { testHour }.ToDbSet();
            MockContext.SetupAllProperties();
            MockContext.Setup(x => x.OperatingHours.Add(It.IsAny<OperatingHours>()))
                .Callback((OperatingHours x) => list.Add(x));
            MockContext.Setup(x=>x.OperatingHours).Returns(list);
            var newHour = new OperatingHours()
            {
                OperatingHoursId = 0,
                StartTime = new TimeSpan(1, 1, 1),
                EndTime = new TimeSpan(2, 2, 2),
                StartDayOfWeek = 1,
                EndDayOfWeek = 7,
                HospitalId = 2
            };


            var hours = repository.AddUpdateOperatingHours(newHour);

            Assert.AreSame(hours, newHour);
            MockContext.VerifyGet(c => c.OperatingHours, Times.AtLeastOnce);
            MockContext.Verify(x=>x.SaveChanges(), Times.Once);
        }


        [Test]
        public void CreateHours_HoursExist_ReturnsUpdated()
        {
            var list = new List<OperatingHours> { testHour }.ToDbSet();
            MockContext.SetupAllProperties();
            MockContext.Setup(x => x.OperatingHours.Add(It.IsAny<OperatingHours>()))
                .Callback((OperatingHours x) => list.Add(x));
            MockContext.Setup(x=>x.OperatingHours).Returns(list);
            var newHour = new OperatingHours()
            {
                OperatingHoursId = 1,
                StartTime = new TimeSpan(3, 3, 3),
                EndTime = new TimeSpan(4, 4, 4),
                StartDayOfWeek = 1,
                EndDayOfWeek = 7,
                HospitalId = 1
            };

            var hours = repository.AddUpdateOperatingHours(newHour);

            Assert.AreEqual(list.First().EndDayOfWeek, newHour.EndDayOfWeek);
            Assert.AreEqual(list.First().EndTime, newHour.EndTime);
            Assert.AreEqual(list.First().StartDayOfWeek, newHour.StartDayOfWeek);
            Assert.AreEqual(list.First().StartTime, newHour.StartTime);
            MockContext.Verify(c => c.SaveChanges(), Times.Once);
        }


        [TearDown]
        public void Cleanup()
        {
            MockContext = null;
            repository = null;
        }

    }
}
