﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;
using UserRole = SnapMD.Data.Entities.UserRole;     //identical class name in ApiModels proj

namespace SnapMD.Tests.Data.Repositories
{
    /*TODO: Need to use the enums at the source*/

    [TestFixture]
    public class ConsultationRepositoryTest
    {
        protected Mock<ISnapContext> DbContext;
        protected ConsultationRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        private void InitializeRepo()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Repo = new ConsultationRepository(DbContext.Object);
        }

        private ConsultationRepository InitializeRepo(ISnapContext context)
        {
            return new ConsultationRepository(context);
        }

        private IList<TSource> BuildList<TSource>(int sampleNumber)
        {
            return Builder<TSource>.CreateListOfSize(sampleNumber).Build();
        }

        private IDbSet<TSource> BuildDbSet<TSource>(int sampleNumber) where TSource : class
        {
            return BuildList<TSource>(sampleNumber).ToDbSet();
        }

        private static List<Consultation> BuildConsultation(params Consultation[] data1)
        {
            int sampleNumber = data1.Count();
            List<Consultation> data2 = Builder<Consultation>.CreateListOfSize(sampleNumber).Build().ToList();
            int index = 0;
            foreach (Consultation item in data2)
            {
                Consultation info = data1[index];
                index++;
                item.ConsultationId = info.ConsultationId;
                item.PatientId = info.PatientId;
                item.AssignedDoctorId = info.AssignedDoctorId;
                item.ConsultantUserId = info.ConsultantUserId;
                item.ConsultationStatus = info.ConsultationStatus;
                item.ConferenceKey = info.ConferenceKey;
            }

            return data2;
        }

        private IDbSet<HospitalStaffProfile> BuildHospitalStaffProfile(params HospitalStaffProfile[] data)
        {
            int sampleNumber = data.Count();
            var dbSet = Builder<HospitalStaffProfile>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            int index = 0;
            foreach (HospitalStaffProfile item in dbSet)
            {
                HospitalStaffProfile info = data[index];
                index++;
                item.HospitalId = info.HospitalId;
                item.UserId = info.UserId;
                item.StaffId = info.StaffId;
            }
            return dbSet;
        }

        private IDbSet<PatientProfile> BuildPatient(params PatientProfile[] data1)
        {
            int sampleNumber = data1.Count();
            IDbSet<PatientProfile> data2 = Builder<PatientProfile>.CreateListOfSize(sampleNumber).Build().ToDbSet();
            int index = 0;
            foreach (PatientProfile item in data2)
            {
                PatientProfile info = data1[index];
                index++;
                item.PatientId = info.PatientId;
                item.UserId = info.UserId;
            }
            return data2;
        }

        [Test]
        public void RepoInitialization()
        {
            Assert.DoesNotThrow(() => new ClinicianSettingsRepository(DbContext.Object));
        }

        [Test]
        public void GetAllConsultationTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            IQueryable<Consultation> allData = Repo.GetAll();
            Assert.AreEqual(allData.Count(), 4, "Must Return 4");
        }
        
        [Test]
        public void GetConsultationTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            var context = new Mock<ISnapContext>();
            context.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            var repo = InitializeRepo(context.Object);
            Consultation data = repo.Get(1);
            Assert.NotNull(data);
            Assert.IsInstanceOf(typeof (Consultation), data);
            Assert.AreEqual(data.ConsultationId, 1);
        }

        [Test]
        public void GetAvailableConsultationTest_Return_No_data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>()
            {
                new PatientProfile(){
                    PatientId=3,
                    UserId=6,

                },
                 new PatientProfile(){
                    PatientId=2,
                    UserId=4,

                }
            }.ToDbSet());

            DbContext.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>()
            {
                new ConsultationPaymentsDetail(){
                    ConsultationId=1,
                    PaymentStatusCode=1,

                },
                 new ConsultationPaymentsDetail(){
                    ConsultationId=2,
                    PaymentStatusCode=1,

                }
            }.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            List<PatientAvailableConsultation> allData = Repo.GetAvailableConsultation(1,12);
            Assert.NotNull(allData);
            Assert.IsInstanceOf(typeof(List<PatientAvailableConsultation>), allData);
            Assert.AreEqual(allData.Count, 0);
        }

        [Test, Explicit("This isn't passing and is blocking on DEV.")]
        public void GetAvailableConsultationTest_Return_data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>()
            {
                new PatientProfile(){
                    PatientId=3,
                    UserId=6,

                },
                 new PatientProfile(){
                    PatientId=2,
                    UserId=4,

                }
            }.ToDbSet());

            DbContext.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>()
            {
                new ConsultationPaymentsDetail(){
                    ConsultationId=1,
                    PaymentStatusCode=1,

                },
                 new ConsultationPaymentsDetail(){
                    ConsultationId=2,
                    PaymentStatusCode=1,

                }
            }.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            List<PatientAvailableConsultation> allData = Repo.GetAvailableConsultation(1, 6);
            Assert.NotNull(allData);
            Assert.IsInstanceOf(typeof(List<PatientAvailableConsultation>), allData);
            Assert.AreEqual(2, allData.Count);
        }

        [Test]
        public void GetPatientConsultationAvgWaitTimeTest()
        {
            var now = DateTime.UtcNow;

            var consultationStatusList = new List<ConsultationStatusLog>()
            {
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.CustomerInWaiting,
                    ChangedDate = now.AddHours(0),
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.CustomerInWaiting,
                    ChangedDate = now.AddHours(1),
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.StartedConsultation,
                    ChangedDate = now.AddHours(2),
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.EndedConsultation,
                    ChangedDate = now.AddHours(2),
                }
            };

            List<Consultation> consultationList = BuildConsultationData();

            var consultation =consultationList.First(x => x.ConsultationId == 4);
            consultation.ConsultationStatusLogs = consultationStatusList;

            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            DbContext.Setup(x => x.WaitingLists).Returns(new List<WaitingList>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs).Returns(consultationStatusList.ToDbSet());

            DbContext.Setup(c => c.ScheduleSlotCalendars).Returns(new List<ScheduleSlotCalendar>
            {
                new ScheduleSlotCalendar
                {
                    DocUserId = 1,
                    SlotStatus = "A",
                    SlotStartTime = DateTime.UtcNow.AddHours(-2),
                    SlotEndTime = DateTime.UtcNow.AddHours(2)
                },
                new ScheduleSlotCalendar
                {
                    DocUserId = 2,
                    SlotStatus = "A",
                    SlotStartTime = DateTime.UtcNow.AddHours(-2),
                    SlotEndTime = DateTime.UtcNow.AddHours(-1)
                }
            }.ToDbSet());
            InitializeRepo();
            TimeSpan avgTime = Repo.GetPatientConsultationAvgWaitTime(3);
            Assert.NotNull(avgTime);
            Assert.That(avgTime.Hours, Is.EqualTo(1));
            Assert.IsInstanceOf(typeof (TimeSpan), avgTime);
        }

        [Test]
        public void GetPatientConsultationAvgWaitTimeTest_GivenWaitTimeGreaterThanStartTime_negativeWaitTimeWillBeIgnored()
        {
            var now = DateTime.UtcNow;

            var consultationStatusList = new List<ConsultationStatusLog>()
            {
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.CustomerInWaiting,
                    ChangedDate = now.AddHours(2), //This time greater than StartedConsultation event time
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.StartedConsultation,
                    ChangedDate = now.AddHours(1),
                }
            };

            List<Consultation> consultationList = BuildConsultationData();

            var consultation = consultationList.First(x => x.ConsultationId == 4);
            consultation.ConsultationStatusLogs = consultationStatusList;
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            DbContext.Setup(x => x.WaitingLists).Returns(new List<WaitingList>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs).Returns(consultationStatusList.ToDbSet());

            DbContext.Setup(c => c.ScheduleSlotCalendars).Returns(new List<ScheduleSlotCalendar>
            {
                new ScheduleSlotCalendar
                {
                    DocUserId = 1,
                    SlotStatus = "A",
                    SlotStartTime = DateTime.UtcNow.AddHours(-2),
                    SlotEndTime = DateTime.UtcNow.AddHours(2)
                },
                new ScheduleSlotCalendar
                {
                    DocUserId = 2,
                    SlotStatus = "A",
                    SlotStartTime = DateTime.UtcNow.AddHours(-2),
                    SlotEndTime = DateTime.UtcNow.AddHours(-1)
                }
            }.ToDbSet());
            InitializeRepo();
            TimeSpan avgTime = Repo.GetPatientConsultationAvgWaitTime(1);
            Assert.NotNull(avgTime);
            Assert.That(avgTime.Ticks, Is.EqualTo(0));
            Assert.IsInstanceOf(typeof(TimeSpan), avgTime);
        }


        [Test]
        public void GetPatientPastConsultationAvgTimeTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());

            DbContext.Setup(x => x.ConsultationStatusLogs).Returns(new List<ConsultationStatusLog>
            {
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.StartedConsultation,
                    ChangedDate = DateTime.UtcNow.AddHours(2)
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.EndedConsultation,
                    ChangedDate = DateTime.UtcNow.AddHours(2)
                }
            }.ToDbSet());
            InitializeRepo();
            TimeSpan avgTime = Repo.GetPatientPastConsultationAvgTime(3);
            Assert.NotNull(avgTime);
            Assert.IsInstanceOf(typeof (TimeSpan), avgTime);
        }

        [Test]
        public void GetPatientPastConsultationAvgTime_With_No_Consultation_DataTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());

            DbContext.Setup(x => x.ConsultationStatusLogs).Returns(new List<ConsultationStatusLog>
            {
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.DoctorReviewConsultation,
                    ChangedDate = DateTime.UtcNow.AddHours(2)
                },
                new ConsultationStatusLog
                {
                    ConsultationId = 4,
                    StatusAfter = ConsultationStatus.EndedConsultation,
                    ChangedDate = DateTime.UtcNow.AddHours(2)
                }
            }.ToDbSet());
            InitializeRepo();
            TimeSpan avgTime = Repo.GetPatientPastConsultationAvgTime(20);
            Assert.NotNull(avgTime);
            Assert.IsInstanceOf(typeof (TimeSpan), avgTime);
        }

        [Test, Ignore("Shibu: DBFunction can only invoked from LINQ to Entites. Not from the mocked dbcontext")]
        public void GetAvailableConsultationTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            List<PatientAvailableConsultation> availableData = Repo.GetAvailableConsultation(1, 4);
            Assert.NotNull(availableData);
            Assert.IsInstanceOf(typeof(List<PatientAvailableConsultation>), availableData);
            Assert.AreEqual(availableData.Count(), 1);
        }

        [Test]
        public void GetConsultationStatusTest()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            int statusID = Repo.GetConsultationStatus(4);
            Assert.NotNull(statusID);
            Assert.IsInstanceOf(typeof (int), statusID);
            Assert.AreEqual(statusID, 72);
        }

        [Test]
        public void GetConferenceKeyTest()
        {
            List<Consultation> consultationList = BuildConsultationData();

            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            var key = Repo.GetConferenceKey(1);
            Assert.AreEqual(key, "AC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A", "Must Return AC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A");
        }


        [Test]
        public void UpdateTokBoxSessionKeyTest()
        {
            List<Consultation> consultationList = BuildConsultationData();

            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            InitializeRepo();
            Repo.UpdateTokBoxSessionKey(1,"BC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A");
            var key = Repo.GetConferenceKey(1);
            Assert.AreEqual(key, "BC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A", "Must Return BC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A");
        }

        [Test]
        public void GetDNAConsultations_Return_No_Data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            DbContext.Setup(c => c.UserDependentRelationAuthorizations)
                .Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();
            IEnumerable<DNAScheduleConsultationDetails> dnaInformation = Repo.GetDNAConsultations(1, 1, 3,
                DateTime.Now.AddHours(-4), DateTime.Now.AddHours(4));
            Assert.IsNotNull(dnaInformation);
            Assert.IsEmpty(dnaInformation);
        }

        [Test, Ignore]
        public void GetDNAConsultations_Return_Data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.Users).Returns(new List<User>
            {
                new User
                {
                    UserId = 1,
                },
                new User
                {
                    UserId = 2,
                }
            }.ToDbSet());
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            DbContext.Setup(c => c.UserDependentRelationAuthorizations)
                .Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            InitializeRepo();
            IEnumerable<DNAScheduleConsultationDetails> dnaInformation = Repo.GetDNAConsultations(1, 1, 6,
                DateTime.UtcNow.AddDays(-1), DateTime.UtcNow.AddDays(1));
            Assert.IsNotNull(dnaInformation);
            Assert.IsInstanceOf(typeof (List<DNAScheduleConsultationDetails>), dnaInformation);
            Assert.AreEqual(dnaInformation.Count(), 1);
        }

        [Test]
        public void GetDoctorScheduledList_Return_Data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.Users).Returns(new List<User>
            {
                new User
                {
                    UserId = 1,
                },
                new User
                {
                    UserId = 2,
                }
            }.ToDbSet());
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            DbContext.Setup(c => c.UserDependentRelationAuthorizations)
                .Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            DbContext.Setup(c => c.WaitingLists).Returns(new List<WaitingList>().ToDbSet());
            InitializeRepo();
            IEnumerable<DoctorScheduleDetails> dnaInformation = Repo.GetDoctorScheduledList(1, null);
            Assert.IsNotNull(dnaInformation);
            Assert.IsAssignableFrom(typeof (List<DoctorScheduleDetails>), dnaInformation);
            Assert.AreEqual(dnaInformation.Count(), 1); //This needs to only return 1 since a consultation was -3 hours and shouldn't show up in the list of consultations
        }

        [Test]
        public void GetInTakeFormByPatientId_Return_no_dataId()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            BuildHospital();
            BuildMedicalHistory();

            InitializeRepo();
            ConsultationIntakeDetails dnaInformation = Repo.GetInTakeFormByPatientId(30);
            Assert.Null(dnaInformation);
        }

        [Test]
        public void GetInTakeFormByPatientId_Return_data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            BuildHospital();
            BuildMedicalHistory();

            DbContext.Setup(x => x.PatientConsultationReports).Returns(new List<PatientConsultationReport>
            {
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 108,
                    ConsultationDescription = "Test Data 1"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 109,
                    ConsultationDescription = "Test Data 2"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 110,
                    ConsultationDescription = "Test Data 3"
                }
            }.ToDbSet());

            InitializeRepo();

            ConsultationIntakeDetails intakeInfo = Repo.GetInTakeFormByPatientId(3);

            Assert.NotNull(intakeInfo);
            Assert.AreEqual(3, intakeInfo.PatientId);
            Assert.AreEqual(intakeInfo.ConsultantUserId, 6);
            Assert.AreEqual(intakeInfo.Assessment, "Test Data 2");
            Assert.AreEqual(intakeInfo.Objective, "Test Data 1");
            Assert.AreEqual(intakeInfo.Subjective, "Test Data");

            Assert.AreEqual(intakeInfo.IsChildBornFullTerm, "N/A");
            Assert.AreEqual(intakeInfo.IsChildBornVaginally, "N/A");
            Assert.AreEqual(intakeInfo.IsChildDischargeMother, "N/A");
        }

        [Test]
        public void GetInTakeFormBConsultationId_Return_data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });

            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            BuildHospital();
            BuildMedicalHistory();

            DbContext.SetupGet(x => x.EncounterMetadata).Returns(new List<EncounterMetadata>().ToDbSet());

            DbContext.Setup(x => x.PatientConsultationReports).Returns(new List<PatientConsultationReport>
            {
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 107,
                    ConsultationDescription = "Test Data"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 108,
                    ConsultationDescription = "Test Data 1"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 109,
                    ConsultationDescription = "Test Data 2"
                },
                new PatientConsultationReport
                {
                    ConsultationId = 1,
                    CodeId = 110,
                    ConsultationDescription = "Test Data 3"
                }

            }.ToDbSet());

            DbContext.Setup(c => c.ConsultationSoapNotesCptCodes).Returns(new List<ConsultationSoapNotesCptCode>
                {
                    new ConsultationSoapNotesCptCode {
                        Id = 1,
                        ConsultationId = 1,
                        CptCode = "0001",
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = 0
                        
                    }
                }.ToDbSet());

            DbContext.Setup(c => c.CptCodes).Returns(new List<CptCode>
                {
                    new CptCode {
                        CptCodeId = 22,
                        CptCode_ = "0001",
                         Description = string.Empty,
                        IsActive = "A",
                        CptcodeMasterId = 5
                    }
                }.ToDbSet());




            InitializeRepo();

            var intakeInfo = Repo.GetIntakeFormByConsultationId(1);

            Assert.NotNull(intakeInfo);
            Assert.AreEqual(3, intakeInfo.PatientId);
            Assert.AreEqual(intakeInfo.ConsultantUserId, 6);
            Assert.AreEqual(intakeInfo.Assessment, "Test Data 2");
            Assert.AreEqual(intakeInfo.Objective, "Test Data 1");
            Assert.AreEqual(intakeInfo.Subjective, "Test Data");

            Assert.AreEqual(intakeInfo.IsChildBornFullTerm, "N/A");
            Assert.AreEqual(intakeInfo.IsChildBornVaginally, "N/A");
            Assert.AreEqual(intakeInfo.IsChildDischargeMother, "N/A");
        }

        [Test]
        public void GetPatientPastConsultation_no_data()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            BuildHospital();


            InitializeRepo();

            IEnumerable<PatientConsultationInfo> pastConsultation = Repo.GetPatientPastConsultation(13);

            Assert.AreEqual(pastConsultation.Count(), 0);
        }

        [Test]
        public void GetPatientPastConsultation_Data_Test()
        {
            List<Consultation> consultationList = BuildConsultationData();
            DbContext.Setup(x => x.Consultations).Returns(consultationList.ToDbSet());
            SetUpTempConsultationData();
            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);
            IDbSet<HospitalStaffProfile> hospitalStaff = BuildHospitalStaffProfile(new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 1
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 2
            }, new HospitalStaffProfile
            {
                HospitalId = 1,
                UserId = 3
            });
            DbContext.Setup(c => c.HospitalStaffProfiles).Returns(hospitalStaff);
            BuildHospital();


            InitializeRepo();

            IEnumerable<PatientConsultationInfo> pastConsultation = Repo.GetPatientPastConsultation(3);
            Assert.NotNull(pastConsultation);
            Assert.AreEqual(pastConsultation.Count(), 1);
        }

        [Test]
        public void Where_Return_NO_Data_Test()
        {
            List<Consultation> conData = BuildConsultationData();
            DbContext.Setup(c => c.Consultations).Returns(conData.ToDbSet());
            InitializeRepo();
            List<Consultation> data = Repo.Where(c => c.ConsultationId == 23).ToList();
            Assert.NotNull(data);
            Assert.AreEqual(data.Count(), 0);
        }

        [Test]
        public void Where_Return_Data_Test()
        {
            List<Consultation> conData = BuildConsultationData();
            DbContext.Setup(c => c.Consultations).Returns(conData.ToDbSet());
            InitializeRepo();
            List<Consultation> data = Repo.Where(c => c.ConsultationId == 1).ToList();
            Assert.NotNull(data);
            Assert.AreEqual(data.Count(), 1);
        }

        [Test]
        public void GetAll_Return_Data_Test()
        {
            List<Consultation> conData = BuildConsultationData();
            DbContext.Setup(c => c.Consultations).Returns(conData.ToDbSet());

            InitializeRepo();
            IQueryable<Consultation> data = Repo.GetAll();
            Assert.NotNull(data);
            Assert.AreEqual(data.Count(), 4);
        }
        
        [Test]
        public void IsUserAuthorizeConsultation_own_reporttest()
        {
            List<Consultation> conData = BuildConsultationData();
            DbContext.Setup(c => c.Consultations).Returns(conData.ToDbSet());
            DbContext.SetupGet(c => c.PatientProfiles)
                .Returns(Builder<PatientProfile>.CreateListOfSize(100).Build().ToDbSet());

            DbContext.Setup(c => c.Users).Returns(new List<User>().ToDbSet());
            DbContext.Setup(c => c.UserRoles).Returns(new List<UserRole>().ToDbSet());
            DbContext.Setup(c => c.Roles).Returns(new List<Role>().ToDbSet());
            DbContext.Setup(c => c.SnapFunctions).Returns(new List<SnapFunction>().ToDbSet());
            DbContext.Setup(c => c.RoleFunctions).Returns(new List<RoleFunction>().ToDbSet());

            IDbSet<PatientProfile> patientList = BuildPatient(new PatientProfile
            {
                PatientId = 3,
                UserId = 6,
                IsActive = "A"
            }, new PatientProfile
            {
                PatientId = 4,
                UserId = 8,
                IsActive = "A"
            });
            DbContext.Setup(c => c.PatientProfiles).Returns(patientList);

            InitializeRepo();
            bool hasAccess = Repo.IsUserAuthorizeConsultation(1, 1);
            Assert.IsTrue(hasAccess);

            hasAccess = Repo.IsUserAuthorizeConsultation(1, 22);
            Assert.IsFalse(hasAccess);
        }

        /*
         * adds items to Consultations
         * usning Consultation tbl
         */

        [Test]
        public void Add()
        {
            Consultation addedItem = null;
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.Consultations.Add(It.IsAny<Consultation>()))
                .Callback((Consultation item) => { addedItem = item; });
            InitializeRepo();

            Repo.Add(new Consultation {ConsultationStatus = (int) ConsultationStatus.DoctorInitiatedConsultation});
            //just for test
            Assert.IsNotNull(addedItem);
        }

        /*
         * returns the description of a code, by codeId
         * using Code tbl
         */

        [Test]
        public void GetCode_CodeFound_Returns_Description()
        {
            DbContext.Setup(c => c.Codes).Returns(new List<Code>
            {
                new Code
                {
                    CodeId = 1,
                    Description = "Test Code 1"
                },
                new Code
                {
                    CodeId = 2,
                    Description = "Test Code 2"
                }
            }.ToDbSet());
            InitializeRepo();

            string result = Repo.GetCode(2);
            Assert.NotNull(result);
            Assert.IsInstanceOf(typeof (string), result);
            Assert.AreEqual(result, "Test Code 2");
        }

        [Test]
        public void GetCode_CodeNotFound_Returns_NullOrEmpty()
        {
            IList<Code> codes = BuildList<Code>(3);
            DbContext.Setup(c => c.Codes).Returns(codes.ToDbSet());
            InitializeRepo();

            Assert.IsNullOrEmpty(Repo.GetCode(100));
        }

        [Test]
        public void GetCode_GotException_Returns_NullOrEmpty()
        {
            //not used mock there will an exception
            var codes = Builder<Code>.CreateListOfSize(99).Build();
            DbContext.SetupGet(c => c.Codes).Returns(codes.ToDbSet());
            InitializeRepo();
            Assert.IsNullOrEmpty(Repo.GetCode(100));
        }

        /*
         * Get a physician past ended consultations(status 72) details 
         *      of active patients 
         *      by physician's userId
         * using consultaion, HospitalStaffProfile, PatientProfiles
         */

        [Test]
        public void GetPatientPastConsultation_Found_Returns_List()
        {
            //data
            var patientProfile = new PatientProfile
            {
                PatientId = 1,
                PatientName = "Patient Name",
                LastName = "Patient Last Name",
                Dob = DateTime.Now,
                IsActive = "A", //important
                UserId = 1
            };

            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "Doctor Name",
                LastName = "Doctor Last Name"
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                ConsultationStatus = (int) ConsultationStatus.EndedConsultation, //important
                PatientId = patientProfile.PatientId,
                AssignedDoctorId = physician.UserId, //important its userId not staffId
                ConsultantUserId = 10,
                ConsultationDate = DateTime.Now,
                ConsultationTime = DateTime.Now.AddHours(4),
                ConsultationDuration = 100
            };

            //mock
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> {patientProfile}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            InitializeRepo();

            IEnumerable<PatientConsultationInfo> result = Repo.GetPatientPastConsultation(physician.UserId);
            //userId not staffId
            Assert.IsInstanceOf<List<PatientConsultationInfo>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetPatientPastConsultation_NotFound_Returns_EmptyList()
        {
            //data
            var patientProfile = new PatientProfile
            {
                PatientId = 1,
                PatientName = "Patient Name",
                LastName = "Patient Last Name",
                Dob = DateTime.Now,
                IsActive = "A", //important
                UserId = 1
            };

            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "Doctor Name",
                LastName = "Doctor Last Name"
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                ConsultationStatus = (int) ConsultationStatus.DoctorInitiatedConsultation, //important
                PatientId = patientProfile.PatientId,
                AssignedDoctorId = physician.UserId, //important its userId not staffId
                ConsultantUserId = 10,
                ConsultationDate = DateTime.Now,
                ConsultationTime = DateTime.Now.AddHours(4),
                ConsultationDuration = 100
            };

            //mock
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> {patientProfile}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            InitializeRepo();

            IEnumerable<PatientConsultationInfo> result = Repo.GetPatientPastConsultation(100);
            //if used physician.UserId, will get data
            Assert.IsInstanceOf<List<PatientConsultationInfo>>(result);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetPatientPastConsultation_Returned_Fields()
        {
            //data
            var patientProfile = new PatientProfile
            {
                PatientId = 1,
                PatientName = "Patient Name",
                LastName = "Patient Last Name",
                Dob = DateTime.Now,
                IsActive = "A", //important
                UserId = 1
            };

            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = 2,
                Name = "Doctor Name",
                LastName = "Doctor Last Name"
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                ConsultationStatus = (int) ConsultationStatus.EndedConsultation, //important
                PatientId = patientProfile.PatientId,
                AssignedDoctorId = physician.UserId, //important its userId not staffId
                ConsultantUserId = 10,
                ConsultationDate = DateTime.Now,
                ConsultationTime = DateTime.Now.AddHours(4),
                ConsultationDuration = 100
            };

            //mock
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> {patientProfile}.ToDbSet());
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            InitializeRepo();

            PatientConsultationInfo result = Repo.GetPatientPastConsultation(physician.UserId).First();
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patientProfile.PatientId, result.PatientId);
            Assert.AreEqual(physician.UserId, result.AssignedDoctorId);
            Assert.AreEqual(physician.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(physician.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(patientProfile.PatientName, result.PatientFirstName);
            Assert.AreEqual(patientProfile.LastName, result.PatientLastName);
            Assert.AreEqual(patientProfile.Dob, result.DOB);
            Assert.AreEqual(consultation.ConsultationTime, result.ConsultationTimeInfo);
            Assert.AreEqual(consultation.ConsultationDuration, result.ConsultationDuration);
        }

        //TODO: better if we check the patient attachted to the consultaion is active
        /*
         * Get consultations attacht to a patient, by patientId
         *      where consultion payment done/ doctor assigned (status 68/69)
         *             created date + 24hr >= utc now
         *  using consultation tbl
         */

        [Test, Ignore("DbFunctions.AddHours need to be using with another static")]
        public void GetAvailableConsultation_Found_Returns_List()
        {
            int physicianUserId = 100;
            var paymentDoneCons = new Consultation
            {
                ConsultationId = 1,
                ConsultantUserId = 1,
                PatientId = physicianUserId, //important 
                ConsultationStatus = (int) ConsultationStatus.PaymentDone, //payment done
                ConsultationDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow.AddHours(-23.50), // +24 hr would be > utc now
            };
            var doctorAssignedCons = new Consultation
            {
                ConsultationId = 2,
                ConsultantUserId = 2,
                PatientId = physicianUserId, //important 
                ConsultationStatus = (int) ConsultationStatus.DoctorAssigned, //doctor assigned
                ConsultationDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow.AddHours(-23.50),
            };

            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation> {paymentDoneCons, doctorAssignedCons}.ToDbSet());
            InitializeRepo();

            List<PatientAvailableConsultation> result = Repo.GetAvailableConsultation(1,physicianUserId);
            Assert.IsInstanceOf<PatientAvailableConsultation>(result);
            Assert.AreEqual(2, result.Count);
        }

        [Test, Ignore("DbFunctions.AddHours need to be using with another static")]
        public void GetAvailableConsultation_NotFound_Returns_EmptyList()
        {
            int physicianUserId = 100;
            var doctorAssignedCons = new Consultation
            {
                ConsultationId = 2,
                ConsultantUserId = 2,
                PatientId = physicianUserId,
                ConsultationStatus = (int) ConsultationStatus.DoctorAssigned,
                ConsultationDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow.AddHours(-27.50), // +24 hr would be < utc now
            };

            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation> {doctorAssignedCons}.ToDbSet());
            InitializeRepo();

            List<PatientAvailableConsultation> result = Repo.GetAvailableConsultation(1,physicianUserId);
            Assert.IsInstanceOf<PatientAvailableConsultation>(result);
            Assert.AreEqual(0, result.Count);
        }

        /*
        * checks if a physician has active schedule, by his user Id
        *      where utc time now is in between schedule start and end time
        * using ScheduleSlotCalendars tbl
        */

        [Test]
        public void IsDoctorHasAvailableSlot_FoundAnyEntity_Returns_True()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "A",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsTrue(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void IsDoctorHasAvailableSlot_SlotNotActive_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "I",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsFalse(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void IsDoctorHasAvailableSlot_UtcNow_NotBetweenSchedule_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "I",
                SlotStartTime = DateTime.UtcNow.AddHours(-2),
                SlotEndTime = DateTime.UtcNow.AddHours(-1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsFalse(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void IsDoctorHasAvailableSlot_NotFound_ByUserId_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "A",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(new List<ScheduleSlotCalendar> {slot}.ToDbSet());

            InitializeRepo();
            Assert.False(Repo.IsDoctorHasAvailableSlot(1));
        }

        //TODO: source code still using ConsultationStatusCode, it would be ConsultationStatus
        /*
        * checks if a consultation is doctorassigned(status = 69) or not 
        *        by consultaionId
        * using Consultation tbl
        */

        [Test]
        public void IsCustomerAvailable_FoundEntity_Returns_True()
        {
            var consultation = new Consultation
            {
                ConsultationId = 100,
                ConsultationStatus = (int) ConsultationStatus.DoctorAssigned
            };
            IList<Consultation> consultations = BuildList<Consultation>(3);
            consultations.Add(consultation);

            DbContext.Setup(x => x.Consultations).Returns(consultations.ToDbSet());
            InitializeRepo();
            Assert.IsTrue(Repo.IsCustomerAvailable(consultation.ConsultationId));
        }

        [Test]
        public void IsCustomerAvailable_NotFoundEntity_Returns_False()
        {
            var consultation = new Consultation
            {
                ConsultationId = int.MaxValue - 24,
                ConsultationStatus = (int)ConsultationStatus.EndedConsultation
            };
            //status not DoctorAssigned(69)
            IList<Consultation> consultations = BuildList<Consultation>(3);
            consultations.Add(consultation);

            DbContext.SetupGet(x => x.Consultations).Returns(consultations.ToDbSet());
            InitializeRepo();
            Assert.IsFalse(Repo.IsCustomerAvailable(int.MaxValue - 24));
        }

        //TODO: duplicate copy of existing IsDoctorHasAvailableSlot
        /*
        * checks if a physician has active schedule, by his user Id
        *      where utc time now is in between schedule start and end time
        * using ScheduleSlotCalendars tbl
        */

        [Test]
        public void ClinicianScheduled_FoundAnyEntity_Returns_True()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "A",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsTrue(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void ClinicianScheduled_SlotNotActive_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "I",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsFalse(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void ClinicianScheduled_UtcNow_NotBetweenSchedule_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "I",
                SlotStartTime = DateTime.UtcNow.AddHours(-2),
                SlotEndTime = DateTime.UtcNow.AddHours(-1)
            };
            IList<ScheduleSlotCalendar> slots = BuildList<ScheduleSlotCalendar>(3);
            slots.Add(slot);

            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(slots.ToDbSet());
            InitializeRepo();
            Assert.IsFalse(Repo.IsDoctorHasAvailableSlot(slot.DocUserId));
        }

        [Test]
        public void ClinicianScheduled_NotFound_ByUserId_Returns_False()
        {
            var slot = new ScheduleSlotCalendar
            {
                DocUserId = 100,
                SlotStatus = "A",
                SlotStartTime = DateTime.UtcNow.AddHours(-1),
                SlotEndTime = DateTime.UtcNow.AddHours(1)
            };
            DbContext.Setup(x => x.ScheduleSlotCalendars).Returns(new List<ScheduleSlotCalendar> {slot}.ToDbSet());

            InitializeRepo();
            Assert.False(Repo.IsDoctorHasAvailableSlot(1));
        }

       

        /// <summary>
        /// returns Dropped consultations details(consultaion, physician, current patient, patient user) 
        ///      of an active patient, by doctorUserId, hospitalId, patientId,
        ///      dateTime range where consultaionDate in range
        ///      and consultaionTime <= current Utc datetime
        ///  
        /// if any parametter is  null, avoid its comparizon
        /// 
        /// using Consultation, DroppedConsultation, PatientProfile, HospitalStaffProfile 
        /// </summary>
        [Test]
        public void GetDroppedConsultations_Found_Returns_List()
        {
            int? doctorUserId = 10;
            int? hospitalId = 1;
            int? patientId = 2;
            DateTime? startDate = DateTime.UtcNow.AddDays(-2);
            DateTime? endDate = DateTime.UtcNow.AddDays(2);

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_NotFound_Returns_EmptyList()
        {
            int doctorUserId = 1;
            int hospitalId = 1;
            int patientId = 2;
            DateTime startDate = DateTime.UtcNow.AddDays(-2);
            DateTime endDate = DateTime.UtcNow.AddDays(2);

            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile>().ToDbSet());
            DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations).Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_Returned_Items_Fields()
        {
            int doctorUserId = 1;
            int hospitalId = 1;
            int patientId = 2;
            DateTime startDate = DateTime.UtcNow.AddDays(-2);
            DateTime endDate = DateTime.UtcNow.AddDays(2);

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = doctorUserId,
                HospitalId = hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is active
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();

            //test
            DroppedConsultationDetails result =
                Repo.GetDroppedConsultations(doctorUserId, hospitalId, patientId, startDate, endDate).First();
            Assert.AreEqual(physician.Name, result.DoctorFirstName);
            Assert.AreEqual(physician.LastName, result.DoctorLastName);
            Assert.AreEqual(hospitalId, result.HospitalId);
            Assert.AreEqual(consultation.ConsultationId, result.ConsultationId);
            Assert.AreEqual(patient.PatientId, consultation.PatientId);
            Assert.AreEqual(guardiantPatient.UserId, consultation.ConsultantUserId);
            Assert.AreEqual(doctorUserId, result.AssignedDoctorId);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.AreEqual(guardiantPatient.PatientName, result.GuardianFirstName);
            Assert.AreEqual(guardiantPatient.LastName, result.GuardianLastName);
            Assert.AreEqual(consultation.ConsultationTime, result.ConsultationTimeInfo);
        }

        [Test]
        public void GetDroppedConsultations_SelectEntitys_When_Patient_IsActive()
        {
            int doctorUserId = 1;
            int hospitalId = 1;
            int patientId = 2;
            DateTime startDate = DateTime.UtcNow.AddDays(-2);
            DateTime endDate = DateTime.UtcNow.AddDays(2);

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = doctorUserId,
                HospitalId = hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardianPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "I" //not active 'A'
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardianPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardianPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();

            //test
            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_SelectEntitys_When_ConsultationTime_LessOrEqual_DateTimeUtcNow()
        {
            int doctorUserId = 1;
            int hospitalId = 1;
            int patientId = 2;
            DateTime startDate = DateTime.UtcNow.AddDays(-2);
            DateTime endDate = DateTime.UtcNow.AddDays(2);

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = doctorUserId,
                HospitalId = hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //active 'A'
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow.AddDays(1), // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow.AddDays(1) // > thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();

            //test
            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.AreEqual(0, result.Count());
        }

        //null params
        [Test]
        public void GetDroppedConsultations_DoctorUserId_IsNull_AvoidComparizon()
        {
            int? doctorUserId = null;
            int? hospitalId = 1;
            int? patientId = 2;
            DateTime? startDate = DateTime.UtcNow.AddDays(-2);
            DateTime? endDate = DateTime.UtcNow.AddDays(2);

            //data
            int? entityDoctorUserId = 10;
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) entityDoctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();

            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_HospitalId_IsNull_AvoidComparizon()
        {
            int? doctorUserId = 10;
            int? hospitalId = null;
            int? patientId = 2;
            DateTime? startDate = DateTime.UtcNow.AddDays(-2);
            DateTime? endDate = DateTime.UtcNow.AddDays(2);

            //data
            int? entityHospitalId = 1;
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) entityHospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = entityHospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = entityHospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) entityHospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_PatientId_IsNull_AvoidComparizon()
        {
            int? doctorUserId = 10;
            int? hospitalId = 1;
            int? patientId = null;
            DateTime? startDate = DateTime.UtcNow.AddDays(-2);
            DateTime? endDate = DateTime.UtcNow.AddDays(2);

            //data
            int? entityPatientId = 2;
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) entityPatientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_StartDate_IsNull_AvoidComparizon()
        {
            int? doctorUserId = 10;
            int? hospitalId = 1;
            int? patientId = 2;
            DateTime? startDate = null;
            DateTime? endDate = DateTime.UtcNow.AddDays(2);

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_EndDate_IsNull_AvoidComparizon()
        {
            int? doctorUserId = 10;
            int? hospitalId = 1;
            int? patientId = 2;
            DateTime? startDate = DateTime.UtcNow.AddDays(-2);
            DateTime? endDate = null;

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(doctorUserId, hospitalId,
                patientId, startDate, endDate);
            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetDroppedConsultations_All_IsNull()
        {
            int? doctorUserId = 10;
            int? hospitalId = 1;
            int? patientId = 2;

            //data
            var physician = new HospitalStaffProfile
            {
                StaffId = 1,
                UserId = (int) doctorUserId,
                HospitalId = (int) hospitalId,
                Name = "Doctor first name",
                LastName = "Doctor last name"
            };

            var guardiantPatient = new PatientProfile
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Guardian first name",
                LastName = "Guardian last name"
            };

            var patient = new PatientProfile
            {
                PatientId = (int) patientId,
                UserId = 3,
                HospitalId = hospitalId,
                PatientName = "Patient first name",
                LastName = "Patient last name",
                IsActive = "A" //is actives
            };

            var consultation = new Consultation
            {
                ConsultationId = 1,
                HospitalId = (int) hospitalId,
                PatientId = patient.PatientId,
                AssignedDoctorId = physician.UserId,
                ConsultantUserId = (int) guardiantPatient.UserId,
                ConsultationDate = DateTime.UtcNow, // >= startDate, <= endDate
                ConsultationTime = DateTime.UtcNow // <= thread UtcNow
            };

            var droppedConsultation = new DroppedConsultation
            {
                DroppedConsultId = 1,
                ConsultationId = consultation.ConsultationId
            };

            //mock
            DbContext.Setup(x => x.HospitalStaffProfiles).Returns(new List<HospitalStaffProfile> {physician}.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile> {guardiantPatient, patient}.ToDbSet());
            DbContext.Setup(x => x.Consultations).Returns(new List<Consultation> {consultation}.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation> {droppedConsultation}.ToDbSet());
            InitializeRepo();


            IEnumerable<DroppedConsultationDetails> result = Repo.GetDroppedConsultations(hospitalId: null,
                doctorId: null,
                patientId: null,
                startDate: null,
                endDate: null);

            Assert.IsInstanceOf<List<DroppedConsultationDetails>>(result);
            Assert.AreEqual(1, result.Count());
        }

        /*
         * selects the schedule/ondeman consultations where they are currently on sesson
         *  by hospitalId
         *      consultation status started or in progress
         *      not dropped (see tbl DroppedConsultations)
         *      first waitng event if any
         *      first starting event if any
         *      schedule datetime if schedule consultation, else null
         *      
         * Using Consultation, ScheduleConsultaionTemp, HospitalStaffProfile, PatientProfile, ConsultationEvent, DroppedConsultations tbl
         */
        [Test]
        public void RunningConsultations_Found_Returns_List()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.IsInstanceOf<IEnumerable<PatientConsultationInfo>>(result);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void RunningConsultations_NotFound_Returns_List()
        {
            int hospitalId = 1;

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>().ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>().ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>().ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(BuildList<DroppedConsultation>(3).ToDbSet());      //has no colsultaion's but, DroppedConsultations tbl has data
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.IsInstanceOf<IEnumerable<PatientConsultationInfo>>(result);
            Assert.AreEqual(0, result.Count());
        }


        //Schedule Consultations
        [Test]
        public void RunningConsultations_Selects_ScheduleConsultations()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()      //has schedule temp, thats why scheduled
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(hospitalStaff.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaff.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.IsNotNull(result.ConsultationTimeInfo);
            Assert.AreEqual(schedule.ScheduledTime, result.ConsultationTimeInfo);
        }

        [Test]
        public void RunningConsultations_Ignors_ScheduleConsultations_If_DrropedConsultaion()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()      //has schedule temp, thats why scheduled
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            var droppedConsultaion = new DroppedConsultation()       
            {
                ConsultationId = consultation.ConsultationId,   //was been droped
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>() { droppedConsultaion }.ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(ConsultationStatus.StartedConsultation)]              //started
        [TestCase(ConsultationStatus.InProgress)]                       //inprogress
        public void RunningConsultations_Selects_ScheduleConsultations_If_Consultation(ConsultationStatus status)
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)status,  //consultation status
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation)]
        [TestCase(ConsultationStatus.CancelConsultaion)]
        [TestCase(ConsultationStatus.DoctorAssigned)]
        [TestCase(ConsultationStatus.DroppedConsultation)]
        [TestCase(ConsultationStatus.EndedConsultation)]
        [TestCase(ConsultationStatus.PaymentDone)]
        [TestCase(null)]
        public void RunningConsultations_Ignors_ScheduleConsultations_For_Other_ConsultationStatus(ConsultationStatus? status)
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int?)status,     //not valid status
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void RunningConsultations_ScheduleConsultations_First_WaitingEvent_Time()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            var firstWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.CustomerInWaiting, //patient at waiting room
                ChangedDate = DateTime.UtcNow
            };
            var lastWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.CustomerInWaiting, //patient at waiting room
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstWaitingEvent, lastWaitingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(firstWaitingEvent.ChangedDate, result.WaitingConsultationInfo);
        }

        [Test]
        public void RunningConsultations_ScheduleConsultations_First_StartingEvent_Time()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            var firstStartingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.StartedConsultation,      //consultation started
                ChangedDate = DateTime.UtcNow
            };
            var lastStartingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.StartedConsultation,      //consultation started
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstStartingEvent, lastStartingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(firstStartingEvent.ChangedDate, result.StartedConsultationInfo);
        }

        [Test]
        public void RunningConsultations_ScheduleConsultations_If_WaitingEvent_Or_StartingEvent_NotFound_Null()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            var firstWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = 100,   //not this consultation
                StatusAfter = ConsultationStatus.CustomerInWaiting,          //patient at waiting
                ChangedDate = DateTime.UtcNow
            };
            var firstStratingEvent = new ConsultationStatusLog()
            {
                ConsultationId = 100,   //not this consultation
                StatusAfter = ConsultationStatus.StartedConsultation,          //consultation started
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstWaitingEvent, firstStratingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.IsNull(result.WaitingConsultationInfo);
            Assert.IsNull(result.StartedConsultationInfo);
        }

        [Test]
        public void RunningConsultations_ScheduleConsultations_SelectsBy_HospitalId()
        {
            int hospitalId = 10;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var schedule = new ScheduledConsultationTemp()
            {
                ConsultationId = consultation.ConsultationId,
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>() { schedule }.ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId: 1); //no row with hospitalId=1
            Assert.AreEqual(0, result.Count());
        }

        //ondemand consultations
        [Test]
        public void RunningConsultations_Selects_OndemandConsultations()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());      //has no schedule temp that's why ondemand
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(hospitalStaff.Name, result.AssignedDoctorFirstName);
            Assert.AreEqual(hospitalStaff.LastName, result.AssignedDoctorLastName);
            Assert.AreEqual(patient.PatientName, result.PatientFirstName);
            Assert.AreEqual(patient.LastName, result.PatientLastName);
            Assert.IsNull(result.ConsultationTimeInfo);
        }

        [Test]
        public void RunningConsultations_Ignors_OndemandConsultations_If_DrropedConsultaion()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var droppedConsultaion = new DroppedConsultation()
            {
                ConsultationId = consultation.ConsultationId,   //was been droped
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());      //has no schedule temp that's why ondemand
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>() { droppedConsultaion }.ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(0, result.Count());
        }


        [Test]
        [TestCase(ConsultationStatus.StartedConsultation)]              //started
        [TestCase(ConsultationStatus.InProgress)]                       //inprogress
        public void RunningConsultations_Selects_OndemandConsultations_If_Consultation(ConsultationStatus status)
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)status,  //consultation status
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation)]
        [TestCase(ConsultationStatus.CancelConsultaion)]
        [TestCase(ConsultationStatus.DoctorAssigned)]
        [TestCase(ConsultationStatus.DroppedConsultation)]
        [TestCase(ConsultationStatus.EndedConsultation)]
        [TestCase(ConsultationStatus.PaymentDone)]
        [TestCase(null)]
        public void RunningConsultations_Ignors_OndemandConsultations_For_Other_ConsultationStatus(ConsultationStatus? status)
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int?)status,     //not valid status
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void RunningConsultations_OndemandConsultations_First_WaitingEvent_Time()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var firstWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.CustomerInWaiting, //patient at waiting room
                ChangedDate = DateTime.UtcNow
            };
            var lastWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.CustomerInWaiting, //patient at waiting room
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstWaitingEvent, lastWaitingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(firstWaitingEvent.ChangedDate, result.WaitingConsultationInfo);
        }

        [Test]
        public void RunningConsultations_OndemandConsultations_First_StartingEvent_Time()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var firstStartingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.StartedConsultation,      //consultation started
                ChangedDate = DateTime.UtcNow
            };
            var lastStartingEvent = new ConsultationStatusLog()
            {
                ConsultationId = consultation.ConsultationId,
                StatusAfter = ConsultationStatus.StartedConsultation,      //consultation started
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstStartingEvent, lastStartingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.AreEqual(firstStartingEvent.ChangedDate, result.StartedConsultationInfo);
        }

        [Test]
        public void RunningConsultations_OndemandConsultations_If_WaitingEvent_Or_StartingEvent_NotFound_Null()
        {
            int hospitalId = 1;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            var firstWaitingEvent = new ConsultationStatusLog()
            {
                ConsultationId = 100,   //not this consultation
                StatusAfter = ConsultationStatus.CustomerInWaiting,          //patient at waiting
                ChangedDate = DateTime.UtcNow
            };
            var firstStratingEvent = new ConsultationStatusLog()
            {
                ConsultationId = 100,   //not this consultation
                StatusAfter = ConsultationStatus.StartedConsultation,        //consultation started
                ChangedDate = DateTime.UtcNow.AddMinutes(1)
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>() { firstWaitingEvent, firstStratingEvent }.ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId).Single();
            Assert.IsNull(result.WaitingConsultationInfo);
            Assert.IsNull(result.StartedConsultationInfo);
        }

        [Test]
        public void RunningConsultations_OndemandConsultations_SelectsBy_HospitalId()
        {
            int hospitalId = 10;
            var hospitalStaff = new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                HospitalId = hospitalId,
                Name = "Doc first name",
                LastName = "Doc last name",
            };

            var patient = new PatientProfile()
            {
                PatientId = 1,
                UserId = 2,
                HospitalId = hospitalId,
                PatientName = "Ptn first name",
                LastName = "Ptn last name",
            };

            var consultation = new Consultation()
            {
                ConsultationId = 1,
                HospitalId = hospitalId,
                ConsultationStatus = (int)ConsultationStatus.StartedConsultation,  //started
                ConsultationDate = DateTime.UtcNow,
                ConsultationTime = DateTime.UtcNow,
                AssignedDoctorId = hospitalStaff.UserId,
                PatientId = patient.PatientId
            };

            DbContext.Setup(x => x.HospitalStaffProfiles)
                .Returns(new List<HospitalStaffProfile>() { hospitalStaff }.ToDbSet());
            DbContext.Setup(x => x.PatientProfiles)
                .Returns(new List<PatientProfile>() { patient }.ToDbSet());
            DbContext.Setup(x => x.Consultations)
                .Returns(new List<Consultation>() { consultation }.ToDbSet());
            DbContext.Setup(x => x.ScheduledConsultationTemps)
                .Returns(new List<ScheduledConsultationTemp>().ToDbSet());
            DbContext.Setup(x => x.ConsultationStatusLogs)
                .Returns(new List<ConsultationStatusLog>().ToDbSet());
            DbContext.Setup(x => x.DroppedConsultations)
                .Returns(new List<DroppedConsultation>().ToDbSet());
            InitializeRepo();

            var result = Repo.RunningConsultations(hospitalId: 1); //no row with hospitalId=1
            Assert.AreEqual(0, result.Count());
        }


        /*
         * check the values of the consultation status codes
         */

        [Test]
        public void Status_PaymentDone_Is_68()
        {
            Assert.AreEqual(68, (int) ConsultationStatus.PaymentDone);
        }

        [Test]
        public void Status_DoctorAssigned_Is_69()
        {
            Assert.AreEqual(69, (int) ConsultationStatus.DoctorAssigned);
        }

        [Test]
        public void Status_DoctorInitiatedConsultation_Is_70()
        {
            Assert.AreEqual(70, (int) ConsultationStatus.DoctorInitiatedConsultation);
        }

        [Test]
        public void Status_StartedConsultation_Is_71()
        {
            Assert.AreEqual(71, (int) ConsultationStatus.StartedConsultation);
        }

        [Test]
        public void Status_EndedConsultation_Is_72()
        {
            Assert.AreEqual(72, (int) ConsultationStatus.EndedConsultation);
        }

        [Test]
        public void Status_CancelConsultaion_Is_79()
        {
            Assert.AreEqual(79, (int) ConsultationStatus.CancelConsultaion);
        }

        [Test]
        public void Status_InProgressConsultaion_Is_80()
        {
            Assert.AreEqual(80, (int) ConsultationStatus.InProgress);
            Assert.AreEqual(80, (int) ConsultationStatus.InProgress);
        }

        [Test]
        public void Status_DroppedConsultaion_Is_81()
        {
            Assert.AreEqual(81, (int)ConsultationStatus.DroppedConsultation);
        }



        private void SetUpTempConsultationData()
        {
            var schedultTemp = new List<ScheduledConsultationTemp>();
            schedultTemp.Add(new ScheduledConsultationTemp
            {
                ScheduledTime = DateTime.UtcNow,
                ScheduledDate = DateTime.UtcNow,
                ConsultationId = 1,
                PatientId = 3,
                AssignedDoctorId = 1,
                CreatedDate = DateTime.UtcNow.AddMinutes(-30)
            });
            schedultTemp.Add(new ScheduledConsultationTemp
            {
                ScheduledTime = DateTime.UtcNow.AddHours(4),
                ScheduledDate = DateTime.UtcNow.AddHours(4),
                CreatedDate = DateTime.UtcNow.AddMinutes(-30),
                ConsultationId = 2,
                PatientId = 3,
                AssignedDoctorId = 1,
            });
            schedultTemp.Add(new ScheduledConsultationTemp
            {
                ScheduledTime = DateTime.UtcNow.AddHours(4),
                ScheduledDate = DateTime.UtcNow.AddHours(4),
                CreatedDate = DateTime.UtcNow.AddMinutes(-45),
                ConsultationId = 3,
                PatientId = 4,
                AssignedDoctorId = 2,
            });
            schedultTemp.Add(new ScheduledConsultationTemp
            {
                ScheduledTime = DateTime.UtcNow.AddHours(1),
                ScheduledDate = DateTime.UtcNow.AddHours(-1),
                CreatedDate = DateTime.UtcNow.AddHours(4),
                ConsultationId = 4,
                PatientId = 4,
                AssignedDoctorId = 3,
            });
            DbContext.Setup(c => c.ScheduledConsultationTemps).Returns(schedultTemp.ToDbSet());
        }

        private void BuildHospital()
        {
            var hospitalId = new List<Hospital>
            {
                new Hospital
                {
                    HospitalId = 1,
                    IsActive = "Y",
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            IsActive = true,
                            AddressText = "Test Address"
                        }
                    }
                }
            };

            DbContext.Setup(c => c.Hospitals).Returns(hospitalId.ToDbSet());
        }

        private void BuildMedicalHistory()
        {
            IList<PatientMedicalHistory> data = Builder<PatientMedicalHistory>.CreateListOfSize(2).Build();
            PatientMedicalHistory info = data.FirstOrDefault();
            info.PatientId = 3;
            info.HistoryId = 1;
            info.MedicalCondition1 = 94;
            info.Code_MedicalCondition1 = new Code
            {
                Description = "Choose"
            };
            info.Code_MedicalCondition2 = new Code
            {
                Description = "Choose"
            };
            info.Code_MedicalCondition3 = new Code
            {
                Description = "Choose"
            };
            info.MedicalCondition2 = 94;

            info.MedicalCondition3 = 94;
            info.MedicalCondition4 = 94;
            info.Code_TakingMedication1 = new Code
            {
                Description = "Choose"
            };
            info.Code_TakingMedication2 = new Code
            {
                Description = "Choose"
            };
            info.Code_TakingMedication3 = new Code
            {
                Description = "Choose"
            };
            info.TakingMedication1 = 94;
            info.TakingMedication2 = 94;
            info.TakingMedication3 = 94;
            info.TakingMedication4 = "";
            info.AllergicMedication1 = 94;
            info.AllergicMedication2 = 94;
            info.AllergicMedication3 = 94;
            info.AllergicMedication4 = 94;

            PatientMedicalHistory info1 = data.Skip(1).FirstOrDefault();

            info1.PatientId = 4;
            info1.HistoryId = 1;
            info1.MedicalCondition1 = 94;
            info1.MedicalCondition2 = 94;
            info1.MedicalCondition3 = 94;
            info1.MedicalCondition4 = 94;
            info1.TakingMedication1 = 94;
            info1.TakingMedication2 = 94;
            info1.TakingMedication3 = 94;
            info1.TakingMedication4 = "";
            info1.AllergicMedication1 = 94;
            info1.AllergicMedication2 = 94;
            info1.AllergicMedication3 = 94;
            info1.AllergicMedication4 = 94;

            DbContext.Setup(c => c.PatientMedicalHistories).Returns(data.ToDbSet());
        }

        public static List<Consultation> BuildConsultationData()
        {
            return BuildConsultation(new Consultation
            {
                ConsultationTime = DateTime.UtcNow.AddHours(-3),
                ConsultationDate = DateTime.UtcNow.AddHours(-3),
                ConsultationId = 1,
                PatientId = 3,
                AssignedDoctorId = 1,
                ConsultantUserId = 6,
                ConsultationStatus = 69,
                HospitalId = 1,
                CreateDate = DateTime.UtcNow.AddMinutes(-30),
                ConferenceKey = "AC5E23EB-19DA-4DE9-9D8C-F6B73AB8D83A"
            }, new Consultation
            {
                ConsultationDate = DateTime.UtcNow.AddHours(4),
                ConsultationTime = DateTime.UtcNow.AddHours(4),
                CreateDate = DateTime.UtcNow.AddMinutes(-30),
                ConsultationId = 2,
                PatientId = 3,
                HospitalId = 1,
                AssignedDoctorId = 1,
                ConsultantUserId = 6,
                ConsultationStatus = (int) ConsultationStatus.PaymentDone,
                ConferenceKey = "A315B49F-4592-428F-BC9F-272812DF89A4"
            }, new Consultation
            {
                ConsultationDate = DateTime.UtcNow.AddHours(4),
                ConsultationTime = DateTime.UtcNow.AddHours(4),
                CreateDate = DateTime.UtcNow.AddMinutes(-45),
                ConsultationStatus = (int) ConsultationStatus.DoctorAssigned,
                ConsultationId = 3,
                PatientId = 4,
                AssignedDoctorId = 2,
                HospitalId = 1,
                ConsultantUserId = 8,
                ConferenceKey = "E92C1C47-57A4-43DD-B684-993CDC93C960"
            }, new Consultation
            {
                CreateDate = DateTime.UtcNow.AddHours(-4),
                ConsultationTime = DateTime.UtcNow.AddHours(4),
                ConsultationDate = DateTime.UtcNow.AddHours(2),
                ConsultationStatus = 72,
                ConsultationId = 4,
                PatientId = 4,
                AssignedDoctorId = 3,
                ConsultantUserId = 8,
                HospitalId = 1,
                ConferenceKey = "C7FEA91D-4BB9-4E00-945D-40BE6011D885"
            });
        }
    }
}
