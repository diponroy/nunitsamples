﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    public class CoUserTokenRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected CoUserTokenRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new CoUserTokenRepository(MockedDb.Object);
        }

        [Test]
        public void Token()
        {
            const int expectedLength = 30;
            List<String> tokens = new List<string>();
            for (int i = 0; i < 1000; i++)
            {
                var token = CoUserTokenRepository.Token();
                string errorMsg = String.Format("Test failled at iteration No. {0}, legnth =  {1}, for token = {2}", i, expectedLength, token);

                Assert.AreEqual(expectedLength, token.Length, errorMsg);
                Assert.True(token.All(char.IsLetterOrDigit), errorMsg);
                Assert.IsFalse(token.Any(char.IsLower), errorMsg);
                Assert.IsFalse(tokens.Contains(token), errorMsg);
                tokens.Add(token);
            }
        }
    }
}
