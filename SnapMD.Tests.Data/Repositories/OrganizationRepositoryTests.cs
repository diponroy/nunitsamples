﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    internal class OrganizationRepositoryTests
    {
        [Test]
        public void TestGet()
        {
            var mock = new Mock<ISnapContext>();
            var mockDbSet = Builder<Organization>.CreateListOfSize(5);
            mock.Setup(m => m.Set<Organization>()).Returns(mockDbSet.Build().ToDbSet());
            var repo = new OrganizationsRepository(mock.Object);
            Organization actual = null;
            Assert.DoesNotThrow(() => actual = repo.Get(2));
            Assert.IsNotNull(actual);
            Assert.AreEqual("Name2", actual.Name);
        }

        [Test]
        public void TestUpsert()
        {
            var mock = new Mock<ISnapContext>();
            var mockDbSet = Builder<Organization>.CreateListOfSize(5).Build();
            var set = mockDbSet.ToDbSet();
            mock.Setup(m => m.Set<Organization>()).Returns(set);
            mock.SetupGet(m => m.Organizations).Returns(set);
            var repo = new OrganizationsRepository(mock.Object);
            Organization target = new Organization();

            mock.Setup(m => m.SetAdded(target)).Callback(() => target.Id = mockDbSet.Max(m => m.Id) + 1);
            mock.Setup(m => m.SaveChanges()).Callback(() => mockDbSet.Add(target));
            Assert.DoesNotThrow(() => repo.Upsert(target));
            Assert.AreEqual(6, target.Id);
            Assert.AreEqual(6, mockDbSet.Count);
        }
    }
}
