﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class CoUsersTempRepositoryRepositoryTest
    {
        protected Mock<ISnapContext> DbContext;
        protected CoUsersTempRepository Repo;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();


            DbContext.SetupGet(m => m.CoUsersTemps).Returns(new List<CoUsersTemp>()
            {
                new CoUsersTemp(){
                    CoUserId=1,
                    EmailIdTemp="shibu.bht@gmail.com",
                    ApplyDst=1,
                    TimeZoneId=1
                },
                new CoUsersTemp(){
                    CoUserId=2,
                    EmailIdTemp="shibu.bht@gmail.com",
                    ApplyDst=1,
                    TimeZoneId=1
                }
            }.ToDbSet());
            DbContext.SetupGet(m => m.HospitalStaffProfileTemps)
                .Returns(new List<HospitalStaffProfileTemp>()
                {
                    new HospitalStaffProfileTemp(){
                        CoUserId=1,
                        HospitalId=1,
                        Name ="Shibu",
                        LastName="Bhattarai",
                        HomePhone="1111111",
                        MobilePhone="11111111",
                        IsActive="A"
                    },
                    new HospitalStaffProfileTemp(){
                        CoUserId=2,
                        HospitalId=1,
                        Name ="Shibu1",
                        LastName="Bhattarai1",
                        HomePhone="222222222",
                        MobilePhone="22222222",
                          IsActive="A"

                    }
                }.ToDbSet());



        }

        private void InitializeRepo()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Repo = new CoUsersTempRepository(DbContext.Object);
        }
        [Test]
        public void Get_Fail()
        {
            InitializeRepo();
            var data = Repo.Get(3);
            Assert.Null(data);
        }
        [Test]
        public void Get_Pass()
        {
            InitializeRepo();
            var data = Repo.Get(1);
            Assert.NotNull(data);
        }
        [Test]
        public void Where_Filter()
        {
            InitializeRepo();
            var data = Repo.Where(c => c.CoUserId == 1);
            Assert.NotNull(data);
            Assert.AreEqual(data.Count(), 1);
        }
        [Test]
        public void Get_Pending_Profile()
        {
            InitializeRepo();
            var data = Repo.GetPendingProfile(1);
            Assert.NotNull(data);
            Assert.AreEqual(data.FirstName, "Shibu");
        }

        [Test]
        public void Save_Profile()
        {
            InitializeRepo();
            var saved = Repo.UpdatePendingProfile(new StaffUserAcount()
            {
                Id=1,
                FirstName="ShibuChange",
                LastName="LastName",
                CountryId=2,
                MobileNumber="555555555",
                PhoneNumber="8888888888",
                TimeZoneId=5,
                Email="Test"
            });
            Assert.NotNull(saved);

            var data = Repo.Get(1);
            Assert.NotNull(data);
            Assert.AreEqual( "ShibuChange LastName",data.Name);
            
        }
        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Repo = null;
        }
    }
}
