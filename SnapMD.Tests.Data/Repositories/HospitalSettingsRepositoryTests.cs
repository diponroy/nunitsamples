﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class HospitalSettingsRepositoryTests
    {
        [Test]
        public void TestUpsertExists()
        {
            var context = new Mock<ISnapContext>();
            var existing = Builder<HospitalSetting>.CreateListOfSize(50).Build();

            context.SetupGet(c => c.HospitalSettings).Returns(existing.ToDbSet());

            using (var c = context.Object)
            {
                using (var con = new HospitalSettingsRepository(c))
                {
                    var result = con.Upsert(50, "Key50", "Modified");
                }
            }

            Assert.Pass();
        }

        [Test]
        public void TestUpsertNew()
        {
            var context = new Mock<ISnapContext>();
            var existing = Builder<HospitalSetting>.CreateListOfSize(50).Build();

            context.SetupGet(c => c.HospitalSettings).Returns(existing.ToDbSet());

            using (var c = context.Object)
            {
                using (var con = new HospitalSettingsRepository(c))
                {
                    var result = con.Upsert(51, "Key51", "New");
                }
            }

            Assert.Pass();
        }
    }
}
