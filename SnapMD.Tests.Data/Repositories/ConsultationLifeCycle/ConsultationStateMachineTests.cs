﻿using System;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.ConsultationLifeCycle;
using SnapMD.Data.Repositories.Interfaces;

namespace SnapMD.Tests.Data.Repositories.ConsultationLifeCycle
{
    [TestFixture]
    public class ConsultationStateMachineTests
    {        
        private const int ConsultationId = 1;

        //ToDo: Implement
        /*
        [TestCase(ConsultationStatusCode.CancelConsultaion, false)]
        [TestCase(ConsultationStatusCode.InProgress, false)]
         */

        [TestCase(ConsultationStatus.CustomerInWaiting, true)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, true)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, false)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, true)]
        [TestCase(ConsultationStatus.StartedConsultation, false)]
        public void ConsultationStateMachine_DoctorReviewConsultation_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.DoctorReviewConsultation;

            Test(c => c.DoctorReviewConsultation(1), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, false)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, false)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, true)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, true)]
        [TestCase(ConsultationStatus.PaymentDone, false)]
        [TestCase(ConsultationStatus.StartedConsultation, true)]
        public void ConsultationStateMachine_ConsultationEnded_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.EndedConsultation;

            Test(c => c.ConsultationEnded(1, 0), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, false)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, true)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, true)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, false)]
        [TestCase(ConsultationStatus.StartedConsultation, false)]
        public void ConsultationStateMachine_DoctorInitializedConsultation_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.DoctorInitiatedConsultation;

            Test(c => c.DoctorInitializedConsultation(1), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, true)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, true)]
        [TestCase(ConsultationStatus.DoctorAssigned, true)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, false)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, true)]
        [TestCase(ConsultationStatus.StartedConsultation, false)]
        public void ConsultationStateMachine_DoctorLeaveConsultationRoom_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.CustomerInWaiting;

            Test(c => c.DoctorLeaveConsultationRoom(1), currerntStatus, ConsultationStatus.CustomerInWaiting, isAceptable);
            Test(c => c.PatientEnteredWaitingRoom(1), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, false)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, false)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, true)]
        [TestCase(ConsultationStatus.DroppedConsultation, true)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, false)]
        [TestCase(ConsultationStatus.StartedConsultation, true)]
        public void ConsultationStateMachine_DropConsultation_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.DroppedConsultation;

            Test(c => c.DropConsultation(1, 0), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, false)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, false)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, true)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, false)]
        [TestCase(ConsultationStatus.StartedConsultation, true)]
        public void ConsultationStateMachine_PatientJoinedConsultation_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.StartedConsultation;

            Test(c => c.PatientJoinedConsultation(1), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }

        [TestCase(ConsultationStatus.CustomerInWaiting, true)]
        [TestCase(ConsultationStatus.DoctorReviewConsultation, false)]
        [TestCase(ConsultationStatus.DoctorAssigned, false)]
        [TestCase(ConsultationStatus.DoctorInitiatedConsultation, false)]
        [TestCase(ConsultationStatus.DroppedConsultation, false)]
        [TestCase(ConsultationStatus.EndedConsultation, false)]
        [TestCase(ConsultationStatus.PaymentDone, true)]
        [TestCase(ConsultationStatus.StartedConsultation, false)]
        public void ConsultationStateMachine_PatientLeaveTheWaitingRoom_NewStatusIfOperationAceptable(ConsultationStatus currerntStatus, bool isAceptable)
        {
            var expectedStatus = ConsultationStatus.PaymentDone;

            Test(c => c.PatientLeaveTheWaitingRoom(1), currerntStatus, expectedStatus, isAceptable);
            Test(c => c.ChangeConsultationStatus(1, expectedStatus), currerntStatus, expectedStatus, isAceptable);
        }


        private void Test(Func<ConsultationStateMachine, ConsultationStatus> act, ConsultationStatus currerntStatus, ConsultationStatus expectedStatus, bool isAceptable)
        {
            var consultationRepository = new Mock<IConsultationRepository>();
            consultationRepository
                .Setup(x => x.GetConsultationStatus(It.IsAny<int>()))
                .Returns((int)currerntStatus);

            consultationRepository.
                Setup(x => x.Get(It.IsAny<int>()))
                .Returns((int id) => new Consultation { ConsultationId = id });

            var consultationStateMachine = new ConsultationStateMachine(
                new ConsultationMetaData()
                {
                    ConsultationId = ConsultationId
                }, 
                consultationRepository.Object,
                new Mock<IDoctorStatusRepository>().Object,
                new Mock<IHospitalRepository>().Object,
                new Mock<ISmsConsultationRepository>().Object);

            if (isAceptable)
            {
                Assert.That(act(consultationStateMachine), Is.EqualTo(expectedStatus));
            }
            else
            {
                Assert.Throws<UnsupportedOperationException>(() => act(consultationStateMachine));
            }
        }

    }
}
