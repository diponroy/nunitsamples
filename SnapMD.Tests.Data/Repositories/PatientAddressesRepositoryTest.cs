﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data.Repositories
{
    [TestFixture]
    public class PatientAddressesRepositoryTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected PatientAddressesRepository Repo { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Repo = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Repo = new PatientAddressesRepository(MockedDb.Object);
        }

        /*
         * Get list of patient's active address, by patientId
         *      using PatientAddress and Address tbl
         */

        [Test]
        [TestCase(true, 1)]
        [TestCase(false, 0)]
        public void ActiveAddressesByPatientId_Returns_Active_Address_List(bool addressStatus, int expectedListCount)
        {
            Address address = Builder<Address>.CreateNew()
                .With(x => x.IsActive = addressStatus)
                .Build();
            PatientAddress patientAddress = Builder<PatientAddress>.CreateNew()
                .With(x => x.AddressId = address.AddressId)
                .Build();

            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>() { address }.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>() { patientAddress }.ToDbSet());
            InitializeRepo();

            var result = Repo.ActiveAddressesByPatientId(patientAddress.PatientProfileId);
            Assert.IsInstanceOf<IList<Address>>(result);
            Assert.AreEqual(expectedListCount, result.Count);
        }

        [Test]
        public void ActiveAddressesByPatientId_Finds_By_PatientId()
        {
            Address address = Builder<Address>.CreateNew()
                .With(x => x.IsActive = true)
                .Build();
            PatientAddress patientAddress = Builder<PatientAddress>.CreateNew()
                .With(x => x.AddressId = address.AddressId)
                .Build();

            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>() { address }.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>() { patientAddress }.ToDbSet());
            InitializeRepo();

            var result = Repo.ActiveAddressesByPatientId(100); /*not patientAddress.PatientProfileId*/
            Assert.IsInstanceOf<IList<Address>>(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void ActiveAddressesByPatientId_Fields()
        {
            Address address = Builder<Address>.CreateNew()
                .With(x => x.IsActive = true)
                .Build();
            PatientAddress patientAddress = Builder<PatientAddress>.CreateNew()
                .With(x => x.AddressId = address.AddressId)
                .Build();

            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>() { address }.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>() { patientAddress }.ToDbSet());
            InitializeRepo();

            var result = Repo.ActiveAddressesByPatientId(patientAddress.PatientProfileId);
            Assert.IsInstanceOf<IList<Address>>(result);
            Assert.AreEqual(1, result.Count);

            Address resultAddress = result.First();
            Assert.AreEqual(address.AddressId, resultAddress.AddressId);
            Assert.AreEqual(address.AddressText, resultAddress.AddressText);
        }
    }
}
