﻿using NUnit.Framework;
using SnapMD.Data.Entities.HelperModel;

namespace SnapMD.Tests.Data.Models
{
    [TestFixture]
    public class TestHelperModels
    {
        [Test]
        [TestCase("4|11", "ft/in", Result = "4\" 11'")]
        [TestCase("5", "ft/in", Result = "5\" 0'")]
        public string TestPatientProfileHeight(string height, string unit)
        {
            var patientProfile = new PatientProfileDetails
            {
                Height = height,
                HeightUnit = unit
            };

            return patientProfile.Height;
        }

        [Test]
        [TestCase("1.06", "m/cm", Result = "1.06|0")]
        [TestCase("0.8", "m/cm", Result = "0.8|0")]
        public string TestPatientProfileHeightMetric(string height, string unit)
        {
            var patientProfile = new PatientProfileDetails
            {
                Height = height,
                HeightUnit = unit
            };

            return patientProfile.Height;
        }

        [Test]
        [TestCase("5|10", "ft/in", Result = 5)]
        [TestCase("0.8", "m/cm", Result = 0.8)]
        public double TestPatientProfileHeightMajor(string height, string unit)
        {
            var patientProfile = new PatientProfileDetails
            {
                Height = height,
                HeightUnit = unit
            };

            return patientProfile.HeightMajor;
        }

        [Test]
        [TestCase("5|10", "ft/in", Result = 10)]
        [TestCase("0.8", "m/cm", Result = 0)]
        public double TestPatientProfileHeightMinor(string height, string unit)
        {
            var patientProfile = new PatientProfileDetails
            {
                Height = height,
                HeightUnit = unit
            };

            return patientProfile.HeightMinor;
        }


    }
}
