﻿using NUnit.Framework;
using SnapMD.Data.Entities;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class OrganizationLocationTests
    {
        [Test]
        public void ConstructorTest()
        {
            var target = new Location();
            Assert.IsNull(target.Organization);
            Assert.Pass();
        }
    }
}
