﻿using NUnit.Framework;
using SnapMD.Data.Entities;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class OrganizationTests
    {
        [Test]
        public void ConstructorTest()
        {
            var target = new Organization();
            Assert.IsNull(target.OrganizationType);
            Assert.IsNotNull(target.Addresses);
            Assert.IsEmpty(target.Addresses);
            Assert.IsNotNull(target.Locations);
            Assert.IsEmpty(target.Locations);
        }
    }
}
