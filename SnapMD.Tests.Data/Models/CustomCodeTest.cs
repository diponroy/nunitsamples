﻿using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Models;

namespace SnapMD.Tests.Data.Models
{
    [TestFixture]
    public class CustomCodeTest
    {
        [Test]
        [TestCase("45?Description", 45, "Description")]
        [TestCase(" 45 ?Description", 45, "Description")]
        [TestCase("", 0, "")]
        [TestCase(null, 0, "")]
        public void Initialization(string stringValue, int expectedCode, string expectedDescription)
        {
            var code = new CustomCode(stringValue);
            Assert.AreEqual(expectedCode, code.Code);
            Assert.AreEqual(expectedDescription, code.Description);
        }

        [Test]
        [TestCase(45, "Description", "45?Description")]
        [TestCase(45, " Description ", "45? Description ")]
        [TestCase(0, "", "")]
        [TestCase(0, null, "")]
        public void ToString(int code, string description, string expectedValue)
        {
            var customCode = new CustomCode() { Code = code, Description = description };
            Assert.AreEqual(expectedValue, customCode.ToString());
        }
    }
}
