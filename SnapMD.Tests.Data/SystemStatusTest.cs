﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class SystemStatusTest
    {
        private readonly Mock<ISnapContext> _context = new Mock<ISnapContext>();

        [SetUp]
        public void SetUpTestData()
        {
            var service = Builder<ServiceStatus>.CreateListOfSize(3).Build();

            _context.Setup(c => c.ServiceStatuses).Returns(service.ToDbSet());
        }

        [Test]
        public void TestGetAllServiceLog()
        {
            var repo = new ServiceStatusRepository(_context.Object);
            Assert.AreEqual(3, repo.GetServiceLogList().Count(), "Should Return All");
        }

        [Test]
        public void TestGetAllServiceWithCondition()
        {
            var repo = new ServiceStatusRepository(_context.Object);
            Assert.AreEqual(1, repo.GetServiceLogList(c => c.ServiceId == 1).Count(), "Should Return Only 1");
        }
    }
}