﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories.External.Integrations;

namespace SnapMD.Tests.Data.Repositories.External
{
    [TestFixture]
    public class ExternalCredentialsRepositoryTests
    {
        [Test]
        public void TestEmptyPartner()
        {
            var patientProfiles = Builder<PatientProfile>.CreateListOfSize(5).Build();

            var user = new User()
            {
                UserId = 1,
                HospitalId = 1,
                UserTypeId = 2,
                Email = "john@doe.com"
            };
            
            var hospital = new Hospital()
            {
                HospitalId = 1,
                HospitalName = "Emerald"
            };

            var hospitalPartners = new Collection<HospitalPartner>();
            var partner = new Collection<Partner>();

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(u => u.Users).Returns(new List<User> { user }.ToDbSet());
            mock.SetupGet(h => h.Hospitals).Returns(new List<Hospital> { hospital }.ToDbSet());
            mock.SetupGet(p => p.PatientProfiles).Returns(patientProfiles.ToDbSet());
            mock.SetupGet(p => p.HospitalPartners).Returns(hospitalPartners.ToDbSet());
            mock.SetupGet(p => p.Partners).Returns(partner.ToDbSet());

            var repo = new ExternalCredentialsRepository(mock.Object);
            var result = repo.Authorize(new LoginRequest()
            {
                HospitalId = 1,
                UserTypeId = 2,
                Email = "john@doe.com",
                Password = "dummy"
            });

            Assert.IsNotNull(result, "No result was given");
            Assert.IsTrue(result.Success, "The result was not sucessful, users will not be able to login on an hospital without partnerid");
            Assert.IsNull(result.PartnerId, "The partner ID should be null here");
        }
    }
}
