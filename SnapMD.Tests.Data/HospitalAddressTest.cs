﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class HospitalAddressTest
    {
        [Test]
        public void TestHospitalRead()
        {
            var context = SetupContext();
            var target = context.Object;
            var actual = target.Hospitals.SingleOrDefault(h => h.HospitalId == 2);
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Addresses);
            Assert.That(actual.Addresses.Any());
        }

        private Mock<ISnapContext> SetupContext()
        {
            var hospitals = Builder<Hospital>.CreateListOfSize(4).Build();
            hospitals.ForEach(h =>
            {
                var address = Builder<Address>.CreateListOfSize(2).Build();
                address.ElementAt(0).IsActive = false;
                h.Addresses = address;
            });

            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.Hospitals).Returns(hospitals.ToDbSet());
            return context;
        }


        [Test]
        public void GetHospitalAddress()
        {
            var context = SetupContext();
            var data = new HospitalRepository(context.Object);
            string address = data.GetHospitalActiveAddress(1);
            Assert.IsNotNull(address);

            /* This below would need a proper token
             * 
             * var target = new SnapMD.Web.Api.Admin.Controllers.HospitalController();
             var actual = target.GetHospitalActiveAddress(1);
             Assert.IsNotNull(actual);
             Assert.AreEqual(1, actual.ToString());
             */
        }

        [Test]
        public void SaveHospitalAddress()
        {

            var context = SetupContext();
            
            var data = new HospitalRepository(context.Object);
            data.SaveHospitalAddress(1, "1000 wilshire blvd, los angeles, ca 90017", 15);
            Assert.IsTrue(data.GetHospitalActiveAddress(1).StartsWith("1000"));
            

            /* This below would need a proper token
             * 
             * var target = new SnapMD.Web.Api.Admin.Controllers.HospitalController();
             var actual = target.GetHospitalActiveAddress(1);
             Assert.IsNotNull(actual);
             Assert.AreEqual(1, actual.ToString());
             */

        }
    }


    public static class TestExtensions
    {
        public static IDbSet<T> ToDbSet<T>(this IList<T> list) where T : class
        {
            // Borrowed: http://msdn.microsoft.com/en-us/library/dn314429.aspx#queryTest
            var data = list.AsQueryable();
            var mockSet = new Mock<IDbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            return mockSet.Object;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }
    }

}