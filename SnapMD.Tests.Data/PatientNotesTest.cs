﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class PatientNotesTest
    {
        [Test]
        public void TestRead()
        {
            var list = Builder<PatientNote>.CreateListOfSize(5).Build().ToList();
            using (var context = SetupContext(list))
            {
                var target = new PatientNotesRepository(context);
                var actual = target.Get(5);
                Assert.IsNotNull(actual);
            }
        }

        [Test]
        public void TestGetAll()
        {
            var list = Builder<PatientNote>.CreateListOfSize(12).Build().ToList();
            list.ForEach(i => i.PatientId = 1);

            // Builder alternates values for Status.
            Assert.AreEqual(list.Count(i => i.Status == PatientNoteStatus.Default), 6);

            using (var context = SetupContext(list))
            {
                var target = new PatientNotesRepository(context);
                var actual = target.All(1);
                Assert.GreaterOrEqual(actual.Count(), 6);
            }
        }

        private ISnapContext SetupContext(IList<PatientNote> mockList)
        {
            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.PatientNotes).Returns(mockList.ToDbSet());
            return context.Object;
        }
    }
}