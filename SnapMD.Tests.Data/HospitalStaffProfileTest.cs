﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class HospitalStaffProfileTest
    {
        [Test]
        public void TestRead()
        {

            var list = Builder<HospitalStaffProfile>.CreateListOfSize(5).Build().ToList();
            using (var context = SetupContext(list))
            {
                var target = new HospitalStaffRepository(context);
                var actual = target.Get(5);
                Assert.IsNotNull(actual);
            }
        }

        [Test]
        public void TestReadAll()
        {
            var list = Builder<HospitalStaffProfile>.CreateListOfSize(5).Build().ToList();
            using (var context = SetupContext(list))
            {
                var target = new HospitalStaffRepository(context);
                var actual = target.GetAll().ToList();
                Assert.IsTrue(actual.Count == 5);
                HospitalStaffProfile first = actual.FirstOrDefault();
                Assert.IsTrue(first.LastName.Length > 0);
            }
        }

        [Test]
        public void TestAllNotInActive()
        {
            var list = Builder<HospitalStaffProfile>.CreateListOfSize(5).Build().ToList();
            int j = 0;
            foreach (var a in list)
            {
                if (j == 0)
                    a.IsActive = "I";
                else
                    a.IsActive = "A";
                j++;

                a.HospitalId=1;
            }
            using (var context = SetupContext(list))
            {
                var target = new HospitalStaffRepository(context);
                var actual = target.GetHospitalStaffNotInactive(1).ToList();
                Assert.IsTrue(actual.Count == 4);

            }
        }

        [Test]
        public void TestSpeciality()
        {
            var list = Builder<HospitalStaffProfile>.CreateListOfSize(5).Build().ToList();

            DoctorSpeciality ds = Builder<DoctorSpeciality>.CreateListOfSize(1).Build().ToList().First();
            //User us = Builder<User>.CreateListOfSize(1).Build().ToList().First();
            list.ForEach(i => i.MedicalSpeciality = ds);
            //  list.ForEach(i => i.User = us);
            int j = 0;
            foreach (var a in list)
            {
                a.UserId = j++;
            }

            //  Assert.IsNotNull(list[0].DoctorSpeciality_SpecialityId);
            // Assert.IsNull(list[0].DoctorSpeciality_SpecialityId1);

            using (var context = SetupContext(list))
            {
                var target = new HospitalStaffRepository(context);
                var actual = target.Get(1);  //fails on get userid
                Assert.IsNotNull(actual.MedicalSpeciality);
                Assert.IsNull(actual.SubSpeciality);
            }

            // Builder alternates values for Status.

        }

        private ISnapContext SetupContext(IList<HospitalStaffProfile> mockList)
        {
            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.HospitalStaffProfiles).Returns(mockList.ToDbSet());
            return context.Object;
        }

        /* private ISnapContext SetupContext(IList<DoctorSpeciality> mockList)
         {
             var context = new Mock<ISnapContext>();
             context.SetupGet(c => c.DoctorSpecialities).Returns(mockList.ToDbSet());
             return context.Object;
         }*/
    }
}