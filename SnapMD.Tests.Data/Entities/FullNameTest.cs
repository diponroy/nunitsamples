﻿using System.Collections.Generic;
using NUnit.Framework;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;

namespace SnapMD.Tests.Data.Entities
{
    [TestFixture]
    public class FullNameTest
    {
        public static IEnumerable<ITestCaseData> TestCases
        {
            get
            {
                yield return new TestCaseData(" Dipon ", "Roy").Returns("Dipon Roy");   //firstName trimmed
                yield return new TestCaseData("Dipon", " Roy ").Returns("Dipon Roy");   //lastName trimmed
                yield return new TestCaseData("", "Roy").Returns("Roy");                //empty firstName  
                yield return new TestCaseData("Dipon", "").Returns("Dipon");            //empty lastName
                yield return new TestCaseData(null, "Roy").Returns("Roy");              //null firstName  
                yield return new TestCaseData("Dipon", null).Returns("Dipon");          //null lastName
                yield return new TestCaseData("Dipon", "Roy").Returns("Dipon Roy");
                yield return new TestCaseData("", "").Returns("");
                yield return new TestCaseData(null, null).Returns("");
            }
        }

        [Test]
        [TestCaseSource("TestCases")]
        public string PatientProfile_FullName(string firstName, string lastName)
        {
            var profile = new PatientProfile
            {
                PatientName = firstName,
                LastName = lastName,
            };
            string fullName = profile.FullName;
            return fullName;
        }

        [Test]
        [TestCaseSource("TestCases")]
        public string HospitalStaffProfile_FullName(string firstName, string lastName)
        {
            var profile = new HospitalStaffProfile
            {
                Name = firstName,
                LastName = lastName,
            };
            string fullName = profile.FullName;
            return fullName;
        }

        [Test]
        [TestCaseSource("TestCases")]
        public string HospitalStaffProfileTemp_FullName(string firstName, string lastName)
        {
            var profile = new HospitalStaffProfileTemp
            {
                Name = firstName,
                LastName = lastName,
            };
            string fullName = profile.FullName;
            return fullName;
        }

        [Test]
        [TestCaseSource("TestCases")]
        public string ScheduledConsultationResult_PatientFullName(string firstName, string lastName)
        {
            var consultationResult = new ScheduledConsultationResult
            {
                PatientFirstName = firstName,
                PatientLastName = lastName,
            };
            string fullName = consultationResult.PatientName;
            return fullName;
        }

        [Test]
        [TestCaseSource("TestCases")]
        public string ScheduledConsultationResult_DoctorFullName(string firstName, string lastName)
        {
            var consultationResult = new ScheduledConsultationResult
            {
                AssignedDoctorFirstName = firstName,
                AssignedDoctorLastName = lastName,
            };
            string fullName = consultationResult.AssignedDoctorName;
            return fullName;
        }
    }
}
