﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities.StatusCodes;

namespace SnapMD.Tests.Data.Entities
{
    [TestFixture]
    public class EnumTest
    {
        [Test]
        [TestCase(CodeSetEnum.CoUser_Registration_Token, 18)]
        [TestCase(CodeSetEnum.NewUserOnboardToken, 33)]
        public void CodeSetEnumTest(CodeSetEnum codeSet, int extectedValue)
        {
            Assert.AreEqual(extectedValue, (int)codeSet);
        }

        [Test]
        [TestCase(UserTypeEnum.Unknown, 0)]
        [TestCase(UserTypeEnum.Customer, 1)]
        [TestCase(UserTypeEnum.HospitalStaff, 2)]
        [TestCase(UserTypeEnum.SnapMDStaff, 3)]

        public void UserTypeEnumTest(UserTypeEnum userType, int extectedValue)
        {
            Assert.AreEqual(extectedValue, (int)userType);
        }
    }
}
