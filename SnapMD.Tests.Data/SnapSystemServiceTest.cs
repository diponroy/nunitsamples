﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class SnapSystemServiceTest
    {
        private readonly Mock<ISnapContext> _context = new Mock<ISnapContext>();


        [SetUp]
        public void SetUpTestData()
        {
            var service = Builder<SystemService>.CreateListOfSize(8).Build();
            service.ForEach(c => { c.StatusId = 1; });

            service.Skip(1).Take(2).ForEach(c => { c.StatusId = 2; });
            service.Skip(5).Take(1).ForEach(c => { c.StatusId = 3; });

            _context.Setup(c => c.SystemService).Returns(service.ToDbSet());
        }

        [Test]
        public void TestGetAllService()
        {
            var repo = new SnapServiceRepository(_context.Object);
            Assert.AreEqual(8, repo.GetServiceList().Count(), "Should Return All");
        }

        [Test]
        public void TestGetAllServiceWithCondition()
        {
            var repo = new SnapServiceRepository(_context.Object);
            Assert.AreEqual(1, repo.GetServiceList(c => c.ServiceId == 1).Count(), "Should Return Only 1");
        }

        [Test]
        public void TestSystemServiceUpdate()
        {
            var repo = new SnapServiceRepository(_context.Object);
            repo.UpdateSystemServiceStatus(2, 3,300);
            var service = repo.Get(2);
            Assert.AreEqual(3, service.StatusId);
        }
    }
}