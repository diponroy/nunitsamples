﻿using System.Collections.Generic;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapTestProject;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class PatientRepositoryTests
    {
        [Test]
        public void TestLocation()
        {
            IList<PatientProfile> set = Builder<PatientProfile>.CreateListOfSize(10).Build();
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.PatientProfiles).Returns(set.ToDbSet());

            var repo = new PatientRepository(mock.Object);

            PatientProfile result = repo.Get(9);
            Assert.AreEqual("Location9", result.Location);
        }

        [Test]
        public void TestOrganization()
        {
            IList<PatientProfile> set = Builder<PatientProfile>.CreateListOfSize(2).Build();
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.PatientProfiles).Returns(set.ToDbSet());
            var repo = new PatientRepository(mock.Object);
            PatientProfile result = repo.Get(2);
            Assert.AreEqual("Organization2", result.Organization);
        }
    }
}