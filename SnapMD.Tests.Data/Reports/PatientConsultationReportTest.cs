﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Consultations;

//using SnapTestProject;

namespace SnapMD.Tests.Data.Reports
{
    [TestFixture]
    public class PatientConsultationReportTest
    {
        Mock<ISnapContext> _context = new Mock<ISnapContext>();

        public PatientConsultationReportTest()
        {
            setUpTestData();
        }

        private void setUpTestData()
        {

            IList<PatientConsultationReport> list = Builder<PatientConsultationReport>.CreateListOfSize(10).Build();
            list.ForEach(c =>
            {
                c.CodeId = 1;
                c.CreatedDate = DateTime.Now;
            });
            list.Skip(2).Take(3).ForEach(c =>
            {
                c.CodeId = 2;
                
            });

            IList<Code> codeList = new List<Code>();
            codeList.Add(new Code()
            {
                CodeId=1,
                HospitalId=1,
                IsActive="A",
                CodeSetId=3,
                Description="B-"
            });
            codeList.Add(new Code()
            {
                CodeId = 2,
                HospitalId = 1,
                IsActive = "A",
                CodeSetId = 4,
                Description = "B+"
            });
            codeList.Add(new Code()
            {
                CodeId = 4,
                HospitalId = 1,
                IsActive = "A",
                CodeSetId = 3,
                Description = "B-"
            });
            codeList.Add(new Code()
            {
                CodeId = 3,
                HospitalId = 1,
                IsActive = "A",
                CodeSetId = 4,
                Description = "B+"
            });

            _context.Setup(c => c.Codes).Returns(codeList.ToDbSet());
            _context.Setup(c => c.PatientConsultationReports).Returns(list.ToDbSet());
        
        }

        [Test]
        public void GetAllTest()
        {
            var repo = new PatientConsultationReportRepository(_context.Object);
            Assert.IsNotNull(repo.GetAll());
            Assert.AreEqual(10, repo.GetAll().Count(), "Must return 10");
        }

        [Test]
        public void TestUpdatePatientReport()
        {  
            var repo = new PatientConsultationReportRepository(_context.Object);
            var oldInfo = repo.Get(5);
            oldInfo.ConsultationDescription = "ChangedDesc";
            repo.UpdatePatientConsultationReport(oldInfo);
            var modified = repo.Get(5);
            Assert.AreEqual("ChangedDesc", modified.ConsultationDescription);
 
        }
       
    }
}
