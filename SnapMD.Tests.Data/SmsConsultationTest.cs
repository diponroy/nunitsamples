﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class SmsConsultationTest
    {
        private readonly Mock<ISnapContext> _context = new Mock<ISnapContext>();

        [SetUp]
        public void SetupTestData()
        {
            IList<SmsConsultation> list = new List<SmsConsultation>();

            list.Add(new SmsConsultation
            {
                SmsId = 1,
                ConsultationId = 1,
                EventId = 1,
                Created = DateTime.UtcNow.AddMinutes(-25),
                MobilePhone = "123456",
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });

            list.Add(new SmsConsultation
            {
                SmsId = 2,
                ConsultationId = 1,
                EventId = 1,
                Created = DateTime.UtcNow.AddHours(-2),
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });

            list.Add(new SmsConsultation
            {
                SmsId = 3,
                ConsultationId = 1,
                EventId = 1,
                Created = DateTime.UtcNow.AddHours(-1),
                MobilePhone = "123456",
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });
            list.Add(new SmsConsultation
            {
                SmsId = 4,
                ConsultationId = 2,
                EventId = 1,
                Created = DateTime.UtcNow.AddMinutes(-25),
                MobilePhone = "123456",
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });
            list.Add(new SmsConsultation
            {
                SmsId = 5,
                ConsultationId = 1,
                EventId = 1,
                Created = DateTime.UtcNow.AddMinutes(-16),
                MobilePhone = "123456",
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });
            list.Add(new SmsConsultation
            {
                SmsId = 6,
                ConsultationId = 2,
                EventId = 1,
                Created = DateTime.UtcNow,
                MobilePhone = "123456",
                SmsGatewayId = (int)SMSGateWay.UnAssigned,
                Status = (int)SMSStatus.New
            });


            //setup Consultation
            var consalationList = new List<Consultation>();
            consalationList.Add(new Consultation
            {
                ConsultationId = 1,
                HospitalId = 1,
                PatientId = 1,
                AssignedDoctorId = 0
            });
            consalationList.Add(new Consultation
            {
                ConsultationId = 2,
                PatientId = 1,
                HospitalId = 2,
                AssignedDoctorId = 0
            });
            consalationList.Add(new Consultation
            {
                ConsultationId = 3,
                AssignedDoctorId = 1,
                PatientId = 1,
                HospitalId = 1
            });

            //setup HospitalList

            var hospitalList = new List<HospitalSetting>();
            //Builder<HospitalSetting>.CreateListOfSize(4).Build();
            hospitalList.Add(new HospitalSetting
            {
                HospitalId = 2,
                Id = 1,
                Key = "mTextMessaging",
                Value = "False"
            });
            hospitalList.Add(new HospitalSetting
            {
                HospitalId = 1,
                Id = 2,
                Key = "mTextMessaging",
                Value = "True"
            });


            var scheduleSlotCalendars = new List<ScheduleSlotCalendar>();
            scheduleSlotCalendars.Add(new ScheduleSlotCalendar
            {
                ScheduleSlotId = 1,
                HospitalId = 1,
                SlotStartTime = DateTime.UtcNow.AddMinutes(-5),
                SlotEndTime = DateTime.UtcNow.AddMinutes(5),
                DocUserId = 1
            });
            scheduleSlotCalendars.Add(new ScheduleSlotCalendar
            {
                ScheduleSlotId = 1,
                HospitalId = 2,
                SlotStartTime = DateTime.UtcNow.AddMinutes(-5),
                SlotEndTime = DateTime.UtcNow.AddMinutes(5),
                DocUserId = 1
            });

            var docStatus = new List<DoctorStatus>
            {
                new DoctorStatus
                {
                    DoctorId = 1,
                    StatusCode = 1,
                    StatusId = 1
                },
                new DoctorStatus
                {
                    DoctorId = 2,
                    StatusCode = 1,
                    StatusId = 1
                }
            };

            var patientProfile = Builder<PatientProfile>.CreateListOfSize(5).Build();
            patientProfile.ForEach(c =>
            {
                c.HospitalId = 1;
                c.IsActive = "True";
                c.PatientId = 1;
                //c.MobilePhone="12345";
            });


            var hosStaff = Builder<HospitalStaffProfile>.CreateListOfSize(4).Build();
            hosStaff.ForEach(c =>
            {
                c.HospitalId = 1;
                c.IsActive = "True";
                c.UserId = 1;
            });
            var smsTemplateList = new List<SmsTemplate>();
            smsTemplateList.Add(new SmsTemplate
            {
                HospitalId = 1,
                Id = 1,
                IsActive = "True",
                SmsText = "This is Dummy Test for SMS",
                Type = "Normal"
            });
            smsTemplateList.Add(new SmsTemplate
            {
                HospitalId = 1,
                Id = 2,
                IsActive = "false",
                SmsText = "This is Dummy Test for SMS 2",
                Type = "Normal"
            });
            smsTemplateList.Add(new SmsTemplate
            {
                HospitalId = 2,
                Id = 3,
                IsActive = "False",
                SmsText = "This is Dummy Test for SMS 3",
                Type = "Normal"
            });

            var hospitals = Builder<Hospital>.CreateListOfSize(4).Build();

            _context.Setup(c => c.SmsTemplates).Returns(smsTemplateList.ToDbSet());
            _context.Setup(c => c.PatientProfiles).Returns(patientProfile.ToDbSet());
            _context.Setup(c => c.Hospitals).Returns(hospitals.ToDbSet());
            _context.Setup(c => c.HospitalStaffProfiles).Returns(hosStaff.ToDbSet());
            _context.Setup(c => c.DoctorStatus).Returns(docStatus.ToDbSet());
            _context.Setup(c => c.ScheduleSlotCalendars).Returns(scheduleSlotCalendars.ToDbSet());
            _context.Setup(c => c.HospitalSettings).Returns(hospitalList.ToDbSet());
            _context.Setup(c => c.Consultations).Returns(consalationList.ToDbSet());
            _context.Setup(c => c.SmsConsultations).Returns(list.ToDbSet());
        }

        [Test]
        public void IsNotNullGetSMSToSend()
        {
            var repo = new SmsConsultationRepository(_context.Object);
            Assert.IsNotNull(repo.GetSMSToSend());
        }

        [Test]
        public void GetSMSTSend()
        {
            var repo = new SmsConsultationRepository(_context.Object);
            Assert.AreEqual(2, repo.GetSMSToSend().Count(), "GetSMSToSend must return 1 Data");
        }

        [Test]
        public void TestSentSMS()
        {
            var repo = new SmsConsultationRepository(_context.Object);
            repo.SMSSent(1, SMSStatus.Sent, SMSGateWay.Twilio, "09876");
            var sentSMS = repo.Get(1);
            Assert.AreEqual("09876", sentSMS.MobilePhone);
            Assert.AreEqual((int)SMSStatus.Sent, sentSMS.Status, "Change Status to sent");
            Assert.AreEqual((int)SMSGateWay.Twilio, sentSMS.SmsGatewayId, "Gateway must be Twilio");
        }

        [Test]
        public void TestGetConsultationInfoForDoctorSMS()
        {
            var repo = new SmsConsultationRepository(_context.Object);

            Assert.AreEqual(1, repo.GetConsultationInfoForDoctorSMS(3).Count());
            Assert.AreEqual(4, repo.GetConsultationInfoForDoctorSMS(1).Count());
        }

        [Test]
        public void TestGetSMSTemplate()
        {
            var repo = new SmsConsultationRepository(_context.Object);
            var _data = repo.GetSMSTemplate("Normal", 1);
            Assert.AreEqual("This is Dummy Test for SMS", _data);
            Assert.AreEqual("This is Dummy Test for SMS", repo.GetSMSTemplate("Normal", 4));
        }

        [Test]
        public void TestSmsInsert()
        {
            var target = new SmsConsultation();
            var mockSet = new Mock<IDbSet<SmsConsultation>>();
            mockSet.Setup(c => c.Add(target)).Callback((() => { target.SmsId = 24; } )).Returns(target);
            _context.SetupGet(c => c.SmsConsultations).Returns(mockSet.Object);
            var repo = new SmsConsultationRepository(_context.Object);
            repo.Add(target);
            Assert.AreEqual(24, target.SmsId);
        }
    }
}
