﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Data.Repositories;
//using SnapTestProject;
using System;

namespace SnapMD.Tests.Data
{
    [TestFixture]
    public class ChatRepoTest
    {
        Mock<ISnapContext> context = new Mock<ISnapContext>();
        public ChatRepoTest()
        {
            setUpTestData();
        }
        public Guid PatientChatToken { get; set; }
        public Guid PhysicianChatToken { get; set; }

        private void setUpTestData()
        {
            this.PatientChatToken = Guid.NewGuid();
            this.PhysicianChatToken = Guid.NewGuid();
            IList<PatientConsultationReport> list = Builder<PatientConsultationReport>.CreateListOfSize(10).Build();
            AddUsers();
            AddChatUser();
            AddChatSession();
            AddHospitalUser();
            AddPatientProfile();
            AddDoctorStatus();
            AddConsultationInfo();
            AddRoleInfo();
           AddChatMessage();
        }

        private void AddChatMessage()
        {
            var chatMList = new List<ChatMessage>();
            chatMList.Add(new ChatMessage()
            {
                ConsultationId = 1,
                ChatMessageId = 1,
                IsDelivered = true,
                Message = "Hello",
                PatientId = 1,
                MessageToPatient = true,
                SendDate = DateTime.Today
            });
            context.Setup(c => c.ChatMessages).Returns(chatMList.ToDbSet());
        }

        private void AddRoleInfo()
        {
            IList<UserRole> userRoles = new List<UserRole>();
            userRoles.Add(new UserRole()
            {
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleId = 1,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1,
                UserId = 1,
                UserRoleId = 1
            });
            userRoles.Add(new UserRole()
            {
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleId = 2,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1,
                UserId = 1,
                UserRoleId = 2
            });




            IList<Role> roles = new List<Role>();
            roles.Add(new Role()
            {
                HospitalId = 1,
                RoleId = 1,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                Description = "Test Role",
                RoleCode = "0001",
                OnOff = "ON",

            });

            roles.Add(new Role()
            {
                HospitalId = 1,
                RoleId = 2,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                Description = "Test Role 2",
                RoleCode = "0001",
                OnOff = "ON",

            });

            IList<RoleFunction> rolesFunction = new List<RoleFunction>();
            rolesFunction.Add(new RoleFunction()
            {
                RoleFunctionId = 10,
                IsActive = "A",
                FunctionId = 1,
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleId = 1,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1


            });
            rolesFunction.Add(new RoleFunction()
            {
                RoleFunctionId = 11,
                IsActive = "A",
                FunctionId = 11,
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleId = 1,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1


            });

            rolesFunction.Add(new RoleFunction()
            {
                RoleFunctionId = 12,
                IsActive = "A",
                FunctionId = 10,
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleId = 1,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1


            });
            IList<SnapFunction> snapFunction = new List<SnapFunction>();
            snapFunction.Add(new SnapFunction()
            {
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                IsActive = "A",
                Description = "Test 1 Role",
                FunctionId = 11,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1
            });
            snapFunction.Add(new SnapFunction()
            {
                CreateDate = DateTime.Now,
                CreatedBy = 2,
                IsActive = "A",
                Description = "Test 2",
                FunctionId = 10,
                UpdateDate = DateTime.Now,
                UpdatedBy = 1
            });

            context.Setup(c => c.UserRoles).Returns(userRoles.ToDbSet());
            context.Setup(c => c.Roles).Returns(roles.ToDbSet());
            context.Setup(c => c.RoleFunctions).Returns(rolesFunction.ToDbSet());
            context.Setup(c => c.SnapFunctions).Returns(snapFunction.ToDbSet());
        }
        private void AddDoctorStatus()
        {
            IList<DoctorStatus> docStatus = new List<DoctorStatus>();
            docStatus.Add(new DoctorStatus()
            {
                DoctorId = 1,
                StatusCode = 73,


            });
            docStatus.Add(new DoctorStatus()
            {
                DoctorId = 2,
                StatusCode = 74

            });
            docStatus.Add(new DoctorStatus()
            {
                DoctorId = 3,
                StatusCode = 75

            });
            docStatus.Add(new DoctorStatus()
            {
                DoctorId = 4,
                StatusCode = 76

            });
            docStatus.Add(new DoctorStatus()
            {
                DoctorId = 5,
                StatusCode = 73

            });
            context.Setup(c => c.DoctorStatus).Returns(docStatus.ToDbSet());
        }

        private void AddConsultationInfo()
        {
            IList<Consultation> conInfo = new List<Consultation>();
            conInfo.Add(new Consultation()
            {
                ConsultationId = 1,
                PatientId = 1,
                ConsultantUserId = 3,
                AssignedDoctorId = 1
            });
            context.Setup(c => c.Consultations).Returns(conInfo.ToDbSet());
        }

        private void AddPatientProfile()
        {
            IList<PatientProfile> patProfile = new List<PatientProfile>();
            patProfile.Add(new PatientProfile()
            {
                HospitalId = 1,
                PatientId = 1,
                UserId = 3,
                PatientName = "Shibu",
                LastName = "Bhattarai",
                ProfileImagePath = "Image"
            });
            patProfile.Add(new PatientProfile()
            {
                HospitalId = 1,
                PatientId = 2,
                UserId = 4,
                PatientName = "Shyam",
                LastName = "Kumar",
                ProfileImagePath = "Image"
            });
            context.Setup(c => c.PatientProfiles).Returns(patProfile.ToDbSet());
        }

        private void AddHospitalUser()
        {
            IList<HospitalStaffProfile> staffProfile = new List<HospitalStaffProfile>();
            staffProfile.Add(new HospitalStaffProfile()
            {
                StaffId = 1,
                UserId = 1,
                Name = "Dan",
                LastName = "Parker",
                ProfileImagePath = "Image",
                IsActive="A"
            });
            staffProfile.Add(new HospitalStaffProfile()
            {

                StaffId = 2,
                UserId = 2,
                Name = "Hari",
                LastName = "Kumar",
                ProfileImagePath = "Image",
                IsActive = "A"
            });
            context.Setup(c => c.HospitalStaffProfiles).Returns(staffProfile.ToDbSet());
        }

        private void AddChatSession()
        {
            IList<ChatUserSession> chatSessions = new List<ChatUserSession>();
            chatSessions.Add(new ChatUserSession()
            {
                ChatUserSessionId = 1,
                UserId = 1,
                LastLoggedInDate = DateTime.UtcNow,
                ChatToken = this.PhysicianChatToken


            });
            chatSessions.Add(new ChatUserSession()
            {
                ChatUserSessionId = 2,
                UserId = 2,
                LastLoggedInDate = DateTime.UtcNow,
                ChatToken = Guid.NewGuid()

            });
            chatSessions.Add(new ChatUserSession()
            {
                ChatUserSessionId = 3,
                UserId = 3,
                LastLoggedInDate = DateTime.UtcNow,
                ChatToken = this.PatientChatToken


            });
            chatSessions.Add(new ChatUserSession()
            {
                ChatUserSessionId = 4,
                UserId = 4,
                LastLoggedInDate = DateTime.UtcNow,
                ChatToken = Guid.NewGuid()

            });
            context.Setup(c => c.ChatUserSessions).Returns(chatSessions.ToDbSet());
        }

        private void AddUsers()
        {
            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                HospitalId = 1,
                UserId = 1,
                UserName = "admin",
                ProfileImage = "AdminProfile"

            });
            userList.Add(new User()
            {
                HospitalId = 1,
                UserId = 2,
                UserName = "Shibu",
                ProfileImage = "AdminProfile"

            });
            userList.Add(new User()
            {
                HospitalId = 1,
                UserId = 3,
                UserName = "hari",
                ProfileImage = "AdminProfile"

            });
            userList.Add(new User()
            {
                HospitalId = 1,
                UserId = 5,
                UserName = "shyam",
                ProfileImage = "AdminProfile"

            });
            context.Setup(c => c.Users).Returns(userList.ToDbSet());
        }
        [Test]
        public void GetPatientChatTokenTest()
        {

            var repo = new SnapChatRepository(context.Object);
            var data = repo.GetPatientChatToken(1);
            Assert.AreEqual(data.ChatToken, this.PatientChatToken);

        }

        [Test]
        public void GetPhysicianChatTokenTest()
        {

            var repo = new SnapChatRepository(context.Object);
            var data = repo.GetPhysicianChatToken(1);
            Assert.AreEqual(data.ChatToken, this.PhysicianChatToken);

        }
        [Test]
        public void GetPhysicianStatusTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetPhysicianStatus(1);
            Assert.AreEqual(_data, "Online", "Physicain Status must be online");
            Assert.NotNull(_data, "Statuse must be not null");


        }
        [Test]
        public void GetPhysicianStatusTestOffline()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetPhysicianStatus(2);
            Assert.AreEqual(_data, "Offline", "Physicain Status must be Offline");
            Assert.NotNull(_data, "Statuse must be not null");


        }
        [Test]
        public void GetPhysicianStatusTestBusy()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetPhysicianStatus(3);
            Assert.AreEqual(_data, "Busy", "Physicain Status must be Busy");
          


        }

        [Test]
        public void GetPhysicianStatusTestAway()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetPhysicianStatus(4);
            Assert.AreEqual(_data, "Away", "Physicain Status must be Away");



        }
        [Test]
        public void GetPhysicianStatusTestOfflineForOther()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetPhysicianStatus(11);
            Assert.AreEqual(_data, "Offline", "Physicain Status must be Offline");



        }
        [Test]
        public void SaveMessagetTest()
        {
            var repo = new SnapChatRepository(context.Object);
          var _data = repo.SaveMessage(new ChatMessage()
          {
              ChatMessageId=1,
              ConsultationId=22,
              IsDelivered=false,
              Message="oK gO oNE",
              MessageToPatient=false,
              PatientId=12,
              SendDate=DateTime.Now
              
          });
          Assert.IsTrue(_data);



        }

        [Test]
        public void ChangeStatus()
        {
            var repo = new SnapChatRepository(context.Object);
            repo.ChangeStatus(5, "Online");
            var _data = repo.GetPhysicianStatus(5);
            Assert.AreEqual(_data, "Online", "Physicain Status must be Online");
            repo.ChangeStatus(5, "Offline");
           _data = repo.GetPhysicianStatus(5);
           Assert.AreEqual(_data, "Offline", "Physicain Status must be Offline");
           repo.ChangeStatus(5, "Busy");
           _data = repo.GetPhysicianStatus(5);
           Assert.AreEqual(_data, "Busy", "Physicain Status must be Busy");
           repo.ChangeStatus(5, "Away");
           _data = repo.GetPhysicianStatus(5);
           Assert.AreEqual(_data, "Away", "Physicain Status must be Away");
           repo.ChangeStatus(5, "Online");
           _data = repo.GetPhysicianStatus(5);
           Assert.AreEqual(_data, "Online", "Physicain Status must be Online");

           repo.ChangeStatus(6, "Online");
           _data = repo.GetPhysicianStatus(5);
           Assert.AreEqual(_data, "Online", "Physicain Status must be Online");

        }
        [Test]
        public void GetUserInfomationFromTokenTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.GetUserInfomationFromToken(this.PhysicianChatToken);
            Assert.AreEqual(_data.Status, "Offline");
            Assert.AreEqual(_data.UserId, 1);
            Assert.NotNull(_data, "Statuse must be not null");


        }

        [Test]
        public void UpdateMessageStatusTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.UpdateMessageStatus(1, false);
            Assert.IsTrue(_data);
            var msgINfo = repo.GetMessages(1).FirstOrDefault();
            Assert.IsFalse(msgINfo.IsDelivered);

            

        }
        [Test]
        public void UpdateMessageStatusFailTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.UpdateMessageStatus(3, false);
            Assert.IsFalse(_data);
           

        }

        [Test]
        public void RemoveUserConnectionTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var _data = repo.RemoveUserConnection(this.PhysicianChatToken);
            Assert.IsTrue(_data);

            var fail = repo.RemoveUserConnection(Guid.NewGuid());
            Assert.False(fail);
            //For Other test that depedent on that ChatToken
            AddChatSession();



        }


        [Test]
        public void UpdateUserConnectionTest()
        {
           var repo = new SnapChatRepository(context.Object);
           var oldConnection = repo.GetUserInfomationFromToken(this.PhysicianChatToken);
           var newConnection = Guid.NewGuid();
           var _data = repo.UpdateUserConnection(1, newConnection);
           Assert.IsTrue(_data);
           var connInfo = repo.GetUserInfomationFromToken(newConnection);
           Assert.AreEqual(oldConnection.Name, connInfo.Name);

           repo.UpdateUserConnection(1, this.PatientChatToken);


           var newUserConnection = repo.UpdateUserConnection(23, newConnection);

           Assert.True(newUserConnection);

        }
        [Test]
        public void GetAllPhycicianStatuseTest()
        {
            var repo = new SnapChatRepository(context.Object);

            var data = repo.GetAllClinicianStatuses(1);
            Assert.AreEqual(1, data.Count());

        }

    
       
        [Test]
        public void GetChatUserNotNullTest()
        {

            var repo = new SnapChatRepository(context.Object);
            var data = repo.GetChatUser(1);
            Assert.NotNull(data);

        }
        [Test]
        public void GetMessagesNotEmptyTest()
        {

         
            var repo = new SnapChatRepository(context.Object);

            var data = repo.GetMessages(33);
            Assert.True(data.Count() == 0);

        }


        [Test]
        public void GetPatientIdTest()
        {

            var repo = new SnapChatRepository(context.Object);
            var patientID = repo.GetPatientId(1);
            Assert.AreEqual(patientID, 3);

        }


        [Test]
        public void GetPhysicianIdTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var physicianId = repo.GetPhysicianId(1);
            Assert.AreEqual(physicianId, 1);
        }

      [Test]
        public void GetPhysicianIdNullTest()
        {
            var repo = new SnapChatRepository(context.Object);
            var physicianId = repo.GetPhysicianId(30);
            Assert.IsTrue(physicianId==0);
        }

        [Test]
        public void TestChatMessageNotEmpty()
        {
            // AddChatUser();
           
            var repo = new SnapChatRepository(context.Object);

            var data = repo.GetMessages(1);
            Assert.True(data.Count() > 0);
            Assert.True(data.FirstOrDefault().PatientId == 1);
            Assert.True(data.FirstOrDefault().Message == "Hello");
            Assert.True(data.FirstOrDefault().MessageToPatient != false);
            Assert.True(data.FirstOrDefault().SendDate == DateTime.Today);
            Assert.True(data.FirstOrDefault().IsDelivered);
            Assert.True(data.FirstOrDefault().ConsultationId == 1);
        }

        private void AddChatUser()
        {
            var user = new List<ChatUserSession>();
            user.Add(new ChatUserSession()
            {
                ChatToken = Guid.NewGuid(),
                ChatUserSessionId = 1,
                LastLoggedInDate = DateTime.Now,
                UserId = 1
            });
            user.Add(new ChatUserSession()
            {
                ChatToken = Guid.NewGuid(),
                ChatUserSessionId = 2,
                LastLoggedInDate = DateTime.Now,
                UserId = 2
            });
            context.Setup(c => c.ChatUserSessions).Returns(user.ToDbSet());
        }
    }
}
