﻿using System.IO;
using System.Web;
using NUnit.Framework;
using SnapMD.Web.Api.HealthPlan.Helper.Authentication;
using SnapMD.Web.Api.HealthPlan.Helper.Credentials;
using SnapMD.Web.Api.Properties;

namespace SnapMD.Web.Api.Tests.Areas.HealthPlan.Helper.Authentication
{
    [TestFixture]
    public class RxntHealthPlanAuthenticatorTests
    {
        [SetUp]
        public void Init()
        {
            HttpContext.Current = new HttpContext(
                new HttpRequest("", "http://tempuri.org", ""),
                new HttpResponse(new StringWriter())
                );
        }

        [Test]
        public void TestAuthenticate()
        {
            var credentials = new RxntHealthPlanValidatorCredentials()
            {
                UserName = Settings.Default.RxNTInsuranceValidationPartnerUserId,
                Password = Settings.Default.RxNTInsuranceValidationPartnerUserPass
            };

            var authenticator = new RxntHealthPlanAuthenticator();

            var auth = authenticator.Authenticate(credentials);

            Assert.IsTrue(auth.IsAuthenticated, "The authentication failed");
            Assert.IsInstanceOf<RxntHealthPlanValidatorCredentials>(auth, "The authentication is not rxnt");
        }

        [Test]
        public void TestAuthenticateTwoTimes()
        {
            var credentials = new RxntHealthPlanValidatorCredentials()
            {
                UserName = Settings.Default.RxNTInsuranceValidationPartnerUserId,
                Password = Settings.Default.RxNTInsuranceValidationPartnerUserPass
            };

            var authenticator = new RxntHealthPlanAuthenticator();

            var auth = authenticator.Authenticate(credentials);

            Assert.IsTrue(auth.IsAuthenticated, "The authentication failed");
            Assert.IsInstanceOf<RxntHealthPlanValidatorCredentials>(auth, "The authentication is not rxnt");
            
            var rxntAuth = auth as RxntHealthPlanValidatorCredentials;
            
            credentials = new RxntHealthPlanValidatorCredentials()
            {
                UserName = Settings.Default.RxNTInsuranceValidationPartnerUserId,
                Password = Settings.Default.RxNTInsuranceValidationPartnerUserPass
            };

            var auth2 = authenticator.Authenticate(credentials);

            var rxntAuth2 = auth2 as RxntHealthPlanValidatorCredentials;

            Assert.IsTrue(rxntAuth.Token == rxntAuth2.Token, "The tokens are different");
        }
    }
}
