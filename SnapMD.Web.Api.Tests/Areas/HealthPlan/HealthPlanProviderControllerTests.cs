﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.HealthPlan.Controllers;
using SnapMD.Web.Api.Areas.HealthPlan.Models;

namespace SnapMD.Web.Api.Tests.Areas.HealthPlan
{
    [TestFixture]
    public class HealthPlanProviderControllerTests
    {
        protected readonly int PatientId = 1;

        [SetUp]
        public void SetUp()
        {
            var claims = new List<Claim>
                {
                    new Claim("provider", PatientId.ToString()),
                    new Claim("nameidentifier",  PatientId.ToString()),
                    new Claim(ClaimTypes.Name, "Doctor"),
                    new Claim(ClaimTypes.Email, "automatedtest_doctor@snap.md"),
                };

            var claimsIdentity = new ClaimsIdentity(claims);
            Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
        }


        [Test]
        public void TestGetAll()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new HealthPlanProviderController(context.Object);

            ApiResponseV2<HealthPlanProviderViewModel> result = controller.GetAll();
            Assert.AreEqual(2, result.Data.Count());

            Assert.IsInstanceOf<ApiResponseV2<HealthPlanProviderViewModel>>(result, "The result is not the expected one");

            var providers = result.Data;

            Assert.IsTrue(providers.Count() == 2, "The result is not just two, dont know where this come from");
        }

        protected static void GetContextWithSetUpDate(out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            context = new Mock<ISnapContext>();
            connectionId = Guid.NewGuid();
            userConnectionId = Guid.NewGuid();

            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "automatedtest@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            userList.Add(new User()
            {
                UserId = 3,
                UserName = "automatedtest_doctor@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest_doctor@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 3,
                Name = "doctor",
                LastName = "testing",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest_doctor@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "6a2505aa-b7f6-4640-842c-60f18c466d5d",
                HospitalId = 99999
            });

            IList<PatientProfile> pasProfile = new List<PatientProfile>();
            pasProfile.Add(new PatientProfile()
            {
                PatientId = 1,
                UserId = 1,
                PatientName = "Test",
                LastName = "Test",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "e942d049-260f-4253-b8dc-b3616e3fb8b0",
                HospitalId = 99999,
                IsDependent = "N",
                FamilyGroupId = 1,
                User = userList.First()
            });

            IList<Hospital> hospitalList = new List<Hospital>();
            hospitalList.Add(new Hospital()
            {
                HospitalId = 99999,
                BrandName = "Automated Test Hospital",
                HospitalName = "Automated Test Hospital",
                Users = new List<User>(userList)
            });

            IList<StandardTimeZone> timeZones = new List<StandardTimeZone>();
            timeZones.Add(new StandardTimeZone()
            {
                TimeZoneId = 1,
                TimeZoneName = "GMT Standard Time",
                TimeZoneDescription = "(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London"
            });

            IList<SnapFunction> functions = new List<SnapFunction>();
            functions.Add(new SnapFunction
            {
                FunctionId = 28,
                Description = "Can access My Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 29,
                Description = "Can view Patient Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 30,
                Description = "Can copy/upload to Patient Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 31,
                Description = "Can Manage Hospital Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });

            IList<Role> roles = new List<Role>();
            roles.Add(new Role()
            {
                RoleId = 3,
                Description = "Doctor",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleType = "C",
                HospitalId = 99999,
                RoleCode = "HDOC"
            });

            IList<RoleFunction> roleFunctions = new List<RoleFunction>();
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 28,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 29,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 30,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 31,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });

            IList<UserRole> userRoles = new List<UserRole>();
            userRoles.Add(new UserRole
            {
                UserRoleId = 1,
                UserId = 3,
                RoleId = 3,
                IsActive = "A"
            });

            IList<Consultation> consultations = new List<Consultation>();
            consultations.Add(new Consultation()
            {
                ConsultationId = 5,
                HospitalId = 99999,
                PatientId = 1,
                ConsultantUserId = 3,
                CreateDate = DateTime.Now,
                ConsultationAmount = 100
            });

            IList<Data.Entities.HealthPlan> healthPlans = new List<Data.Entities.HealthPlan>();
            healthPlans.Add(new Data.Entities.HealthPlan()
            {
                HealthPlanId = 1,
                FamilyGroupId = 1,
                PatientId = 1,
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                HealthPlanNameId = 1,
                PayerId = "G4719802",
                InsuranceCompany = "ANTHEM BLUE CROSS CA MGD CARE",
                InsuranceCompanyPhone = "123456",
                IsMemberActiveInPlan = "A",
                IsActive = "A",
                PolicyNumber = "P209090",
                IsDefaultPlan = "N",
                SubsciberId = "1234",
                SubscriberFirstName = "Test",
                SubscriberLastName = "Test",
                SubscriberDob = new DateTime(1998, 10, 10)
            });

            IList<HealthPlanName> healthPlanNames = new List<HealthPlanName>();
            healthPlanNames.Add(new HealthPlanName
            {
                Id = 1,
                PayerName = "ANTHEM BLUE CROSS CA MGD CARE",
                PayerId = "G4719802"
            });
            healthPlanNames.Add(new HealthPlanName()
            {
                Id = 2,
                PayerName = "Anthem Blue Cross California",
                PayerId = "00039"
            });

            IList<HospitalSetting> hospitalSettings = new List<HospitalSetting>();
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "mInsVerification",
                Value = "True"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "mInsVerificationDummy",
                Value = "True"
            });


            IList<ChatMessage> chatMessageList = new List<ChatMessage>();
            context.Setup(m => m.ChatMessages).Returns(chatMessageList.ToDbSet());
            context.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(pasProfile.ToDbSet());
            context.SetupGet(m => m.Hospitals).Returns(hospitalList.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());
            context.SetupGet(m => m.StandardTimeZones).Returns(timeZones.ToDbSet());
            context.SetupGet(m => m.Roles).Returns(roles.ToDbSet());
            context.SetupGet(m => m.SnapFunctions).Returns(functions.ToDbSet());
            context.SetupGet(m => m.RoleFunctions).Returns(roleFunctions.ToDbSet());
            context.SetupGet(m => m.UserRoles).Returns(userRoles.ToDbSet());
            context.SetupGet(m => m.Consultations).Returns(consultations.ToDbSet());
            context.SetupGet(m => m.HealthPlans).Returns(healthPlans.ToDbSet());
            context.SetupGet(m => m.HealthPlanNames).Returns(healthPlanNames.ToDbSet());
            context.SetupGet(m => m.HospitalSettings).Returns(hospitalSettings.ToDbSet());
        }
    }
}
