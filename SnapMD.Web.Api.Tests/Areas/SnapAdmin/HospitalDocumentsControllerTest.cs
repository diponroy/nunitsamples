﻿using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.SnapAdmin.Controllers;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;
using System.Linq;
using System.Web.Http;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;

namespace SnapMD.Web.Api.Tests.Areas.SnapAdmin
{
    [TestFixture]
    public class HospitalDocumentsControllerTest
    {
        private const int HospitalId = 1;
        private const int TOCid = 1;
        private const int CTTid = 2;
        private const int InvalidDocTypeId = 999;
        private const int SnapAdminUserId = 96;
        private const int NonSnapAdminUserId = 30716;

        private ISnapContext _mockContext;


        [SetUp]
        public void Setup()
        {
            var docTypeRepo = new MockDbSet<DocumentTypes>();
            var hospitalDocRepo = new MockDbSet<HospitalDocuments>();

            var mockContext = new Mock<ISnapContext>();
            mockContext.SetupGet(c => c.DocumentTypes).Returns(docTypeRepo);
            mockContext.SetupGet(c => c.HospitalDocuments).Returns(hospitalDocRepo);
            _mockContext = mockContext.Object;

            // documet types
            docTypeRepo.Add(CreateDocType(TOCid, "TOC"));
            docTypeRepo.Add(CreateDocType(CTTid, "TCC"));

            // hospital document
            hospitalDocRepo.Add(CreateHospitalDoc(TOCid, "TOC Doc text for hospital - 1", HospitalId));
            hospitalDocRepo.Add(CreateHospitalDoc(TOCid, "TOC Doc text for hospital - 2", 2));
            hospitalDocRepo.Add(CreateHospitalDoc(CTTid, "CTT Doc text for hospital - 2", 2));

            _mockContext.SaveChanges();
        }

        private HospitalDocuments CreateHospitalDoc(short documentTypeId, string documentText, int hospitalId)
        {
            var hosDoc = new HospitalDocuments
            {
                DocumentType = documentTypeId,
                DocumentText = documentText,
                HospitalId = hospitalId
            };
            return hosDoc;
        }

        private DocumentTypes CreateDocType(short id, string docText)
        {
            var documentTypes = new DocumentTypes
            {
                DocumentType = id,
                DefaultDocumentText = docText
            };
            return documentTypes;
        }

        [Test]
        public void GET_All_Hoppital_DocumentTypes()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            // Act
            ApiResponseV2<DocumentTypes> actionResult = controller.Get();

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(2, actionResult.Data.Count(), "There are two doc types");
        }

        [Test]
        public void GET_Hospital_DocumentText_By_DocTypeId_And_HopitalId()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>()))
                .Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            var controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            // Act
            var actionResult = controller.Get(1, 1);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(1, actionResult.Data.Count(), "Only one doc text will be return at a time");
        }

        [Test]
        public void GET_Hoppital_DocumentText_When_NonExistDocument_ReturnDefaultDocText()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            // Act
            ApiResponseV2<string> actionResult = controller.Get(2, 1);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(1, actionResult.Data.Count(), "Only one doc text will be return at a time");
        }

        [Test]
        public void GET_Hoppital_DocumentText_When_NonExistDocumentType_ReturnNotFound()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            // Act
            TestDelegate td = () => controller.Get(InvalidDocTypeId, 1);

            // Assert
            Assert.Throws<HttpResponseException>(td, "Invalid Document Type.");
        }

        [Test]
        public void POST_InvalidDocumentType_ReturnBadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("HospitalId", HospitalId),
                new JProperty("DocumentType", InvalidDocTypeId),
                new JProperty("documentText", "Test")
            );

            //Act
            var result = controller.Post(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.BadRequest);
        }

        [Test]
        public void POST_Empty_DocText_ReturnBadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("hospitalId", HospitalId),
                new JProperty("documentType", TOCid),
                new JProperty("documentText", "")
            );

            //Act
            var result = controller.Post(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.BadRequest);
        }

        [Test]
        public void POST_ValidData_ReturnSuccess()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("hospitalId", HospitalId),
                new JProperty("documentType", TOCid),
                new JProperty("documentText", "Test TOC")
            );

            //Act
            var result = controller.Post(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.NoContent, "Document created successfully");
        }

        [Test]
        public void DELETE_NotExist_Doc_ReturnBadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("hospitalId", HospitalId),
                new JProperty("documentType", CTTid)
            );

            //Act
            var result = controller.Delete(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.BadRequest);
        }

        [Test]
        public void DELETE_NonSnapAdmin_User_ReturnUnauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, NonSnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("hospitalId", HospitalId),
                new JProperty("documentType", TOCid)
            );

            //Act
            var result = controller.Delete(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.Unauthorized);
        }

        [Test]
        public void DELETE_ReturnSuccess()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            HospitalDocumentsController controller = new HospitalDocumentsController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            JObject obj = new JObject(
                new JProperty("hospitalId", HospitalId),
                new JProperty("documentType", TOCid)
            );

            //Act
            var result = controller.Delete(obj) as System.Web.Http.Results.StatusCodeResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, System.Net.HttpStatusCode.NoContent, "Document deleted successfully");
        }
    }
}
