﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Consultations;

namespace SnapMD.Web.Api.Tests.Areas.SnapAdmin
{
    [TestFixture]
    public class ScheduleRepositoryTests
    {
        private ISnapContext _mockContext;
        private int _consultationCount;
        [SetUp]
        public void Setup()
        {
            var mockContext = new Mock<ISnapContext>();

            var patientProfiles = Builder<PatientProfile>.CreateListOfSize(1).All().With(u => u.IsDependent = "N").Build();
            mockContext.SetupGet(c => c.PatientProfiles).Returns(patientProfiles.ToDbSet());

            var users = Builder<User>.CreateListOfSize(1).All().Build();
            mockContext.SetupGet(c => c.Users).Returns(users.ToDbSet());

            var metadata = new List<EncounterMetadata>();
            mockContext.SetupGet(c => c.EncounterMetadata).Returns(metadata.ToDbSet());

            var consultations = new List<Consultation>();
            _consultationCount = consultations.Count;
            mockContext.Setup(c => c.Consultations.Add(It.IsAny<Consultation>())).Callback<Consultation>(c =>
            {
                c.ConsultationId = ++_consultationCount;
                mockContext.Setup(x => x.Consultations).Returns(new List<Consultation> { c }.ToDbSet());
            });
            
            var scheduledConsultationTemps = new List<ScheduledConsultationTemp>();
            mockContext.SetupGet(c => c.ScheduledConsultationTemps).Returns(scheduledConsultationTemps.ToDbSet());
            
            var scheduleSlots = Builder<ScheduleSlotCalendar>.CreateListOfSize(1).All()
                .With(u => u.SlotStartTime = new DateTime(2015, 6, 1, 10, 0, 0))        /* 10am, june 1, 2015 */
                .With(u => u.SlotEndTime = new DateTime(2015, 6, 1, 20, 0, 0))          /* 8pm, june 1, 2015 */
                .With(u => u.SlotStatus = "A")
                .Build();
            mockContext.SetupGet(c => c.ScheduleSlotCalendars).Returns(scheduleSlots.ToDbSet());
            mockContext.Setup(c => c.Hospitals).Returns(new List<Hospital>().ToDbSet());

            var codes = Builder<Code>.CreateListOfSize(1).All().Build();
            mockContext.SetupGet(c => c.Codes).Returns(codes.ToDbSet());

            var doctors = Builder<HospitalStaffProfile>.CreateListOfSize(1).All().Build();
            mockContext.SetupGet(c => c.HospitalStaffProfiles).Returns(doctors.ToDbSet());

            var doctorSpecialities = Builder<DoctorSpeciality>.CreateListOfSize(1).All().Build();
            mockContext.SetupGet(c => c.DoctorSpecialities).Returns(doctorSpecialities.ToDbSet());
            
            _mockContext = mockContext.Object;
        }

        [Test]
        public void CreatingConsultationShouldIncludeMetadata()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            
            ScheduleConsultationDetail scheduler = new ScheduleConsultationDetail
            {
                AssignedDoctorId = 1,
                ConsultationId = 1,
                DoctorName = "John Doe",
                Id = 1,
                IsNoCharge = false,
                Note = "",
                PatientId = 1,
                PrevConsultationID = 0,
                PrimaryConsern = "45?Description",
                ScheduledFrom = "Online",
                ScheduledTime = DateTime.UtcNow.AddDays(5),
                ProfileImagePath = "",
                SecondaryConsern = ""
            };

            int consultationId = repo.CreateConsultation(scheduler, 1);
            
            Assert.IsTrue(consultationId > 0);

            EncounterMetadataRepository encounterRepo = new EncounterMetadataRepository(_mockContext);
            IPatientMetadata metadata = encounterRepo.GetMetadata(consultationId);
            Assert.IsNotNull(metadata);
        }

        [Test]
        public void TestDeleteSlotBasic()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            var slot = repo.DeleteSlot(1);

            Assert.IsNotNull(slot);
            Assert.AreEqual(1, slot.ScheduleSlotId);
            Assert.IsTrue(slot.SlotStartTime.HasValue && slot.SlotEndTime.HasValue);
            Assert.AreEqual(new DateTime(2015, 6, 1, 10, 0, 0), slot.SlotStartTime.Value);
            Assert.AreEqual(new DateTime(2015, 6, 1, 20, 0, 0), slot.SlotEndTime.Value);
        }

        /// <summary>
        /// slot id does not exist
        /// </summary>
        [Test]
        public void TestDeleteSlotNonExistent()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            var slotArray = repo.DeleteSlot(99999, new DateTime(2015, 6, 1, 11, 0, 0), new DateTime(2015, 6, 1, 13, 0, 0));

            Assert.Null(slotArray);
        }

        /// <summary>
        /// when the deleted time is within the slot
        /// </summary>
        [Test]
        public void TestDeleteSlotInBetween()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            /* delete the 11am to 1pm span */
            var slotArray = repo.DeleteSlot(1, new DateTime(2015, 6, 1, 11, 0, 0), new DateTime(2015, 6, 1, 13, 0, 0));

            Assert.IsNotNull(slotArray, "returned array must not be null");
            Assert.AreEqual(2, slotArray.Length, "there must be 2 returned slots");
            Assert.IsTrue(slotArray[0] != null && slotArray[1] != null);
            Assert.AreEqual(1, 
                slotArray.Count(a => a.SlotStartTime.HasValue && a.SlotStartTime.Value.Hour == 10 && a.SlotEndTime.HasValue && a.SlotEndTime.Value.Hour == 11),
                "exactly one slot has to have start and end time from 10am to 11am");
            Assert.AreEqual(1,
                slotArray.Count(a => a.SlotStartTime.HasValue && a.SlotStartTime.Value.Hour == 13 && a.SlotEndTime.HasValue && a.SlotEndTime.Value.Hour == 20),
                "exactly one slot has to have start and end time from 1pm to 8pm");
        }

        /// <summary>
        /// when the deleted time is outside of the slot's time
        /// </summary>
        [Test]
        public void TestDeleteSlotTotalMiss()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            /* specified date is the next day */
            var slotArray = repo.DeleteSlot(1, new DateTime(2015, 6, 2, 11, 0, 0), new DateTime(2015, 6, 2, 13, 0, 0));

            Assert.IsNotNull(slotArray, "returned array must not be null");
            Assert.AreEqual(1, slotArray.Length, "there must be 1 returned slot");
            Assert.AreEqual(1, slotArray[0].ScheduleSlotId, "returned slot must be the original slot");
            Assert.AreEqual("A", slotArray[0].SlotStatus, "returned slot must be active");
        }

        /// <summary>
        /// when the deleted time consumes the entire slot's time
        /// </summary>
        [Test]
        public void TestDeleteSlotTotal()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            /* date is wider and covers the entire slot */
            var slotArray = repo.DeleteSlot(1, new DateTime(2015, 5, 20, 11, 0, 0), new DateTime(2015, 6, 15, 13, 0, 0));

            Assert.IsNotNull(slotArray);
            Assert.AreEqual(0, slotArray.Length);
        }

        /// <summary>
        /// when the deleted time is at the edge
        /// </summary>
        [Test]
        public void TestDeleteSlotAtTheEdge()
        {
            ScheduleRepository repo = new ScheduleRepository(_mockContext);
            /* date is at the edge of the slot */
            var slotArray = repo.DeleteSlot(1, new DateTime(2015, 6, 1, 16, 0, 0), new DateTime(2015, 6, 1, 23, 0, 0));

            Assert.IsNotNull(slotArray);
            Assert.AreEqual(1, slotArray.Length);
            Assert.IsTrue(slotArray[0].SlotStartTime.HasValue && slotArray[0].SlotStartTime.Value.Hour == 10 &&
                slotArray[0].SlotEndTime.HasValue && slotArray[0].SlotEndTime.Value.Hour == 16);
        }
    }
}
