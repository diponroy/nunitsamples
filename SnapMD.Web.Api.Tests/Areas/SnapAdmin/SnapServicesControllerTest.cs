﻿using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.SnapAdmin.Controllers;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;
using System.Linq;
using System.Web.Http;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using System;

namespace SnapMD.Web.Api.Tests.Areas.SnapAdmin
{
    [TestFixture]
    public class SnapServicesControllerTest
    {


        private ISnapContext _mockContext;
        private const int HospitalId = 1;
        private const int SnapAdminUserId = 96;

        [SetUp]
        public void Setup()
        {
            var serviceLogRepo = new MockDbSet<ServiceLog>();
 

            var mockContext = new Mock<ISnapContext>();
            mockContext.SetupGet(c => c.ServiceLogs).Returns(serviceLogRepo);

            _mockContext = mockContext.Object;

            // documet types
            serviceLogRepo.Add(CreateServiceLog(1, "Test Message 1"));
            serviceLogRepo.Add(CreateServiceLog(1, "Test Message 2"));
            serviceLogRepo.Add(CreateServiceLog(1, "Test Message 3"));
            serviceLogRepo.Add(CreateServiceLog(1, "Test Message 4"));
            serviceLogRepo.Add(CreateServiceLog(2, "Test Message 5"));

            _mockContext.SaveChanges();
        }

        private ServiceLog CreateServiceLog(int serviceId, string message)
        {
            var serviceLog = new ServiceLog
            {
                ServiceId = serviceId,
                 Message = message,
                 Inserted = DateTime.UtcNow
            };
            return serviceLog;
        }

        [Test]
        public void GET_SnapServiceLog1()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff });
            SnapServicesController controller = new SnapServicesController(authenticationManager.Object, _mockContext, SnapAdminUserId);

            // Act
            ApiResponseV2<ServiceLog> actionResult = controller.Get(1);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(4, actionResult.Data.Count(), "There are four logs");
        }


    }
}
