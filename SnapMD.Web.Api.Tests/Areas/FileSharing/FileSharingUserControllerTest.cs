﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    /// <summary>
    /// Stand-in extensions to allow the unit test to compile.
    /// </summary>
    public static class FileSharingTestIsBroken
    {
        public static IHttpActionResult CreateProfile(this FileSharingUserController target, JObject input)
        {
            throw new NotImplementedException();
        }
    }

    [TestFixture, Explicit]
    public class FileSharingUserControllerTest
    {
        protected readonly int UserIdToCreate = 19;
        protected readonly int PhysicianIdToCreate = 1716;
        protected readonly JObject UserIdToJObject = new JObject();
        protected readonly JObject PhysicianIdJObject = new JObject();
        [Test]
        public void TestCreateUser()
        {
            var controller = new FileSharingUserController();
            UserIdToJObject.Add("userId", UserIdToCreate);
            var resp = controller.CreateProfile(UserIdToJObject);

            Assert.IsFalse(resp.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");

            Assert.IsTrue(resp.GetType() == typeof(OkResult), "The result is not ok, something went wrong");
        }

        [Test]
        public void TestCreatePhysician()
        {
            var controller = new FileSharingUserController();
            PhysicianIdJObject.Add("userId", PhysicianIdToCreate);
            var resp = controller.CreateProfile(PhysicianIdJObject);

            Assert.IsFalse(resp.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");

            Assert.IsTrue(resp.GetType() == typeof(OkResult), "The result is not ok, something went wrong");
        }

        
        [Test]
        public void AddReadWritePermission()
        {
            var controller = new FileSharingUserController();
            controller.AddPermissions("fe1c601a-54bc-4cac-bfbb-04fa470b9970", "ben@phdlabs.com", "READ");
            controller.AddPermissions("fe1c601a-54bc-4cac-bfbb-04fa470b9970", "ben@phdlabs.com", "WRITE");
        }

        [Test]
        public void AddUserToGroupTest()
        {
            var controller = new FileSharingUserController();

            controller.AddUserToGroup("doctorkiki@snapmdtests.net", "Emerald Physicians");
        }
    }

    
}
