﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using NUnit.Framework;
using SnapMD.Web.Api.Areas.FileSharing.Models;
using SnapMD.Web.Api.FileSharing;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]
    public class FolderGridTests
    {
        protected readonly string TestFolder = "Automated Test";
        protected readonly string TestFolderToDelete = "Automated Delete";
        protected readonly string TestUser = "automatedtest@snap.md";
        protected readonly string TestPassword = "Password@123";
        protected readonly string TestDomain = "snapdev";
        protected readonly string TestBaseFolder = "e942d049-260f-4253-b8dc-b3616e3fb8b0";
        protected readonly string TestUploadFileName = "test-excel.xlsx";
        protected readonly string TestCopyFolder = "1e57e0ab-bb80-49d9-89c4-a2284db2cace";
        protected readonly string TestCopyFile = "test.txt";
        protected readonly string TestDownloadFile = "test-other-excel.xlsx";

        [Test]
        public void TestLogin()
        {
            var fgClient = new FGClient();

            var result = fgClient.authenticate(TestDomain, TestUser, TestPassword);

            Assert.IsTrue(result, "The master user failed on login");
        }

        [Test]
        public void TestGetFolder()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folderObj = fgClient.fetchFolderContent(TestBaseFolder);
                
                Assert.IsNotNull(folderObj, "Null value returned from the call");

                var folder = folderObj.ToObject<FolderGridFile>();

                Assert.IsNotNull(folder, "Couldn't convert the folder to FolderGridFile");

                Assert.IsTrue(folder.name == TestUser, string.Format("Received a folder just not the correct one got: {0}", folder.name));
            }
        }

        [Test]
        public void TestFolderNotFound()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                Assert.Throws<WebException>(() => fgClient.fetchFolderContent("You will not find me"));
            }
        }

        [Test, Explicit]
        public void TestCreateFolder()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var target = new FolderGridFile()
                {
                    name = TestFolder,
                    parent = new FolderGridFile()
                    {
                        duid = TestBaseFolder
                    }
                };

                var result = fgClient.mkdir(target);

                Assert.IsTrue(result, "There was an error creating the folder");

                var folder = fgClient.fetchFolderContent(TestBaseFolder);

                var newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolder);

                Assert.IsNotNull(newFolder, "The new folder was not found");
                Assert.IsTrue(!string.IsNullOrWhiteSpace(newFolder.duid), "The duid is empty");
            }
        }

        [Test, Explicit]
        public void TestDeleteFolder()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);

                var newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolderToDelete);

                if (newFolder != null)
                {
                    fgClient.delete(newFolder);

                    folder = fgClient.fetchFolderContent(TestBaseFolder);

                    newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolder);

                    Assert.IsNull(newFolder, "The folder is still there");
                }
                
            }
        }

        [Test]
        public void TestUpload()
        {
            var file = FileSharingResources.test_excel;
            var stream = new MemoryStream(file);

            string bufferMD5;
            var fgClient = new FGClient();
            using (var md5 = MD5.Create())
            {
                byte[] bufferMd5 = md5.ComputeHash(file);
                 bufferMD5 = bufferMd5.ToHex(true);
            }

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var target = new FolderGridFile()
                {
                    name = TestUploadFileName,
                    lastModified = DateTime.Now,
                    created = DateTime.Now,
                    bufferMD5Hash = bufferMD5,
                    parent = new FolderGridFile()
                    {
                        duid = TestBaseFolder,
                    }
                };

                var result = fgClient.putFileContent(target, stream);

                Assert.IsTrue(result, "The file was not uploaded");

                var folder = fgClient.fetchFolderContent(TestBaseFolder);

                var newFile = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestUploadFileName);

                //Assert.IsNotNull(newFile, "Could not retrieve the file back");
                Assert.Inconclusive();
            }
        }

        [Test]
        public void TestCopy()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);

                var file = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestCopyFile);

                if (file != null)
                {
                    var to = new FolderGridFile()
                    {
                        duid = TestCopyFolder
                    };

                    var result = fgClient.copy(file.id, to);

                    Assert.IsTrue(result > 0, "The copy file failed");

                    folder = fgClient.fetchFolderContent(TestCopyFolder);

                    file = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestCopyFile);

                    Assert.IsNotNull(file, "Couldnt find the recently copied file");
                }
                else
                {
                    Assert.Fail("Couldnt find test.txt on the folder, please check if its there or add it back");   
                }
            }
        }

        [Test, Explicit("Requires permission to write to file system")]
        public void TestDownload()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);

                var file = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestDownloadFile);

                if (file != null)
                {
                    var url = fgClient.download(file);
                    
                    Assert.IsNotNullOrEmpty(url, "The url to download is empty");

                    var wc = new WebClient();
                    using (var stream = new MemoryStream(wc.DownloadData(url)))
                    {
                        Assert.IsNotNull(stream, "The stream we received is null");

                        var bytes = stream.ToArray();

                        var originalFile = FileSharingResources.test_other_excel;
                        using (var md5 = MD5.Create())
                        {
                            byte[] bufferMd5 = md5.ComputeHash(originalFile);
                            var originalMd5 = bufferMd5.ToHex(true);

                            byte[] receivedBufferMd5 = md5.ComputeHash(bytes);
                            var receivedMd5 = receivedBufferMd5.ToHex(true);

                            Assert.IsTrue(originalMd5 == receivedMd5, "The files dont match");
                        }
                    }
                }
                else
                {
                    Assert.Fail("Couldnt find test.txt on the folder, please check if its there or add it back");
                }
            }
        }


        [TearDown]
        public void TearDown()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);
                var newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolderToDelete);
                if (newFolder == null)
                {
                    var target = new FolderGridFile()
                    {
                        name = TestFolderToDelete,
                        parent = new FolderGridFile()
                        {
                            duid = TestBaseFolder
                        }
                    };

                    fgClient.mkdir(target);
                }

                newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolder);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }

                newFolder = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestUploadFileName);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }

                var copyFolder = fgClient.fetchFolderContent(TestCopyFolder);
                newFolder = copyFolder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestCopyFile);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }
            }
        }
    }
}
