﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using SnapMD.Data.Entities;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    public class FileSharingTests
    {
        protected static void GetContextWithSetUpDate(out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            context = new Mock<ISnapContext>();
            connectionId = Guid.NewGuid();
            userConnectionId = Guid.NewGuid();

            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "automatedtest@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            userList.Add(new User()
            {
                UserId = 3,
                UserName = "automatedtest_doctor@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest_doctor@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            userList.Add(new User()
            {
                UserId = 4,
                UserName = "automatedtest_doctor@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest_doctor@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 3,
                Name = "doctor",
                LastName = "testing",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest_doctor@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "6a2505aa-b7f6-4640-842c-60f18c466d5d",
                HospitalId = 99999
            });
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 4,
                Name = "doctor",
                LastName = "testing no permission",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest_doctor@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "6a2505aa-b7f6-4640-842c-60f18c466d5d",
                HospitalId = 99999
            });


            IList<PatientProfile> pasProfile = new List<PatientProfile>();
            pasProfile.Add(new PatientProfile()
            {
                PatientId = 1,
                UserId = 1,
                PatientName = "Test",
                LastName = "Test",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "e942d049-260f-4253-b8dc-b3616e3fb8b0",
                HospitalId = 99999,
                IsDependent = "N",
                FamilyGroupId = 1,
                User = userList.First()
            });

            IList<HospitalSetting> hospitalSettings = new List<HospitalSetting>();
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.BaseFolder",
                Value = "0313022e-f8b6-44f3-9019-162c5e3c774b"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomers",
                Value = "97e64afe-0ab9-4617-8b43-5a1cfbc74742"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.StaffFolder",
                Value = "8750d915-8b49-4f26-abac-cc8cbb6f42c1"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.ConsultationsFolder",
                Value = "8c969fa3-4357-4a90-9b32-c5d7767a5b4a"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.SharedFolder",
                Value = "9d614b57-9c1d-4855-bfc9-bc508354a91f"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalMainGroup",
                Value = "99999-Clinicians"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomersGroup",
                Value = "99999-Customers"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.Customers.SharedFolder",
                Value = "860e2c7e-087d-4c36-8036-da6b98d4e2be"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.Clinicians.SharedFolder",
                Value = "334c39a8-ac10-4325-99f4-b6a3c9f32b57"
            });

            IList<Hospital> hospitalList = new List<Hospital>();
            hospitalList.Add(new Hospital()
            {
                HospitalId = 99999,
                BrandName = "Automated Test Hospital",
                HospitalName = "Automated Test Hospital",
                Users = new List<User>(userList)
            });

            IList<StandardTimeZone> timeZones = new List<StandardTimeZone>();
            timeZones.Add(new StandardTimeZone()
            {
                TimeZoneId = 1,
                TimeZoneName = "GMT Standard Time",
                TimeZoneDescription = "(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London"
            });

            IList<SnapFunction> functions = new List<SnapFunction>();
            functions.Add(new SnapFunction
            {
                FunctionId = 28,
                Description = "Can access My Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 29,
                Description = "Can view Patient Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 30,
                Description = "Can copy/upload to Patient Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            functions.Add(new SnapFunction
            {
                FunctionId = 31,
                Description = "Can Manage Hospital Files",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });

            IList<Role> roles = new List<Role>();
            roles.Add(new Role()
            {
                RoleId = 3,
                Description = "Doctor",
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1,
                RoleType = "C",
                HospitalId = 99999,
                RoleCode = "HDOC"
            });

            IList<RoleFunction> roleFunctions = new List<RoleFunction>();
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 28,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 29,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 30,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });
            roleFunctions.Add(new RoleFunction
            {
                RoleFunctionId = 1,
                FunctionId = 31,
                RoleId = 3,
                IsActive = "A",
                CreateDate = DateTime.Now,
                CreatedBy = 1
            });

            IList<UserRole> userRoles = new List<UserRole>();
            userRoles.Add(new UserRole
            {
                UserRoleId = 1,
                UserId = 3,
                RoleId = 3,
                IsActive = "A"
            });

            IList<Consultation> consultations = new List<Consultation>();
            consultations.Add(new Consultation()
            {
                HospitalId = 99999,
                PatientId = 1,
                ConsultantUserId = 3,
                ConsultationId = 5,
                CreateDate = DateTime.Now
            });

            IList<TagFile> tagsList = new List<TagFile>();
            IList<FileSharingTag> tags = new List<FileSharingTag>();

            IList<ChatMessage> chatMessageList = new List<ChatMessage>();
            context.Setup(m => m.ChatMessages).Returns(chatMessageList.ToDbSet());
            context.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(pasProfile.ToDbSet());
            context.SetupGet(m => m.Hospitals).Returns(hospitalList.ToDbSet());
            context.SetupGet(m => m.HospitalSettings).Returns(hospitalSettings.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());
            context.SetupGet(m => m.StandardTimeZones).Returns(timeZones.ToDbSet());
            context.SetupGet(m => m.Roles).Returns(roles.ToDbSet());
            context.SetupGet(m => m.SnapFunctions).Returns(functions.ToDbSet());
            context.SetupGet(m => m.RoleFunctions).Returns(roleFunctions.ToDbSet());
            context.SetupGet(m => m.UserRoles).Returns(userRoles.ToDbSet());
            context.SetupGet(m => m.Consultations).Returns(consultations.ToDbSet());
            context.SetupGet(m => m.TagFiles).Returns(tagsList.ToDbSet());
            context.SetupGet(m => m.FileSharingTags).Returns(tags.ToDbSet());
        }

        protected static void AddFunctionsToRole()
        {
            
        }
    }
}
