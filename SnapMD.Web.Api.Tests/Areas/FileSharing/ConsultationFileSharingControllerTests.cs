﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.FileSharing;
using SnapMD.Web.Api.Areas.FileSharing.Controllers;
using SnapMD.Web.Api.Areas.FileSharing.Models;
using SnapMD.Web.Api.FileSharing;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]
    public class ConsultationFileSharingControllerTests : FileSharingTests
    {
        protected readonly int PatientUserId = 1;
        protected readonly int DoctorId = 3;
        protected readonly string TestUser = "automatedtest_doctor@snap.md";
        protected readonly string TestPassword = "Password@123";
        protected readonly string TestDomain = "snapdev";
        protected readonly string ConsultationsFolder = "8c969fa3-4357-4a90-9b32-c5d7767a5b4a";

        [SetUp]
        public void SetUp()
        {
            var claims = new List<Claim>
                {
                    new Claim("provider", DoctorId.ToString()),
                    new Claim("nameidentifier",  DoctorId.ToString()),
                    new Claim(ClaimTypes.Name, "Doctor"),
                    new Claim(ClaimTypes.Email, "automatedtest_doctor@snap.md"),
                };

            var claimsIdentity = new ClaimsIdentity(claims);
            Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
        }

        public void SwitchPrincipal()
        {
            var claims = new List<Claim>
                {
                    new Claim("provider", PatientUserId.ToString()),
                    new Claim("nameidentifier",  PatientUserId.ToString()),
                    new Claim(ClaimTypes.Name, "Patient"),
                    new Claim(ClaimTypes.Email, "automatedtest@snap.md"),
                };

            var claimsIdentity = new ClaimsIdentity(claims);
            Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
        }

        [Test]
        public void TestCreateConsultationFolder()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var result = GetConsultationFolder(context);

            Assert.IsFalse(result.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");
            Assert.IsInstanceOf<OkNegotiatedContentResult<string>>(result, "The result is not successfull");

            var okNegotiatedResult = result as OkNegotiatedContentResult<string>;


            Assert.IsNotNull(okNegotiatedResult, "The result was not of type SnapFile");
            Assert.IsNotNull(okNegotiatedResult.Content, "The folder returned a null value, was not found");
            Assert.IsNotNull(context.Object.Consultations.SingleOrDefault(c => c.ConsultationId == 5));
            Assert.IsNotNullOrEmpty(context.Object.Consultations.SingleOrDefault(c => c.ConsultationId == 5).ConsultationFolderId, "There is no consultation folder id in the EF");

            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(ConsultationsFolder);
                const string testFolder = "Consultation5";
                var newFolder =
                    folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == testFolder);

                Assert.IsTrue(newFolder.duid == context.Object.Consultations.SingleOrDefault(c => c.ConsultationId == 5).ConsultationFolderId, "The consultation folder id does not match");
            }
        }

        [Test]
        public void TestGetConsultationFolder()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var folderResult = GetConsultationFolder(context);

            if (folderResult.GetType() == typeof (OkNegotiatedContentResult<string>))
            {
                SwitchPrincipal();

                var controller = new ConsultationFileSharingController(context.Object);
                var result = controller.Get(5);

                Assert.IsFalse(result.GetType() == typeof (NotFoundResult), "The response didnt finshed successfully");
                Assert.IsInstanceOf<OkNegotiatedContentResult<string>>(result, "The result is not successfull");

                var okNegotiatedResult = result as OkNegotiatedContentResult<string>;


                Assert.IsNotNull(okNegotiatedResult, "The result was not of type SnapFile");
                Assert.IsNotNull(okNegotiatedResult.Content, "The folder returned a null value, was not found");

                var fgClient = new FGClient();

                if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
                {
                    var folder = fgClient.fetchFolderContent(ConsultationsFolder);
                    const string testFolder = "Consultation5";
                    var newFolder =
                        folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == testFolder);

                    Assert.IsTrue(newFolder.duid == okNegotiatedResult.Content,
                        "The consultation folder id does not match");
                }
            }
            else
            {
                throw new InconclusiveException("The consultation folder couldn't be created, this test cannot proceed");
            }
        }

        private static IHttpActionResult GetConsultationFolder(Mock<ISnapContext> context)
        {
            var controller = new ConsultationFileSharingController(context.Object);
            var result = controller.Post(new AddConsultationFolderRequest()
            {
                ConsultationId = 5
            });
            return result;
        }

        [TearDown]
        public void TearDown()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(ConsultationsFolder);
                const string testFolder = "Consultation5";
                var newFolder =
                    folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == testFolder);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }
            }
        }
    }
}
