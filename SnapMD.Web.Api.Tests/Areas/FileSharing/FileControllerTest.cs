﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.FileSharing.Controllers;
using SnapMD.Web.Api.Areas.FileSharing.Models;
using System.Security.Claims;
using System.Threading;
using SnapMD.Web.Api.Areas.FileSharing;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.FileSharing;
using SnapMD.Web.Api.FileSharing.Models;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]

    public class FileControllerTest : FileSharingTests
    {
        protected readonly string TestFolder = "Automated Test";
        protected readonly string TestFolderToDelete = "Automated Delete";
        protected readonly string TestUser = "automatedtest@snap.md";
        protected readonly string TestPassword = "Password@123";
        protected readonly string TestDomain = "snapdev";
        protected readonly string TestBaseFolder = "e942d049-260f-4253-b8dc-b3616e3fb8b0";
        protected readonly string TestUploadFileName = "test-excel.xlsx";
        protected readonly string TestCopyFolder = "1e57e0ab-bb80-49d9-89c4-a2284db2cace";
        protected readonly string TestCopyFile = "test.txt";
        protected readonly string TestDownloadFile = "test-other-excel.xlsx";


        protected readonly int DoctorId = 3;
        protected readonly string DoctorKikiBaseDir = "ed732525-761b-438b-b25a-e09947127149";
        protected readonly string TestFolderToGet = "9115ea3f-c1e9-4c2d-b7cd-c229d1d5845d";
        protected readonly string TestFileID = "4944962";
        protected readonly string folderUserType = "2";


        [SetUp]
        public void SetUp()
        {
            var claims = new List<Claim>
                {
                    new Claim("provider", DoctorId.ToString()),
                    new Claim("nameidentifier",  DoctorId.ToString()),
                    new Claim(ClaimTypes.Name, "Doctor"),
                    new Claim(ClaimTypes.Email, "automatedtest_doctor@snap.md"),
                };

            var claimsIdentity = new ClaimsIdentity(claims);
            Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
        }

        [Test]
        public void TestUpload()
        {
            var file = FileSharingResources.test_excel;
            var stream = new MemoryStream(file);

            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new FolderController(context.Object);


            var folder = controller.GetAll("physician");

            var okNegotiatedResult = folder;

            Assert.IsNotNull(okNegotiatedResult, "Couldn't find the base folder");


            var fileController = new FileController();
            var snapFile = new SnapFile()
            {
                Name = TestUploadFileName
            };

            var resp = fileController.Provision(DoctorId, okNegotiatedResult.Data.First().SnapFile, snapFile, stream);

            Assert.IsFalse(resp.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");

            Assert.IsTrue(resp.GetType() == typeof(OkResult), "The result is not ok, something went wrong");


        }

        [Test]
        public void TestDelete()
        {
            var controller = new FolderController();
            var folder = controller.GetAll("physician");
            var okNegotiatedResult = folder as ApiResponseV2<SnapFolderResponse>;

            Assert.IsNotNull(okNegotiatedResult, "Couldn't find the base folder");

            if (okNegotiatedResult.Data.First().SnapFile.Files.Any(x => x.Name == "admin.png"))
            {
                var fileController = new FileController();
                var file = okNegotiatedResult.Data.First().SnapFile.Files.First(x => x.Name == "admin.png");
                var gFile = new FolderGridFile()
                {
                    id = Convert.ToInt32(file.Id),
                    name = file.Name,
                    type = file.Type,
                    size = file.Size,
                    isReadable = file.IsReadable,
                    isWritable = file.IsWritable,
                    isSharedFolder = file.IsSharedFolder

                };

                var fromFile = new SnapFile(gFile);

                var cObj = new DeleteFileSharingRequest()
                {

                    PatientId = DoctorId
                };

                cObj.Target.FolderUserId = DoctorId;
                var resp = fileController.Delete(folderUserType, fromFile.Id, cObj);

                var okResult = resp;

                Assert.IsFalse(resp.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");
                //  Assert.IsTrue(resp.Content == true, "The result is not ok, something went wrong");

            }
            else
            {
                   throw new FileNotFoundException("The admin.png was not found , please run TestUpload method first.");
            }
        }

        [Test]
        public void TestCopy()
        {
            var controller = new FolderController();
            var folder = controller.GetAll("physician");
            var okNegotiatedResult = folder as ApiResponseV2<SnapFolderResponse>;

            Assert.IsNotNull(okNegotiatedResult, "Couldn't find the base folder");

            if (okNegotiatedResult.Data.First().SnapFile.Files.Any(x => x.Name == "admin.png"))
            {
                var fileController = new FileController();
                var file = okNegotiatedResult.Data.First().SnapFile.Files.First(x => x.Name == "admin.png");
                var folderCopy = okNegotiatedResult.Data.First().SnapFile.Folders.FirstOrDefault(x => x.Name == "Test Inner Folder");

                if (folderCopy != null)
                {
                    var gFile = new FolderGridFile()
                    {
                        id = Convert.ToInt32(file.Id),
                        name = file.Name,
                        type = file.Type,
                        size = file.Size,
                        isReadable = file.IsReadable,
                        isWritable = file.IsWritable,
                        isSharedFolder = file.IsSharedFolder,
                    };

                    var fromFile = new SnapFile(gFile);

                    var gFolder = new FolderGridFile()
                    {
                        duid = folderCopy.Id,
                        name = "My Files - Test Copy Folder",
                        type = folderCopy.Type,
                        isReadable = folderCopy.IsReadable,
                        isWritable = folderCopy.IsWritable,
                        isSharedFolder = folderCopy.IsSharedFolder,
                    };

                    var ToFolder = new SnapFile(gFolder);

                    FileSharingRequest cObj = new FileSharingRequest();

                    var resp = fileController.Copy("physician", Convert.ToInt32(fromFile.Id), ToFolder.Id, cObj);

                    var okResult = resp;

                    Assert.IsFalse(resp.GetType() == typeof(NotFoundResult), "The response didnt finshed successfully");

                    // Assert.IsTrue(resp.Content == true || resp.Content == false, "Fail to Copy");
                }
                else
                {
                    throw new FileNotFoundException("The folder Test Inner Folder was not found");
                }

            }
            else
            {
                throw new FileNotFoundException("The test-file.txt was not found");
            }
        }


        [Test]
        public void TestShare()
        {
            var controller = new FileController();

            var flController = new FolderController();
            var folder = flController.GetAll("physician");
            var okNegotiatedResult = folder as ApiResponseV2<SnapFolderResponse>;

            Assert.IsNotNull(okNegotiatedResult, "Couldn't find the base folder");

            if (okNegotiatedResult.Data.First().SnapFile.Files.Any(x => x.Name == "admin.png"))
            {
                var file = okNegotiatedResult.Data.First().SnapFile.Files.First(x => x.Name == "admin.png");
                var gFile = new FolderGridFile()
                {
                    id = Convert.ToInt32(file.Id),
                    name = file.Name,
                    type = file.Type,
                    size = file.Size,
                    isReadable = file.IsReadable,
                    isWritable = file.IsWritable,
                    isSharedFolder = file.IsSharedFolder,
                };
                var fromFile = new SnapFile(gFile);

                var response = controller.Share(folderUserType, Convert.ToInt32(fromFile.Id), null);
                var okResult = response;

                Assert.IsNotNull(okResult, "The result came null");
                Assert.IsNotEmpty(okResult.Data.First().Url, "The result was successfull but the payload was empty");
            }
        }

        [Test]
        public void TestSearch()
        {
            var controller = new FileController();

            FileSharingRequest cObj = new FileSharingRequest()
            {
                Terms = "Home"
            };

            var response = controller.Search(cObj);
            var okResult = response as OkNegotiatedContentResult<List<SnapFile>>;

            Assert.IsNotNull(okResult, "The result came null");
            Assert.IsNotEmpty(okResult.Content, "The result was successfull but the payload was empty");
        }

        [Test]
        public void TestDownload()
        {
            var controller = new FileController();

            var flController = new FolderController();
            var folder = flController.GetAll("physician");
            var okNegotiatedResult = folder as ApiResponseV2<SnapFolderResponse>;

            Assert.IsNotNull(okNegotiatedResult, "Couldn't find the base folder");

            if (okNegotiatedResult.Data.First().SnapFile.Files.Any(x => x.Name == "admin.png"))
            {
                var file = okNegotiatedResult.Data.First().SnapFile.Files.First(x => x.Name == "admin.png");

                var response = controller.Download(DoctorId, file.Id, folderUserType);
                var okResult = response as OkNegotiatedContentResult<string>;

                Assert.IsNotNull(okResult, "The result came null");
                Assert.IsNotEmpty(okResult.Content, "The result was successfull but the payload was empty");
            }
        }

        [TearDown]
        public void TearDown()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);
                var newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolderToDelete);
                if (newFolder == null)
                {
                    var target = new FolderGridFile()
                    {
                        name = TestFolderToDelete,
                        parent = new FolderGridFile()
                        {
                            duid = TestBaseFolder
                        }
                    };

                    fgClient.mkdir(target);
                }

                newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolder);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }

                newFolder = folder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestUploadFileName);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }

                var copyFolder = fgClient.fetchFolderContent(TestCopyFolder);
                newFolder = copyFolder.ToObject<FolderGridFile>().files.SingleOrDefault(x => x.name == TestCopyFile);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }
            }
        }
    }
}
