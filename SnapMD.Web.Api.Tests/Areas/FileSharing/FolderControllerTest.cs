﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using FizzWare.NBuilder.Extensions;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using SnapMD.Web.Api.Areas.FileSharing.Controllers;
using SnapMD.Web.Api.Areas.FileSharing.Models;
using Newtonsoft.Json.Linq;
using Moq;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using System.Web.Http;
using System.Security.Claims;
using System.Threading;
using SnapMD.Web.Api.Areas.FileSharing;
using SnapMD.Web.Api.FileSharing;
using SnapMD.Web.Api.FileSharing.Models;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]
    public class FolderControllerTest : FileSharingTests
    {
        protected readonly string TestFolder = "Automated Test";
        protected readonly string TestFolderToDelete = "Automated Test Delete";
        protected readonly string TestUser = "automatedtest_doctor@snap.md";
        protected readonly string TestPassword = "Password@123";
        protected readonly string TestDomain = "snapdev";
        protected readonly string TestBaseFolder = "6a2505aa-b7f6-4640-842c-60f18c466d5d";
        protected readonly string TestUploadFileName = "test-excel.xlsx";
        protected readonly string TestCopyFolder = "1e57e0ab-bb80-49d9-89c4-a2284db2cace";
        protected readonly string TestCopyFile = "test.txt";
        protected readonly string TestDownloadFile = "test-other-excel.xlsx";
        protected readonly string TestGetPatientFolder = "e942d049-260f-4253-b8dc-b3616e3fb8b0";
        protected readonly int DoctorId = 3;
        protected readonly int PatientId = 1;
        protected readonly string TestFolderToGet = "9115ea3f-c1e9-4c2d-b7cd-c229d1d5845d";
        
        [SetUp]
        public void SetUp()
        {
            var claims = new List<Claim>
                {
                    new Claim("provider", DoctorId.ToString()),
                    new Claim("nameidentifier",  DoctorId.ToString()),
                    new Claim(ClaimTypes.Name, "Doctor"),
                    new Claim(ClaimTypes.Email, "automatedtest_doctor@snap.md"),
                };

            var claimsIdentity = new ClaimsIdentity(claims);
            Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
        }


        [Test]
        public void TestGet()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new FolderController(context.Object);
            object result = controller.GetAll("physician");

            Assert.IsInstanceOf<ApiResponseV2<SnapFolderResponse>>(result, "The result is not successfull");

            var okNegotiatedResult = result as ApiResponseV2<SnapFolderResponse>;
            Assert.IsNotNull(okNegotiatedResult, "The result was not of type SnapFile");
            Assert.IsNotNull(okNegotiatedResult.Data, "The folder returned a null value, was not found");
            Assert.IsTrue(okNegotiatedResult.Data.First().SnapFile.Name == "Home", "He is not on the home directory");
            Assert.IsTrue(okNegotiatedResult.Data.First().SnapFile.Id == TestBaseFolder, "The folders don't match, check the credentials");
            Assert.IsTrue(okNegotiatedResult.Data.First().SnapFile.Size == 442039, "The folder size is not correct");
            Assert.IsTrue(okNegotiatedResult.Data.First().MyFolderSizeBytes == 442039, "The folder size is not correct");
        }

        [Test]
        public void TestGetPatient()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new FolderController(context.Object);
            object result = controller.GetAll("physician", PatientId);
            
            Assert.IsInstanceOf<ApiResponseV2<SnapFolderResponse>>(result, "The result is not successfull");

            var okNegotiatedResult = result as ApiResponseV2<SnapFolderResponse>;
            Assert.IsNotNull(okNegotiatedResult, "The result was not of type SnapFile");
            Assert.IsNotNull(okNegotiatedResult.Data, "The folder returned a null value, was not found");
            Assert.IsTrue(okNegotiatedResult.Data.First().SnapFile.Id == TestGetPatientFolder, "The folders don't match, check the credentials");
        }

        [Test]
        public void TestPut()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new FolderController(context.Object);

            var rObj = new CreateFileSharingRequest()
            {
                Name = TestFolder,
                ConsultationId = 0,
                Parent = new SnapFile()
                {
                    Id = TestBaseFolder,
                    IsFolder = true
                }
            };

            var result = controller.Post("physician", rObj);

            Assert.IsNotNull(result, "The result was not of type SnapFile");
            Assert.IsNotNull(result.Data.First(), "The folder returned a null value, was not found");
            Assert.IsTrue(result.Data.First().Success, "The folder was not created successfully");
        }

        [Test, Explicit]
        public void TestDelete()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var controller = new FolderController(context.Object);

            var folderResult = controller.GetAll("physician");

            var folders = folderResult.Data.First().SnapFile;

            var folder = folders.Folders.Single(f => f.Name == TestFolderToDelete);

            var rObj = new DeleteFileSharingRequest();

            var folderController = new FolderController(context.Object);
            var result = folderController.Delete("physician", folder.Id, rObj);

            Assert.IsNotNull(result, "The result was not of type SnapFile");
            Assert.IsNotNull(result.Data.First(), "The folder returned a null value, was not found");
            Assert.IsTrue(result.Data.First().Success, "The folder was not created successfully");
        }

        [TearDown]
        public void TearDown()
        {
            var fgClient = new FGClient();

            if (fgClient.authenticate(TestDomain, TestUser, TestPassword))
            {
                var folder = fgClient.fetchFolderContent(TestBaseFolder);
                var newFolder =
                    folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == TestFolderToDelete);
                if (newFolder == null)
                {
                    var target = new FolderGridFile()
                    {
                        name = TestFolderToDelete,
                        parent = new FolderGridFile()
                        {
                            duid = TestBaseFolder
                        }
                    };

                    fgClient.mkdir(target);
                }

                var testDelete = TestFolder.ToStringToHex(true);
                newFolder = folder.ToObject<FolderGridFile>().folders.SingleOrDefault(x => x.name == testDelete);
                if (newFolder != null)
                {
                    fgClient.delete(newFolder);
                }
            }
        }
    }
}
