﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hosting;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.FileSharing.Controllers;
using SnapMD.Web.Api.Areas.FileSharing.Models;
namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]
    public class PatientFileSharingControllerTest
    {
        private static Mock<ITokenIdentity> MakeUserMock()
        {
            var mockUser = new Mock<ITokenIdentity>();
            mockUser.SetupGet(m => m.ProviderId).Returns(1);
            mockUser.SetupGet(m => m.UserId).Returns(1);

            string hashCode = String.Format("{0:X}", DateTime.Now.Ticks.GetHashCode());
            var emailHash = string.Format("automatedtest+{0}@snap.md", hashCode).ToLower();

            mockUser.SetupGet(m => m.User).Returns(emailHash);
            return mockUser;
        }

        [SetUp]
        public void SetUp()
        {
            var identity = MakeUserMock();
        }

        [Test]
        public void TestGet()
        {
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var token = MakeUserMock();

        }

        private static void GetContextWithSetUpDate(out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            context = new Mock<ISnapContext>();
            connectionId = Guid.NewGuid();
            userConnectionId = Guid.NewGuid();

            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "automatedtest@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest@snap.md",
                HospitalId = 99999
            });
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "testing",
                ProfileImage = "asdf"
            });

            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 3,
                Name = "SHibu",
                LastName = "Bhattarai",
                ProfileImagePath = "pathtoprofile"


            });
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 2,
                Name = "Hari",
                LastName = "Kumar",
                ProfileImagePath = "pathtoprofile"


            });

            IList<PatientProfile> pasProfile = new List<PatientProfile>();
            pasProfile.Add(new PatientProfile()
            {
                PatientId = 1,
                UserId = 1,
                PatientName = "Test",
                LastName = "Test",
                ProfileImagePath = "pathtoprofile",
                FileSharingSiteUserName = "automatedtest@snap.md",
                FileSharingSitePassword = "Password@123",
                BaseFolderId = "e942d049-260f-4253-b8dc-b3616e3fb8b0",
                HospitalId = 99999,
                IsDependent = "N",
                FamilyGroupId = 1,
                User = userList.First()
            });

            IList<HospitalSetting> hospitalSettings = new List<HospitalSetting>();
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.BaseFolder",
                Value = "0313022e-f8b6-44f3-9019-162c5e3c774b"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomers",
                Value = "97e64afe-0ab9-4617-8b43-5a1cfbc74742"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.StaffFolder",
                Value = "8750d915-8b49-4f26-abac-cc8cbb6f42c1"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.ConsultationsFolder",
                Value = "8c969fa3-4357-4a90-9b32-c5d7767a5b4a"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.SharedFolder",
                Value = "9d614b57-9c1d-4855-bfc9-bc508354a91f"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalMainGroup",
                Value = "99999-Clinicians"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomersGroup",
                Value = "99999-Customers"
            });

            IList<Hospital> hospitalList = new List<Hospital>();
            hospitalList.Add(new Hospital()
            {
                HospitalId = 99999,
                BrandName = "Automated Test Hospital",
                HospitalName = "Automated Test Hospital",
                Users = new List<User>(userList)
            });


            IList<ChatMessage> chatMessageList = new List<ChatMessage>();
            context.Setup(m => m.ChatMessages).Returns(chatMessageList.ToDbSet());
            context.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(pasProfile.ToDbSet());
            context.SetupGet(m => m.Hospitals).Returns(hospitalList.ToDbSet());
            context.SetupGet(m => m.HospitalSettings).Returns(hospitalSettings.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());
        }
    }
}
