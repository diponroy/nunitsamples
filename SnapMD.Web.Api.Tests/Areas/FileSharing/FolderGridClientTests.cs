﻿using System.IO;
using System.Web;
using NUnit.Framework;
using SnapMD.Web.Api.Areas.FileSharing.Helper;
using SnapMD.Web.Api.Areas.FileSharing.Models;

namespace SnapMD.Web.Api.Tests.Areas.FileSharing
{
    [TestFixture, Explicit]
    public class FolderGridClientTests
    {
        protected readonly string TestFolder = "Automated Test";
        protected readonly string TestFolderToDelete = "Automated Delete";
        protected readonly string TestUser = "automatedtest@snap.md";
        protected readonly string TestPassword = "Password@123";
        protected readonly string TestDomain = "snapdev";
        protected readonly string TestBaseFolder = "e942d049-260f-4253-b8dc-b3616e3fb8b0";
        protected readonly string TestUploadFileName = "test-excel.xlsx";
        protected readonly string TestCopyFolder = "1e57e0ab-bb80-49d9-89c4-a2284db2cace";
        protected readonly string TestCopyFile = "test.txt";
        protected readonly string TestDownloadFile = "test-other-excel.xlsx";

        [SetUp]
        public void Init()
        {
            HttpContext.Current = new HttpContext(
                new HttpRequest("", "http://tempuri.org", ""),
                new HttpResponse(new StringWriter())
                );
        }

        [Test]
        public void TestLogin()
        {
            var auth = new FileSharingAuthenticator();

            var result = auth.Authenticate(new FileSharingCredential()
            {
                Email = TestUser,
                Password = TestPassword
            });

            Assert.IsInstanceOf<IFileSharingCredential>(result, "The result is not a credential type of thing");
            Assert.IsNotNull(result.AuthCookie, "The token is empty, the authentication failed");
        }

        [Test, Explicit("Unable to get this working on developer machines.")]
        public void TestLoginTwice()
        {
            var auth = new FileSharingAuthenticator();

            var result = auth.Authenticate(new FileSharingCredential()
            {
                Email = TestUser,
                Password = TestPassword
            });

            Assert.IsInstanceOf<IFileSharingCredential>(result, "The result is not a credential type of thing");
            Assert.IsNotNull(result.AuthCookie, "The token is empty, the authentication failed");

            var authToken = result.AuthCookie.Value;

            result = auth.Authenticate(new FileSharingCredential()
            {
                Email = TestUser,
                Password = TestPassword
            });

            Assert.IsTrue(result.AuthCookie.Value == authToken, "The token was not stored in cache, they are different");
        }

        [Test]
        public void TestFetchFolder()
        {
            var auth = new FileSharingAuthenticator();

            var result = new FileSharingCredential()
            {
                Email = TestUser,
                Password = TestPassword
            };

            var client = new FolderGridClient(result, auth);

            var folder = client.FetchFolderContent(TestBaseFolder);

            Assert.IsNotNull(folder, "Couldn't convert the folder to FolderGridFile");

            Assert.IsTrue(folder.name == TestUser, string.Format("Received a folder just not the correct one got: {0}", folder.name));
        }
    }
}
