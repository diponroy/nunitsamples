﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.Patients.Controllers
{
    [TestFixture]
    public class PatientConsultationsControllerTest
    {
        protected class TargetController : PatientConsultationsController
        {
            public TargetController(ISnapContext mockContext)
                : base(mockContext)
            {
            }

            public static ITokenIdentity MockedTokenIdentity { get; set; }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    if (MockedTokenIdentity == null)
                    {
                        throw new NullReferenceException(
                            "controller trying to use user's TokenIdentity, need to set MockedTokenIdentity.");
                    }
                    return MockedTokenIdentity;
                }
            }
        }

        protected Mock<ISnapContext> MockedDb { get; set; }

        protected TargetController Controller { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;

            TargetController.MockedTokenIdentity = null;
            Controller = null;
        }

        protected void InitializeController(ITokenIdentity mockedTokenIdentity = null)
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }

            TargetController.MockedTokenIdentity = mockedTokenIdentity;
            Controller = new TargetController(MockedDb.Object);
        }

        private List<CustomCode> GetUniqueCustomCode(List<Code> sampleCodes, int listSize)
        {
            if (sampleCodes.Count == 0)
            {
                throw new IndexOutOfRangeException("sampleCodes is not sufficient to create requested unique list.");
            }

            var customCodes = new List<CustomCode>();
            for (int i = 0; i < sampleCodes.Count(); i++)
            {
                Code code = sampleCodes[i];
                if (customCodes.Count == listSize)
                {
                    break;
                }
                if (customCodes.Any(x => x.Code == code.CodeId))
                {
                    continue;
                }
                customCodes.Add(GetCustomCode(code));
            }

            if (customCodes.Count != listSize)
            {
                throw new IndexOutOfRangeException("sampleCodes is not sufficient to create requested unique list.");
            }

            return customCodes;
        }

        private CustomCode GetCustomCode(Code code)
        {
            return new CustomCode
            {
                Code = code.CodeId,
                Description = code.Description
            };
        }

        /*
         * UpdateIntakeV2
         *      updates concerns to consultation tbl by consultationId
         *      add/update metadata to EncounterMetadata tbl by consultationId
         *      add/update historyDetail to PatientMedicalHistory tbl by patientId
         */

        [Test]
        public void UpdateIntakeV2_UpdatesConsultation_UpdatesEncounterMetadata_UpdatesPatientMedicalHistory()
        {
            /*mock data*/
            int consultationId = 1;
            int hospitalId = 1;

            List<Code> codes = Builder<Code>.CreateListOfSize(5).All()
                .With(x => x.HospitalId = hospitalId)
                .Build().ToList();
            IntakeQuestionnaire data = Builder<IntakeQuestionnaire>.CreateNew()
                .With(x => x.Concerns = new List<EncounterConcern> /*impt: Nbuilder doesn't work with struct*/
                {
                    new EncounterConcern { IsPrimary = true, CustomCode = GetCustomCode(codes[0]) },
                    new EncounterConcern { IsPrimary = false, CustomCode = GetCustomCode(codes[0]) }
                })
                .And(x => x.MedicationAllergies = GetUniqueCustomCode(codes, 4))
                .And(x => x.MedicalConditions = GetUniqueCustomCode(codes, 4))
                .And(x => x.Medications = GetUniqueCustomCode(codes, 4))
                .And(x => x.Surgeries = new List<SurgeryRecord>
                {
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 },
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 },
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 }
                })
                .And(x => x.InfantData = Builder<NewbornRecord>.CreateNew()
                    .With(l => l.PatientAgeUnderOneYear = "Y") /*"Y" is important, to consider other props*/
                    .Build())
                .Build();

            ITokenIdentity tokenIdentity = Builder<TokenIdentity>.CreateNew()
                .With(x => x.ProviderId = hospitalId)
                .Build();

            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.ConsultationId = consultationId)
                .And(x => x.HospitalId = hospitalId)
                .And(x => x.ConsultationStatus = null)
                .And(x => x.AssignedDoctorId = null)
                .And(x => x.AssignedHospitalUserId = null)
                .And(x => x.UpdateDate = null)
                .And(x => x.UpdatedBy = null).Build();
            PatientProfile patientProfile =
                Builder<PatientProfile>.CreateNew().With(x => x.PatientId = consultation.PatientId).Build();
            var encounterMetadata = new EncounterMetadata /*Nbuilder was throwing error*/
            {
                ConsultationId = consultation.ConsultationId,
                Metadata = null,
                CreatedBy = null,
                CreatedDate = null
            };
            PatientMedicalHistory history = Builder<PatientMedicalHistory>.CreateNew()
                .With(x => x.PatientId = patientProfile.PatientId)
                .And(x => x.UpdatedBy = null)
                .And(x => x.UpdateDate = null)
                .Build();

            /*mock db for PUT*/
            MockedDb.Setup(x => x.Consultations).Returns(new List<Consultation> { consultation }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.UserDependentRelationAuthorizations)
                .Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            MockedDb.Setup(x => x.Codes).Returns(codes.ToDbSet());
            MockedDb.Setup(x => x.EncounterMetadata)
                .Returns(new List<EncounterMetadata> { encounterMetadata }.ToDbSet());
            MockedDb.Setup(x => x.PatientMedicalHistories)
                .Returns(new List<PatientMedicalHistory> { history }.ToDbSet());
            InitializeController(tokenIdentity);

            /*test*/
            DateTime utcDateTime = DateTime.UtcNow;
            Controller.UpdateIntakeV2(consultationId, data);

            /*updated consultation*/
            Assert.AreEqual(data.Concerns[0].CustomCode.ToString(), consultation.PrimaryConsern);
            Assert.AreEqual(data.Concerns[1].CustomCode.ToString(), consultation.SecondaryConsern);
            Assert.LessOrEqual(utcDateTime, consultation.UpdateDate);
            Assert.AreEqual(tokenIdentity.UserId, consultation.UpdatedBy);


            /*updated encounterMetadata*/
            Assert.IsNotNullOrEmpty(encounterMetadata.Metadata);
            Assert.LessOrEqual(utcDateTime, encounterMetadata.CreatedDate);
            Assert.AreEqual(tokenIdentity.UserId, encounterMetadata.CreatedBy);


            /*updated patientMedicalHistorie*/
            Assert.AreEqual(data.MedicationAllergies[0].Code, history.AllergicMedication1);
            Assert.AreEqual(data.MedicationAllergies[1].Code, history.AllergicMedication2);
            Assert.AreEqual(data.MedicationAllergies[2].Code, history.AllergicMedication3);
            Assert.AreEqual(data.MedicationAllergies[3].Code, history.AllergicMedication4);

            Assert.AreEqual(data.MedicalConditions[0].Code, history.MedicalCondition1);
            Assert.AreEqual(data.MedicalConditions[1].Code, history.MedicalCondition2);
            Assert.AreEqual(data.MedicalConditions[2].Code, history.MedicalCondition3);
            Assert.AreEqual(data.MedicalConditions[3].Code, history.MedicalCondition4);

            Assert.AreEqual(data.Medications[0].Code, history.TakingMedication1);
            Assert.AreEqual(data.Medications[1].Code, history.TakingMedication2);
            Assert.AreEqual(data.Medications[2].Code, history.TakingMedication3);
            Assert.AreEqual(data.Medications[3].Description, history.TakingMedication4);

            Assert.AreEqual(data.Surgeries[0].Description, history.PriorSurgery1);
            Assert.AreEqual(data.Surgeries[0].Month, history.Surgery1Month);
            Assert.AreEqual(data.Surgeries[0].Year, history.Surgery1Year);
            Assert.AreEqual(data.Surgeries[1].Description, history.PriorSurgery2);
            Assert.AreEqual(data.Surgeries[1].Month, history.Surgery2Month);
            Assert.AreEqual(data.Surgeries[1].Year, history.Surgery2Year);
            Assert.AreEqual(data.Surgeries[2].Description, history.PriorSurgery3);
            Assert.AreEqual(data.Surgeries[2].Month, history.Surgery3Month);
            Assert.AreEqual(data.Surgeries[2].Year, history.Surgery3Year);

            Assert.AreEqual(data.InfantData.PatientAgeUnderOneYear, history.IsOneYearBelowChild);
            Assert.AreEqual(data.InfantData.FullTerm, history.IsChildBornFullTerm);
            Assert.AreEqual(data.InfantData.VaginalBirth, history.IsChildBornVaginally);
            Assert.AreEqual(data.InfantData.DischargedWithMother, history.IsChildDischargeMother);
            Assert.AreEqual(data.InfantData.VaccinationsCurrent, history.IsVaccinationUpToDate);

            Assert.LessOrEqual(utcDateTime, history.UpdateDate);
            Assert.AreEqual(tokenIdentity.UserId, history.UpdatedBy);


            /*additional mocks for GET*/
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.ConsultationSoapNotesCptCodes)
                .Returns(new List<ConsultationSoapNotesCptCode>().ToDbSet());
            MockedDb.Setup(x => x.PatientConsultationReports).Returns(new List<PatientConsultationReport>().ToDbSet());
            InitializeController(tokenIdentity);

            /*updated encounterMetadata xml, deserialized*/
            ApiResponseV2<PatientConsultationResult> result = Controller.GetAllPatientinformationV2(consultationId);
            Assert.IsInstanceOf<ApiResponseV2<PatientConsultationResult>>(result);
            IntakeQuestionnaire intakeData = result.Data.ToList().First().IntakeForm;

            Assert.AreEqual(data.Concerns[0].IsPrimary, intakeData.Concerns[0].IsPrimary);
            Assert.AreEqual(data.Concerns[0].CustomCode.Code, intakeData.Concerns[0].CustomCode.Code);
            Assert.AreEqual(data.Concerns[0].CustomCode.Description, intakeData.Concerns[0].CustomCode.Description);
            Assert.AreEqual(data.Concerns[1].IsPrimary, intakeData.Concerns[1].IsPrimary);
            Assert.AreEqual(data.Concerns[1].CustomCode.Code, intakeData.Concerns[1].CustomCode.Code);
            Assert.AreEqual(data.Concerns[1].CustomCode.Description, intakeData.Concerns[1].CustomCode.Description);

            Assert.AreEqual(data.MedicationAllergies[0].Code, intakeData.MedicationAllergies[0].Code);
            Assert.AreEqual(data.MedicationAllergies[0].Description, intakeData.MedicationAllergies[0].Description);
            Assert.AreEqual(data.MedicationAllergies[1].Code, intakeData.MedicationAllergies[1].Code);
            Assert.AreEqual(data.MedicationAllergies[1].Description, intakeData.MedicationAllergies[1].Description);
            Assert.AreEqual(data.MedicationAllergies[2].Code, intakeData.MedicationAllergies[2].Code);
            Assert.AreEqual(data.MedicationAllergies[2].Description, intakeData.MedicationAllergies[2].Description);
            Assert.AreEqual(data.MedicationAllergies[3].Code, intakeData.MedicationAllergies[3].Code);
            Assert.AreEqual(data.MedicationAllergies[3].Description, intakeData.MedicationAllergies[3].Description);

            Assert.AreEqual(data.MedicalConditions[0].Code, intakeData.MedicalConditions[0].Code);
            Assert.AreEqual(data.MedicalConditions[0].Description, intakeData.MedicalConditions[0].Description);
            Assert.AreEqual(data.MedicalConditions[1].Code, intakeData.MedicalConditions[1].Code);
            Assert.AreEqual(data.MedicalConditions[1].Description, intakeData.MedicalConditions[1].Description);
            Assert.AreEqual(data.MedicalConditions[2].Code, intakeData.MedicalConditions[2].Code);
            Assert.AreEqual(data.MedicalConditions[2].Description, intakeData.MedicalConditions[2].Description);
            Assert.AreEqual(data.MedicalConditions[3].Code, intakeData.MedicalConditions[3].Code);
            Assert.AreEqual(data.MedicalConditions[3].Description, intakeData.MedicalConditions[3].Description);

            Assert.AreEqual(data.Medications[0].Code, intakeData.Medications[0].Code);
            Assert.AreEqual(data.Medications[0].Description, intakeData.Medications[0].Description);
            Assert.AreEqual(data.Medications[1].Code, intakeData.Medications[1].Code);
            Assert.AreEqual(data.Medications[1].Description, intakeData.Medications[1].Description);
            Assert.AreEqual(data.Medications[2].Code, intakeData.Medications[2].Code);
            Assert.AreEqual(data.Medications[2].Description, intakeData.Medications[2].Description);
            Assert.AreEqual(data.Medications[3].Description, intakeData.Medications[3].Description);

            Assert.AreEqual(data.Surgeries[0].Description, intakeData.Surgeries[0].Description);
            Assert.AreEqual(data.Surgeries[0].Month, intakeData.Surgeries[0].Month);
            Assert.AreEqual(data.Surgeries[0].Year, intakeData.Surgeries[0].Year);
            Assert.AreEqual(data.Surgeries[1].Description, intakeData.Surgeries[1].Description);
            Assert.AreEqual(data.Surgeries[1].Month, intakeData.Surgeries[1].Month);
            Assert.AreEqual(data.Surgeries[1].Year, intakeData.Surgeries[1].Year);
            Assert.AreEqual(data.Surgeries[2].Description, intakeData.Surgeries[2].Description);
            Assert.AreEqual(data.Surgeries[2].Month, intakeData.Surgeries[2].Month);
            Assert.AreEqual(data.Surgeries[2].Year, intakeData.Surgeries[2].Year);

            Assert.AreEqual(data.InfantData.PatientAgeUnderOneYear, intakeData.InfantData.PatientAgeUnderOneYear);
            Assert.AreEqual(data.InfantData.FullTerm, intakeData.InfantData.FullTerm);
            Assert.AreEqual(data.InfantData.VaginalBirth, intakeData.InfantData.VaginalBirth);
            Assert.AreEqual(data.InfantData.DischargedWithMother, intakeData.InfantData.DischargedWithMother);
            Assert.AreEqual(data.InfantData.VaccinationsCurrent, data.InfantData.VaccinationsCurrent);
        }

        [Test]
        public void UpdateIntakeV2_UpdatesConsultation_AddsToEncounterMetadata_AddsToPatientMedicalHistory()
        {
            /*mock data*/
            int consultationId = 1;
            int hospitalId = 1;

            List<Code> codes = Builder<Code>.CreateListOfSize(5).All()
                .With(x => x.HospitalId = hospitalId)
                .Build().ToList();
            IntakeQuestionnaire data = Builder<IntakeQuestionnaire>.CreateNew()
                .With(x => x.Concerns = new List<EncounterConcern> /*impt: Nbuilder doesn't work with struct*/
                {
                    new EncounterConcern { IsPrimary = true, CustomCode = GetCustomCode(codes[0]) },
                    new EncounterConcern { IsPrimary = false, CustomCode = GetCustomCode(codes[0]) }
                })
                .And(x => x.MedicationAllergies = GetUniqueCustomCode(codes, 4))
                .And(x => x.MedicalConditions = GetUniqueCustomCode(codes, 4))
                .And(x => x.Medications = GetUniqueCustomCode(codes, 4))
                .And(x => x.Surgeries = new List<SurgeryRecord>
                {
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 },
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 },
                    new SurgeryRecord { Description = "des", Month = 2, Year = 2000 }
                })
                .And(x => x.InfantData = Builder<NewbornRecord>.CreateNew()
                    .With(l => l.PatientAgeUnderOneYear = "Y") /*"Y" is important, to consider other props*/
                    .Build())
                .Build();

            ITokenIdentity tokenIdentity = Builder<TokenIdentity>.CreateNew()
                .With(x => x.ProviderId = hospitalId)
                .Build();
            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.ConsultationId = consultationId)
                .And(x => x.HospitalId = hospitalId)
                .And(x => x.ConsultationStatus = null)
                .And(x => x.AssignedDoctorId = null)
                .And(x => x.AssignedHospitalUserId = null)
                .And(x => x.UpdateDate = null)
                .And(x => x.UpdateDate = null)
                .Build();
            PatientProfile patientProfile =
                Builder<PatientProfile>.CreateNew().With(x => x.PatientId = consultation.PatientId).Build();
            var metadatas = new List<EncounterMetadata>();
            var histories = new List<PatientMedicalHistory>();

            /*mock to PUT*/
            MockedDb.Setup(x => x.Consultations).Returns(new List<Consultation> { consultation }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.UserDependentRelationAuthorizations)
                .Returns(new List<UserDependentRelationAuthorization>().ToDbSet());
            MockedDb.Setup(x => x.Codes).Returns(codes.ToDbSet());
            MockedDb.Setup(x => x.EncounterMetadata).Returns(metadatas.ToDbSet());
            MockedDb.Setup(x => x.PatientMedicalHistories).Returns(histories.ToDbSet());
            InitializeController(tokenIdentity);
            DateTime utcDateTime = DateTime.UtcNow;
            Controller.UpdateIntakeV2(consultationId, data);


            /*updated consultation tbl*/
            Assert.AreEqual(data.Concerns[0].CustomCode.ToString(), consultation.PrimaryConsern);
            Assert.AreEqual(data.Concerns[1].CustomCode.ToString(), consultation.SecondaryConsern);
            Assert.LessOrEqual(utcDateTime, consultation.UpdateDate);
            Assert.AreEqual(tokenIdentity.UserId, consultation.UpdatedBy);


            /*added entity at encounterMetadata tbl*/
            EncounterMetadata encounterMetadata = metadatas.First();
            Assert.IsNotNullOrEmpty(encounterMetadata.Metadata);
            Assert.LessOrEqual(utcDateTime, encounterMetadata.CreatedDate);
            Assert.AreEqual(tokenIdentity.UserId, encounterMetadata.CreatedBy);


            /*added entity to patientMedicalHistorie tbl*/
            PatientMedicalHistory history = histories.First();
            Assert.AreEqual(data.MedicationAllergies[0].Code, history.AllergicMedication1);
            Assert.AreEqual(data.MedicationAllergies[1].Code, history.AllergicMedication2);
            Assert.AreEqual(data.MedicationAllergies[2].Code, history.AllergicMedication3);
            Assert.AreEqual(data.MedicationAllergies[3].Code, history.AllergicMedication4);

            Assert.AreEqual(data.MedicalConditions[0].Code, history.MedicalCondition1);
            Assert.AreEqual(data.MedicalConditions[1].Code, history.MedicalCondition2);
            Assert.AreEqual(data.MedicalConditions[2].Code, history.MedicalCondition3);
            Assert.AreEqual(data.MedicalConditions[3].Code, history.MedicalCondition4);

            Assert.AreEqual(data.Medications[0].Code, history.TakingMedication1);
            Assert.AreEqual(data.Medications[1].Code, history.TakingMedication2);
            Assert.AreEqual(data.Medications[2].Code, history.TakingMedication3);
            Assert.AreEqual(data.Medications[3].Description, history.TakingMedication4);

            Assert.AreEqual(data.Surgeries[0].Description, history.PriorSurgery1);
            Assert.AreEqual(data.Surgeries[0].Month, history.Surgery1Month);
            Assert.AreEqual(data.Surgeries[0].Year, history.Surgery1Year);
            Assert.AreEqual(data.Surgeries[1].Description, history.PriorSurgery2);
            Assert.AreEqual(data.Surgeries[1].Month, history.Surgery2Month);
            Assert.AreEqual(data.Surgeries[1].Year, history.Surgery2Year);
            Assert.AreEqual(data.Surgeries[2].Description, history.PriorSurgery3);
            Assert.AreEqual(data.Surgeries[2].Month, history.Surgery3Month);
            Assert.AreEqual(data.Surgeries[2].Year, history.Surgery3Year);

            Assert.AreEqual(data.InfantData.PatientAgeUnderOneYear, history.IsOneYearBelowChild);
            Assert.AreEqual(data.InfantData.FullTerm, history.IsChildBornFullTerm);
            Assert.AreEqual(data.InfantData.VaginalBirth, history.IsChildBornVaginally);
            Assert.AreEqual(data.InfantData.DischargedWithMother, history.IsChildDischargeMother);
            Assert.AreEqual(data.InfantData.VaccinationsCurrent, history.IsVaccinationUpToDate);

            Assert.LessOrEqual(utcDateTime, history.CreateDate);
            Assert.AreEqual(tokenIdentity.UserId, history.CreatedBy);
            Assert.AreEqual(consultation.PatientId, history.PatientId);


            /*additional mocks for GET*/
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.ConsultationSoapNotesCptCodes)
                .Returns(new List<ConsultationSoapNotesCptCode>().ToDbSet());
            MockedDb.Setup(x => x.PatientConsultationReports).Returns(new List<PatientConsultationReport>().ToDbSet());
            InitializeController(tokenIdentity);

            /*added encounterMetadata xml, deserialized*/
            ApiResponseV2<PatientConsultationResult> result = Controller.GetAllPatientinformationV2(consultationId);
            Assert.IsInstanceOf<ApiResponseV2<PatientConsultationResult>>(result);
            IntakeQuestionnaire intakeData = result.Data.ToList().First().IntakeForm;

            Assert.AreEqual(data.Concerns[0].IsPrimary, intakeData.Concerns[0].IsPrimary);
            Assert.AreEqual(data.Concerns[0].CustomCode.Code, intakeData.Concerns[0].CustomCode.Code);
            Assert.AreEqual(data.Concerns[0].CustomCode.Description, intakeData.Concerns[0].CustomCode.Description);
            Assert.AreEqual(data.Concerns[1].IsPrimary, intakeData.Concerns[1].IsPrimary);
            Assert.AreEqual(data.Concerns[1].CustomCode.Code, intakeData.Concerns[1].CustomCode.Code);
            Assert.AreEqual(data.Concerns[1].CustomCode.Description, intakeData.Concerns[1].CustomCode.Description);

            Assert.AreEqual(data.MedicationAllergies[0].Code, intakeData.MedicationAllergies[0].Code);
            Assert.AreEqual(data.MedicationAllergies[0].Description, intakeData.MedicationAllergies[0].Description);
            Assert.AreEqual(data.MedicationAllergies[1].Code, intakeData.MedicationAllergies[1].Code);
            Assert.AreEqual(data.MedicationAllergies[1].Description, intakeData.MedicationAllergies[1].Description);
            Assert.AreEqual(data.MedicationAllergies[2].Code, intakeData.MedicationAllergies[2].Code);
            Assert.AreEqual(data.MedicationAllergies[2].Description, intakeData.MedicationAllergies[2].Description);
            Assert.AreEqual(data.MedicationAllergies[3].Code, intakeData.MedicationAllergies[3].Code);
            Assert.AreEqual(data.MedicationAllergies[3].Description, intakeData.MedicationAllergies[3].Description);

            Assert.AreEqual(data.MedicalConditions[0].Code, intakeData.MedicalConditions[0].Code);
            Assert.AreEqual(data.MedicalConditions[0].Description, intakeData.MedicalConditions[0].Description);
            Assert.AreEqual(data.MedicalConditions[1].Code, intakeData.MedicalConditions[1].Code);
            Assert.AreEqual(data.MedicalConditions[1].Description, intakeData.MedicalConditions[1].Description);
            Assert.AreEqual(data.MedicalConditions[2].Code, intakeData.MedicalConditions[2].Code);
            Assert.AreEqual(data.MedicalConditions[2].Description, intakeData.MedicalConditions[2].Description);
            Assert.AreEqual(data.MedicalConditions[3].Code, intakeData.MedicalConditions[3].Code);
            Assert.AreEqual(data.MedicalConditions[3].Description, intakeData.MedicalConditions[3].Description);

            Assert.AreEqual(data.Medications[0].Code, intakeData.Medications[0].Code);
            Assert.AreEqual(data.Medications[0].Description, intakeData.Medications[0].Description);
            Assert.AreEqual(data.Medications[1].Code, intakeData.Medications[1].Code);
            Assert.AreEqual(data.Medications[1].Description, intakeData.Medications[1].Description);
            Assert.AreEqual(data.Medications[2].Code, intakeData.Medications[2].Code);
            Assert.AreEqual(data.Medications[2].Description, intakeData.Medications[2].Description);
            Assert.AreEqual(data.Medications[3].Description, intakeData.Medications[3].Description);

            Assert.AreEqual(data.Surgeries[0].Description, intakeData.Surgeries[0].Description);
            Assert.AreEqual(data.Surgeries[0].Month, intakeData.Surgeries[0].Month);
            Assert.AreEqual(data.Surgeries[0].Year, intakeData.Surgeries[0].Year);
            Assert.AreEqual(data.Surgeries[1].Description, intakeData.Surgeries[1].Description);
            Assert.AreEqual(data.Surgeries[1].Month, intakeData.Surgeries[1].Month);
            Assert.AreEqual(data.Surgeries[1].Year, intakeData.Surgeries[1].Year);
            Assert.AreEqual(data.Surgeries[2].Description, intakeData.Surgeries[2].Description);
            Assert.AreEqual(data.Surgeries[2].Month, intakeData.Surgeries[2].Month);
            Assert.AreEqual(data.Surgeries[2].Year, intakeData.Surgeries[2].Year);

            Assert.AreEqual(data.InfantData.PatientAgeUnderOneYear, intakeData.InfantData.PatientAgeUnderOneYear);
            Assert.AreEqual(data.InfantData.FullTerm, intakeData.InfantData.FullTerm);
            Assert.AreEqual(data.InfantData.VaginalBirth, intakeData.InfantData.VaginalBirth);
            Assert.AreEqual(data.InfantData.DischargedWithMother, intakeData.InfantData.DischargedWithMother);
            Assert.AreEqual(data.InfantData.VaccinationsCurrent, data.InfantData.VaccinationsCurrent);
        }
    }
}
