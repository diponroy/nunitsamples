﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web.Http;
using FizzWare.NBuilder;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.Patients.Models;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.Patients.Controllers
{
    [TestFixture]
    public class AccountSettingControllerTest
    {
        protected Mock<ISnapContext> DbContext;
        protected AccountSettingsController Controller;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        private void InitializeController()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Controller = new AccountSettingsController(DbContext.Object);
        }

        private IList<TSource> BuildList<TSource>(int sampleNumber)
        {
            return Builder<TSource>.CreateListOfSize(sampleNumber).Build();
        }

        private IDbSet<TSource> BuildDbSet<TSource>(int sampleNumber) where TSource : class
        {
            return BuildList<TSource>(sampleNumber).ToDbSet();
        }


        [Test]
        public void ControllerInitialization()
        {
            Assert.DoesNotThrow(() => new AccountSettingsController(DbContext.Object));
        }


        private void SetUpEnvironment(bool doMockUser, bool doMockPatientProfile, out User user, out PatientProfile patient)
        {
            user = new User
            {
                UserId = 1,
                Email = "test@example.com",
                TimeZoneId = 1,
                IsActive = "A",
                UserTypeId = 1,
                HospitalId = 0

            };
            var user2 = new User
            {
                UserId = 2,
                Email = "test2@example.com",
                IsActive = "A",
                UserTypeId = 1,
                HospitalId = 0
            };
            var hospital = new Hospital
            {
                BrandName = "Test",
                HospitalName = "Test",
                HospitalDomainName = "example.com",
                BrandTitle = "Test",
                BrandColor = "#FFFFFF"
            };
            if (doMockPatientProfile)
            {
                patient = new PatientProfile
                {
                    PatientName = "Name",
                    LastName = "Lastname",
                    User = user,
                    UserId = 1,
                    IsDependent = "N",
                    FamilyGroupId = 1
                };



                FamilyProfile fp = new FamilyProfile
                {
                    FamilyGroupId = 1,
                    PatientProfiles = new List<PatientProfile> { patient }

                };

                patient.FamilyProfile = fp;

                user.PatientProfiles = new List<PatientProfile> { patient };
                DbContext.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile> { fp }.ToDbSet());
                DbContext.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile> { patient }.ToDbSet());
            }
            else
            {
                patient = null;
            }
            user.Hospital = hospital;
            user2.Hospital = hospital;

            DbContext.Setup(x => x.Users).Returns(new List<User> { user, user2 }.ToDbSet());

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken>().ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting>().ToDbSet());

            DbContext.Setup(x => x.ConfigMasters).Returns(
                new List<ConfigMaster>
                {
                    new ConfigMaster
                    {
                        ConfigCode = "SystemMailAddress",
                        ConfigValue = "example@example.com"
              
                    },
                    new ConfigMaster
                    {
                        ConfigCode = "SystemMailName",
                        ConfigValue = "TEST"
                    }
                }.ToDbSet());
            DbContext.Setup(x => x.Hospitals).Returns(new List<Hospital> { hospital }.ToDbSet());
            if (doMockUser)
            {
                Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();

                List<Claim> claims = new List<Claim>();

                claims.Add(new Claim("name", user.Email));
                claims.Add(new Claim("nameidentifier", user.UserId.ToString()));
                claims.Add(new Claim("provider", "0"));

                ClaimsIdentity c = new ClaimsIdentity(claims);
                mockPrincipal.Setup(e => e.Identity).Returns(c);
                Controller.User = mockPrincipal.Object;
            }
        }

        [Test]
        public void UpdateCustomerSettings_Change_Email_CreatedToken()
        {
            InitializeController();

            User user;
            PatientProfile patient;
            SetUpEnvironment(true, true, out user, out patient);

            Controller.Request = new HttpRequestMessage
            {
                RequestUri = new Uri("Http://snap-testing/api//patients/accountSettings")
            };

            var resp = Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
            {
                Email = "newemail@example.com",
                Lastname = "Newlastname",
                TimezoneId = 2,
                Name = "NewName"
            });

            Regex reg = new Regex("[ \n\r\t]+");
            var result = reg.Replace(JsonConvert.SerializeObject(resp), "");
            Assert.AreEqual(1, resp.Data.Count());
            Assert.AreEqual(AccountSettingsChangeStatusEnum.EmailChanged, resp.Data.First());
            var newPatient = DbContext.Object.PatientProfiles.First();
            var newUser = DbContext.Object.Users.First();
            //No changes exept email should be saved;
            Assert.AreEqual(patient.PatientName, newPatient.PatientName);
            Assert.AreEqual(patient.LastName, newPatient.LastName);
            Assert.AreEqual(user.TimeZoneId, newUser.TimeZoneId);
            //New token should be crated
            Assert.AreEqual(1, DbContext.Object.UserTokens.Count());
            var token = DbContext.Object.UserTokens.First();
            Assert.AreEqual(user.UserId, token.UserId);
            Assert.AreEqual("newemail@example.com", DbContext.Object.UserAccountSettings.First().NewEmail);
        }

        [Test]
        public void UpdateCustomerSettings_Email_Exists()
        {
            InitializeController();

            User user;
            PatientProfile patient;
            SetUpEnvironment(true, true, out user, out patient);

            var resp = Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
            {
                Email = "test2@example.com"
            });

            Assert.AreEqual(1, resp.Data.Count());
            Assert.AreEqual(AccountSettingsChangeStatusEnum.EmailExists, resp.Data.First());

            Assert.AreEqual(0, DbContext.Object.UserTokens.Count());
        }

        [Test]
        public void UpdateCustomerSettings_Change_NotEmail_Success()
        {
            InitializeController();

            User user;
            PatientProfile patient;
            SetUpEnvironment(true, true, out user, out patient);

            var resp = Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
              {
                  Email = user.Email,
                  Lastname = "Newlastname",
                  TimezoneId = 2,
                  Name = "NewName"
              });

            Assert.AreEqual(1, resp.Data.Count());
            Assert.AreEqual(AccountSettingsChangeStatusEnum.Success, resp.Data.First());
            var newPatient = DbContext.Object.PatientProfiles.First();
            var newUser = DbContext.Object.Users.First();
            Assert.AreEqual("NewName", newPatient.PatientName);
            Assert.AreEqual("Newlastname", newPatient.LastName);
            Assert.AreEqual(2, newUser.TimeZoneId);
        }
        [Test]
        public void UpdateCustomerSettings_NoPatient()
        {
            InitializeController();

            User user;
            PatientProfile patient;
            SetUpEnvironment(true, false, out user, out patient);

            Assert.Throws<HttpResponseException>(
                 () => Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
                 {
                     Email = "newtest@example.com"
                 }));
            try
            {
                Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
                {
                    Email = "newtest@example.com"
                });
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.Response.StatusCode);
            }

        }

        [Test]
        public void UpdateCustomerSettings_NoUser_Throws_Unauthorized()
        {
            InitializeController();

            User user;
            PatientProfile patient;
            SetUpEnvironment(false, false, out user, out patient);

            Assert.Throws<HttpResponseException>(() => Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
            {
                Email = "newtest@example.com"
            }));
            try
            {
                Controller.UpdateCustomerSettings(new AccountSettingsWithTimezone
                {
                    Email = "newtest@example.com"
                });
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.Response.StatusCode);
            }
        }

        [Test]
        public void GetCustomerSettings_NoPatients_Returns_DataIsNull()
        {
            InitializeController();
            User user;
            PatientProfile patient;
            SetUpEnvironment(true, false, out user, out patient);
            var resp = Controller.GetCustomerSettings();
            Assert.IsNull(resp.Data);
        }

        [Test]
        public void GetCustomerSettings_NoUser_throws_Unauthorized()
        {
            InitializeController();
            User user;
            PatientProfile patient;
            SetUpEnvironment(false, false, out user, out patient);
            Assert.Throws<UnauthorizedAccessException>(() => Controller.GetCustomerSettings());
        }

        [Test]
        public void GetCustomerSettings_Found()
        {
            InitializeController();
            User user;
            PatientProfile patient;
            SetUpEnvironment(true, true, out user, out patient);
            var resp = Controller.GetCustomerSettings();
            Assert.AreEqual(1, resp.Data.Count());

            foreach (var dataPiece in resp.Data)
            {
                Assert.AreEqual(user.UserId, dataPiece.UserId);
                Assert.AreEqual(patient.PatientName, dataPiece.Name);
                Assert.AreEqual(patient.LastName, dataPiece.Lastname);
                Assert.AreEqual(user.Email, dataPiece.Email);
                Assert.AreEqual(user.TimeZoneId, dataPiece.TimezoneId);
            }

        }


        /*
         * returns newEmail from UserAccountSetting attached to 
         *      an active 
         *      account setting token (code = 27) UserToken
         *  by token
         *  
         * using UserAccountSetting, UserToken tbl
         */
        [Test]
        public void GetNewEmailForUserToken_Found_Returns_Email()
        {
            string tokenString = "The token";

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 27         //code for account setting token
            };

            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email"
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.GetNewEmailForUserToken(tokenString);
            Assert.AreEqual(accountSetting.NewEmail, result);
        }

        [Test]
        public void GetNewEmailForUserToken_NotFound_UserToken_ByToken_Returns_EmptyString()
        {
            string tokenString = "The token";

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = "Any token",
                CodeSetId = 27         //code for account setting token
            };

            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email"
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.GetNewEmailForUserToken(tokenString);
            Assert.AreEqual("", result);
        }

        [Test]
        public void GetNewEmailForUserToken_UserToken_Expired_Returns_EmptyString()
        {
            string tokenString = "The token";

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "E",      //expired
                Token = "Any token",
                CodeSetId = 27         //code for account setting token
            };

            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email"
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.GetNewEmailForUserToken(tokenString);
            Assert.AreEqual("", result);
        }

        public void GetNewEmailForUserToken_UserToken_CodeSetId_NotForAccountSetting_Returns_EmptyString()
        {
            string tokenString = "The token";

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //Active
                Token = "Any token",
                CodeSetId = 27         //code is not for account setting token
            };

            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email"
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.GetNewEmailForUserToken(tokenString);
            Assert.AreEqual("", result);
        }

        [Test]
        public void GetNewEmailForUserToken_NotFound_UserAccountSetting_Returns_EmptyString()
        {
            string tokenString = "The token";

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = "Any token",
                CodeSetId = 27         //code for account setting token
            };

            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = 3,
                NewEmail = "The email"
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.GetNewEmailForUserToken(tokenString);
            Assert.AreEqual("", result);
        }

        /*
         * expires a UserToken by token
         *      where token is active
         *      and token is for account setting (code = 27)
         *  using UserToken table
         * 
         */
        [Test]
        public void CancleEmailResetUserToken_Found_SetStatus_Expired_Return_True()
        {
            string tokenString = "The token";
            DateTime dateTimeUtc = DateTime.UtcNow;

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 27         //account setting token
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            InitializeController();

            var result = Controller.CancleEmailResetUserToken(tokenString);
            Assert.IsTrue(result);
            Assert.AreEqual("E", token.TokenStatus);
            Assert.LessOrEqual(dateTimeUtc, token.ConfirmedDate);   //used utc date time
        }

        public void CancleEmailResetUserToken_NotFound_ByToken_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = "Any token",
                CodeSetId = 27         //account setting token
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            InitializeController();
            Assert.Throws<NullReferenceException>(() => Controller.CancleEmailResetUserToken(tokenString));
        }

        [Test]
        public void CancleEmailResetUserToken_NotFound_ActiveToken_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "E",      //inactive
                Token = tokenString,
                CodeSetId = 27         //account setting token
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            InitializeController();
            Assert.Throws<NullReferenceException>(() => Controller.CancleEmailResetUserToken(tokenString));
        }

        [Test]
        public void CancleEmailResetUserToken_NotFound_ByAccountSettingCode_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 26         //not account setting token
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            InitializeController();
            Assert.Throws<NullReferenceException>(() => Controller.CancleEmailResetUserToken(tokenString));
        }

        /*
         * apply changed email form UserAccountSetting, to User, and inactive's UserToken
         *      By token
         */

        [Test]
        public void ResetEmailForUserToken_Updateds_Email_Returns_True()
        {
            string tokenString = "The token";
            DateTime utcNow = DateTime.UtcNow;

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 27         //code for account setting token
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.ResetEmailForUserToken(tokenString);
            Assert.IsTrue(result);
        }

        [Test]
        public void ResetEmailForUserToken_Updateds_Email()
        {
            string tokenString = "The token";
            DateTime utcNow = DateTime.UtcNow;

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 27         //code for account setting token
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            var result = Controller.ResetEmailForUserToken(tokenString);
            Assert.AreEqual(accountSetting.NewEmail, user.Email);
            Assert.LessOrEqual(utcNow, user.UpdateDate);        //utc now used
            Assert.AreEqual(user.UserId, user.UpdatedBy);
        }

        [Test]
        public void ResetEmailForUserToken_NotFound_UserToken_ByToken_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = "Any token",
                CodeSetId = 27         //code for account setting token
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            Assert.Throws<NullReferenceException>(() => Controller.ResetEmailForUserToken(tokenString));
        }

        [Test]
        public void ResetEmailForUserToken_NotFound_UserToken_Experiec_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "E",      //inactive
                Token = tokenString,
                CodeSetId = 27         //code for account setting
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            Assert.Throws<NullReferenceException>(() => Controller.ResetEmailForUserToken(tokenString));
        }


        [Test]
        public void ResetEmailForUserToken_NotFound_UserToken_ByAccountSettingCode_Throws_Exception()
        {
            string tokenString = "The token";
            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 26         //not code for account setting
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = token.TokenId,
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            Assert.Throws<NullReferenceException>(() => Controller.ResetEmailForUserToken(tokenString));
        }

        [Test]
        public void ResetEmailForUserToken_NotFound_UserAccountSetting_Throws_Exception()
        {
            string tokenString = "The token";
            DateTime utcNow = DateTime.UtcNow;

            UserToken token = new UserToken
            {
                TokenId = 1,
                TokenStatus = "A",      //active
                Token = tokenString,
                CodeSetId = 27         //code for account setting token
            };
            User user = new User
            {
                UserId = 1,
                Email = "Old Email",
                UpdateDate = null,
                UpdatedBy = null
            };
            UserAccountSetting accountSetting = new UserAccountSetting
            {
                TokenId = 2,              //not 1
                NewEmail = "The email",
                UserId = user.UserId
            };

            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken> { token }.ToDbSet());
            DbContext.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            DbContext.Setup(x => x.UserAccountSettings).Returns(new List<UserAccountSetting> { accountSetting }.ToDbSet());
            InitializeController();

            Assert.Throws<NullReferenceException>(() => Controller.ResetEmailForUserToken(tokenString));
        }


        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Controller = null;
        }
    }
}
