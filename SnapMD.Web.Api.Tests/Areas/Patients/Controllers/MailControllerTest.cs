﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Enumerations;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Patients.Controllers;
using SnapMD.Web.Api.Utilities;

namespace SnapMD.Web.Api.Tests.Areas.Patients.Controllers
{
    [TestFixture]
    public class MailControllerTest
    {
        protected Mock<ISnapContext> DbContext;
        protected MailController Controller;

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        private void InitializeController()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Controller = new MailController(DbContext.Object, new Mock<ISnapAuthenticationManager>().Object);
        }

        protected Mock<ISnapContext> SetupDbSet<T>(List<T> dataSource) where T : class
        {
            var set = new Mock<IDbSet<T>>();
            set.Setup(drs => drs.Provider).Returns(dataSource.AsQueryable().Provider);
            set.Setup(drs => drs.Expression).Returns(dataSource.AsQueryable().Expression);
            set.Setup(drs => drs.ElementType).Returns(dataSource.AsQueryable().ElementType);
            set.Setup(drs => drs.GetEnumerator()).Returns(dataSource.AsQueryable().GetEnumerator());
            set.Setup(s => s.Add(It.IsAny<T>())).Callback((T item) => dataSource.Add(item));

            Type type = typeof (IDbSet<T>);
            Type contextType = typeof (ISnapContext);
            ParameterExpression parameter = Expression.Parameter(contextType);
            PropertyInfo info = contextType.GetProperties().First(pi => pi.PropertyType == type);
            MemberExpression body = Expression.Property(parameter, info);
            dynamic func = Expression.Lambda(body, parameter);

            DbContext.SetupProperty(func, set.Object);
            return DbContext;
        }

        protected IDbSet<TSource> DbSetWith<TSource>(params TSource[] items) where TSource : class
        {
            return items.ToList().ToDbSet();
        }

        /// <summary>
        /// send password reset email to a patient by emailId and type
        ///      when patient active, hospital active
        /// adds token to UserToken tbl, inactive previous active tokens
        /// </summary>
        /// <param name="type">input parameter for test case attributes</param>
        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("unknown")]
        public void EmailToRecoverPassword_EmailType_Throw_BadRequest(string type)
        {
            InitializeController();
            var ex = Controller.EmailToRecoverPassword("Test@gmail.com", type);
            Assert.IsInstanceOf<InvalidModelStateResult>(ex);
            Assert.IsTrue(((InvalidModelStateResult)ex).ModelState.ContainsKey("type"));
        }

        [Test]
        public void EmailToRecoverPassword_Email_NotFound_Throw_Exception()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A"
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com",
                IsActive = "A",
                UserTypeId = (int) UserTypeEnum.Customer
            };

            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            InitializeController();

            Exception ex =
                Assert.Throws<NullReferenceException>(
                    () => Controller.EmailToRecoverPassword("Test@gmail.com", "resetPassword"));
            Assert.AreEqual("active user not found with same email", ex.Message);
        }

        [Test]
        public void EmailToRecoverPassword_Patient_NotActive_Throw_Exception()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A"
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com",
                UserTypeId = (int) UserTypeEnum.Customer,
                IsActive = "I" //inactive
            };

            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            InitializeController();

            Exception ex =
                Assert.Throws<NullReferenceException>(
                    () => Controller.EmailToRecoverPassword(user.Email, "resetPassword"));
        }

        [Test]
        [TestCase(UserTypeEnum.HospitalStaff)]
        [TestCase(UserTypeEnum.SnapMDStaff)]
        [TestCase(UserTypeEnum.Unknown)]
        public void EmailToRecoverPassword_UserNot_Patient_Throw_Exception(UserTypeEnum userType)
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A"
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com",
                IsActive = "A",
                UserTypeId = (int) userType
            };


            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            InitializeController();

            Exception ex =
                Assert.Throws<NullReferenceException>(
                    () => Controller.EmailToRecoverPassword(user.Email, "resetPassword"));
            Assert.AreEqual("user need to be a patient only.", ex.Message);
        }

        [Test]
        public void EmailToRecoverPassword_Hospital_NotActive_Throw_Exception()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "I" //inactive
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com",
                IsActive = "A",
                UserTypeId = (int) UserTypeEnum.Customer
            };

            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            InitializeController();

            Exception ex =
                Assert.Throws<NullReferenceException>(
                    () => Controller.EmailToRecoverPassword(user.Email, "resetPassword"));
            Assert.AreEqual("active hospital not found.", ex.Message);
        }

        [Test, Explicit]
        public void EmailToRecoverPassword_Inactives_Old_Password_ResetTokens()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A" //imp
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com", //mail to
                IsActive = "A",
                UserTypeId = (int) UserTypeEnum.Customer
            };

            var mailAddressConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailAddress",
                ConfigValue = "aaron.lord+fromtest@snap.md" //mail from
            };
            var mailNameConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailName",
                ConfigValue = "aaron.lord"
            };

            var oldToken1 = new UserToken
            {
                UserId = user.UserId,
                CodeSetId = (int) UserTokenCodeSetEnum.ResetPassword,
                TokenStatus = "A"
            };
            var oldToken2 = new UserToken
            {
                UserId = user.UserId,
                CodeSetId = (int) UserTokenCodeSetEnum.ResetPassword,
                TokenStatus = "A"
            };
            var tokens = new List<UserToken> {oldToken1, oldToken2};

            HttpContextUtility.MockCurrent("",
                "http://snap.local/api/patients/diponsust@outlook.com/issueEmails/passwordRecovery", "");
            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            DbContext.Setup(x => x.ConfigMasters).Returns(DbSetWith(mailAddressConfig, mailNameConfig));
            SetupDbSet(tokens);
            InitializeController();

            Controller.EmailToRecoverPassword(user.Email, "resetPassword");
            Assert.AreEqual("E", oldToken1.TokenStatus);
            Assert.AreEqual("E", oldToken2.TokenStatus);
        }

        [Test, Explicit]
        public void EmailToRecoverPassword_Adds_New_ResetTokens()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A" //imp
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = hospital.HospitalId,
                Email = "diponsust@outlook.com", //mail to
                IsActive = "A",
                UserTypeId = (int) UserTypeEnum.Customer
            };

            var mailAddressConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailAddress",
                ConfigValue = "aaron.lord+fromtest@snap.md" //mail from
            };
            var mailNameConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailName",
                ConfigValue = "aaron.lord"
            };

            var tokens = new List<UserToken>();


            HttpContextUtility.MockCurrent("",
                "http://snap.local/api/patients/diponsust@outlook.com/issueEmails/passwordRecovery", "");
            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user));
            DbContext.Setup(x => x.ConfigMasters).Returns(DbSetWith(mailAddressConfig, mailNameConfig));
            SetupDbSet(tokens);
            InitializeController();

            DateTime utcNow = DateTime.UtcNow;
            Controller.EmailToRecoverPassword(user.Email, "resetPassword");
            UserToken addedToken = tokens.First();

            Assert.Less(utcNow, addedToken.RequestDate);
            Assert.AreEqual((int) UserTokenCodeSetEnum.ResetPassword, addedToken.CodeSetId);
            Assert.AreEqual("Reset Password", addedToken.RequestInfo);
            Assert.AreEqual(user.UserId, addedToken.UserId);
            Assert.IsNotNullOrEmpty(addedToken.Token);
            Assert.AreEqual("A", addedToken.TokenStatus);
            Assert.IsNull(addedToken.ConfirmedDate);
        }


        [Test, Explicit]
        public void EmailToRecoverPassword_Sends_Email()
        {
            var hospital = new Hospital
            {
                HospitalId = 1,
                BrandName = "BrandName",
                HospitalName = "HospitalName",
                HospitalDomainName = "http://snap.local",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A" //imp
            };

            var hospital2 = new Hospital
            {
                HospitalId = 2,
                BrandName = "BrandName2",
                HospitalName = "HospitalName2",
                HospitalDomainName = "http://emerald.snap.local",
                BrandColor = "red",
                BrandTitle = "BrandTitle",
                ContactNumber = "123123",
                IsActive = "A" //imp
            };

            var user = new User
            {
                UserId = 1,
                HospitalId = 1,
                Email = "aaron.lord+testemail@snap.mddiponsust@outlook.com", //mail to
                IsActive = "A",
                UserTypeId = (int) UserTypeEnum.Customer
            };

            var user2 = new User
            {
                UserId = 2,
                HospitalId = 2,
                Email = "aaron.lord+testemail@snap.md", //mail to
                IsActive = "A",
                UserTypeId = (int)UserTypeEnum.Customer
            };

            var mailAddressConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailAddress",
                ConfigValue = "aaron.lord+fromtest@snap.md" //mail from
            };
            var mailNameConfig = new ConfigMaster
            {
                ConfigCode = "SystemMailName",
                ConfigValue = "aaron.lord"
            };

            HttpContextUtility.MockCurrent("",
                "http://snap.local/api/patients/aaron.lord+testemail@snap.md/mail/passwordRecovery", "hospitalId=2");
            DbContext.Setup(x => x.Hospitals).Returns(DbSetWith(hospital, hospital2));
            DbContext.Setup(x => x.Users).Returns(DbSetWith(user, user2));
            DbContext.Setup(x => x.ConfigMasters).Returns(DbSetWith(mailAddressConfig, mailNameConfig));
            DbContext.Setup(x => x.UserTokens).Returns(new List<UserToken>().ToDbSet());
            InitializeController();

            Assert.DoesNotThrow(() => Controller.EmailToRecoverPassword(user2.Email, "resetPassword", 2));
        }


        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Controller = null;
        }
    }
}
