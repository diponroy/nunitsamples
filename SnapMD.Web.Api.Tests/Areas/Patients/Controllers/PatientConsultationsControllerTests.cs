﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.Patients.Controllers
{
    [TestFixture]
    public class PatientConsultationsControllerTests
    {
        [Test]
        public void TestIntakeUpdateException()
        {
            var mockContext = new Mock<ISnapContext>();
            var target = new PatientConsultationsController(mockContext.Object);

            Assert.Throws<ArgumentNullException>(() =>
                target.UpdateIntake(1, null, 3, 4, 3));
        }

        [Test]
        public void TestIntakeUpdate()
        {
            //var mockContext = new Mock<ISnapContext>();
            //mockContext.SetupGet(m => m.EncounterMetadata).Returns(new MockDbSet<EncounterMetadata>());

            ISnapContext context = new MockSnapContext();
            
            var target = new PatientConsultationsController(context);

            target.UpdateIntake(1, new IntakeQuestionnaire(), 3, 4, 3);
            Assert.Pass();
        }
    }
}
