﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.Patients.Controllers
{
    [TestFixture]
    public class OnboardControllerTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected AccountController Controller { get; set; }

        protected const string ValidPassword = "Password@123";      //use this for the password inputs

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Controller = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Controller = new AccountController(MockedDb.Object);
            Controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            Controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [Test]
        public void Password_Validator_Test()
        {
            List<string> errors;
            var isValid = PasswordUtility.Validate(ValidPassword, out errors);
            Assert.IsTrue(isValid);
            Assert.AreEqual(0, errors.Count);
        }

        /*
         * IsValidOnboardToken: find active on board token(CodeSetEnum.NewUserOnboardToken), by token:string
         *      returns true if token is valid
         *      returns faluse if token not valid
         */

        [Test]
        [TestCase("my token")]
        [TestCase("My Token")]
        [TestCase("MY TOKEN")]
        public void IsValidOnboardToken_Token_Valid_Returns_True(string token)
        {
            UserToken entityToken = Builder<UserToken>.CreateNew()
                    .With(x => x.Token = "my token")
                    .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                    .And(x => x.TokenStatus = "A")
                    .Build();

            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { entityToken }.ToDbSet());
            InitializeRepo();

            var result = Controller.IsValidOnboardToken(token);
            Assert.IsTrue(result);
        }

        [Test]
        [TestCase(CodeSetEnum.NewUserOnboardToken, "E")]     //status not active
        [TestCase(CodeSetEnum.NewUserOnboardToken, "")]
        [TestCase(CodeSetEnum.NewUserOnboardToken, null)]
        [TestCase(CodeSetEnum.New_User_Registation_Token, "A")] // code set not for on boarding
        [TestCase(CodeSetEnum.Reset_Password, "A")]
        public void IsValidOnboardToken_Token_Valid_Returns_False(CodeSetEnum codeSet, string entityStatus)
        {
            string token = "asfdsdfssfsdfsd";
            UserToken entityToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token == token)
                .And(x => x.CodeSetId = (int)codeSet)
                .And(x => x.TokenStatus = entityStatus)
                .Build();

            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { entityToken }.ToDbSet());
            InitializeRepo();

            var result = Controller.IsValidOnboardToken(token);
            Assert.IsFalse(result);
        }

        /*
         * update password for active onboard token, using User, UserToken, PatientProfile tbl
         *      set password with hash in user entity, actives the user 
         *      confirms the onboard token
         *      actives the patient profile entity
         */
        [Test]
        public void UpdatePassword_Success()
        {
            DateTime startTime = DateTime.UtcNow.AddSeconds(-0.1);

            string token = "asfdsdfssfsdfsd";
            string password = ValidPassword;

            User user = Builder<User>.CreateNew()
                .With(x => x.Password = null)
                .And(x => x.IsActive = "I")
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.IsActive ="I")
                .Build();
            UserToken userToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token = token)
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "A")
                .And(x => x.UserId = user.UserId)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { userToken }.ToDbSet());
            InitializeRepo();

            var result = Controller.UpdatePassword(token, password);
            Assert.IsInstanceOf<OkResult>(result);

            /*user token*/
            Assert.AreEqual("E", userToken.TokenStatus);

            Assert.Less(startTime, userToken.ConfirmedDate);

            /*user*/
            Assert.IsNotNull(user.Password);
            Assert.AreEqual("A", user.IsActive);
            Assert.AreEqual(user.UserId, user.UpdatedBy);
            Assert.Less(startTime, user.UpdateDate);
            Assert.IsTrue(PasswordUtility.Verify(password, user.Password)); //impt: for login

            /*patient profile*/
            Assert.AreEqual("A", patientProfile.IsActive);
            Assert.AreEqual(user.UserId, patientProfile.UpdatedBy);
            Assert.Less(startTime, patientProfile.UpdateDate);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void UpdatePassword_OnBoardToken_NullOrEmpty_Throws_Error(string token)
        {
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>().ToDbSet());
            InitializeRepo();

            var error = Assert.Catch<HttpResponseException>(() => Controller.UpdatePassword(token, ValidPassword));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Token required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase("expired onboard token")]     //On board token used/expired(not active)
        [TestCase("active not onboard token")]  //active but not OnBoard token  
        [TestCase("asdfasdfasdf")]              //token doen't exist
        public void UpdatePassword_OnBoardToken_ExpiredOrNotFound_Throws_Error(string token)
        {
            User user = Builder<User>.CreateNew()
                .With(x => x.Password = null)
                .And(x => x.IsActive = "I")
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.IsActive = "I")
                .Build();
            UserToken expiredOnboardToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token = "expired onboard token")
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "E")
                .And(x => x.UserId = user.UserId)
                .Build();
            UserToken activeNonOnboardToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token = "active not onboard token")
                .And(x => x.CodeSetId = (int)CodeSetEnum.Reset_Password)
                .And(x => x.TokenStatus = "A")
                .And(x => x.UserId = user.UserId)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { expiredOnboardToken, activeNonOnboardToken }.ToDbSet());
            InitializeRepo();

            var error = Assert.Catch<HttpResponseException>(() => Controller.UpdatePassword(token, ValidPassword));
            Assert.AreEqual(401, (int)error.Response.StatusCode);
            Assert.AreEqual("Token already used/expired or not found.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        /*
         * validation for
         * Length 8-20
         * At least one capital letter
         * At least one lowercase letter
         * At least one number
         * No spaces
         */
        [Test]
        [TestCase("Pas@123", @"Length 8-20: ^.{8,20}$")]
        [TestCase("Pas@123Pas@123Pas@123", @"Length 8-20: ^.{8,20}$")]
        [TestCase("password@123", @"At least one capital letter: [A-Z]")]
        [TestCase("PASSWORD@123", @"At least one lowercase letter: [a-z]")]
        [TestCase("PASSWORD@qwe", @"At least one number: [0-9]")]
        [TestCase("Password@ 123", @"No spaces")]
        [TestCase(" Password@123", @"No spaces")]
        [TestCase("Password@123 ", @"No spaces")]
        [TestCase("Passwor@  123", @"No spaces")]
        [TestCase("  Passwor@123", @"No spaces")]
        [TestCase("Passwor@123  ", @"No spaces")]
        public void UpdatePassword_Password_NotValid_Throws_Error(string password, string expectedMessage)
        {
            string token = "asfdsdfssfsdfsd";
            User user = Builder<User>.CreateNew()
                .With(x => x.Password = null)
                .And(x => x.IsActive = "I")
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.IsActive = "I")
                .Build();
            UserToken userToken = Builder<UserToken>.CreateNew()
                .With(x => x.Token = token)
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "A")
                .And(x => x.UserId = user.UserId)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { userToken }.ToDbSet());
            InitializeRepo();

            var error = Assert.Catch<HttpResponseException>(() => Controller.UpdatePassword(token, password));
            string errorMessage = error.Response.Content.ReadAsAsync<string>().Result;
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.True(errorMessage.StartsWith("Password is invalid."));
            Assert.True(errorMessage.EndsWith(expectedMessage));
        }
    }
}
