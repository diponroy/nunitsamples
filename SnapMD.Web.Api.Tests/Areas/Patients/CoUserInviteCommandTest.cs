﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Models;
using SnapMD.Web.Api.Patients;

namespace SnapMD.Web.Api.Tests.Areas.Patients
{
    [TestFixture]
    public class CoUserInviteCommandTest
    {
        protected Mock<ISnapContext> MockedDb { get; set; }

        protected CoUserInviteCommand Command { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            Command = null;
        }

        protected void InitializeRepo()
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            Command = new CoUserInviteCommand(MockedDb.Object);
        }

        [Test]
        [TestCase(UserTypeEnum.HospitalStaff)]  /*not customer user's, but at same hospital*/
        [TestCase(UserTypeEnum.SnapMDStaff)]
        [TestCase(UserTypeEnum.Unknown)]
        public void InviteCoUser_Success(UserTypeEnum userType)
        {
            string email = "test@user.com";
            int hospitalId = 1;

            var user = Builder<User>.CreateNew()
                .With(x => x.UserTypeId = (int)userType)
                .And(x => x.Email = email)
                .And(x => x.HospitalId = hospitalId)
                .And(x => x.IsActive = "A")
                .Build();
            List<User> users = new List<User>() { user };
            List<CoUserToken> tokens = new List<CoUserToken>();
            List<CoUsersTemp> coUsersTemps = new List<CoUsersTemp>();

            var mockSet = new Mock<IDbSet<CoUsersTemp>>();
            var data = coUsersTemps.AsQueryable();
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            mockSet.Setup(s => s.Add(It.IsAny<CoUsersTemp>())).Callback((CoUsersTemp item) =>
            {
                item.CoUserId = coUsersTemps.Count + 1;
                coUsersTemps.Add(item);
            });
            MockedDb.Setup(x => x.CoUsersTemps).Returns(mockSet.Object);
            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            MockedDb.Setup(x => x.CoUserTokens).Returns(tokens.ToDbSet());
            MockedDb.Setup(x => x.SaveChanges()).Returns(1);
            InitializeRepo();

            var profile = Builder<PatientUpdateRequest>.CreateNew()
                .With(x => x.Dob = DateTime.Now.AddYears(-30))
                .Build();
            var newPatientProfile = Builder<PatientProfile>.CreateNew().Build();

            string result = Command.InviteCoUser(profile, email, hospitalId, newPatientProfile);
            Assert.IsTrue(result.StartsWith("##"));
            /*token*/
            string coUserToken = result.TrimStart('#');
            Assert.AreEqual(30, coUserToken.Length);
            Assert.IsTrue(coUserToken.All(char.IsLetterOrDigit));
            Assert.IsFalse(coUserToken.Any(char.IsLower));
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("A")]
        [TestCase("I")]
        public void InviteCoUser_Email_Already_Used_Avoid_Invite(string entityStatus)
        {
            /*data*/
            string email = "test@user.com";
            int hospitalId = 1;

            var user = Builder<User>.CreateNew()
                .With(x => x.UserTypeId = (int)UserTypeEnum.Customer)
                .And(x => x.Email = email)
                .And(x => x.HospitalId = hospitalId)
                .And(x => x.IsActive = entityStatus)
                .Build();
            List<User> users = new List<User>() { user };
            List<CoUserToken> tokens = new List<CoUserToken>();
            List<CoUsersTemp> coUsersTemps = new List<CoUsersTemp>();

            /*mock*/
            var mockSet = new Mock<IDbSet<CoUsersTemp>>();
            var data = coUsersTemps.AsQueryable();
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<CoUsersTemp>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            mockSet.Setup(s => s.Add(It.IsAny<CoUsersTemp>())).Callback((CoUsersTemp item) =>
            {
                item.CoUserId = coUsersTemps.Count + 1;
                coUsersTemps.Add(item);
            });
            MockedDb.Setup(x => x.CoUsersTemps).Returns(mockSet.Object);
            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            MockedDb.Setup(x => x.CoUserTokens).Returns(tokens.ToDbSet());
            MockedDb.Setup(x => x.SaveChanges()).Returns(1);
            InitializeRepo();

            var profile = Builder<PatientUpdateRequest>.CreateNew()
                .With(x => x.Dob = DateTime.Now.AddYears(-30))
                .Build();
            var newPatientProfile = Builder<PatientProfile>.CreateNew().Build();
            string result = Command.InviteCoUser(profile, email, hospitalId, newPatientProfile);

            /*test*/
            Assert.AreEqual("EmailId Already Registered", result);
            Assert.AreEqual(1, users.Count);    //no entity added to users, coUsertokens, and coUserTemps
            Assert.AreEqual(0, tokens.Count);
            Assert.AreEqual(0, coUsersTemps.Count);
        }

    }
}
