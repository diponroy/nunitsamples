﻿using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces;
using SnapMD.Core.Interfaces.Models.ApiKeys;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Models;
using SnapMD.Web.Api.Patients.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SnapMD.Web.Api.Tests.Areas.Patients
{
    [TestFixture]
    public class PatientControllerTest
    {
        #region setup for lists
        private IPrincipal SetUpEnv(bool DoMockUser, out ISnapContext mockObject)
        {
            var mock = new Mock<ISnapContext>();

            var user = new User
            {
                UserId = 1,
                Email = "test@example.com"
            };
            var user2 = new User
            {
                UserId = 2,
                Email = "test2@example.com"
            };
            var role = new UserRole()
            {
                UserId = 1,
                User = user,
                Role = new Role
                {
                    RoleId = 1,
                    Description = "Test description 1"
                }
            };
            user.UserRoles = new List<UserRole> { role };
            var role2 = new UserRole()
            {
                UserId = 2,
                User = user2,
                Role = new Role
                {
                    RoleId = 2,
                    Description = "Test description 2"
                }
            };
            user2.UserRoles = new List<UserRole> { role2 };
            mock.Setup(x => x.Users).Returns(new List<User> { user, user2 }.ToDbSet());
            mock.Setup(x => x.PatientProfiles).Returns(
                new List<PatientProfile> 
                { 
                    new PatientProfile 
                    {
                        FamilyGroupId = 1,
                        UserId = 1,
                        PatientName = "Name",
                        LastName = "Lastname",
                        ProfileImagePath = "stub.jpg"
                    },
                    new PatientProfile 
                    {
                        FamilyGroupId = 1,
                        PatientName = "Name2",
                        LastName = "Lastname2",
                        UserId = 2,
                        ProfileImagePath = "stub.jpg"
                    }
            }.ToDbSet());
            mock.Setup(x => x.UserRoles).Returns(new List<UserRole> { role, role2 }.ToDbSet());

            mockObject = mock.Object;
            if (!DoMockUser)
            {
                return null;
            }
            else
            {
                Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();

                List<Claim> claims = new List<Claim>();

                claims.Add(new Claim("name", "test@example.com"));
                claims.Add(new Claim("nameidentifier", "1"));
                claims.Add(new Claim("provider", "0"));

                ClaimsIdentity c = new ClaimsIdentity(claims);
                mockPrincipal.Setup(e => e.Identity).Returns(c);

                return mockPrincipal.Object;
            }
        }

        #endregion

        #region Setup Specific

        [SetUp]
        public void SetUp()
        {
            //var identity = MakeUserMock();
        }

        private static Mock<IPrincipal> MakePrincipalMock()
        {
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();

            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim("name", "sameerfairgoogl@gmail.com"));
            claims.Add(new Claim("nameidentifier", "1"));
            claims.Add(new Claim("provider", "0"));

            ClaimsIdentity c = new ClaimsIdentity(claims);

            mockPrincipal.Setup(e => e.IsInRole("Admin")).Returns(true);
            //mockIIdentity.Setup(e=>e.Name).Returns("sameer");
            mockPrincipal.Setup(e => e.Identity).Returns(c);//mockIIdentity.Object);
            //More detail to add later for token

            return mockPrincipal;
        }

        private static Mock<ITokenIdentity> MakeUserMock()
        {
            var mockUser = new Mock<ITokenIdentity>();

            mockUser.SetupGet(m => m.ProviderId).Returns(1);
            mockUser.SetupGet(m => m.UserId).Returns(1);

            string hashCode = String.Format("{0:X}", DateTime.Now.Ticks.GetHashCode());
            var emailHash = string.Format("automatedtest+{0}@snap.md", hashCode).ToLower();

            mockUser.SetupGet(m => m.User).Returns(emailHash);

            return mockUser;
        }

        private static Mock<IDeveloperCredentialsRepository> MakeDeveloperAuthMock(out Guid DeveloperId, out string APIKey)
        {
            var repository = new Mock<IDeveloperCredentialsRepository>();
            var entity = new Mock<IApiCredentials>();
            const string sha1 = "12ad03c3f1ec45a1e571650943e6d0448fd057e6";
            entity.SetupGet(d => d.ApiKey).Returns(sha1);

            var devId = Guid.NewGuid();
            repository.Setup(r => r.Get(devId)).Returns(entity.Object);

            //var dev = new DeveloperIdentity(repository.Object) { DeveloperId = devId };
            //Assert.IsTrue(dev.IsValid(sha1));

            APIKey = sha1;
            DeveloperId = devId;

            return repository;
        }

        private static void GetContextWithSetUpDate(out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            context = new Mock<ISnapContext>();

            connectionId = Guid.NewGuid();
            userConnectionId = Guid.NewGuid();

            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "automatedtest@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest@snap.md",
                HospitalId = 99999,
                TimeZoneId = 1
            });

            userList.Add(new User()
            {
                UserId = 2,
                UserName = "testing",
                ProfileImage = "asdf",
                TimeZoneId = 1
            });

            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();

            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 3,
                Name = "SHibu",
                LastName = "Bhattarai",
                ProfileImagePath = "pathtoprofile"
            });

            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 2,
                Name = "Hari",
                LastName = "Kumar",
                ProfileImagePath = "pathtoprofile"
            });

            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 1,
                Name = "Crazy",
                LastName = "Going",
                ProfileImagePath = "pathtoprofile"
            });

            var usRelation = new UserDependentRelationAuthorization
            {
                CreatedBy = 1,
                IsAuthorized = "Y",
                PatientId = 2,
                UserId = 1,
                User = userList[0],
                RelatAuthId = 1,
                RelationCodeId = 1

            };


            IList<UserDependentRelationAuthorization> dependentRelation = new List<UserDependentRelationAuthorization>() { usRelation };


            IList<PatientProfile> pasProfile = new List<PatientProfile>();

            pasProfile.Add(new PatientProfile()
            {
                PatientId = 1,
                UserId = 1,

                PatientName = "Test",
                LastName = "Test",

                ProfileImagePath = "pathtoprofile",

                FileSharingSiteUserName = "automatedtest@snap.md",
                FileSharingSitePassword = "Password@123",

                BaseFolderId = "e942d049-260f-4253-b8dc-b3616e3fb8b0",

                HospitalId = 99999,

                IsDependent = "N",

                FamilyGroupId = 1,

                User = userList.First(),

                Dob = new DateTime(1983, 06, 04),
                Enthicity = 1,
                BloodType = 1,
                HairColor = 1,
                EyeColor = 1,

                Gender = "M",

                SchoolName = "Hamdard",
                SchoolContact = "1111",
                IsChild = "Y",
                IsActive = "A",
                CreateDate = new DateTime(2001, 1, 1),
                UpdateDate = new DateTime(2001, 1, 1),
                CreatedBy = 1,
                UpdatedBy = 1,

                HomePhone = "1231",
                MobilePhone = "1231",

                PrimaryPhysician = "Crazy",
                PrimaryPhysicianContact = "Crazy",
                PhysicianSpecialist = "Crazy",
                PhysicianSpecialistContact = "Crazy",

                PreferredPharmacy = "None",
                PharmacyContact = "None",

                FamilyPediatrician = "None",
                FamilyPediatricianContact = "None",

                Address = "None",

                City = "KHI",
                State = "Sindh",
                ZipCode = "None",
                StateId = 1,
                RxNtPatientId = null,

                Height = "11",
                Weight = "11",
                Country = "None",

                HeightUnit = 1,

                WeightUnit = 1,

                // Organization = "SnapMD",
                //   Location = "Location",

                UserDependentRelationAuthorizations = { },
                Consultations = { new Consultation() { PrimaryConsern = "consultation", SecondaryConsern = "consultation" } },
                PatientAddresses = { new PatientAddress() { AddressId = 1, AddressType = 1 } },
                PatientMedicalHistories = { 
                    new PatientMedicalHistory 
                    {
                        AllergicMedication1 = 1,
                        AllergicMedication2 = 2,
                        AllergicMedication3 = 3,
                        AllergicMedication4 = 4,
                        
                        MedicalCondition1 = 1,
                        MedicalCondition2 = 1,
                        MedicalCondition3 = 1,
                        MedicalCondition4 = 1,

                        CreateDate = DateTime.UtcNow,
                        CreatedBy = 1,
                        
                        HistoryId = 1,

                        IsChildBornFullTerm = "Y",
                        IsChildBornVaginally = "Y",
                        IsChildDischargeMother = "Y",
                        IsOneYearBelowChild = "Y",
                        IsVaccinationUpToDate = "Y",

                        PatientId = 1,
                        
                        PriorSurgery1 = "Prior1", 
                        Surgery1Month = 10, 
                        Surgery1Year = 2015, 

                        PriorSurgery2 = "Prior2", 
                        Surgery2Month = 10, 
                        Surgery2Year = 2015, 

                        PriorSurgery3 = "Prior3", 
                        Surgery3Month = 10, 
                        Surgery3Year = 2015, 

                        TakingMedication1 = 1,
                        TakingMedication2 = 1,
                        TakingMedication3 = 1,
                        TakingMedication4 = "1",

                        UpdateDate = DateTime.UtcNow,
                        UpdatedBy = 1
                        
                    } 
                },
                ScheduledConsultationTemps = { new ScheduledConsultationTemp { PrimaryConsern = "Consultation" } },
                PatientLogin = { },
                CoUsersTemps = { }
            });

            pasProfile.Add(new PatientProfile()
            {
                PatientId = 2,

                PatientName = "Test",
                LastName = "Test",

                ProfileImagePath = "pathtoprofile",

                FileSharingSiteUserName = "automatedtest@snap.md",
                FileSharingSitePassword = "Password@123",

                BaseFolderId = "e942d049-260f-4253-b8dc-b3616e3fb8b0",

                HospitalId = 99999,

                IsDependent = "N",

                FamilyGroupId = 1,

                User = userList.First(),

                Dob = new DateTime(1983, 06, 04),
                Enthicity = 1,
                BloodType = 1,
                HairColor = 1,
                EyeColor = 1,

                Gender = "M",

                SchoolName = "Hamdard",
                SchoolContact = "1111",
                IsChild = "Y",
                IsActive = "A",
                CreateDate = new DateTime(2001, 1, 1),
                UpdateDate = new DateTime(2001, 1, 1),
                CreatedBy = 1,
                UpdatedBy = 1,

                HomePhone = "1231",
                MobilePhone = "1231",

                PrimaryPhysician = "Crazy",
                PrimaryPhysicianContact = "Crazy",
                PhysicianSpecialist = "Crazy",
                PhysicianSpecialistContact = "Crazy",

                PreferredPharmacy = "None",
                PharmacyContact = "None",

                FamilyPediatrician = "None",
                FamilyPediatricianContact = "None",

                Address = "None",

                City = "KHI",
                State = "Sindh",
                ZipCode = "None",
                StateId = 1,
                RxNtPatientId = null,

                Height = "11",
                Weight = "11",
                Country = "None",

                HeightUnit = 1,

                WeightUnit = 1,

                // Organization = "SnapMD",
                //   Location = "Location",

                UserDependentRelationAuthorizations = dependentRelation,
                Consultations = { new Consultation() { PrimaryConsern = "consultation", SecondaryConsern = "consultation" } },
                PatientAddresses = { new PatientAddress() { AddressId = 1, AddressType = 1 } },
                PatientMedicalHistories = { 
                    new PatientMedicalHistory 
                    {
                        AllergicMedication1 = 1,
                        AllergicMedication2 = 2,
                        AllergicMedication3 = 3,
                        AllergicMedication4 = 4,
                        
                        MedicalCondition1 = 1,
                        MedicalCondition2 = 1,
                        MedicalCondition3 = 1,
                        MedicalCondition4 = 1,

                        CreateDate = DateTime.UtcNow,
                        CreatedBy = 1,
                        
                        HistoryId = 1,

                        IsChildBornFullTerm = "Y",
                        IsChildBornVaginally = "Y",
                        IsChildDischargeMother = "Y",
                        IsOneYearBelowChild = "Y",
                        IsVaccinationUpToDate = "Y",

                        PatientId = 1,
                        
                        PriorSurgery1 = "Prior1", 
                        Surgery1Month = 10, 
                        Surgery1Year = 2015, 

                        PriorSurgery2 = "Prior2", 
                        Surgery2Month = 10, 
                        Surgery2Year = 2015, 

                        PriorSurgery3 = "Prior3", 
                        Surgery3Month = 10, 
                        Surgery3Year = 2015, 

                        TakingMedication1 = 1,
                        TakingMedication2 = 1,
                        TakingMedication3 = 1,
                        TakingMedication4 = "1",

                        UpdateDate = DateTime.UtcNow,
                        UpdatedBy = 1
                        
                    } 
                },
                PatientLogin = { },
                CoUsersTemps = { }
            });

            IList<Code> CodesList = new List<Code>();
            CodesList.Add(new Code
            {
                CodeId = 1,
                HospitalId = 99999,
                Description = "CodeID1"
            });
            CodesList.Add(
            new Code { CodeId = 2, CodeSetId = 12, Description = "Self", HospitalId = 99999 }
            );


            IList<HospitalSetting> hospitalSettings = new List<HospitalSetting>();
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.BaseFolder",
                Value = "0313022e-f8b6-44f3-9019-162c5e3c774b"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomers",
                Value = "97e64afe-0ab9-4617-8b43-5a1cfbc74742"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.StaffFolder",
                Value = "8750d915-8b49-4f26-abac-cc8cbb6f42c1"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.ConsultationsFolder",
                Value = "8c969fa3-4357-4a90-9b32-c5d7767a5b4a"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.SharedFolder",
                Value = "9d614b57-9c1d-4855-bfc9-bc508354a91f"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalMainGroup",
                Value = "99999-Clinicians"
            });
            hospitalSettings.Add(new HospitalSetting()
            {
                HospitalId = 99999,
                Key = "FileSharing.HospitalCustomersGroup",
                Value = "99999-Customers"
            });

            IList<Hospital> hospitalList = new List<Hospital>();
            hospitalList.Add(new Hospital()
            {
                HospitalId = 99999,
                BrandName = "Automated Test Hospital",
                HospitalName = "Automated Test Hospital",
                Users = new List<User>(userList)
            });


            IList<PatientAddress> patientAddressList = new List<PatientAddress>();
            patientAddressList.Add(new PatientAddress()
            {
                Address = new Address { AddressId = 1, AddressText = "test", IsActive = true },
                AddressId = 1,
                AddressType = 1,
                PatientProfileId = 1
            });

            IList<Address> addressList = new List<Address>();
            addressList.Add(new Address()
            {
                AddressId = 1,
                AddressText = "this is a sample address",
                IsActive = true
            });

            IList<ScheduledConsultationTemp> scheduledConsultationTempList = new List<ScheduledConsultationTemp>();
            scheduledConsultationTempList.Add(new ScheduledConsultationTemp()
            {
                AssignedDoctorId = 1,
                ConsultationId = 1,
                CreatedBy = 1,
                CreatedDate = DateTime.UtcNow,
                IsEmailSecondSent = "Y",
                IsEmailSent = "Y",
                IsNoCharge = "Y",
                IsSmsSent = "Y",
                Note = "Note",
                PatientId = 1,
                PrimaryConsern = "Primary Concern",
                ScheduledDate = DateTime.UtcNow,
                ScheduledTime = DateTime.UtcNow,
                SchedulingReasonType = 1,
                SecondaryConsern = "Sec Concern",
                ShcheduledId = 1
            });

            IList<Consultation> consultationList = new List<Consultation>();
            consultationList.Add(new Consultation()
            {
                ConsultationId = 1,
                AssignedDoctorId = 1,
                AssignedDateTime = DateTime.UtcNow,
                AssignedHospitalUserId = 1,
                CardId = 1,
                ChatHistoryFiles = null,
                ConferenceKey = "Key",
                ConsultantUserId = 1,
                ConsultationAmount = 1,
                ConsultationDate = DateTime.UtcNow,
                ConsultationDuration = 1,
                ConsultationFolderId = "FolderID",
                ConsultationPaymentsDetails = null,
                ConsultationStatus = 1,
                ConsultationTime = DateTime.UtcNow,
                CopayAmount = 12,
                CreateDate = DateTime.UtcNow,
                CreatedBy = 1,
                DroppedConsultations = null,
                HealthPlanId = 1,
                HospitalId = 1,
                IsHealthPlanApply = "Y",
                IsScheduled = "Y",
                Note = "Note",
                OrderNo = "1231",
                PatientId = 1,
                PatientProfile = null,
                PhoneNumber = "12312",
                PrevConsultationId = 1,
                ScheduledConsultationTemps = null,
                UpdateDate = DateTime.UtcNow,
                UpdatedBy = 1,
                WaitingLists = null,
                IsNoCharge = "Y",
                PrimaryConsern = "Primary Concern",
                SecondaryConsern = "Sec Concern",
            });

            IList<UserDependentRelationAuthorization> userDependentRelationAuthorizationList = new List<UserDependentRelationAuthorization>();
            userDependentRelationAuthorizationList.Add(new UserDependentRelationAuthorization()
            {
                CreatedBy = 1,
                CreatedDate = DateTime.Now,
                IsAuthorized = "Y",
                PatientId = 1,
                PatientProfile = null,
                RelatAuthId = 1,
                RelationCodeId = 1,
                UpdatedBy = 1,
                UpdatedDate = DateTime.Now,
                User = null,
                UserId = 1
            });
           userDependentRelationAuthorizationList = userDependentRelationAuthorizationList.Concat(dependentRelation).ToList();

            IList<StandardTimeZone> StandardTimeZonesList = new List<StandardTimeZone>();
            StandardTimeZonesList.Add(new StandardTimeZone()
            {
                TimeZoneDescription = "PST",
                TimeZoneId = 1,
                TimeZoneName = "Pacific Standard Time"
            });

            IList<ChatMessage> chatMessageList = new List<ChatMessage>();

            IList<PatientMedicalHistory> medicalHistory = new List<PatientMedicalHistory>();
            medicalHistory.Add(new PatientMedicalHistory()
            {

                PatientId = 1,
                MedicalCondition1 = 1,
                AllergicMedication1 = 1,
                PriorSurgery1 = "1",
                TakingMedication1 = 1,
                IsChildBornFullTerm = "Y",
                IsChildBornVaginally = "Y",
                IsChildDischargeMother = "Y",
                IsOneYearBelowChild = "Y",
                IsVaccinationUpToDate = "Y",
                Surgery1Month = 05,
                Surgery1Year = 2014
            });
            List<TelephoneCountryCode> codes = new List<TelephoneCountryCode>
            {
                new TelephoneCountryCode()
                {
                     CountryCode = "+1",
                     CountryName = "United States",
                     TelephoneCountryID = 1
                }
            };
            List<PatientAddress> addresses = new List<PatientAddress>()
            {
                new PatientAddress
                {
                    Address = addressList[0],
                    AddressId = 1,
                    AddressType = 0,
                    PatientProfile = pasProfile[0],
                    PatientProfileId = 1
                },
                new PatientAddress
                {
                    Address = addressList[0],
                    AddressId = 1,
                    AddressType = 0,
                    PatientProfile = pasProfile[1],
                    PatientProfileId = 2
                },
            };

            List<PatientMedicalHistory> mhis = new List<PatientMedicalHistory>()
            {
                new PatientMedicalHistory()
                {
                    AllergicMedication1 = 1,
                    AllergicMedication2 = 2,
                    AllergicMedication3 = 3,
                    AllergicMedication4 = 4,

                    MedicalCondition1 = 1,
                    MedicalCondition2 = 1,
                    MedicalCondition3 = 1,
                    MedicalCondition4 = 1,

                    CreateDate = DateTime.UtcNow,
                    CreatedBy = 1,

                    HistoryId = 1,

                    IsChildBornFullTerm = "Y",
                    IsChildBornVaginally = "Y",
                    IsChildDischargeMother = "Y",
                    IsOneYearBelowChild = "Y",
                    IsVaccinationUpToDate = "Y",

                    PatientId = 1,

                    PriorSurgery1 = "Prior1",
                    Surgery1Month = 10,
                    Surgery1Year = 2015,

                    PriorSurgery2 = "Prior2",
                    Surgery2Month = 10,
                    Surgery2Year = 2015,

                    PriorSurgery3 = "Prior3",
                    Surgery3Month = 10,
                    Surgery3Year = 2015,

                    TakingMedication1 = 1,
                    TakingMedication2 = 1,
                    TakingMedication3 = 1,
                    TakingMedication4 = "1",

                    UpdateDate = DateTime.UtcNow,
                    UpdatedBy = 1

                },
                new PatientMedicalHistory()
                {
                    AllergicMedication1 = 1,
                    AllergicMedication2 = 2,
                    AllergicMedication3 = 3,
                    AllergicMedication4 = 4,

                    MedicalCondition1 = 1,
                    MedicalCondition2 = 1,
                    MedicalCondition3 = 1,
                    MedicalCondition4 = 1,

                    CreateDate = DateTime.UtcNow,
                    CreatedBy = 1,

                    HistoryId = 2,

                    IsChildBornFullTerm = "Y",
                    IsChildBornVaginally = "Y",
                    IsChildDischargeMother = "Y",
                    IsOneYearBelowChild = "Y",
                    IsVaccinationUpToDate = "Y",

                    PatientId = 2,

                    PriorSurgery1 = "Prior1",
                    Surgery1Month = 10,
                    Surgery1Year = 2015,

                    PriorSurgery2 = "Prior2",
                    Surgery2Month = 10,
                    Surgery2Year = 2015,

                    PriorSurgery3 = "Prior3",
                    Surgery3Month = 10,
                    Surgery3Year = 2015,

                    TakingMedication1 = 1,
                    TakingMedication2 = 1,
                    TakingMedication3 = 1,
                    TakingMedication4 = "1",

                    UpdateDate = DateTime.UtcNow,
                    UpdatedBy = 1

                },
            };
        
            context.SetupGet(m => m.PatientMedicalHistories).Returns(medicalHistory.ToDbSet());
            context.SetupGet(c => c.StandardTimeZones).Returns(StandardTimeZonesList.ToDbSet());

            context.Setup(m => m.ChatMessages).Returns(chatMessageList.ToDbSet());
            context.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(pasProfile.ToDbSet());
            context.SetupGet(m => m.Hospitals).Returns(hospitalList.ToDbSet());
            context.SetupGet(m => m.HospitalSettings).Returns(hospitalSettings.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());
            context.SetupGet(m => m.Codes).Returns(CodesList.ToDbSet());
            context.SetupGet(m => m.PatientAddresses).Returns(patientAddressList.ToDbSet());
            context.SetupGet(m => m.Addresses).Returns(addressList.ToDbSet());
            context.SetupGet(m => m.ScheduledConsultationTemps).Returns(scheduledConsultationTempList.ToDbSet());
            context.SetupGet(m => m.Consultations).Returns(consultationList.ToDbSet());
            context.SetupGet(m => m.UserDependentRelationAuthorizations).Returns(userDependentRelationAuthorizationList.ToDbSet());
            context.SetupGet(m => m.Codes).Returns(CodesList.ToDbSet());
            context.SetupGet(m => m.TelephoneCountryCodes).Returns(codes.ToDbSet());
            context.SetupGet(m => m.PatientAddresses).Returns(addresses.ToDbSet());
            context.SetupGet(m => m.PatientMedicalHistories).Returns(mhis.ToDbSet());
        }

        #endregion

        private static ApiResponseV2<PatientDetails> Patient_GetSpecific(string requestParam, out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            PatientsController controller = new PatientsController(context.Object);
            controller.User = MakePrincipalMock().Object;

            return controller.GetV2(requestParam);
        }

        private static ApiResponseV2<PatientDetails> Patient_GetSpecificOtherId(string requestParam, out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            PatientsController controller = new PatientsController(context.Object);
            controller.User = MakePrincipalMock().Object;

            return controller.GetDetailsForIdV2(2, requestParam);
        }

        [Test]
        public void GetAdultsFromfamilyGroup_Authorized_ReturnsProfiles()
        {
            ISnapContext context;
            var principal = SetUpEnv(true, out context);
            var target = new PatientsController(context);
            target.User = principal;

            var resp = target.GetAdultsFromfamilyGroup();
            foreach (var dataPiece in resp.Data)
            {


                int id = dataPiece.UserId;

                var pp = context.PatientProfiles.First(x => x.UserId == id);

                Assert.AreEqual(pp.PatientName, dataPiece.Name);
                Assert.AreEqual(pp.LastName, dataPiece.Lastname);
                Assert.AreEqual(pp.ProfileImagePath, dataPiece.ImagePath);
                Assert.AreEqual(pp.User.UserRoles.First().Role.Description, dataPiece.Description);
                Assert.AreEqual(pp.User.UserRoles.First().Role.RoleId, dataPiece.RoleId);


            }

        }

        [Test]
        public void GetAdultsFromfamilyGroup_NoUser_Throws_InvalidOperation()
        {
            ISnapContext context;
            SetUpEnv(false, out context);
            var target = new PatientsController(context);

            Assert.Throws<InvalidOperationException>(() => target.GetAdultsFromfamilyGroup());

        }
        [Test]
        public void Patient_Get_IsNotNull()
        {
            //Mock SnapContext to implement Patient Repo

            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            ApiResponseV2<PatientDetails> returnVal = Patient_GetSpecific("", out context, out connectionId, out userConnectionId);

            Assert.IsNotNull(returnVal);
        }

        [Test]
        public void Patient_Get_GetsOnlyBasicInformationWithoutParameters()
        {
            //Mock SnapContext to implement Patient Repo
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            ApiResponseV2<PatientDetails> returnVal = Patient_GetSpecific("", out context, out connectionId, out userConnectionId);

            PatientDetails p = returnVal.Data.First();
            //TODO: Sameer - 28APR2015, Add More checks later

            Assert.IsNotNull(p.PatientName);
        }

        [Test]
        public void Patient_GetDetailsForIdV2_GetsAllInformation()
        {
            //Mock SnapContext to implement Patient Repo
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            var returnVal =
                Patient_GetSpecificOtherId(
                    "All",
                    out context, out connectionId, out userConnectionId);

            var p = returnVal.Data.First();
            //TODO: Sameer - 28APR2015, Add More checks later

            Assert.IsNotNull(p.PatientName, "Patient Name is null");

            Assert.IsNotNull(p.Account, "Account is null");
            Assert.IsTrue(p.Account.PatientId != 0);

            Assert.IsNotNull(p.Anatomy.EyeColor, "EyeColor is null");
            Assert.IsTrue(p.Anatomy.EyeColor != string.Empty, "EyeColor Missing");



            Assert.IsNotNull(p.PharmacyDetails, "Pharmacy Details is null");
            Assert.IsTrue(p.PharmacyDetails.PharmacyContact != string.Empty, "Pharmacy Details Missing");

            Assert.IsNotNull(p.PhysicianDetails, "Physician Details is null");
            Assert.IsTrue(p.PhysicianDetails.FamilyPediatrician != string.Empty, "Physician Details Missing");

            Assert.IsNotNull(p.Addresses, "Addresses is null");
            Assert.IsTrue(p.Addresses.Count() > 0 && p.Addresses.First().AddressText != string.Empty, "Patient Addresses Missing");

            Assert.IsNotNull(p.MedicalHistory, "Medical History is null");
            Assert.Greater(p.MedicalHistory.Surgeries.Count(), 0, "Prior Surgeries Count is 0");
            Assert.Greater(p.MedicalHistory.MedicationAllergies.Count(), 0, "Medical Allergies Count is 0");
            Assert.Greater(p.MedicalHistory.Medications.Count(), 0, "Medications Count is 0");
            Assert.Greater(p.MedicalHistory.MedicalConditions.Count(), 0, "Medical Conditions Count is 0");


        }

        [Test]
        public void Patient_Get_GetsAllInformation()
        {
            //Mock SnapContext to implement Patient Repo
            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;

            var returnVal =
                Patient_GetSpecific(
                    "AccountDetails, Physician, Pharmacy, Anatomy, Addresses, MedicalHistory",
                    out context, out connectionId, out userConnectionId);

            var p = returnVal.Data.First();
            //TODO: Sameer - 28APR2015, Add More checks later

            Assert.IsNotNull(p.PatientName, "Patient Name is null");

            Assert.IsNotNull(p.Account, "Account is null");
            Assert.IsTrue(p.Account.PatientId != 0);

            Assert.IsNotNull(p.Anatomy.EyeColor, "EyeColor is null");
            Assert.IsTrue(p.Anatomy.EyeColor != string.Empty, "EyeColor Missing");

            //Assert.IsNotNull(p.Consultations, "Consultations is null");
            //Assert.IsTrue(p.Consultations.Count() > 0 && p.Consultations.First().PrimaryConcern != string.Empty,
            //    "Consultations Missing");

            Assert.IsNotNull(p.PharmacyDetails, "Pharmacy Details is null");
            Assert.IsTrue(p.PharmacyDetails.PharmacyContact != string.Empty, "Pharmacy Details Missing");

            Assert.IsNotNull(p.PhysicianDetails, "Physician Details is null");
            Assert.IsTrue(p.PhysicianDetails.FamilyPediatrician != string.Empty, "Physician Details Missing");

            Assert.IsNotNull(p.Addresses, "Addresses is null");
            Assert.IsTrue(p.Addresses.Count() > 0 && p.Addresses.First().AddressText != string.Empty, "Patient Addresses Missing");

            Assert.IsNotNull(p.MedicalHistory, "Medical History is null");
            //Assert.Greater(p.MedicalHistory.Count(), 0, "Medical History Count is 0");
            Assert.Greater(p.MedicalHistory.Surgeries.Count(), 0, "Prior Surgeries Count is 0");
            Assert.Greater(p.MedicalHistory.MedicationAllergies.Count(), 0, "Medical Allergies Count is 0");
            Assert.Greater(p.MedicalHistory.Medications.Count(), 0, "Medications Count is 0");
            Assert.Greater(p.MedicalHistory.MedicalConditions.Count(), 0, "Medical Conditions Count is 0");

            //Assert.Greater(p.MedicalHistory.First().PriorSurgeries[0].Length, 0, "Prior Surgeries First item string is empty");

            //Assert.IsNotNull(p.ScheduledConsultations, "Scheduled Consultations is null");
            //Assert.Greater(p.ScheduledConsultations.Count(), 0, "Scheduled Consultations Missing");
            //Assert.IsTrue(p.ScheduledConsultations.First().FirstName != string.Empty, "Scheduled Consultations Missing");
        }
    }
}