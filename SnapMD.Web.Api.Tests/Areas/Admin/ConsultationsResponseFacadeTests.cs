﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder.Dates;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;
using SnapMD.Web.Api.Areas.Admin.Models;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    class ConsultationsResponseFacadeTests
    {
        private const int HospitalId = 1;
        private ConsultationsResponseFacade _consultationsResponseFacade;
        private ConsultationRequest _defaultRequest;
        private TimeZoneInfo _timeZone;
        private Mock<IConsultationRepository> _consultationRepositoryMock;
        private List<Consultation> _consultationsList;

        private DateTime? _now;
        private DateTime CurrentDate
        {
            get
            {
                if (!_now.HasValue)
                {
                    _now = DateTime.Now;
                }

                return _now.Value;
            }
        }


        [SetUp]
        public void SetUp()
        {
            _consultationRepositoryMock = new Mock<IConsultationRepository>();

            _consultationRepositoryMock.Setup(x => x.GetPatientsConsultations(It.IsAny<IEnumerable<int>>()))
                 .Returns((IEnumerable<int> ids) => ids.Select(id => new PatientConsultationInfo
                 {
                     ConsultationId = id
                 }));

            _consultationsList = new List<Consultation>
            {
                new Consultation()
                {
                    ConsultationId = 1,
                    ConsultationTime = CurrentDate.ToUniversalTime(),
                    PatientId = 10,
                    AssignedDoctorId = 20
                    
                },
                new Consultation()
                {
                    ConsultationId = 2,
                    ConsultationTime = CurrentDate.AddDays(2).ToUniversalTime(),
                    PatientId = 11,
                    AssignedDoctorId = 21
                },
                new Consultation()
                {
                    ConsultationId = 3,
                    ConsultationTime = CurrentDate.AddDays(3).ToUniversalTime(),
                    PatientId = 12,
                    AssignedDoctorId = 22
                }
            };
            _consultationRepositoryMock.Setup(x => x.GetScheduledConsultations(It.IsAny<int>()))
                .Returns(_consultationsList.AsQueryable());

            _consultationRepositoryMock.Setup(x => x.GetPastConsultations(It.IsAny<int>()))
                 .Returns(Enumerable.Empty<Consultation>().AsQueryable());
            _consultationRepositoryMock.Setup(x => x.GetActiveConsultations(It.IsAny<int>()))
                 .Returns(Enumerable.Empty<Consultation>().AsQueryable());
            _consultationRepositoryMock.Setup(x => x.GetDroppedConsultations(It.IsAny<int>()))
                 .Returns(Enumerable.Empty<Consultation>().AsQueryable());
            _consultationRepositoryMock.Setup(x => x.GetDNAConsultations(It.IsAny<int>()))
                 .Returns(Enumerable.Empty<Consultation>().AsQueryable());

            _timeZone = TimeZoneInfo.Local;
            _consultationsResponseFacade = new ConsultationsResponseFacade(_consultationRepositoryMock.Object, _timeZone, HospitalId);
            _defaultRequest = new ConsultationRequest
            {
                ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Scheduled
            };
        }

        [Test]
        public void Get_ConsultationStatusNotProvided_ThrownSnapServiceException()
        {
            var exception = Assert.Throws<SnapServiceException>(() => { _consultationsResponseFacade.Get(new ConsultationRequest()); });

            Assert.That(exception.Message, Is.EqualTo("Consultation status not provided"));
        }

        [Test]
        public void Get_UnknownConsultationStatus_ThrownSnapServiceException()
        {
            var exception = Assert.Throws<SnapServiceException>(() => { _consultationsResponseFacade.Get(new ConsultationRequest()
            {
                ConsultationStatus = "Foo"
            }); });

            Assert.That(exception.Message, Is.EqualTo("Unknown status: Foo"));
        }

        [Test]
        public void Get_DateFilter_ConvertToUTC()
        {
            var strartDate = DateTime.Now;
            var endDate = DateTime.Now.AddDays(5);

            _defaultRequest.StartDate = strartDate;
            _defaultRequest.EndDate = endDate;

            _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(_defaultRequest.StartDate, Is.EqualTo(TimeZoneInfo.ConvertTimeToUtc(strartDate, _timeZone)));
            Assert.That(_defaultRequest.EndDate, Is.EqualTo(TimeZoneInfo.ConvertTimeToUtc(endDate, _timeZone)));
        }

        [Test]
        public void Get_GivenConsultationStatus_AppropriateConsultationsQueryCalled()
        {
            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Scheduled;
            _consultationsResponseFacade.Get(_defaultRequest);
            _consultationRepositoryMock.Verify(x => x.GetScheduledConsultations(HospitalId));

            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Active;
            _consultationsResponseFacade.Get(_defaultRequest);
            _consultationRepositoryMock.Verify(x => x.GetActiveConsultations(HospitalId));

            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.DNA;
            _consultationsResponseFacade.Get(_defaultRequest);
            _consultationRepositoryMock.Verify(x => x.GetDNAConsultations(HospitalId));

            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Dropped;
            _consultationsResponseFacade.Get(_defaultRequest);
            _consultationRepositoryMock.Verify(x => x.GetDroppedConsultations(HospitalId));

            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Past;
            _consultationsResponseFacade.Get(_defaultRequest);
            _consultationRepositoryMock.Verify(x => x.GetPastConsultations(HospitalId));
        }

        [Test]
        public void Get_GivenTakeAndSkipParameters_PagingWorkCorrectly()
        {
            _defaultRequest.ConsultationStatus = ConsultationsResponseFacade.ConsultationStatuses.Scheduled;
            _defaultRequest.Skip = 10;
            _defaultRequest.Take = 15;

            var consultations = new List<Consultation>();
            for (int i = 0; i < 100; i++)
            {
                consultations.Add(new Consultation()
                {
                    HospitalId = HospitalId,
                    ConsultationId = i
                });
            }

            _consultationRepositoryMock.Setup(x => x.GetScheduledConsultations(HospitalId))
                .Returns(consultations.AsQueryable());

            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Total, Is.EqualTo(100));
            Assert.That(result.Data.Count(), Is.EqualTo(_defaultRequest.Take));
            Assert.That(result.Data.First().ConsultationId, Is.EqualTo(_defaultRequest.Skip));
            Assert.That(result.Data.Last().ConsultationId, Is.EqualTo(_defaultRequest.Skip + _defaultRequest.Take - 1));
        }

        [Test]
        public void Get_ConsultationsDatesInUTC_ConvertedToUserTime()
        {
            var date = DateTime.UtcNow.AddDays(-1);

            _consultationRepositoryMock.Setup(x => x.GetPatientsConsultations(It.IsAny<IEnumerable<int>>()))
                .Returns(new List<PatientConsultationInfo>()
                {
                    new PatientConsultationInfo
                    {
                        ConsultationId = 1,
                        CreatedDate = date
                    }
                });

            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Data.First().CreatedDate, Is.EqualTo(TimeZoneInfo.ConvertTimeFromUtc(date, _timeZone)));
        }

        [Test]
        public void Get_GivenStartDateFilter_ReturnConsultationsWhichMeetCriteria()
        {
            _defaultRequest.StartDate = CurrentDate.AddDays(3);
            
            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Data.Count(), Is.EqualTo(1));
            Assert.That(result.Data.First().ConsultationId, Is.EqualTo(3));
        }

        [Test]
        public void Get_GivenEndDateFilteer_ReturnConsultationsWhichMeetCriteria()
        {
            var now = DateTime.Now;
            _defaultRequest.EndDate = now.AddDays(2);

            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Data.Count(), Is.EqualTo(2));
            Assert.That(result.Data.First().ConsultationId, Is.EqualTo(2));
            Assert.That(result.Data.Last().ConsultationId, Is.EqualTo(1));
        }

        [Test]
        public void Get_GivenPatientFilter_ReturnConsultationsWhichMeetCriteria()
        {
            _defaultRequest.PatientId = 11;

            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Data.Count(), Is.EqualTo(1));
            Assert.That(result.Data.First().ConsultationId, Is.EqualTo(2));
        }

        [Test]
        public void Get_GivenDoctorFilter_ReturnConsultationsWhichMeetCriteria()
        {
            _defaultRequest.PhysicianUserId = 22;

            var result = _consultationsResponseFacade.Get(_defaultRequest);

            Assert.That(result.Data.Count(), Is.EqualTo(1));
            Assert.That(result.Data.First().ConsultationId, Is.EqualTo(3));
        }

        [Test]
        public void Get_GivenDefaultFilter_ReturnConsultationsOrderedDescendingByConsultationTime()
        {
            var actualOrder = 
                _consultationsResponseFacade.Get(_defaultRequest).Data
                .Select(x => x.ConsultationId).ToArray();

            var expectedOrder =
                _consultationsList.OrderByDescending(x => x.ConsultationTime)
                .Select(x => x.ConsultationId).ToArray();

            CollectionAssert.AreEqual(expectedOrder, actualOrder);
        }
    }
}
