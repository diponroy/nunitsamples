﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Interfaces;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Areas.Admin.Models;
using UserRole = SnapMD.Data.Entities.UserRole;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class RolesControllerTest
    {
        private const int HospitalId = 1;
        private const int ExistedInHospitalRoleId = 1;
        private const int NotExistedInHospitaRoleId = 2;
        private const int UserId = 1;

        private readonly List<Role> _roles;
        private readonly List<Role> _staffRoles;
        private readonly List<SnapFunction> _functions;

        private readonly Mock<ISnapContext> context = new Mock<ISnapContext>();
        private readonly Mock<IRoleRepository> roleRepository = new Mock<IRoleRepository>();
        private readonly Mock<IRoleFunctionRepository> roleFunctionRepository = new Mock<IRoleFunctionRepository>();
        private readonly Mock<ISnapAuthenticationManager> authenticationManager = new Mock<ISnapAuthenticationManager>();

        public RolesControllerTest()
        {
            _roles = new List<Role>
            {
                new Role
                {
                    HospitalId = HospitalId,
                    RoleId = ExistedInHospitalRoleId,
                    Description = "Good Available Role",
                    IsActive = "A",
                    RoleType = "H",
                    OnOff = "A"
                },
                new Role
                {
                    HospitalId = 77,
                    RoleId = NotExistedInHospitaRoleId,
                    Description = "Role not belong hospital Role",
                    IsActive = "A",
                    RoleType = "H",
                    OnOff = "A"
                },
                new Role
                {
                    HospitalId = HospitalId,
                    RoleId = ExistedInHospitalRoleId,
                    Description = "Not Active Role",
                    IsActive = "I", //NOT ACTIVE
                    RoleType = "H",
                    OnOff = "A"
                },
                new Role
                {
                    HospitalId = HospitalId,
                    RoleId = ExistedInHospitalRoleId,
                    Description = "Not Staff Role",
                    IsActive = "A", 
                    RoleType = "X",//NOT Staff Role
                    OnOff = "A"
                },
                new Role
                {
                    HospitalId = HospitalId,
                    RoleId = ExistedInHospitalRoleId,
                    Description = "Off role",
                    IsActive = "A",
                    RoleType = "H",
                    OnOff = "I" //Off role
                }
            };

            _staffRoles = new List<Role>
            {
                new Role
                {
                    HospitalId = HospitalId,
                    RoleId = ExistedInHospitalRoleId,
                    Description = "Some Description"
                },
                new Role
                {
                    HospitalId = 77,
                    RoleId = NotExistedInHospitaRoleId,
                    Description = "Tag from 77 Hospital"
                },
            };

            _functions = new List<SnapFunction>
            {
                new SnapFunction
                {
                    FunctionId = 1,
                    Description = "F1"
                },
                new SnapFunction
                {
                    FunctionId = 2,
                    Description = "F2"
                },
                new SnapFunction
                {
                    FunctionId = 3,
                    Description = "F3"
                }
            };
        }

        [TestCase(UserTypeEnum.Customer)]
        public void GET_GetRoleByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            RolesController controller = CreateController();

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Get(); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GET_GetRoleByAdmin_ReturnActiveRolesWhichBelongToTheHospital()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            context.SetupGet(x => x.Roles).Returns(_roles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Get();

            //Check that only one valid role were selected.
            Assert.That(result.Data.Count(), Is.EqualTo(1));

            var role = result.Data.First();

            Assert.That(role.Description, Is.EqualTo("Good Available Role"));
        }


        /* DELETE  */


        [TestCase(UserTypeEnum.Customer)]
        public void DELETE_DeleteRoleByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            RolesController controller = CreateController();

            var result = controller.Delete(ExistedInHospitalRoleId) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_RoleWhichNotBelongToUserHospital_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_roles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(NotExistedInHospitaRoleId) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_NotExistedRole_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_roles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(999) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_existedRole_RoleTurnOff()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var role = new Role()
            {
                HospitalId = HospitalId,
                RoleId = ExistedInHospitalRoleId,
                Description = "Test role",
                IsActive = "A",
                RoleCode = "H",
                OnOff = "A"
            };

            context.SetupGet(x => x.Roles).Returns(new List<Role> {role}.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(ExistedInHospitalRoleId) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(context.Object.Roles.Count(), Is.EqualTo(1));
            Assert.That(role.OnOff, Is.EqualTo("I"));
        }

        /* roles/{id}/staff */

        [TestCase(UserTypeEnum.Customer)]
        public void POST_AssignRoleByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            RolesController controller = CreateController();

            var result = controller.Post(ExistedInHospitalRoleId, new StaffAccount[0]) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_RoleFromHospitalWhichNotBelongToUser_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Post(NotExistedInHospitaRoleId, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_NotExistedRole_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Post(99, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_ValidNewRoleAndActivatedAccount_RoleAssignedReturnNoContent()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserRoles = new List<UserRole>()
            };

            var users = new List<User>() { user };

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            RolesController controller = CreateController();

            var result =
                controller.Post(ExistedInHospitalRoleId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag assigned to the user.
            Assert.That(user.UserRoles.Count, Is.EqualTo(1));
        }

        [Test]
        public void POST_ValidRoleAndActivatedAccount_RoleAssignedReturnNoContent()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var userRole = new UserRole()
            {
                RoleId = ExistedInHospitalRoleId,
                UserId = UserId,
                IsActive = "I" // tag Map exist but NOT Active.
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserRoles = new List<UserRole>() { userRole }
            };

            var users = new List<User>() { user };

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Post(ExistedInHospitalRoleId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(user.UserRoles.Count, Is.EqualTo(1));
            Assert.That(userRole.IsActive, Is.EqualTo("A"));
        }

        /* DELETE */

        [TestCase(UserTypeEnum.Customer)]
        public void DELETE_AssignRoleByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            RolesController controller = CreateController();

            var result = controller.Delete(ExistedInHospitalRoleId, new StaffAccount[0]) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_RoleFromHospitalWhichNotBelongToUser_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(NotExistedInHospitaRoleId, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_NotExistedRoleFromStaff_ReturnNotFound()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(99, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_ValidRoleAndActivatedAccount_RoleAssignedReturnNoContent()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var userRole = new UserRole()
            {
                RoleId = ExistedInHospitalRoleId,
                UserId = UserId,
                IsActive = "A" // Active tag Map.
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserRoles = new List<UserRole>() { userRole }
            };

            var users = new List<User>() { user };

            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            RolesController controller = CreateController();

            var result = controller.Delete(ExistedInHospitalRoleId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(user.UserRoles.Count, Is.EqualTo(1));
            Assert.That(userRole.IsActive, Is.EqualTo("I"));
        }

        [Test]
        public void GET_GetAllRoleFunctions()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            RolesController controller = new RolesController(authenticationManager.Object, context.Object, roleRepository.Object, roleFunctionRepository.Object);

            roleFunctionRepository.Setup(repository => repository.GetAvailableRoleFunctions()).Returns(_functions);

            var response = controller.GetAllRoleFunctions();

            Assert.AreEqual(_functions.Count, response.Total);
            Assert.NotNull(response.Data);
            AssertResponseFunctions(_functions, response.Data.ToList());
        }

        [Test]
        public void GET_GetRoleFunctions()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            RolesController controller = CreateController();

            roleFunctionRepository.Setup(repository => repository.GetAssignedRoleFunctions(It.IsAny<int>(), HospitalId)).Returns(_functions);
            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            var response = controller.GetRoleFunctions(ExistedInHospitalRoleId);

            Assert.AreEqual(_functions.Count, response.Total);
            Assert.NotNull(response.Data);
            AssertResponseFunctions(_functions, response.Data.ToList());
        }

        [Test]
        public void GET_GetRoleFunctions_NotExistingRole()
        {
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            RolesController controller = CreateController();

            roleFunctionRepository.Setup(repository => repository.GetAssignedRoleFunctions(It.IsAny<int>(), HospitalId)).Returns(_functions);
            context.SetupGet(x => x.Roles).Returns(_staffRoles.ToDbSet());

            Assert.Throws<HttpResponseException>(() => controller.GetRoleFunctions(NotExistedInHospitaRoleId));
        }

        private void AssertResponseFunctions(List<SnapFunction> expectedFunctions, List<Function> actualFunctions)
        {   
            Assert.AreEqual(_functions.Count, actualFunctions.Count());

            for (int i = 0; i < actualFunctions.Count(); i++)
            {
                Assert.AreEqual(expectedFunctions[i].FunctionId, actualFunctions[i].FunctionId);
                Assert.AreEqual(expectedFunctions[i].Description, actualFunctions[i].Description);
            }
        }

        private RolesController CreateController()
        {
            return new RolesController(authenticationManager.Object, context.Object, new RoleRepository(context.Object), roleFunctionRepository.Object);
        }
    }
}
