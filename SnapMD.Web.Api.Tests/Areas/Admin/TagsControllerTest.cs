﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Areas.Admin.Models;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class TagsControllerTest
    {
        private const int HospitalId = 1;
        private const int ExistedInHospitalTagId = 1;
        private const int NotExistedInHospitalTagId = 2;
        private const int UserId = 1;

        private readonly List<Tag> _tags;
        private readonly List<Tag> _staffTags;

        public TagsControllerTest()
        {
            _tags = new List<Tag>()
            {
                new Tag()
                {
                    HospitalId = HospitalId,
                    TagId = ExistedInHospitalTagId,
                    Description = "Good Available TAG",
                    IsActive = "A"
                  
                },

                new Tag()
                {
                    HospitalId = 77,
                    TagId = NotExistedInHospitalTagId,
                    Description = "Tag not belong hospital",
                    IsActive = "A"
                },

                new Tag()
                {
                    HospitalId = HospitalId,
                    TagId = ExistedInHospitalTagId,
                    Description = "Not Active Tag",
                    IsActive = "I"
                }
            };

            _staffTags = new List<Tag>()
            {
                new Tag()
                {
                    HospitalId = HospitalId,
                    TagId = ExistedInHospitalTagId,
                    Description = "Some Description"
                },

                new Tag()
                {
                    HospitalId = 77,
                    TagId = NotExistedInHospitalTagId,
                    Description = "Tag from 77 Hospital"
                },

            };
        }

        [TestCase(UserTypeEnum.Customer)]
        public void GET_GetTagsByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            TagsController controller = new TagsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Get(); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GET_GetTagByAdmin_ReturnActiveTagsWhichBelongToTheNetwork()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_tags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Get();

            //Check that only one valid role were selected.
            Assert.That(result.Data.Count(), Is.EqualTo(1));

            var role = result.Data.First();

            Assert.That(role.Description, Is.EqualTo("Good Available TAG"));
        }

        /* DELETE  */


        [TestCase(UserTypeEnum.Customer)]
        public void DELETE_DeleteTagByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            TagsController controller = new TagsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            var result = controller.Delete(ExistedInHospitalTagId) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_TagWhichNotBelongToUserHospital_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_tags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Delete(NotExistedInHospitalTagId) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_NotExistedTag_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_tags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Delete(999) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_ExistedTag_RoleTurnOff()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var role = new Tag()
            {
                HospitalId = HospitalId,
                TagId = ExistedInHospitalTagId,
                Description = "Test role",
                IsActive = "A"
            };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(new List<Tag> { role }.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Delete(ExistedInHospitalTagId) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(context.Object.Tags.Count(), Is.EqualTo(1));
            Assert.That(role.IsActive, Is.EqualTo("I"));
        }

        /* tags/{id}/staff */
        //=================================================================================================================================================

        [TestCase(UserTypeEnum.Customer)]
        public void POST_AssignTagByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            TagsController controller = new TagsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            var result = controller.Post(ExistedInHospitalTagId, new StaffAccount[0]) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_TagFromHospitalWhichNotBelongToUser_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(NotExistedInHospitalTagId, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_NotExistedTag_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(99, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void POST_ValidNewTagAndActivatedAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserTagMaps = new List<UserTagMap>()
            };

            var users = new List<User>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag assigned to the user.
            Assert.That(user.UserTagMaps.Count, Is.EqualTo(1));
        }

        [Test]
        public void POST_ValidTagAndActivatedAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var userTagMap = new UserTagMap()
            {
                TagId = ExistedInHospitalTagId,
                UserId = UserId,
                IsActive = "I" // tag Map exist but NOT Active.
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserTagMaps = new List<UserTagMap>() { userTagMap }
            };

            var users = new List<User>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(user.UserTagMaps.Count, Is.EqualTo(1));
            Assert.That(userTagMap.IsActive, Is.EqualTo("A"));
        }


        [Test]
        public void POST_ValidNewTagAndPendingAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = false // PENDING Account
            };

            var user = new CoUsersTemp()
            {
                CoUserId = UserId,
                UserTagMapTemps = new List<UserTagMapTemp>()
            };

            var users = new List<CoUsersTemp>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.CoUsersTemps).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag assigned to the user.
            Assert.That(user.UserTagMapTemps.Count, Is.EqualTo(1));
        }

        [Test]
        public void POST_ValidTagAndPendingAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = false // PENDING Account
            };

            var userTagMap = new UserTagMapTemp()
            {
                TagId = ExistedInHospitalTagId,
                CoUserId = UserId,
                IsActive = "I" // tag Map exist but NOT Active.
            };

            var user = new CoUsersTemp()
            {
                CoUserId = UserId,
                UserTagMapTemps = new List<UserTagMapTemp>() { userTagMap }
            };

            var users = new List<CoUsersTemp>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.CoUsersTemps).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Post(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag assigned to the user.
            Assert.That(user.UserTagMapTemps.Count, Is.EqualTo(1));
            Assert.That(userTagMap.IsActive, Is.EqualTo("A"));
        }

        /* DELETE */


        [TestCase(UserTypeEnum.Customer)]
        //[TestCase(UserTypeEnum.HospitalStaff)]
        public void DELETE_AssignTagByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            TagsController controller = new TagsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            var result = controller.Delete(ExistedInHospitalTagId, new StaffAccount[0]) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_TagFromHospitalWhichNotBelongToUser_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Delete(NotExistedInHospitalTagId, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_NotExistedTagFromStaff_ReturnNotFound()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result = controller.Delete(99, new StaffAccount[0]) as NotFoundResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_ValidTagAndActivatedAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = true //ACTIVATED Account
            };

            var userTagMap = new UserTagMap()
            {
                TagId = ExistedInHospitalTagId,
                UserId = UserId,
                IsActive = "A" // Active tag Map.
            };

            var user = new User()
            {
                HospitalId = HospitalId,
                UserId = UserId,
                UserTagMaps = new List<UserTagMap>() { userTagMap }
            };

            var users = new List<User>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.Users).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result =
                controller.Delete(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag updated.
            Assert.That(user.UserTagMaps.Count, Is.EqualTo(1));
            Assert.That(userTagMap.IsActive, Is.EqualTo("I"));
        }

        [Test]
        public void DELETE_ValidTagAndPendingAccount_TagAssignedReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var staffAccount = new StaffAccount()
            {
                userId = UserId,
                isActivated = false // PENDING Account
            };

            var userTagMap = new UserTagMapTemp()
            {
                TagId = ExistedInHospitalTagId,
                CoUserId = UserId,
                IsActive = "A" // tag Map exist but NOT Active.
            };

            var user = new CoUsersTemp()
            {
                CoUserId = UserId,
                UserTagMapTemps = new List<UserTagMapTemp>() { userTagMap }
            };

            var users = new List<CoUsersTemp>() { user };

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.Tags).Returns(_staffTags.ToDbSet());
            context.SetupGet(x => x.CoUsersTemps).Returns(users.ToDbSet());

            TagsController controller = new TagsController(authenticationManager.Object, context.Object);

            var result =
                controller.Delete(ExistedInHospitalTagId, new[] { staffAccount }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            //Check that tag assigned to the user.
            Assert.That(user.UserTagMapTemps.Count, Is.EqualTo(1));
            Assert.That(userTagMap.IsActive, Is.EqualTo("I"));
        }

    }
}
