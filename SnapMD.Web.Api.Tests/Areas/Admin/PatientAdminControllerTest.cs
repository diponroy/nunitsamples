﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.Core;
using SnapMD.Core.Interfaces;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Admin.Models;
using UserRole = SnapMD.Data.Entities.UserRole;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class PatientAdminControllerTest
    {
        protected class TargetController : PatientAdminController
        {
            public TargetController(ISnapContext mockContext)
                : base(mockContext)
            {
            }

            public static ITokenIdentity MockedTokenIdentity { get; set; }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    if (MockedTokenIdentity == null)
                    {
                        throw new NullReferenceException(
                            "controller trying to use user's TokenIdentity, need to set MockedTokenIdentity.");
                    }
                    return MockedTokenIdentity;
                }
            }

            protected override void EnableLazyLoading()
            {
            }
        }

        protected Mock<ISnapContext> MockedDb { get; set; }

        protected TargetController Controller { get; set; }

        protected string[] InvalidEmailTestCaseList = new string[]
        {
            "",
            null,
            "Test 20@test.com",
            "Test%40test.com"
        };

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;

            TargetController.MockedTokenIdentity = null;
            Controller = null;
        }

        protected void InitializeController(ITokenIdentity mockedTokenIdentity)
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }
            TargetController.MockedTokenIdentity = mockedTokenIdentity;
            Controller = new TargetController(MockedDb.Object);
            Controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            Controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        protected void SetOnBoardEmailSettings(ITokenIdentity mockedTokenIdentity, bool sendEmail)
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }

            List<HospitalSetting> settings = new List<HospitalSetting>();
            if (sendEmail == false)
            {
                HospitalSetting setting = Builder<HospitalSetting>.CreateNew()
                    .With(x => x.HospitalId = mockedTokenIdentity.ProviderId)
                    .And(x => x.Key = HospitalSettingsKeys.CustomerSso)
                    .And(x => x.Value = CustomerSsoType.Mandatory.ToString())
                    .Build();
                settings.Add(setting);
            }
            MockedDb.Setup(x => x.HospitalSettings).Returns(settings.ToDbSet());
        }

        protected void MockCountryAndTimeZone(PatientOnBoardShortDetail detail)
        {
            if (detail.CountryId == null || detail.TimeZoneId == null)
            {
                throw new Exception("CountryId or TimezoneId is null at mocking country and timezone");
            }
            var country = Builder<TelephoneCountryCode>.CreateNew().With(x => x.TelephoneCountryID = (int)detail.CountryId).Build();
            var timeZone = Builder<StandardTimeZone>.CreateNew().With(x => x.TimeZoneID = (int)detail.TimeZoneId).Build();
            MockedDb.Setup(x => x.TelephoneCountryCodes).Returns(new List<TelephoneCountryCode>() { country }.ToDbSet());
            MockedDb.Setup(x => x.StandardTimeZones).Returns(new List<StandardTimeZone>() { timeZone }.ToDbSet());
        }

        [Test]
        public void SetOnBoardEmailSettings_Test()
        {
            ITokenIdentity authUser = Builder<TokenIdentity>.CreateNew().Build();

            /*send email*/
            SetOnBoardEmailSettings(authUser, false);
            InitializeController(authUser);
            Assert.IsFalse(Controller.OnboardWithChangePasswordEmail());

            /*don't send email*/
            SetOnBoardEmailSettings(authUser, true);
            InitializeController(authUser);
            Assert.True(Controller.OnboardWithChangePasswordEmail());
        }

        /*
         * Hospital got settings to send onboard email or not
         *  hospital settings key 'CustomerSSO' got value 
         *      'Mandatory', don't send email
         *      'Optional' or 'No', send email
         *      no row found with key 'CustomerSSO', send email
         *      Throw error for unknown value
         */
        [Test]
        [TestCase("Mandatory", false)]
        [TestCase("Optional", true)]
        [TestCase("None", true)]
        public void OnboardWithChangePasswordEmail(string customerSsoSettingsValue, bool expectedResult)
        {
            ITokenIdentity authUser = Builder<TokenIdentity>.CreateNew().Build();
            HospitalSetting setting = Builder<HospitalSetting>.CreateNew()
                .With(x => x.HospitalId = authUser.ProviderId)
                .And(x => x.Key = HospitalSettingsKeys.CustomerSso)
                .And(x => x.Value = customerSsoSettingsValue)
                .Build();

            MockedDb.Setup(x => x.HospitalSettings).Returns(new List<HospitalSetting>() { setting }.ToDbSet());
            InitializeController(authUser);
            Assert.AreEqual(expectedResult, Controller.OnboardWithChangePasswordEmail());
        }

        [Test]
        public void OnboardWithChangePasswordEmail_CustomerSso_Setting_NotFound_Returns_True()
        {
            ITokenIdentity authUser = Builder<TokenIdentity>.CreateNew().Build();
            MockedDb.Setup(x => x.HospitalSettings).Returns(new List<HospitalSetting>().ToDbSet());
            InitializeController(authUser);
            Assert.IsTrue(Controller.OnboardWithChangePasswordEmail());
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("UnknownValue")]
        [TestCase(" Mandatory")]
        [TestCase("Mandatory ")]
        [TestCase(" Mandatory ")]
        public void OnboardWithChangePasswordEmail_Unknown_Value_Found_Throws_Error(string customerSsoSettingsValue)
        {
            ITokenIdentity authUser = Builder<TokenIdentity>.CreateNew().Build();
            HospitalSetting setting = Builder<HospitalSetting>.CreateNew()
                .With(x => x.HospitalId = authUser.ProviderId)
                .And(x => x.Key = HospitalSettingsKeys.CustomerSso)
                .And(x => x.Value = customerSsoSettingsValue)
                .Build();

            MockedDb.Setup(x => x.HospitalSettings).Returns(new List<HospitalSetting>() { setting }.ToDbSet());
            InitializeController(authUser);
            var error = Assert.Catch<Exception>(() => Controller.OnboardWithChangePasswordEmail());
            Assert.AreEqual("Unknown value for 'CustomerSso' hospital setting key", error.Message);
        }

        /// <summary>
        /// find patient user of a hospital, by email
        ///      status active/inactive/what ever
        ///      hospital id from login user's hospital Id
        /// </summary>
        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void Get_ShortDetail(string userStatus)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            var user = Builder<User>.CreateNew()
                .With(x => x.HospitalId = identity.ProviderId)
                .And(x => x.Email = "test@test.com")
                .And(x => x.UserTypeId = (int)UserTypeEnum.Customer)
                .And(x => x.IsActive = userStatus)
                .Build();
            var patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.Country = 100.ToString())
                .Build();
            var address = Builder<Address>.CreateNew()
                .With(x => x.IsActive = true)
                .Build();
            var patientAddress = Builder<PatientAddress>.CreateNew()
                .With(x => x.PatientProfileId = patientProfile.PatientId)
                .And(x => x.AddressId = address.AddressId)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>() { address }.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>() { patientAddress }.ToDbSet());
            InitializeController(identity);

            var result = Controller.ShortDetail(user.Email);
            Assert.IsInstanceOf<ApiResponse<PatientOnBoardShortDetail>>(result);
            Assert.IsTrue(result.Success);

            var data = result.Data;
            Assert.AreEqual(patientProfile.PatientName, data.FirstName);
            Assert.AreEqual(patientProfile.LastName, data.LastName);
            Assert.AreEqual(user.Email, data.Email);
            Assert.AreEqual(patientProfile.Dob, data.Dob);
            Assert.AreEqual(address.AddressText, data.Address);
            Assert.AreEqual(int.Parse(patientProfile.Country), data.CountryId);
            Assert.AreEqual(user.TimeZoneId, data.TimeZoneId);
            Assert.AreEqual(patientProfile.MobilePhone, data.MobileNumberWithCountryCode);
            Assert.AreEqual(patientProfile.Gender, data.Gender);
        }

        [Test]
        public void Get_ShortDetail_PatientUser_NotFound_By_Email_Throws_Error()
        {
            const string email = "test@test.com";
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            InitializeController(identity);

            var error = Assert.Catch<HttpResponseException>(() => Controller.ShortDetail(email));
            Assert.AreEqual((int)HttpStatusCode.NotFound, (int)error.Response.StatusCode);
            Assert.AreEqual("Patient user not found.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        /*on board patient user with short detail*/
        [Test]
        [TestCaseSource("InvalidEmailTestCaseList")]
        public void Get_ShortDetail_ForInvalidEmail_Throws_Error(string email)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();
            InitializeController(identity);
            var error = Assert.Catch<HttpResponseException>(() => Controller.ShortDetail(email));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Valid email address required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase("M", "/images/Patient-Male.gif", "", "")]
        [TestCase("F", "/images/Patient-Female.gif", null, "")]
        [TestCase("F", "/images/Patient-Female.gif", "LastName", "LastName")]
        public void Post_ShortDetail_Success(string gender, string expectedDefaultImagePath, string lastName, string expectedLastName)
        {
            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Email = "user@test.com")
                .And(x => x.Gender = gender)
                .And(x => x.LastName = lastName)
                .Build();
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            var users = new List<User>();
            var userRoles = new List<UserRole>();
            var familyProfiles = new List<FamilyProfile>();
            var patientProfiles = new List<PatientProfile>();
            var addresses = new List<Address>();
            var patientAddresses = new List<PatientAddress>();

            MockedDb.Setup(x => x.Users).Returns(users.ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(userRoles.ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(familyProfiles.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(addresses.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(patientAddresses.ToDbSet());
            MockCountryAndTimeZone(detail);
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var utcNow = DateTime.UtcNow;
            Assert.DoesNotThrow(() => Controller.AddShortDetail(detail));

            /*user*/
            var user = users.Single();
            Assert.AreEqual((int)UserTypeEnum.Customer, user.UserTypeId);
            Assert.AreEqual(detail.Email, user.Email);
            Assert.AreEqual(null, user.Password);
            Assert.AreEqual(expectedDefaultImagePath, user.ProfileImage);
            Assert.AreEqual(detail.TimeZoneId, user.TimeZoneId);
            Assert.AreEqual("A", user.IsActive);
            Assert.AreEqual(identity.ProviderId, user.HospitalId);
            Assert.LessOrEqual(utcNow, user.CreateDate);
            Assert.AreEqual(identity.UserId, user.CreatedBy);

            /*user role*/
            var userRole = userRoles.Single();
            Assert.AreEqual(user.UserId, userRole.UserId);
            Assert.AreEqual(1, userRole.RoleId);
            Assert.AreEqual("A", userRole.IsActive);
            Assert.LessOrEqual(utcNow, userRole.CreateDate);
            Assert.AreEqual(identity.UserId, userRole.CreatedBy);

            /*family profile*/
            var familyProfile = familyProfiles.Single();
            Assert.LessOrEqual(utcNow, familyProfile.CreateDate);
            Assert.AreEqual(identity.UserId, familyProfile.CreatedBy);

            /*patient profile*/
            var patientProfile = patientProfiles.Single();
            Assert.AreEqual(detail.CountryId.ToString(), patientProfile.Country);
            Assert.AreEqual(familyProfile.FamilyGroupId, patientProfile.FamilyGroupId);
            Assert.AreEqual(detail.Dob, patientProfile.Dob);
            Assert.AreEqual(detail.FirstName, patientProfile.PatientName);
            Assert.AreEqual(expectedLastName, patientProfile.LastName);
            Assert.AreEqual(detail.Gender, patientProfile.Gender);
            Assert.AreEqual(expectedDefaultImagePath, patientProfile.ProfileImagePath);
            Assert.AreEqual(detail.MobileNumberWithCountryCode, patientProfile.MobilePhone);
            Assert.AreEqual("N", patientProfile.IsChild);
            Assert.AreEqual("N", patientProfile.IsDependent);
            Assert.AreEqual("A", patientProfile.IsActive);
            Assert.AreEqual(identity.ProviderId, patientProfile.HospitalId);
            Assert.LessOrEqual(utcNow, patientProfile.CreateDate);
            Assert.AreEqual(identity.UserId, patientProfile.CreatedBy);

            /*address*/
            var address = addresses.Single();
            Assert.AreEqual(detail.Address, address.AddressText);
            Assert.AreEqual(true, address.IsActive);
            Assert.LessOrEqual(utcNow, address.CreateDate);
            Assert.AreEqual(identity.UserId, address.CreatorUserId);

            /*patientAddress*/
            var patientAddress = patientAddresses.Single();
            Assert.AreEqual(patientProfile.PatientId, patientAddress.PatientProfileId);
            Assert.AreEqual(address.AddressId, patientAddress.AddressId);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void Post_ShortDetail_FirstName_NullOrEmpty_Throw_Error(string firstName)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .And(x => x.FirstName = firstName)
                .Build();
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("First name required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCaseSource("InvalidEmailTestCaseList")]
        public void Post_ShortDetail_Email_NullOrEmpty_Throw_Error(string email)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .And(x => x.Email = email)
                .Build();
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Valid email address required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(UserTypeEnum.Customer, "A")]
        [TestCase(UserTypeEnum.Customer, "I")]
        [TestCase(UserTypeEnum.Customer, "")]
        [TestCase(UserTypeEnum.Customer, null)]
        public void Post_ShortDetail_CustomerUse_Exists_Throw_Error(UserTypeEnum userType, string isActive)
        {
            const string email = "user@test.com";
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();
            User activePatientUser = Builder<User>.CreateNew()
                .With(x => x.HospitalId = identity.ProviderId)
                .And(x => x.UserTypeId = (int)userType)
                .And(x => x.Email = email)
                .And(x => x.IsActive = isActive)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { activePatientUser }.ToDbSet());
            InitializeController(identity);

            var shortDetail = Builder<PatientOnBoardShortDetail>.CreateNew().With(x => x.Email = email).Build();
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(shortDetail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual(String.Format("Patient already exists with email {0}.", email), error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        public void Post_ShortDetail_Dob_Null_Throw_Error()
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Email = "test@test.com")
                .And(x => x.Gender = "M")
                .And(x => x.Dob = null)
                .Build();
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Date of birth required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void Post_ShortDetail_Address_NullOrEmpty_Throw_Error(string address)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Email = "test@test.com")
                .And(x => x.Address = address)
                .Build();
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Address required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void Post_ShortDetail_MobileNumber_NullOrEmpty_Throw_Error(string mobileNumber)
        {
            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Email = "test@test.com")
                .And(x => x.Gender = "M")
                .And(x => x.MobileNumberWithCountryCode = mobileNumber)
                .Build();
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockCountryAndTimeZone(detail);
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("mobile number required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        [TestCase("m")]
        [TestCase("f")]
        public void Post_ShortDetail_Gender_Unknown_Throw_Error(string gender)
        {
            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Email = "test@test.com")
                .And(x => x.Gender = gender)
                .Build();
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockCountryAndTimeZone(detail);
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Unknown gender. Expected gender any [M, F]", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(null)]
        [TestCase(100)]
        public void Post_ShortDetail_CountryId_NullOrUnknown_Throw_Error(int? countryId)
        {
            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Gender = "M")
                .And(x => x.Email = "test@test.com")
                .Build();
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockCountryAndTimeZone(detail);
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            detail.CountryId = countryId;
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Unknown countryId.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(null)]
        [TestCase(100)]
        public void Post_ShortDetail_TimeZoneId_NullOrUnknown_Throw_Error(int? timeZoneId)
        {
            var detail = Builder<PatientOnBoardShortDetail>.CreateNew()
                .With(x => x.Gender = "M")
                .And(x => x.Email = "test@test.com")
                .Build();
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockCountryAndTimeZone(detail);
            SetOnBoardEmailSettings(identity, false);
            InitializeController(identity);

            detail.TimeZoneId = timeZoneId;
            var error = Assert.Catch<HttpResponseException>(() => Controller.AddShortDetail(detail));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Unknown timezoneId.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        /*find patients onboarded detail with email tokens*/
        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void Get_PatientDetail_Success(string userStatus)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();

            var user = Builder<User>.CreateNew()
                .With(x => x.HospitalId = identity.ProviderId)
                .And(x => x.Email = "test@test.com")
                .And(x => x.UserTypeId = (int)UserTypeEnum.Customer)
                .And(x => x.IsActive = userStatus)
                .Build();
            var userRole = Builder<UserRole>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .Build();
            var familyProfile = Builder<FamilyProfile>.CreateNew()
                .Build();
            var patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.FamilyGroupId = familyProfile.FamilyGroupId)
                .Build();
            var address = Builder<Address>.CreateNew()
                .With(x => x.IsActive = true)
                .Build();
            var patientAddress = Builder<PatientAddress>.CreateNew()
                .With(x => x.PatientProfileId = patientProfile.PatientId)
                .And(x => x.AddressId = address.AddressId)
                .Build();
            var activeToken = Builder<UserToken>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "A")
                .Build();
            var oldToken = Builder<UserToken>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.CodeSetId = (int)CodeSetEnum.NewUserOnboardToken)
                .And(x => x.TokenStatus = "E")
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>() { userRole }.ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>() { familyProfile }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>() { address }.ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>() { patientAddress }.ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>() { oldToken, activeToken }.ToDbSet());
            InitializeController(identity);

            var result = Controller.GetDetail(user.Email);
            Assert.IsInstanceOf<ApiResponse<PatientOnBoardDetail>>(result);
            var data = result.Data;
            Assert.AreEqual(user.UserId, data.User.UserId);
            Assert.AreEqual("", data.User.Password);    /*impt: not to show password*/
            Assert.AreEqual(patientProfile.PatientId, data.PatientProfile.PatientId);
            Assert.AreEqual(1, data.Addresses.Count);
            Assert.AreEqual(2, data.SendEmailTokens.Count);
            Assert.AreEqual(1, data.ActiveEmailTokens.Count);
        }

        [Test]
        public void Get_PatientDetail_PatientUser_NotFound_By_Email_()
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();
            MockedDb.Setup(x => x.Users).Returns(new List<User>().ToDbSet());
            MockedDb.Setup(x => x.UserRoles).Returns(new List<UserRole>().ToDbSet());
            MockedDb.Setup(x => x.FamilyProfiles).Returns(new List<FamilyProfile>().ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>().ToDbSet());
            MockedDb.Setup(x => x.Addresses).Returns(new List<Address>().ToDbSet());
            MockedDb.Setup(x => x.PatientAddresses).Returns(new List<PatientAddress>().ToDbSet());
            MockedDb.Setup(x => x.UserTokens).Returns(new List<UserToken>().ToDbSet());
            InitializeController(identity);

            var error = Assert.Catch<HttpResponseException>(() => Controller.GetDetail("test@gmail.com"));
            Assert.AreEqual((int)HttpStatusCode.NotFound, (int)error.Response.StatusCode);
            Assert.AreEqual("Patient user not found.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCaseSource("InvalidEmailTestCaseList")]
        public void Get_PatientDetail_ForInvalidEmail_Throws_Error(string email)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();
            InitializeController(identity);
            var error = Assert.Catch<HttpResponseException>(() => Controller.GetDetail(email));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Valid email address required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        /*send onboard email to onboard user*/
        [Test]
        [TestCaseSource("InvalidEmailTestCaseList")]
        public void Send_Email_ForInvalidEmail_Throws_Error(string email)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew().Build();
            InitializeController(identity);
            var error = Assert.Catch<HttpResponseException>(() => Controller.Email(email));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Valid email address required.", error.Response.Content.ReadAsAsync<string>().Result);
        }
    }
}
