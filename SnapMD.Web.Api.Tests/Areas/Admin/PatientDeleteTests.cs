﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Admin.Controllers;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class PatientDeleteTests
    {
        [Test]
        public void DeletePatientThrows404()
        {
            Mock<ISnapAuthenticationManager> authenticationManager;
            var context = Setup(out authenticationManager);

            using (var con = new PatientsAccountsController(authenticationManager.Object, context.Object))
            {
                var result = con.Delete(25);
                Assert.IsTrue(result is NotFoundResult);
            }
        }

        [Test]
        public void DeletePatient()
        {
            Mock<ISnapAuthenticationManager> authenticationManager;
            var context = Setup(out authenticationManager);

            using (var con = new PatientsAccountsController(authenticationManager.Object, context.Object))
            {
                var result = con.Delete(5);
                Assert.IsTrue(result is StatusCodeResult && ((StatusCodeResult)result).StatusCode == HttpStatusCode.NoContent);
            }
        }

        [Test]
        public void DeletePatientWithNoUser()
        {
            Mock<ISnapAuthenticationManager> authenticationManager;
            var context = Setup(out authenticationManager);
            context.SetupGet(m => m.Users).Returns(new List<User>().ToDbSet());
            
            using (var con = new PatientsAccountsController(authenticationManager.Object, context.Object))
            {
                var result = con.Delete(5);
                Assert.IsTrue(result is StatusCodeResult && ((StatusCodeResult)result).StatusCode == HttpStatusCode.NoContent);
            }
        }

        private static Mock<ISnapContext> Setup(out Mock<ISnapAuthenticationManager> authenticationManager)
        {
            var context = new Mock<ISnapContext>();
            var users = Builder<User>.CreateListOfSize(20).Build();
            var patients = Builder<PatientProfile>.CreateListOfSize(20).Build();
            context.SetupGet(m => m.Users).Returns(users.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(patients.ToDbSet());
            authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>()))
                .Returns(new UserLogin { HospitalId = 25, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            return context;
        }
    }
}
