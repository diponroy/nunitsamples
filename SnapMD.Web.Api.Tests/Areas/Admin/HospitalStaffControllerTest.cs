﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Admin.Models;
using SnapMD.Utilities;
using Newtonsoft.Json.Linq;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Interfaces;
using SnapMD.Web.Api.Areas.Admin.Models;
using SnapMD.Web.Api.Utilities;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class HospitalStaffControllerTest
    {
        private const int HospitalId = 1;
        private const int UserId = 1;
        private const string DoctorName = "Dr. Kiki";

        private readonly ISnapAuthenticationManager _hospitalStaffAuthenticationManager;
        private readonly List<Tag> _tags;
        private readonly List<Role> _roles;
        private List<HospitalStaffProfile> StaffProfiles;

        private ISnapContext _mockContext;

        public HospitalStaffControllerTest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            _hospitalStaffAuthenticationManager = authenticationManager.Object;

            _tags = new List<Tag>()
            {
                new Tag(){HospitalId = HospitalId, IsActive = "A", Description = "First Tag"}
            };

            _roles = new List<Role>()
            {
                new Role(){HospitalId = HospitalId, IsActive = "A", RoleId = 11, Description = "Admin"},
                new Role(){HospitalId = HospitalId, IsActive = "A", RoleId = 12, Description = "Doctor"},
            };

            var mockContext = new Mock<ISnapContext>();
            CreateMokeHospitalStaff(mockContext);
            CreateMokeDept(mockContext);
            CreateMokeSpeciality(mockContext);
            CreateMokeUsers(mockContext);
            CreateMokeRoles(mockContext);
            CreateMokeUserRoles(mockContext);
            _mockContext = mockContext.Object;
            _mockContext.SaveChanges();
        }

        private void CreateMokeUsers(Mock<ISnapContext> mockContext)
        {
            var repo = new MockDbSet<User>();
            mockContext.SetupGet(c => c.Users).Returns(repo);
            repo.Add(new User()
            {
                UserId = 1,
                HospitalId=1,
                IsActive = "A"
            });
            repo.Add(new User()
            {
                UserId = 2,
                HospitalId = 1,
                IsActive = "A"
            });
        }

        private void CreateMokeUserRoles(Mock<ISnapContext> mockContext)
        {
            var repo = new MockDbSet<UserRole>();
            mockContext.SetupGet(c => c.UserRoles).Returns(repo);
            repo.Add(new UserRole()
            {
                UserRoleId=1,
                RoleId=1,
                UserId=1,
                IsActive = "A"
            });
            repo.Add(new UserRole()
            {
                UserRoleId = 2,
                RoleId = 1,
                UserId = 1,
                IsActive = "A"
            });
            repo.Add(new UserRole()
            {
                UserRoleId = 3,
                RoleId = 1,
                UserId = 1,
                IsActive = "A"
            });
        }

        private void CreateMokeRoles(Mock<ISnapContext> mockContext)
        {
            var repo = new MockDbSet<Role>();
            mockContext.SetupGet(c => c.Roles).Returns(repo);
            repo.Add(new Role()
            {
                RoleId = 1,
                RoleCode="HADM",
                HospitalId=1,
                IsActive = "A"
            });
            repo.Add(new Role()
            {
                RoleId = 2,
                RoleCode = "HADM",
                HospitalId = 2,
                IsActive = "A"
            });
        }

        private void CreateMokeSpeciality(Mock<ISnapContext> mockContext)
        {
            var specRepo = new MockDbSet<DoctorSpeciality>();
            mockContext.SetupGet(c => c.DoctorSpecialities).Returns(specRepo);
            specRepo.Add(new DoctorSpeciality()
            {
                SpecialityId = 1,
                Description = "Speciality 1",
                IsActive = "A",
                HospitalId = 1
            });
            specRepo.Add(new DoctorSpeciality()
            {
                SpecialityId = 2,
                Description = "Speciality 2",
                IsActive = "A",
                HospitalId = 1
            });
            specRepo.Add(new DoctorSpeciality()
            {
                SpecialityId = 3,
                Description = "Speciality 3",
                IsActive = "I",
                HospitalId = 1
            });
            specRepo.Add(new DoctorSpeciality()
            {
                SpecialityId = 4,
                Description = "Speciality 4",
                IsActive = "A",
                HospitalId = 2
            });

        }
        private void CreateMokeHospitalStaff(Mock<ISnapContext> mockContext)
        {
            var hospitalStaffRepo = new MockDbSet<HospitalStaffProfile>();
            mockContext.SetupGet(c => c.HospitalStaffProfiles).Returns(hospitalStaffRepo);

            StaffProfiles = new List<HospitalStaffProfile>()
            {
                new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "A"
                    },
                    StaffId = 1,
                    Name = DoctorName,
                    UserId=1
                },
                 new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "I" //Not Active User
                    },
                    StaffId = 2,
                    Name = DoctorName,
                    UserId=2
                },
                new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "I", //Not Active 
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "I"
                    },
                    StaffId = 3,
                    Name = DoctorName,
                    UserId=3
                },
                new HospitalStaffProfile()
                {
                    HospitalId = 99, //Another Hospital
                    IsActive = "A", 
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>()
                    },
                    StaffId = 4,
                    Name = DoctorName,
                    UserId=4
                }
            };
            foreach (var profile in StaffProfiles)
                hospitalStaffRepo.Add(profile);
        }
        private void CreateMokeDept(Mock<ISnapContext> mockContext)
        {
            var deptRepo = new MockDbSet<Department>();
            mockContext.SetupGet(c => c.Departments).Returns(deptRepo);
            deptRepo.Add(new Department()
            {
                DepartmentId = 1,
                Description = "Dept 1",
                IsActive = "A",
                HospitalId = 1
            });
            deptRepo.Add(new Department()
            {
                DepartmentId = 2,
                Description = "Dept 2",
                IsActive = "A",
                HospitalId = 2
            });
            deptRepo.Add(new Department()
            {
                DepartmentId = 3,
                Description = "Dept 3",
                IsActive = "A",
                HospitalId = 1
            });
            deptRepo.Add(new Department()
            {
                DepartmentId = 4,
                Description = "Dept 4",
                IsActive = "I",
                HospitalId = 1
            });
        }

        [TestCase(UserTypeEnum.Customer)]
        public void CreatePendingStaffProfile_CreatePendingStaffAccountByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, new Mock<ISnapContext>().Object, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.CreatePendingStaffProfile(new StaffAccountModel()); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [TestCase(UserTypeEnum.Customer)]
        public void Updatependingstaffprofile_UpdatePendingStaffAccountByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, new Mock<ISnapContext>().Object, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.UpdatePendingStaffProfile(0, new StaffAccountModel()); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [TestCase(UserTypeEnum.Customer)]
        public void UpdateStaffProfile_UpdateStaffAccountByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, new Mock<ISnapContext>().Object, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.UpdateStaffProfile(0, new StaffProfileDTO()); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        //This unit test send email when new user cretated, that is why it marked us explicit.
        [Test, Explicit]
        public void CreatePendingStaffProfile_NewPendingStaffProfile_ProfileCreated()
        {
            var context = new Mock<ISnapContext>();
            HttpContextUtility.MockCurrent("",
               "http://snap.local/api/v2/clinicians/pendingstaffprofile", "");

            context.Setup(x => x.ConfigMasters).Returns((new List<ConfigMaster>()
            {
                new ConfigMaster()
                {
                    ConfigCode = "SystemMailAddress",
                    ConfigValue = "noreply@webservice.snap-stage.com",
                },
                new ConfigMaster()
                {
                    ConfigCode = "SystemMailName",
                    ConfigValue = "Snap Staging Website",
                }
            }).ToDbSet());

            context.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>().ToDbSet());


            var CoUsersTempList = new List<CoUsersTemp>();

            context.SetupGet(x => x.CoUsersTemps).Returns(CoUsersTempList.ToDbSet());
            context.SetupGet(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>().ToDbSet());
            context.SetupGet(x => x.UserTagMapTemps).Returns(new List<UserTagMapTemp>().ToDbSet());
            context.SetupGet(x => x.UserRolesTemps).Returns(new List<UserRolesTemp>().ToDbSet());
            context.Setup(x => x.Departments).Returns(new List<Department>().ToDbSet());
            context.Setup(x => x.DoctorSpecialities).Returns(new List<DoctorSpeciality>().ToDbSet());
            context.Setup(x => x.Hospitals).Returns((new List<Hospital>()
            {
                new Hospital()
                {
                    Addresses = new List<Address>(),
                    HospitalId = 1,

                    HospitalName = "HospitalName",
                    BrandName = "BrandName",
                    BrandColor = "BrandColor",
                    BrandTitle = "BrandTitle",
                    ContactNumber = "ContactNumber",
                    ITDeptContactNumber = "ITDeptContactNumber",
                    HospitalImage = "HospitalImage",
                    HospitalDomainName = "HospitalDomainName",
                    AppointmentsContactNumber = "AppointmentsContactNumber",
                    OperatingHours = new List<OperatingHours>(),

                    State = "State",
                    City = "City"
                }
            }).ToDbSet());

            context.Setup(x => x.SaveChanges()).Callback(() =>
            {
                CoUsersTempList.ForEach(x => x.CoUserId = 1);
            });

            context.Setup(x => x.Roles).Returns(_roles.ToDbSet());

            //Only one tag exist
            context.Setup(x => x.Tags).Returns(
                new List<Tag>()
                {
                    new Tag {HospitalId = HospitalId, IsActive = "A", Description = "First Tag", TagId = 11}
                }.ToDbSet());

            HospitalStaffController controller = new HospitalStaffController(_hospitalStaffAuthenticationManager, context.Object, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            var accountModel = new StaffAccountModel()
            {
                UserDetails = new UserDetails()
                {
                    FirstName = "Billie",
                    LastName = "Jean",
                    Email = "TestEmail@test.test",
                    CellPhoneNumber = "940-223-36",
                    PhoneNumber = "862-78-72",
                    ProfileImage = "C:\\MyPhoto.jpg",
                    TimeZoneId = 23,
                    ReceiveTextAlerts = true,
                },

                Roles = "11,12",
                Tags = "First Tag, Second Tag",

                DoctorProfileDetails = new DoctorProfileDetails()
                {
                    DOB = new DateTime(1988, 11, 11),
                    Gender = "M",
                    Internship = "Internship",
                    YearOfStateRegistration = "2011",
                    BusinessAddress = "La Caver st 9",
                    Department = "Surgery",
                    MedicalLicense = "LC #123-345",
                    MedicalSpeciality = "Chin bone sergery",
                    PreMedicalEducation = "Midle G.G. Abram shcool",
                    MedicalSubSpeciality = "Deep sergery",
                    PracticingSinceYear = 5,
                    StatesLicensed = "LC #452-MQ-692",
                    HomeAddress = "LA 136000 Saint Poly st. Flat 182",
                    MedicineSchool = "First surgery school in Glazgo",
                    
                }
            };

            controller.CreatePendingStaffProfile(accountModel);

            //Assert that profile created
            Assert.That(context.Object.HospitalStaffProfileTemps.Count() == 1);

            var createdProfile = context.Object.HospitalStaffProfileTemps.First();

            Assert.That(createdProfile.Name == accountModel.UserDetails.FirstName);
            Assert.That(createdProfile.LastName == accountModel.UserDetails.LastName);
            Assert.That(createdProfile.Gender == accountModel.DoctorProfileDetails.Gender);
            Assert.That(createdProfile.BusinessAddress == accountModel.DoctorProfileDetails.BusinessAddress);
            Assert.That(createdProfile.IsActive == "A");
            Assert.That(createdProfile.MedicalLicence == accountModel.DoctorProfileDetails.MedicalLicense);
            Assert.That(createdProfile.MedicalSpeciality.Description == accountModel.DoctorProfileDetails.MedicalSpeciality);
            Assert.That(createdProfile.SubSpeciality.Description == accountModel.DoctorProfileDetails.MedicalSubSpeciality);

            //Assert that temp user created
            Assert.That(context.Object.CoUsersTemps.Count() == 1);

            //One tag already existed, second was added during profile creation process.
            Assert.That(context.Object.Tags.Count() == 2);

            //Check that tags mapping were added
            var tempUser = context.Object.CoUsersTemps.First();

            Assert.That(tempUser.UserTagMapTemps.Count == 2);

            //Check that roles mapping were added
            Assert.That(tempUser.UserRolesTemps.Count == 2);

            //Check that medSpeciality and SubSpeciality were added.
            Assert.That(context.Object.DoctorSpecialities.Count() == 2);
        }

        [Test]
        public void DELETE_StaffProfile_ByNotAdminUser_ReturnUnauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Delete(1); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void DELETE_Try_To_Delete_LoggedIn_Hospital_Staff_ReturnBadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Delete(UserId); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void DELETE_Try_To_Delete_Different_Hospital_Staff_ReturnBadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Delete(4); });//hospitalId=99

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void GetDept_ByHospitalId_Return_Data()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            var result = controller.GetDept(1, "");

            // Assert
            Assert.That(result.Data.Count() == 2);
        }

        [Test]
        public void GetDept_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetDept(1, ""); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GetSpeciality_ByHospitalId_Return_Data()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            var result = controller.GetSpeciality(1, "");

            // Assert
            Assert.That(result.Data.Count() == 2);
        }

        [Test]
        public void GetSpeciality_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            //Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetSpeciality(1, ""); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GetStaffActiveRoles_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetStaffActiveRoles(UserId, 1); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GetStaffActiveRoles_Try_Different_Hospital_Roles_Return_BadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetStaffActiveRoles(UserId, 2); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void GetValidateCliniciansLimit_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetValidateCliniciansLimit(UserId, 1, "1,2,3,4"); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GetValidateCliniciansLimit_NotAdminUser_Return_BadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.GetValidateCliniciansLimit(UserId, 2, "1,2,3,4"); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void CheckRoleFunctions_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.CheckRoleFunctions((int)eRoleFunctions.Create_Staff_Accounts, "add"); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void CheckRoleFunctions_No_Add_Edit_Role_Function_Return_BadRequest()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.HospitalStaff });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.CheckRoleFunctions(1, "add"); });//expected id: 12 or 15

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void CheckHasPatientInteraction_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.CheckHasPatientInteraction("1,2,3,4,5,6,7,8,9,10"); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void ValidateUserPermission_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.ValidateUserPermission(); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }


        [Test]
        public void ApplyOrInsertTagToAdminUser_NotAdminUser_Return_Unauthorized()
        {
            // Arrange
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { UserId = UserId, HospitalId = 1, UserTypeId = (int)UserTypeEnum.Customer });
            HospitalStaffController controller = new HospitalStaffController(authenticationManager.Object, _mockContext, new Mock<IAdminRolesRepository>().Object, new Mock<IUserProfileRepository>().Object, new Mock<IHospitalStaffRepository>().Object);
            
            JObject obj = new JObject(
                new JProperty("staffId", 1),
                new JProperty("hospitalId", 1),
                new JProperty("insertText", "Test Tags")
            );

            // Act
            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.ApplyOrInsertTagToAdminUser(1, obj); });

            // Assert
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }
    }
}
