﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Admin.Models;
using SnapMD.Web.Api.Areas.Patients.Models;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class PatientsAccountsControllerTest
    {
        private const int HospitalId = 1;
        private DateTime currentDate = DateTime.Now;
        private DateTime futureDate = DateTime.Now.AddYears(1);

        private int _inactivePatientId = 10;
        private int _currentPatientId = 11;
        private int _futurePatientId = 12;

        private string _currentPatienName = "Max";

        private readonly List<PatientProfile> _patientProfiles;
        public PatientsAccountsControllerTest()
        {
            _patientProfiles = CreatePatientList();
        }

        [TestCase(UserTypeEnum.Customer)]
        public void GET_GetStaffAccountsByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Get(1, 10); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GET_AllPatients_ReturnAllActivePatients()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.All);

            Assert.That(data.Total == 2);

            Assert.That(data.Data.Any(x => x.PatientId == _currentPatientId));
            Assert.That(data.Data.Any(x => x.PatientId == _futurePatientId));
        }

        [Test] 
        public void GET_AllPatientsWithNameFilter_ReturnPatientsWithMatchedName()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.All, _currentPatienName);

            Assert.That(data.Total == 1);

            Assert.That(data.Data.First().FullName.Contains(_currentPatienName));
        }

        [Test]
        public void GET_AllPatientsIncludeInactive_ReturnAllPatients()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.All, "", true);

            Assert.That(data.Total == 3);

            Assert.That(data.Data.Any(x => x.PatientId == _currentPatientId));
            Assert.That(data.Data.Any(x => x.PatientId == _futurePatientId));
            Assert.That(data.Data.Any(x => x.PatientId == _inactivePatientId));
        }

        [Test]
        public void GET_AllPatientsWithCreateionDateFilter_ReturnPatientsWithMatchedCreationDate()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.All, "", false, true, currentDate.AddMonths(-1), currentDate.AddMonths(1));

            Assert.That(data.Total == 1);

            Assert.That(data.Data.First().PatientId == _currentPatientId);
        }

        [Test]
        public void GET_RecentConsultation_ReturnAllActivePatientsWithRecentConsultations()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.RecentVisits, "", false, true, currentDate.AddMonths(-1), currentDate.AddMonths(1));

            Assert.That(data.Total == 1);

            Assert.That(data.Data.First().PatientId == _currentPatientId);
        }

        [Test]
        public void GET_DroppedConsultation_ReturnAllActivePatientsWithDroppedConsultations()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(_patientProfiles.ToDbSet());

            var controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var data = controller.Get(10, 0, (int)ViewType.DroppedAppointments, "", false, true, futureDate.AddMonths(-1), futureDate.AddMonths(1));

            Assert.That(data.Total == 1);

            Assert.That(data.Data.First().PatientId == _futurePatientId);
        }


        /* PUT */

        [TestCase(UserTypeEnum.Customer)]
        public void PUT_GetStaffAccountsByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Get(); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }


        [Test]
        public void PUT_ActiveStaffAccounts_ReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            var patientProfiles = CreatePatientList();

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var result = controller.Put(new PatientsAccountsRequest()
            {
                data = new[]
                {
                    new PatientAccountInfo()
                    {
                        PatientId = _currentPatientId,
                        IsActive = true,
                    }
                }
            }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            Assert.That(patientProfiles.Single(x => x.PatientId == _currentPatientId).IsActive == "A");
        }

        [Test]
        public void PUT_InActiveStaffAccounts_ReturnNoContent()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            var patientProfiles = CreatePatientList();

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.PatientProfiles).Returns(patientProfiles.ToDbSet());

            PatientsAccountsController controller = new PatientsAccountsController(authenticationManager.Object, context.Object);

            var result = controller.Put(new PatientsAccountsRequest()
            {
                data = new[]
                {
                    new PatientAccountInfo()
                    {
                        PatientId = _currentPatientId,
                        IsActive = false,
                    }
                }
            }) as StatusCodeResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

            Assert.That(patientProfiles.Single(x => x.PatientId == _currentPatientId).IsActive == "I");
        }

        private List<PatientProfile> CreatePatientList()
        {
            return new List<PatientProfile>
            {
                new PatientProfile()
                {
                    IsActive = "I",
                    HospitalId = 99,
                    CreateDate = currentDate,
                    UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>()
                },

                new PatientProfile()
                {
                    IsActive = "A",
                    HospitalId = 99,
                    CreateDate = currentDate,
                    UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>()
                },

                new PatientProfile()
                {
                    PatientId = _inactivePatientId,
                    IsActive = "I",
                    HospitalId = HospitalId,
                    CreateDate = currentDate,
                    UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>()
                },

                new PatientProfile()
                {
                    PatientId = _currentPatientId,
                    PatientName = _currentPatienName,
                    IsActive = "A",
                    HospitalId = HospitalId,
                    CreateDate = currentDate,
                    UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>(),
                    Consultations = new List<Consultation>()
                    {
                        new Consultation()
                        {
                            CreateDate = currentDate,
                            DroppedConsultations = new List<DroppedConsultation>()
                        }
                    },
                },

                new PatientProfile()
                {
                    PatientId = _futurePatientId,
                    IsActive = "A",
                    HospitalId = HospitalId,
                    CreateDate = futureDate,
                    UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>(),
                    Consultations = new List<Consultation>()
                    {
                        new Consultation()
                        {
                            CreateDate = futureDate,
                            DroppedConsultations = new List<DroppedConsultation>()
                            {
                                new DroppedConsultation()
                                {
                                    CreateDate = futureDate
                                }
                            }
                        }
                    }
                },
            };
        }
    }
}
