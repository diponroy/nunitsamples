﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Admin.Controllers;
using SnapMD.Web.Api.Areas.Admin.Models;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    [TestFixture]
    public class StaffAccountsControllerTest
    {
        private const int HospitalId = 1;
        private const int ExistedInHospitalStaffId = 1;
        private const int ExistedInHospitaPendingStaffId = 1;

        private string DoctorName = "Natan Dort";

        private List<HospitalStaffProfile> StaffProfiles;
        private List<HospitalStaffProfileTemp> PendingProfiles;


        public StaffAccountsControllerTest()
        {
            StaffProfiles = new List<HospitalStaffProfile>()
            {
                new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "A"
                    },
                    StaffId = ExistedInHospitalStaffId,
                    Name = DoctorName,
                },

                 new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "I" //Not Active User
                    },
                    StaffId = ExistedInHospitalStaffId,
                    Name = DoctorName,
                },

                new HospitalStaffProfile()
                {
                    HospitalId = HospitalId,
                    IsActive = "I", //Not Active 
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>(),
                        IsActive = "I"
                    }
                },

                new HospitalStaffProfile()
                {
                    HospitalId = 99, //Another Hospital
                    IsActive = "A", 
                    User = new User()
                    {
                        HospitalId = HospitalId,
                        UserRoles = new List<UserRole>()
                    }
                }
            };

            PendingProfiles = new List<HospitalStaffProfileTemp>()
            {
                new HospitalStaffProfileTemp()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    CoUsersTemp = new CoUsersTemp()
                    {
                        CoUserTokens = new List<CoUserToken>() {new CoUserToken() { TokenStatus = "A"}},
                        UserRolesTemps =new List<UserRolesTemp>(),
                        IsActive = "A"
                    },
                    CoUserStaffId = ExistedInHospitaPendingStaffId,
                    Name = "Some Doctor"
                },

                new HospitalStaffProfileTemp()
                {
                    HospitalId = HospitalId,
                    IsActive = "A",
                    CoUsersTemp = new CoUsersTemp()
                    {
                        UserRolesTemps =new List<UserRolesTemp>(),
                        IsActive = "I" //Not active user
                    },
                    CoUserStaffId = ExistedInHospitaPendingStaffId,
                    Name = "Some Doctor"
                },

                new HospitalStaffProfileTemp()
                {
                    HospitalId = HospitalId,
                    IsActive = "I", //Not Active 
                    CoUsersTemp = new CoUsersTemp()
                    {
                        UserRolesTemps =new List<UserRolesTemp>()
                    }
                },

                 new HospitalStaffProfileTemp()
                {
                    HospitalId = 99, //Another Hospital
                    IsActive = "A",
                    CoUsersTemp = new CoUsersTemp()
                    {
                        UserRolesTemps =new List<UserRolesTemp>()
                    }
                }
            };
        }

        [TestCase(UserTypeEnum.Customer)]
        public void GET_GetStaffAccountsByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            StaffAccountsController controller = new StaffAccountsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            HttpResponseException ex = Assert.Throws<HttpResponseException>(delegate { controller.Get(1, 10); });

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void GET_GetStaffAccountsByAdmin_ReturnAccountsWhichBelongToTheNetwork()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.HospitalStaffProfiles).Returns(StaffProfiles.ToDbSet());
            context.SetupGet(x => x.HospitalStaffProfileTemps).Returns(PendingProfiles.ToDbSet());


            StaffAccountsController controller = new StaffAccountsController(authenticationManager.Object, context.Object);

            var result = controller.Get(2, 0);

            Assert.IsNotNull(result);

            //Check that only one valid role were selected.
            Assert.That(result.Data.Count(), Is.EqualTo(2));

            var activatedAccount = result.Data.ElementAt(0);
            var pendingAccount = result.Data.ElementAt(1);

            Assert.That(activatedAccount.staffId, Is.EqualTo(ExistedInHospitalStaffId));
            Assert.That(pendingAccount.staffId, Is.EqualTo(ExistedInHospitaPendingStaffId));
        }


        [Test]
        public void GET_StaffAccountsWithSpecificName_ReturnAccountsFilteredByName()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.HospitalStaff }); //User without admin permissions

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.HospitalStaffProfiles).Returns(StaffProfiles.ToDbSet());
            context.SetupGet(x => x.HospitalStaffProfileTemps).Returns(PendingProfiles.ToDbSet());


            StaffAccountsController controller = new StaffAccountsController(authenticationManager.Object, context.Object);

            var result = controller.Get(1, 0, -1, -1, DoctorName);

            Assert.IsNotNull(result);

            //Check that only one valid role were selected.

            Assert.That(result.Data.Count(), Is.EqualTo(1));

            var staffAccount = result.Data.First();

            Assert.That(staffAccount.staffId, Is.EqualTo(ExistedInHospitalStaffId));
            Assert.True(staffAccount.fullName.Contains(DoctorName));
        }

        [Test]
        public void GetUserStaffProfile()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(0));

            var user = new User { TimeZoneId = 1, UserId = 0 };
            var dbSet = Builder<HospitalStaffProfile>.CreateListOfSize(1)
                .All().With(c => c.User = user).With(c => c.UserId = 0).Build().ToDbSet();
            var dbSet2 = Builder<StandardTimeZone>.CreateListOfSize(3).Build().ToDbSet();
            var dbSet3 = Builder<User>.CreateListOfSize(1).All().With(c => user).Build().ToDbSet();

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(db => db.HospitalStaffProfiles).Returns(dbSet);
            mock.SetupGet(db => db.StandardTimeZones).Returns(dbSet2);
            mock.SetupGet(db => db.Users).Returns(dbSet3);

            var target = new StaffAccountsController(authenticationManager.Object, mock.Object);
            var result = target.GetUserStaffProfile().Data.First();

            Assert.AreEqual(result.TimeZoneId, 1);
            Assert.AreEqual(result.FullName, "Name1 LastName1");
            Assert.AreEqual(result.TimeZone, "UTC");

        }

        /* Delete */

        [TestCase(UserTypeEnum.Customer)]
        public void DELETE_StaffAccountsByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            StaffAccountsController controller = new StaffAccountsController(authenticationManager.Object, new Mock<ISnapContext>().Object);

            var result = controller.Delete(new StaffAccountsRequest()) as UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

    }
}
