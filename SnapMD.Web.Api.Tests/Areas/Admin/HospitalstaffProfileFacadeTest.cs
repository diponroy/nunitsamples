﻿using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;
using SnapMD.Web.Api.Admin.Models;
using SnapMD.Web.Api.Areas.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapMD.Web.Api.Tests.Areas.Admin
{
    class HospitalstaffProfileFacadeTest
    {
        private ISnapContext MockContext()
        {
            var mock = new Mock<ISnapContext>();

            var codes = new List<TelephoneCountryCode>{
                new TelephoneCountryCode{
                     CountryCode = "+1",
                     CountryName = "United States",
                     TelephoneCountryID = 1
                }
            };

            var users = new List<User>{
                new User{
                    UserId = 1,
                    TimeZoneId = 1,
                    Email = "test@test.com",
                    IsActive = "A"
                },
                new User{
                    UserId = 2,
                    TimeZoneId = 1,
                    Email = "test@test.com",
                     IsActive = "A"
                },
                new User{
                    UserId = 3,
                    TimeZoneId = 1,
                    Email = "test@test.com",
                     IsActive = "A"
                }
            };

            var profiles = new List<HospitalStaffProfile>()
            {
                new HospitalStaffProfile{
                 Name = "test",
                 LastName = "test",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 ProfileImagePath = "test.jpeg",
                 UserId = 1,
                 StaffId = 1,
                 Dob = new DateTime(1990,11,12),
                 TextAlerts = "test",
                 Gender = "M",
                 MedicalSpeciality = new DoctorSpeciality{
                      SpecialityId = 1,
                      Description = "test"
                 },
                 SubSpeciality = new DoctorSpeciality{
                      SpecialityId = 1,
                      Description = "test"
                },
                User = users[0],
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc ="test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                 DepartmentId = 1
                },
                new HospitalStaffProfile{
                 StaffId = 2,
                 Name = "test",
                 LastName = "test",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 ProfileImagePath = "test.jpeg",
                 UserId = 2,
                 Dob = new DateTime(1990,11,12),
                 TextAlerts = "test",
                 Gender = "M",
                 MedicalSpeciality = null,
                 SubSpeciality = null,
                 User = users[1],
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc ="test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                 DepartmentId = 1
                },
                 new HospitalStaffProfile{
                 StaffId = 3,
                 Name = "test",
                 LastName = "test",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 ProfileImagePath = "test.jpeg",
                 UserId = 3,
                 Dob = new DateTime(1990,11,12),
                 TextAlerts = "test",
                 Gender = "M",
                 MedicalSpeciality = null,
                 SubSpeciality = null,
                 User = users[2],
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc ="test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                }
            };
            var settings = new List<DoctorSetting>{
                new DoctorSetting{
                    Key =  DoctorSettingKeys.RxntUsernameKey,
                    DoctorId = 1,
                    SettingId = 1,
                    User = users[0],
                    Value = "test"
                },
                new DoctorSetting{
                    Key =  DoctorSettingKeys.RxntPasswordKey,
                    DoctorId = 1,
                    SettingId = 2,
                    User = users[0],
                    Value = "test"
                },
                 new DoctorSetting{
                    Key =  DoctorSettingKeys.RxntUsernameKey,
                    DoctorId = 2,
                    SettingId = 3,
                    User = users[0],
                    Value = "test"
                },
                new DoctorSetting{
                    Key =  DoctorSettingKeys.RxntPasswordKey,
                    DoctorId = 2,
                    SettingId = 4,
                    User = users[0],
                    Value = "test"
                },
            };
            var departments = new List<Department>{
                new Department{
                     DepartmentId = 1,
                     Description = "test",
                     HospitalId = 1,
                     IsActive = "A"
                }
            };
            var roles = new List<Role>{
                 new Role{
                      HospitalId = 1,
                       IsActive = "A",
                        RoleCode = "HADM",
                        RoleId = 1
                 }
            };

            var userRoles = new List<UserRole>{
                new UserRole{
                    IsActive = "A",
                    Role = roles[0],
                    RoleId = roles[0].RoleId,
                    User = users[0],
                    UserId = users[0].UserId
                }
            };

            mock.SetupGet( x=> x.Users).Returns( users.ToDbSet());
            mock.SetupGet( x => x.Roles).Returns( roles.ToDbSet());
            mock.SetupGet( x => x.UserRoles).Returns( userRoles.ToDbSet());
            mock.SetupGet( x => x.TelephoneCountryCodes).Returns(codes.ToDbSet());
            mock.SetupGet( x => x.HospitalStaffProfiles).Returns(profiles.ToDbSet());
            mock.SetupGet( x => x.DoctorSettings).Returns(settings.ToDbSet());
            mock.SetupGet( x=> x.Departments).Returns( departments.ToDbSet());
            return mock.Object;
        }

        private List<HospitalStaffProfileDTO> GetExpected()
        {
            return new List<HospitalStaffProfileDTO>{
                new HospitalStaffProfileDTO{
                 Name = "test",
                 LastName = "test",
                 EmailID = "test@test.com",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 PhoneCountryCode = "+1",
                 MobileCountryCode = "+1",
                 ProfileImagePath = "test.jpeg",
                 UserId = 1,
                 Password = "",
                 Dob = new DateTime(1990,11,12),
                 AgeString = SnapDate.GetAgeString(new DateTime(1990,11,12), DateTime.UtcNow),
                 TextAlerts ="test",
                 Gender = "M",
                 MedicalSpecialityId = 1,
                 MedicalSpecialityDescription = "test",
                 SubSpecialityId =1,
                 SubSpecialityDescription ="test",
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc = "test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                 DepartmentId = 1,
                 Department = "test",
                 TimeZoneId = 1,
                 RxntUserName = "test",
                 RxntPassword = "test",
                 Roles = "",
                 Tags = "",
                 PrimaryAdmin = false
                },
                 new HospitalStaffProfileDTO{
                 Name = "test",
                 LastName = "test",
                 EmailID = "test@test.com",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 PhoneCountryCode = "+1",
                 MobileCountryCode = "+1",
                 ProfileImagePath = "test.jpeg",
                 UserId = 2,
                 Password = "",
                 Dob = new DateTime(1990,11,12),
                 AgeString = SnapDate.GetAgeString(new DateTime(1990,11,12), DateTime.UtcNow),
                 TextAlerts ="test",
                 Gender = "M",
                 MedicalSpecialityId = 0,
                 MedicalSpecialityDescription = "",
                 SubSpecialityId = 0,
                 SubSpecialityDescription = "",
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc = "test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                 DepartmentId = 1,
                 Department = "test",
                 TimeZoneId = 1,
                 RxntUserName = "test",
                 RxntPassword = "test",
                 Roles = "",
                 Tags = "",
                 PrimaryAdmin = true
                },

                 new HospitalStaffProfileDTO{
                 Name = "test",
                 LastName = "test",
                 EmailID = "test@test.com",
                 HomePhone = "+1234567890",
                 MobilePhone = "+1234567890",
                 PhoneCountryCode = "+1",
                 MobileCountryCode = "+1",
                 ProfileImagePath = "test.jpeg",
                 UserId = 3,
                 Password = "",
                 Dob = new DateTime(1990,11,12),
                 AgeString = SnapDate.GetAgeString(new DateTime(1990,11,12), DateTime.UtcNow),
                 TextAlerts ="test",
                 Gender = "M",
                 MedicalSpecialityId = 0,
                 MedicalSpecialityDescription = "",
                 SubSpecialityId = 0,
                 SubSpecialityDescription = "",
                 MedicineSchool = "test",
                 MedicalLicence = "test",
                 StatesLicenced = "test",
                 Npinumber = 1,
                 YearsOfExperience = 5,
                 PreMedicalEduc = "test",
                 InternShip = "test",
                 HomeAddress = "test",
                 BusinessAddress = "test",
                 DepartmentId = null,
                 Department = "",
                 TimeZoneId = 1,
                 RxntUserName = null,
                 RxntPassword = null,
                 Roles = "",
                 Tags = "",
                 PrimaryAdmin = true
                },
            };
        }

        private static void CompareDTOAssert(HospitalStaffProfileDTO expected, HospitalStaffProfileDTO actual){
            Assert.AreEqual(expected.AgeString, actual.AgeString);
            Assert.AreEqual(expected.BaseFolderId, actual.BaseFolderId);
            Assert.AreEqual(expected.BusinessAddress, actual.BusinessAddress);
            Assert.AreEqual(expected.CreateDate, actual.CreateDate);
            Assert.AreEqual(expected.CreatedBy, actual.CreatedBy);
            Assert.AreEqual(expected.Department, actual.Department);
            Assert.AreEqual(expected.DepartmentId, actual.DepartmentId);
            Assert.AreEqual(expected.Dob, actual.Dob);
            Assert.AreEqual(expected.EmailID, actual.EmailID);
            Assert.AreEqual(expected.FileSharingSitePassword, actual.FileSharingSitePassword);
            Assert.AreEqual(expected.FileSharingSiteUserName, actual.FileSharingSiteUserName);
            Assert.AreEqual(expected.FullName, actual.FullName);
            Assert.AreEqual(expected.Gender, actual.Gender);
            Assert.AreEqual(expected.HomeAddress, actual.HomeAddress);
            Assert.AreEqual(expected.HomePhone, actual.HomePhone);
            Assert.AreEqual(expected.HospitalId, actual.HospitalId);
            Assert.AreEqual(expected.HospitalStaffAddresses, actual.HospitalStaffAddresses);
            Assert.AreEqual(expected.InternShip, actual.InternShip);
            Assert.AreEqual(expected.IsActive, actual.IsActive);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.MedicalLicence, actual.MedicalLicence);
            Assert.AreEqual(expected.MedicalSpeciality, actual.MedicalSpeciality);
            Assert.AreEqual(expected.MedicalSpecialityDescription, actual.MedicalSpecialityDescription);
            Assert.AreEqual(expected.MedicalSpecialityId, actual.MedicalSpecialityId);
            Assert.AreEqual(expected.MedicineSchool, actual.MedicineSchool);
            Assert.AreEqual(expected.MobileCountryCode, actual.MobileCountryCode);
            Assert.AreEqual(expected.MobilePhone, actual.MobilePhone);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Npinumber, actual.Npinumber);
            Assert.AreEqual(expected.Password, actual.Password);
            Assert.AreEqual(expected.PhoneCountryCode, actual.PhoneCountryCode);
            Assert.AreEqual(expected.PreMedicalEduc, actual.PreMedicalEduc);
            Assert.AreEqual(expected.PrimaryAdmin, actual.PrimaryAdmin);
            Assert.AreEqual(expected.ProfileImage, actual.ProfileImage);
            Assert.AreEqual(expected.ProfileImagePath, actual.ProfileImagePath);
            Assert.AreEqual(expected.Roles, actual.Roles);
            Assert.AreEqual(expected.RxntPassword, actual.RxntPassword);
            Assert.AreEqual(expected.RxntUserName, actual.RxntUserName);
            Assert.AreEqual(expected.StaffId, actual.StaffId);
            Assert.AreEqual(expected.StatesLicenced, actual.StatesLicenced);
            Assert.AreEqual(expected.SubSpeciality, actual.SubSpeciality);
            Assert.AreEqual(expected.SubSpecialityDescription, actual.SubSpecialityDescription);
            Assert.AreEqual(expected.SubSpecialityId, actual.SubSpecialityId);
            Assert.AreEqual(expected.Tags, actual.Tags);
            Assert.AreEqual(expected.TextAlerts, actual.TextAlerts);
            Assert.AreEqual(expected.TimeZoneId, actual.TimeZoneId);
            Assert.AreEqual(expected.UpdateDate, actual.UpdateDate);
            Assert.AreEqual(expected.UpdatedBy, actual.UpdatedBy);
            Assert.AreEqual(expected.User, actual.User);
            Assert.AreEqual(expected.UserId, actual.UserId);
            Assert.AreEqual(expected.YearOfStateRegistration, actual.YearOfStateRegistration);
            Assert.AreEqual(expected.YearsOfExperience, actual.YearsOfExperience);
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void Get_Returns_Data(int profileId)
        {
            var _context = MockContext();
            HospitalStaffProfileFacade facade = new HospitalStaffProfileFacade(new CountryCodeRepository(_context), new DoctorSettingRepository(_context), new DepartmentRepository(_context), new HospitalStaffRepository(_context));
            var profile = _context.HospitalStaffProfiles.First( x => x.User.UserId == profileId);
            var dto = facade.Get(profile,profileId,1);
            CompareDTOAssert(GetExpected()[profileId - 1],dto);
        }
    }
}
