﻿using System;
using System.Collections.Generic;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces.Models.ApiKeys;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.ApiKeys;
using SnapMD.Data.Repositories.ApiCredentials;
using DeveloperIdentity = SnapMD.Web.Api.Auth.DeveloperIdentity;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class DeveloperApiKeysTests
    {
        [Test]
        public void VerifySha1Hash()
        {
            var repository = new Mock<IDeveloperCredentialsRepository>();
            var entity = new Mock<IApiCredentials>();
            const string sha1 = "12ad03c3f1ec45a1e571650943e6d0448fd057e6";
            entity.SetupGet(d => d.ApiKey).Returns(sha1);

            var devId = Guid.NewGuid();
            repository.Setup(r => r.Get(devId)).Returns(entity.Object);

            var dev = new DeveloperIdentity(repository.Object) { DeveloperId = devId };
            Assert.IsTrue(dev.IsValid(sha1));
        }

        [Test]
        public void VerifyInvalidHashFails()
        {
            var repository = new Mock<IDeveloperCredentialsRepository>();
            const string sha1 = "ieielp";
            var dev = new DeveloperIdentity(repository.Object) { DeveloperId = Guid.NewGuid() };
            Assert.IsFalse(dev.IsValid(sha1));
        }

        [Test]
        public void VerifyEmptyEntityFails()
        {
            var dev = new DeveloperIdentity();
            Assert.IsFalse(dev.IsValid(null));
        }

        [Test]
        public void VerifyExpiredKeyFails()
        {
            var repository = new Mock<IDeveloperCredentialsRepository>();
            var entity = new Mock<IApiCredentials>();
            const string sha1 = "12ad03c3f1ec45a1e571650943e6d0448fd057e6";
            entity.SetupGet(d => d.ApiKey).Returns(sha1);
            entity.SetupGet(d => d.Expires).Returns(DateTime.Today.AddDays(-1));

            var devId = Guid.NewGuid();
            repository.Setup(r => r.Get(devId)).Returns(entity.Object);

            var dev = new DeveloperIdentity(repository.Object) { DeveloperId = devId };
            Assert.IsFalse(dev.IsValid(sha1));
        }

        [Test]
        public void VerifyApiKeys()
        {
            const string sha1 = "12ad03c3f1ec45a1e571650943e6d0448fd057e6";
            Guid devId = Guid.Parse("84F6101F-F82D-494F-8FCC-5C0E54005895");
            var fake = new List<ApiCredentials>
            {
                Builder<ApiCredentials>.CreateNew().Build(),
                new ApiCredentials
                {
                    ApiKey = sha1,
                    Id = devId,
                    AccessLevel = ApiAccessLevel.Internal
                }
            };

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.ApiCredentials).Returns(fake.ToDbSet());
            var dev = new DeveloperIdentity(new DeveloperCredentialsRepository(mock.Object)) { DeveloperId = devId };
            Assert.That(dev.IsValid(sha1));
        }
    }
}