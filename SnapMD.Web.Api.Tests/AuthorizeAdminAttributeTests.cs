﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;
using SnapMD.Web.Api.Auth;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class AuthorizeAdminAttributeTest
    {
        [Test]
        public void TestAuthorizationSuccessful()
        {
            /*
            Users
            join r in _context.UserRoles on m.UserId equals r.UserId
            join rl in _context.Roles on r.RoleId equals rl.RoleId
            join rf in _context.RoleFunctions on r.RoleId equals rf.RoleId
            join fnt in _context.SnapFunctions
            */
            var users = Builder<User>.CreateListOfSize(10).All().With(u => u.IsActive = "A").Build();
            var userRoles = Builder<UserRole>.CreateListOfSize(10).All().With(ur => ur.IsActive = "A").Build();
            var roles = Builder<Role>.CreateListOfSize(10).All().With(r => r.IsActive = "A").With(r => r.OnOff = "A").Build();
            var roleFunctions = Builder<RoleFunction>.CreateListOfSize(10).All().With(rf => rf.IsActive = "A").Build();
            var functions = Builder<SnapFunction>.CreateListOfSize(10).All().With(f => f.IsActive = "A").Build();
            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.Users).Returns(users.ToDbSet());
            context.SetupGet(c => c.UserRoles).Returns(userRoles.ToDbSet());
            context.SetupGet(c => c.Roles).Returns(roles.ToDbSet());
            context.SetupGet(c => c.RoleFunctions).Returns(roleFunctions.ToDbSet());
            context.SetupGet(c => c.SnapFunctions).Returns(functions.ToDbSet());

            var repo = new RoleFunctionRepository(context.Object);
            bool authorized = AuthorizeAdminAttribute.CheckAdminRoles(1, eRoleFunctions.E_Prescription_Creation, repo);
            Assert.IsTrue(authorized);
        }
    }
}
