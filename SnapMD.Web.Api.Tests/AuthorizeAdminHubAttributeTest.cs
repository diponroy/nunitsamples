﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using FizzWare.NBuilder;
using Moq;
using Ninject;
using NUnit.Framework;
using SnapMD.Core;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Utilities;
using SnapMD.Web.Api.Auth;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class AuthorizeAdminHubAttributeTest
    {
        [Test]
        public void TestAuthorizationSuccessful()
        {
            /*mock db*/
            var users = Builder<User>.CreateListOfSize(10).All().With(u => u.IsActive = "A").Build();
            var userRoles = Builder<UserRole>.CreateListOfSize(10).All().With(ur => ur.IsActive = "A").Build();
            var roles = Builder<Role>.CreateListOfSize(10).All().With(r => r.IsActive = "A").With(r => r.OnOff = "A").Build();
            var roleFunctions = Builder<RoleFunction>.CreateListOfSize(10).All().With(rf => rf.IsActive = "A").Build();
            var functions = Builder<SnapFunction>.CreateListOfSize(10).All().With(f => f.IsActive = "A").Build();
            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.Users).Returns(users.ToDbSet());
            context.SetupGet(c => c.UserRoles).Returns(userRoles.ToDbSet());
            context.SetupGet(c => c.Roles).Returns(roles.ToDbSet());
            context.SetupGet(c => c.RoleFunctions).Returns(roleFunctions.ToDbSet());
            context.SetupGet(c => c.SnapFunctions).Returns(functions.ToDbSet());

            /*mock Ioc*/
            var repo = new RoleFunctionRepository(context.Object);
            Mock<IRepositoryFactory> factoryMock = new Mock<IRepositoryFactory>();
            factoryMock.Setup(x => x.GetAdminRolesRepository()).Returns(repo);
            var kernel = new StandardKernel();
            kernel.Bind<IRepositoryFactory>().ToMethod(x => factoryMock.Object);
            IocAdapter.SetContainer(new NinjectContainer().WithKernel(kernel));

            /*test*/
            bool isAuthorized = AuthorizeAdminHubAttribute.IsAuthorized(1, eRoleFunctions.E_Prescription_Creation);
            Assert.IsTrue(isAuthorized);
        }
    }
}
