﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SnapMD.Core;
using SnapMD.Data.Entities;

namespace SnapMD.Web.Api.Tests
{
    [SetUpFixture]
    class Startup
    {
        [SetUp]
        public void Start()
        {
            SnapMD.Web.Api.Startup.CreateKernel();
        }
    }
}
