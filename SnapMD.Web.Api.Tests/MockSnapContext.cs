﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.ApiKeys;
using SnapMD.Data.Entities.Billing;
using SnapMD.Data.Entities.Consultations;
using SnapMD.Data.Entities.FileSharing;
using SnapMD.Data.Entities.Integration;
using SnapMD.Data.Entities.HealthPlans;
using SnapMD.Data.Repositories.Integration;

namespace SnapMD.Web.Api.Tests
{
    public class MockSnapContext : ISnapContext
    {
        public MockSnapContext()
        {
            CptCodes = new MockDbSet<CptCode>();
            PatientMetadata = new MockDbSet<PatientMetadata>();
            EncounterMetadata = new MockDbSet<EncounterMetadata>();
            DoctorSpecialities = new MockDbSet<DoctorSpeciality>();
        }

        public class MockConsultationRepository : MockDbSetBase<Consultation>
        {
            public override Consultation Find(params object[] keyValues)
            {
                if (keyValues.Length == 1)
                {
                    return _data.FirstOrDefault(a => a.ConsultationId == (int) keyValues[0]);
                }
                return null;
            }
        }

        public class MockDbSetBase<T> : DbSet<T>, IQueryable, IEnumerable<T>
            where T : class
        {
            protected List<T> _data;
            protected IQueryable _query;

            public MockDbSetBase()
            {
                _data = new List<T>();
                _query = _data.AsQueryable();
            }

            public override ObservableCollection<T> Local
            {
                get { return new ObservableCollection<T>(_data); }
            }

            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return _data.GetEnumerator();
            }

            Type IQueryable.ElementType
            {
                get { return _query.ElementType; }
            }

            System.Linq.Expressions.Expression IQueryable.Expression
            {
                get { return _query.Expression; }
            }

            IQueryProvider IQueryable.Provider
            {
                get { return _query.Provider; }
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return _data.GetEnumerator();
            }

            public override T Add(T item)
            {
                _data.Add(item);
                return item;
            }

            public override T Remove(T item)
            {
                _data.Remove(item);
                return item;
            }

            public override T Attach(T item)
            {
                _data.Add(item);
                return item;
            }

            public override T Find(params object[] keyValues)
            {
                return base.Find(keyValues);
            }

            public override T Create()
            {
                return Activator.CreateInstance<T>();
            }

            public override TDerivedEntity Create<TDerivedEntity>()
            {
                return Activator.CreateInstance<TDerivedEntity>();
            }
        }

        #region properties / methods

        public IDbSet<UrlPageRoute> UrlPageRoutes { get; set; }

        IQueryable<IUser> ITimeContext.Users
        {
            get { throw new NotImplementedException(); }
        }

        public IDbSet<UserAccountSetting> UserAccountSettings { get; set; }

        public IDbSet<UserAgentActivity> UserAgentActivities { get; set; }

        private IDbSet<UserDependentRelationAuthorization> _userDependentRelationAuthorizations =
            new MockDbSetBase<UserDependentRelationAuthorization>();

        public IDbSet<UserDependentRelationAuthorization> UserDependentRelationAuthorizations
        {
            get { return _userDependentRelationAuthorizations; }
            set { _userDependentRelationAuthorizations = value; }
        }

        public IDbSet<UserRole> UserRoles { get; set; }

        public IDbSet<UserRolePermission> UserRolePermissions { get; set; }

        public IDbSet<UserRolesTemp> UserRolesTemps { get; set; }

        public IDbSet<UsersBCrypt> UsersBCrypts { get; set; }
        
        public IDbSet<UserTagMap> UserTagMaps { get; set; }

        public IDbSet<UserTagMapTemp> UserTagMapTemps { get; set; }

        public IDbSet<UserToken> UserTokens { get; set; }

        public IDbSet<UserType> UserTypes { get; set; }

        public IDbSet<WaitingList> WaitingLists { get; set; }

        public IDbSet<ZipCode> ZipCodes { get; set; }

        public IDbSet<SystemService> SystemService { get; set; }

        public IDbSet<ServiceStatus> ServiceStatuses { get; set; }

        public IDbSet<ServiceLog> ServiceLogs { get; set; }

        public IDbSet<SmsReminder> SmsReminders { get; set; }

        public IDbSet<SmsSent> SmsSend { get; set; }

        public IDbSet<EmailLog> EmaiLogs { get; set; }

        public IDbSet<EmailReminder> EmailReminders { get; set; }

        public IDbSet<EmailQueue> EmailQueues { get; set; }

        public IDbSet<ApiCredentials> ApiCredentials
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        private IDbSet<EncounterMetadata> _encounterMetadatas = new MockDbSetBase<EncounterMetadata>();

        public IDbSet<EncounterMetadata> EncounterMetadata
        {
            get { return _encounterMetadatas; }
            set { _encounterMetadatas = value; }
        }

        public IDbSet<PatientMetadata> PatientMetadata { get; set; }

        public IDbSet<Organization> Organizations
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<OrganizationTypeCode> OrganizationTypes
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<Location> Locations
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<T> Set<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public IDbSet<ChatMessage> ChatMessages { get; set; }

        public IDbSet<ChatUserSession> ChatUserSessions { get; set; }

        public IDbSet<Developer> Developers
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<ApiAccessLog> ApiAccessLogs { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public DbEntityEntry<T> Entry<T>(T @object) where T : class
        {
            throw new NotImplementedException();
        }

        public void SetAdded(object @object)
        {
            throw new NotImplementedException();
        }

        public void SetModified(object @object)
        {
            throw new NotImplementedException();
        }

        public IDbSet<User> Users { get; set; }

        IDbSet<StandardTimeZone> ISnapContext.StandardTimeZones { get; set; }

        public IDbSet<State> States { get; set; }

        public IDbSet<TelephoneCountryCode> TelephoneCountryCodes { get; set; }

        public IDbSet<Tag> Tags { get; set; }

        public IDbSet<UpdateUserEmailDetailsTemp> UpdateUserEmailDetailsTemps { get; set; }

        public Database Database { get; private set; }

        public DbContextConfiguration Configuration { get; private set; }

        public IDbSet<Address> Addresses
        {
            get { return _addresses; }
            set { _addresses = value; }
        }

        public IDbSet<AddressLocation> AddressesLocations
        {
            get { return _addressesLocation; }
            set { _addressesLocation = value; }
        }

        private IDbSet<Address> _addresses = new List<Address>().ToDbSet();
        private IDbSet<AddressLocation> _addressesLocation = new List<AddressLocation>().ToDbSet();

        public IDbSet<BillingProfile> BillingProfiles { get; set; }

        public IDbSet<ChatHistoryFile> ChatHistoryFiles { get; set; }

        public IDbSet<ClientPublicKeys> ClientPublicKeys
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        private IDbSet<Code> _codes = new MockDbSet<Code>();

        public IDbSet<Code> Codes
        {
            get { return _codes; }
            set { _codes = value; }
        }

        public IDbSet<CodeSet> CodeSets { get; set; }
        public IDbSet<ConsultationMedicalCode> ConsultationMedicalCodes { get; set; }
        public IDbSet<MedicalCodingSystem> MedicalCodingSystemses { get; set; }
        public IDbSet<MedicalCode> MedicalCodes { get; set; }
        public IDbSet<ConfigMaster> ConfigMasters { get; set; }

        private IDbSet<Consultation> _consultations = new MockConsultationRepository();

        public IDbSet<Consultation> Consultations
        {
            get { return _consultations; }
            set { _consultations = value; }
        }

        public IDbSet<ConsultationPaymentsDetail> ConsultationPaymentsDetails { get; set; }

        private IDbSet<ConsultationSoapNotesCptCode> _consultationSoapNotesCptCodes =
            new MockDbSetBase<ConsultationSoapNotesCptCode>();

        public IDbSet<ConsultationSoapNotesCptCode> ConsultationSoapNotesCptCodes
        {
            get { return _consultationSoapNotesCptCodes; }
            set { _consultationSoapNotesCptCodes = value; }
        }

        public IDbSet<ConsultationSurvey> ConsultationSurveys { get; set; }

        public IDbSet<ConsultationSurveyQuestion> ConsultationSurveyQuestions { get; set; }

        public IDbSet<ConsultationStatusLog> ConsultationStatusLogs { get; set; }

        public IDbSet<CoUsersTemp> CoUsersTemps { get; set; }

        public IDbSet<CoUserToken> CoUserTokens { get; set; }

        public IDbSet<CptCode> CptCodes { get; set; }

        public IDbSet<CptCodesMaster> CptCodesMasters { get; set; }

        public IDbSet<Department> Departments { get; set; }

        public IDbSet<DoctorSetting> DoctorSettings { get; set; }

        public IDbSet<DoctorSpeciality> DoctorSpecialities { get; set; }

        public IDbSet<DoctorStatus> DoctorStatus { get; set; }

        public IDbSet<DoctorStatusLog> DoctorStatusLogs { get; set; }

        public IDbSet<DocumentTypes> DocumentTypes { get; set; }

        public IDbSet<DroppedConsultation> DroppedConsultations { get; set; }

        private IDbSet<FamilyProfile> _familyProfiles = new MockDbSetBase<FamilyProfile>();

        public IDbSet<FamilyProfile> FamilyProfiles
        {
            get { return _familyProfiles; }
            set { _familyProfiles = value; }
        }

        public IDbSet<Feature> Features { get; set; }

        public IDbSet<SnapFunction> SnapFunctions { get; set; }

        public IDbSet<Data.Entities.HealthPlan> HealthPlans { get; set; }

        public IDbSet<HealthPlanName> HealthPlanNames { get; set; }

        private IDbSet<Hospital> _hospitals = new MockDbSetBase<Hospital>();

        public IDbSet<Hospital> Hospitals
        {
            get { return _hospitals; }
            set { _hospitals = value; }
        }

        public IDbSet<HospitalComment> HospitalComments { get; set; }

        public IDbSet<HospitalContactPerson> HospitalContactPersons { get; set; }

        public IDbSet<HospitalDocuments> HospitalDocuments { get; set; }

        public IDbSet<HospitalRepresentative> HospitalRepresentatives { get; set; }

        public IDbSet<HospitalRepresentativesTemp> HospitalRepresentativesTemps { get; set; }

        public IDbSet<HospitalSetting> HospitalSettings { get; set; }

        public IDbSet<HospitalSite> HospitalSites { get; set; }

        public IDbSet<HospitalStaffAddress> HospitalStaffAddresses { get; set; }

        private IDbSet<HospitalStaffProfile> _hospitalStaffProfiles = new MockDbSetBase<HospitalStaffProfile>();

        public IDbSet<HospitalStaffProfile> HospitalStaffProfiles
        {
            get { return _hospitalStaffProfiles; }
            set { _hospitalStaffProfiles = value; }
        }

        public IDbSet<HospitalStaffProfileTemp> HospitalStaffProfileTemps { get; set; }

        public IDbSet<HospitalTagMap> HospitalTagMaps { get; set; }

        public IDbSet<HospitalToken> HospitalTokens { get; set; }

        public IDbSet<LogMessage> LogMessages { get; set; }

        public IDbSet<OperatingHours> OperatingHours { get; set; }

        public IDbSet<PatientAddress> PatientAddresses
        {
            get { return _patientAddresses; }
            set { _patientAddresses = value; }
        }

        private IDbSet<PatientAddress> _patientAddresses = new List<PatientAddress>().ToDbSet();


        private IDbSet<PatientConsultationReport> _patientConsultationReports =
            new MockDbSetBase<PatientConsultationReport>();

        public IDbSet<PatientConsultationReport> PatientConsultationReports
        {
            get { return _patientConsultationReports; }
            set { _patientConsultationReports = value; }
        }

        public IDbSet<PatientLogin> PatientLogins { get; set; }

        private IDbSet<PatientMedicalHistory> _patientMedicalHistories = new MockDbSetBase<PatientMedicalHistory>();

        public IDbSet<PatientMedicalHistory> PatientMedicalHistories
        {
            get { return _patientMedicalHistories; }
            set { _patientMedicalHistories = value; }
        }

        public IDbSet<PatientNote> PatientNotes { get; set; }

        private IDbSet<PatientProfile> _patientProfiles = new MockDbSetBase<PatientProfile>();

        public IDbSet<PatientProfileFieldsChangedByClinician> PatientProfileFieldsChangedByClinician { get; set; }

        public IDbSet<PatientProfile> PatientProfiles
        {
            get { return _patientProfiles; }
            set { _patientProfiles = value; }
        }

        public IDbSet<PharmacyInfo> PharmacyInfoes { get; set; }

        public IDbSet<PrescriptionInfo> PrescriptionInfoes { get; set; }

        public IDbSet<RefactorLog> RefactorLogs { get; set; }

        public IDbSet<ReportTemplate> ReportTemplate { get; set; }

        public IDbSet<Role> Roles { get; set; }

        public IDbSet<RoleFunction> RoleFunctions { get; set; }

        public IDbSet<ScheduleCalendar> ScheduleCalendars { get; set; }

        public IDbSet<ScheduledConsultationTemp> ScheduledConsultationTemps { get; set; }

        public IDbSet<ScheduleSlotCalendar> ScheduleSlotCalendars { get; set; }

        public IDbSet<SmsConsultation> SmsConsultations { get; set; }

        private IDbSet<SmsTemplate> _smsTemplates = new MockDbSetBase<SmsTemplate>();

        public IDbSet<SmsTemplate> SmsTemplates
        {
            get { return _smsTemplates; }
            set { _smsTemplates = value; }
        }

        public IDbSet<SnapMDStaffProfile> SnapMDStaffProfiles { get; set; }

        public IDbSet<SnapMDStaffProfileTemp> SnapMdStaffProfileTemps { get; set; }

        public IDbSet<DownloadToken> DownloadTokens { get; set; }

        IQueryable<ITimeZone> ITimeContext.StandardTimeZones
        {
            get { throw new NotImplementedException(); }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion


        public IDbSet<TagFile> TagFiles
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public IDbSet<FileSharingTag> FileSharingTags
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<ConsultationHealthPlanRequest> ConsultationHealthPlanRequests
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<CodePartnerTranslation> CodePartnerTranslations
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #region Integration

        public IDbSet<Partner> Partners
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<PartnerParameter> PartnerParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<IntegrationService> IntegrationServices
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<HospitalPartner> HospitalPartners
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<PartnerIntegrationService> PartnerIntegrationServices
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<PartnerIntegrationRepository> PartnerIntegrationRepositories
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<IntegrationRepository> IntegrationRepositories
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<PatientProfileExtension> PatientProfileExtensions
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<HospitalStaffProfileExtension> HospitalStaffProfileExtensions
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbSet<ConsultationExtension> ConsultationExtensions
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        IDbSet<IntegrationRepositoryEntity> ISnapContext.IntegrationRepositories
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

    }
}
