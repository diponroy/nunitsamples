﻿using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Patients;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class OnDemandConsultationCreatorTests
    {
        [Test]
        public void TestConstructor()
        {
            Assert.DoesNotThrow(() =>
            {
                var thing = new OnDemandConsultationCreator(1, 1, 1, null);
            });
        }

        [Test]
        public void TestNotAuthorized()
        {

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(m => m.UserDependentRelationAuthorizations)
                .Returns(new MockDbSet<UserDependentRelationAuthorization>());
            mock.SetupGet(m => m.PatientProfiles).Returns(new MockDbSet<PatientProfile>());
            
            var target = new OnDemandConsultationCreator(1, 1, 1, mock.Object);

            Assert.IsFalse(target.IsAuthorized);
        }
    }
}
