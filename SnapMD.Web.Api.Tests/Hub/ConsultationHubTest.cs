﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hosting;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using System.Threading.Tasks;

namespace SnapMD.Web.Api.Tests.Hub
{
    [TestFixture]
    public class ConsultationHubTest
    {
        [Test]
        public void ConsultationHubObjectCreated()
        {
            var hub = new ConsultationHub();
            var mockRequest = new Mock<IRequest>();
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            Assert.IsInstanceOf(typeof(ConsultationHub), hub);
            Assert.IsInstanceOf(typeof(HubCallerContext), hub.Context);
            

        }
        [Test]
        public void TestSignalRUserConnection()
        {
            var hub = new ConsultationHub();
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();
            
            var mocClaimPrincipal = new Mock<ClaimsPrincipal>();
            
            mocClaimPrincipal.Setup(m => m.HasClaim(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(true);
            mocClaimPrincipal.Setup(c => c.Claims).Returns(() =>
            {
                var claimList = new List<Claim>
                {
                    new Claim("name", "Shibu"),
                    new Claim("nameidentifier", "1"),
                    new Claim("provider", "1")
                };
                return claimList;
            });
            mocClaimPrincipal.Setup(c => c.Identity).Returns(() => new ClaimsIdentity(mocClaimPrincipal.Object.Claims));
            mockRequest.Setup(c => c.User).Returns(mocClaimPrincipal.Object);
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            var userInfo= hub.GetUserInfo();
            Assert.AreEqual(1, userInfo.UserId);
            Assert.AreEqual("Shibu", userInfo.User);
            Assert.AreEqual(1, userInfo.ProviderId);
        }
        [Test]
        public void TestIsClientAvailable()
        {
            var context = GetDbContext();
            var hub = new ConsultationHub(context.Object);
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();
            var mocClaimPrincipal = new Mock<ClaimsPrincipal>();
            mocQueryString.Setup(c => c["consultationId"]).Returns("1");
            mockRequest.Setup(c => c.QueryString).Returns(mocQueryString.Object);

            mocClaimPrincipal.Setup(m => m.HasClaim(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(true);
            mocClaimPrincipal.Setup(c => c.Claims).Returns(() =>
            {
                var claimList = new List<Claim>
                {
                    new Claim("name", "Shibu"),
                    new Claim("nameidentifier", "1"),
                    new Claim("provider", "1")
                };
                return claimList;
            });
            mocClaimPrincipal.Setup(c => c.Identity).Returns(() => new ClaimsIdentity(mocClaimPrincipal.Object.Claims));
            mockRequest.Setup(c => c.User).Returns(mocClaimPrincipal.Object);
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.OnConnected().ContinueWith((c) =>
            {
                var userInfo = hub.IsClientAvailable();
                Assert.IsTrue(userInfo);
            });
          
        }


        [Test]
        public void TestRemoveConnection()
        {
            var context = GetDbContext();
            var hub = new ConsultationHub(context.Object);
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();
            var mocClaimPrincipal = new Mock<ClaimsPrincipal>();
            mocQueryString.Setup(c => c["consultationId"]).Returns("1");
            mockRequest.Setup(c => c.QueryString).Returns(mocQueryString.Object);

            mocClaimPrincipal.Setup(m => m.HasClaim(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(true);
            mocClaimPrincipal.Setup(c => c.Claims).Returns(() =>
            {
                var claimList = new List<Claim>
                {
                    new Claim("name", "Shibu"),
                    new Claim("nameidentifier", "1"),
                    new Claim("provider", "1")
                };
                return claimList;
            });
            mocClaimPrincipal.Setup(c => c.Identity).Returns(() => new ClaimsIdentity(mocClaimPrincipal.Object.Claims));
            mockRequest.Setup(c => c.User).Returns(mocClaimPrincipal.Object);
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.OnConnected().ContinueWith((c) =>
            {
                var countConnection = hub.GetAll().Count();
                Assert.AreEqual(countConnection, 1);

                hub.RemoveConnection(new ConsultationConnectionModel()
                {
                    ConnectionId = connectionId.ToString(),
                    ConsultationId = 1,
                });
                countConnection = hub.GetAll().Count();

                Assert.AreEqual(countConnection, 0, "Connection Id Should be Zero");

            });
         
        }
        [Test]
        public void TestIsClientNotAvailable()
        {
            var context = GetDbContext(true);
            var hub = new ConsultationHub(context.Object);
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();
            var mocClaimPrincipal = new Mock<ClaimsPrincipal>();
            mocQueryString.Setup(c => c["consultationId"]).Returns("1");
            mockRequest.Setup(c => c.QueryString).Returns(mocQueryString.Object);

            mocClaimPrincipal.Setup(m => m.HasClaim(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(true);
            mocClaimPrincipal.Setup(c => c.Claims).Returns(() =>
            {
                var claimList = new List<Claim>
                {
                    new Claim("name", "Shibu"),
                    new Claim("nameidentifier", "1"),
                    new Claim("provider", "1")
                };
                return claimList;
            });
            mocClaimPrincipal.Setup(c => c.Identity).Returns(() => new ClaimsIdentity(mocClaimPrincipal.Object.Claims));
            mockRequest.Setup(c => c.User).Returns(mocClaimPrincipal.Object);
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.OnConnected().ContinueWith((c) =>
           {
               var isAvailable = hub.IsClientAvailable();
               Assert.IsFalse(isAvailable);
           });

        }


        [Test]
        public void TestStartAndEndCountation()
        {
            var context = GetDbContext();
            var hub = new ConsultationHub(context.Object);
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();
            var mocClaimPrincipal = new Mock<ClaimsPrincipal>();
            mocQueryString.Setup(c => c["consultationId"]).Returns("1");
            mockRequest.Setup(c => c.QueryString).Returns(mocQueryString.Object);

            mocClaimPrincipal.Setup(m => m.HasClaim(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(true);
            mocClaimPrincipal.Setup(c => c.Claims).Returns(() =>
            {
                var claimList = new List<Claim>
                {
                    new Claim("name", "Shibu"),
                    new Claim("nameidentifier", "1"),
                    new Claim("provider", "1")
                };
                return claimList;
            });
            mocClaimPrincipal.Setup(c => c.Identity).Returns(() => new ClaimsIdentity(mocClaimPrincipal.Object.Claims));
            mockRequest.Setup(c => c.User).Returns(mocClaimPrincipal.Object);
            var connectionId = Guid.NewGuid();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.OnConnected().ContinueWith((cc) =>
            {
                var result = hub.StartConsultation();
                Assert.IsTrue(result);

            });
            
            //result = await hub.EndConsultation();
            //Assert.IsTrue(result);
            
        }

       

        private static Mock<ISnapContext> GetDbContext(bool returnEmptyList=false)
        {
            Mock<ISnapContext> context = new Mock<ISnapContext>();
            IList<Consultation> conList = new List<Consultation>();
            if (!returnEmptyList)
            {
               
                conList.Add(new Consultation()
                {
                    ConsultationId = 1,
                    AssignedDoctorId = 1,
                    ConsultantUserId = 2,
                    ConsultationStatus=69
                });
                conList.Add(new Consultation()
                {
                    ConsultationId = 2,
                    AssignedDoctorId = 1,
                    ConsultantUserId = 3,
                    ConsultationStatus = 70
                });
               
            }
            context.Setup(c => c.Consultations).Returns(conList.ToDbSet());
            return context;
        }
    }
}
