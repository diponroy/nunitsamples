﻿using System;
using System.Linq;
using NUnit.Framework;
using SnapMD.Data.Entities;

namespace SnapMD.Web.Api.Tests.DbAlterTests
{
    /*Todo: remove me when #3440 is done*/

    [TestFixture, Explicit]
    public class ContactNumberAlterTests
    {
        private SnapContext _dbContext;
        private string _contactNumber;
        private const int _maxLength = 50;

        [SetUp]
        public void SetUp()
        {
            _dbContext = new SnapContext();

            _contactNumber = "+";
            string chars = "1234567890";
            var random = new Random();
            int maxLength = _maxLength - 1; //as to count '+' sign
            var result = new string(
                Enumerable.Repeat(chars, maxLength)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());
            _contactNumber += result;
        }

        [Test]
        public void IsContactLenth_50()
        {
            Assert.AreEqual(50, _contactNumber.Length);
        }

        [Test]
        public void CoUsersTemp()
        {
            var entity = _dbContext.CoUsersTemps.First();
            entity.PhoneNumber = _contactNumber;
            entity.MobileNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void Hospital()
        {
            var entity = _dbContext.Hospitals.First();
            entity.ContactNumber = _contactNumber;
            entity.AppointmentsContactNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void HospitalContactPerson()
        {
            var entity = _dbContext.HospitalContactPersons.First();
            entity.OfficeNumber = _contactNumber;
            entity.CellNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void HospitalStaffProfile()
        {
            var entity = _dbContext.HospitalStaffProfiles.First();
            entity.HomePhone = _contactNumber;
            entity.MobilePhone = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void HospitalStaffProfileTemp()
        {
            var entity = _dbContext.HospitalStaffProfileTemps.First();
            entity.HomePhone = _contactNumber;
            entity.MobilePhone = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }


        [Test]
        public void PatientProfile()
        {
            var entity = _dbContext.PatientProfiles.First();
            entity.SchoolContact = _contactNumber;
            entity.HomePhone = _contactNumber;
            entity.MobilePhone = _contactNumber;
            entity.PrimaryPhysicianContact = _contactNumber;
            entity.PhysicianSpecialistContact = _contactNumber;
            entity.PharmacyContact = _contactNumber;
            entity.FamilyPediatricianContact = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void PharmacyInfoe()
        {
            var entity = _dbContext.PharmacyInfoes.First();
            entity.PharmaPhone1 = _contactNumber;
            entity.PharmaPhone2 = _contactNumber;
            entity.PharmaFax = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void SnapMDStaffProfile()
        {
            var entity = _dbContext.SnapMDStaffProfiles.First();
            entity.PhoneNumber = _contactNumber;
            entity.CellNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void SnapMdStaffProfileTemp()
        {
            var entity = _dbContext.SnapMdStaffProfileTemps.First();
            entity.PhoneNumber = _contactNumber;
            entity.CellNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }

        [Test]
        public void User()
        {
            var entity = _dbContext.Users.First();
            entity.MobileNumber = _contactNumber;
            entity.PhoneNumber = _contactNumber;
            Assert.DoesNotThrow(() => _dbContext.SaveChanges());
        }
    }
}