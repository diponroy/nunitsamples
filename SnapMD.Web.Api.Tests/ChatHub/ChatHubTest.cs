﻿using System;
using Moq;

using System.Dynamic;
using NUnit.Framework;
using Microsoft.AspNet.SignalR.Hubs;
using SnapMD.Data.Entities;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using System.Security.Principal;
using Microsoft.AspNet.SignalR.Hosting;
using SnapMD.Web.Api.ChatHub;

namespace SnapMD.Web.Api.Tests.ChatHub
{
    [TestFixture]
    public class ChatHubTest
    {
        [Test]
        public void SendChatToAnyUser()
        {
            var context = new Mock<ISnapContext>();
            Guid connectionId=Guid.NewGuid();
            Guid connectionId2 = Guid.NewGuid();
            IList<ChatUserSession> set = new List<ChatUserSession>();
            set.Add(new ChatUserSession()
            {
                ChatToken = connectionId,
                ChatUserSessionId = 1,
                UserId = 1,
                LastLoggedInDate = DateTime.Now
            });
            set.Add(new ChatUserSession()
            {
                ChatToken = connectionId2,
                ChatUserSessionId = 2,
                UserId = 2,
                LastLoggedInDate = DateTime.Now
            });
            set.Add(new ChatUserSession()
            {
                ChatToken = Guid.NewGuid(),
                ChatUserSessionId = 3,
                UserId = 3,
                LastLoggedInDate = DateTime.Now
            });
            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();
            hosStaffList.Add(new HospitalStaffProfile()
            {
                HospitalId =1,
                UserId = 1,
                Name = "Shibub",
                LastName = "Bhattarai",
                ProfileImagePath = "pathtoprofile",
                IsActive ="A"


            });
            hosStaffList.Add(new HospitalStaffProfile()
            {
                HospitalId = 1,
                UserId = 2,
                Name = "Hari",
                LastName = "Kumar",
                ProfileImagePath = "pathtoprofile",
                IsActive ="A"

            });

            context.SetupGet(m => m.ChatUserSessions).Returns(set.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());


            var mockConnection = new Mock<IConnection>();
            var mockUser = new Mock<IPrincipal>();
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(r => r.User).Returns(mockUser.Object);
         
            var hub = new SnapChatService(context.Object);
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.Clients = mockClients.Object;


          
            dynamic all = new ExpandoObject();
            all.deliverToUser = new Action<ChatUser, ChatUser, string>((senderInfo, deliverInformation, message) =>
            {
                Assert.AreEqual(message, "TestMessage");
                Assert.AreEqual(senderInfo.FirstName, "Shibub");
                Assert.AreEqual(deliverInformation.FirstName, "Hari");
            });
            mockClients.Setup(m => m.All).Returns((ExpandoObject)all);
            mockClients.Setup(c => c.Client(connectionId.ToString())).Returns((ExpandoObject)all);
            mockClients.Setup(c => c.Client(connectionId2.ToString())).Returns((ExpandoObject)all);

            
            hub.sendMessageToUser(2, "TestMessage");
        }


        [Test]
        public void SendMessageToPhysicianOrPatient()
        {

            Mock<ISnapContext> context;
            Guid connectionId;
            Guid userConnectionId;
           
            GetContextWithSetUpDate(out context, out connectionId, out userConnectionId);

            var mockConnection = new Mock<IConnection>();
            var mockUser = new Mock<IPrincipal>();
            var mockRequest = new Mock<IRequest>();
            var mocQueryString = new Mock<INameValueCollection>();


            mocQueryString.Setup(c => c["consultationId"]).Returns("1");
            mockRequest.Setup(r => r.User).Returns(mockUser.Object);


            var hub = new SnapChatService(context.Object);
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();

            mockRequest.Setup(c => c.QueryString).Returns(mocQueryString.Object);
            //hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.Context = new HubCallerContext(mockRequest.Object, connectionId.ToString());
            hub.Clients = mockClients.Object;

            dynamic all = new ExpandoObject();
            all.deliverMessageToPhysician = new Action<ConsultationChatUserInfo, string>((senderInfo, msg) =>
            {
                Assert.AreEqual(msg, "Hello Shibu", "Message Should Return Hello Sibu");
                Assert.AreEqual(senderInfo.ConsultionId, 1, "ConsultionId must be 1");
                Assert.AreEqual(senderInfo.PatientName, "Test2 Test2");
            });
            mockClients.Setup(m => m.All).Returns((ExpandoObject)all);
            mockClients.Setup(c => c.Client(connectionId.ToString())).Returns((ExpandoObject)all);
            mockClients.Setup(c => c.Client(userConnectionId.ToString())).Returns((ExpandoObject)all);
            hub.SendMessageToPhysician("Hello Shibu");


        }

        private static void GetContextWithSetUpDate(out Mock<ISnapContext> context, out Guid connectionId, out Guid userConnectionId)
        {
            context = new Mock<ISnapContext>();
            connectionId = Guid.NewGuid();
            userConnectionId = Guid.NewGuid();
            IList<ChatUserSession> set = new List<ChatUserSession>();
            set.Add(new ChatUserSession()
            {
                ChatToken = connectionId,
                ChatUserSessionId = 1,
                UserId = 1,
                LastLoggedInDate = DateTime.Now
            });
            set.Add(new ChatUserSession()
            {
                ChatToken = userConnectionId,
                ChatUserSessionId = 1,
                UserId = 2,
                LastLoggedInDate = DateTime.Now
            });
            set.Add(new ChatUserSession()
            {
                ChatToken = Guid.NewGuid(),
                ChatUserSessionId = 1,
                UserId = 3,
                LastLoggedInDate = DateTime.Now
            });

            IList<PatientProfile> pasProfile = new List<PatientProfile>();
            pasProfile.Add(new PatientProfile()
            {
                UserId = 1,
                PatientName = "Test ",
                LastName = "Test",
                ProfileImagePath = "pathtoprofile"


            });
            pasProfile.Add(new PatientProfile()
            {
                UserId = 2,
                PatientName = "Test2",
                LastName = "Test2",
                ProfileImagePath = "pathtoprofile"


            });
            IList<HospitalStaffProfile> hosStaffList = new List<HospitalStaffProfile>();
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 3,
                Name = "SHibu",
                LastName = "Bhattarai",
                ProfileImagePath = "pathtoprofile"


            });
            hosStaffList.Add(new HospitalStaffProfile()
            {
                UserId = 2,
                Name = "Hari",
                LastName = "Kumar",
                ProfileImagePath = "pathtoprofile"


            });
            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "shibubh",
                ProfileImage = "asdf"
            });
            userList.Add(new User()
            {
                UserId = 2,
                UserName = "shibubh",
                ProfileImage = "asdf"
            });
            IList<Consultation> consultationList = new List<Consultation>();
            consultationList.Add(new Consultation()
            {
                ConsultationId = 1,
                PatientId = 1,
                ConsultantUserId = 2,
                AssignedDoctorId = 1
            });
            consultationList.Add(new Consultation()
            {
                ConsultationId = 2,
                PatientId = 2,
                ConsultantUserId = 2,
                AssignedDoctorId = 1
            });

            IList<ChatMessage> chatMessageList = new List<ChatMessage>();
            context.Setup(m => m.ChatMessages).Returns(chatMessageList.ToDbSet());
            context.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            context.SetupGet(m => m.Consultations).Returns(consultationList.ToDbSet());
            context.SetupGet(m => m.ChatUserSessions).Returns(set.ToDbSet());
            context.SetupGet(m => m.HospitalStaffProfiles).Returns(hosStaffList.ToDbSet());
            context.SetupGet(m => m.PatientProfiles).Returns(pasProfile.ToDbSet());
        }
    }
}

