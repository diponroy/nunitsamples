﻿using System;
using JetBrains.dotMemoryUnit;
using NUnit.Framework;
using SnapMD.Data.Entities;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class MemoryTests
    {
        [Test]
        [DotMemoryUnit(FailIfRunWithoutSupport = false)]
        public void TestMethod1()
        {
            var foo = new SnapContext();
            
            dotMemory.Check(memory => 
                Assert.That(memory.GetObjects(where => where.Type.Is<User>()).ObjectsCount, Is.EqualTo(0))); 

            GC.KeepAlive(foo); // prevent objects from GC if this is implied by test logic
        }
    }
}