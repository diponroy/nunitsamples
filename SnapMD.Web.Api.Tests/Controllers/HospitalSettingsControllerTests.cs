﻿using System.Collections.Generic;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.SnapAdmin.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class HospitalSettingsControllerTests
    {
        [Test]
        public void TestUpsertExists()
        {
            var context = new Mock<ISnapContext>();
            var existing = Builder<HospitalSetting>.CreateListOfSize(10).Build();

            context.SetupGet(c => c.HospitalSettings).Returns(existing.ToDbSet());

            using (var c = context.Object)
            {
                using (var con = new HospitalSettingsController(c))
                {
                    var result = con.Put(10, new Dictionary<string, string> { { "Key10", "Modified" } });
                    Assert.AreEqual(1, result.Total);
                }
            }

            Assert.Pass();
        }

        [Test]
        public void TestUpsertNew()
        {
            var context = new Mock<ISnapContext>();
            var existing = Builder<HospitalSetting>.CreateListOfSize(10).Build();

            context.SetupGet(c => c.HospitalSettings).Returns(existing.ToDbSet());

            using (var c = context.Object)
            {
                using (var con = new HospitalSettingsController(c))
                {
                    var result = con.Put(1, new Dictionary<string, string> { { "Key123", "New" } });
                    Assert.AreEqual(1, result.Total);
                }
            }

            Assert.Pass();
        }

        [Test]
        public void TestUpsertMultiple()
        {
            var context = new Mock<ISnapContext>();
            var existing = Builder<HospitalSetting>.CreateListOfSize(10).Build();

            context.SetupGet(c => c.HospitalSettings).Returns(existing.ToDbSet());

            using (var c = context.Object)
            {
                using (var con = new HospitalSettingsController(c))
                {
                    var result = con.Put(1,
                        new Dictionary<string, string>
                        {
                            { "Key10", "Modified" },
                            { "Key123", "New" }
                        });
                    Assert.AreEqual(2, result.Total);
                }
            }

            Assert.Pass();
        }
    }
}
