﻿using AuthorizeNet;
using Moq;
using SnapMD.Core.Interfaces;

namespace SnapMD.Web.Api.Tests.Controllers
{
    internal static class GatewaySettings
    {
        public static Mock<IAuthorizeNetSettings> MakeSettingsMock()
        {
            var mockSettings = new Mock<IAuthorizeNetSettings>();
            mockSettings.SetupGet(s => s.LoginId).Returns("867hTNWkm8");
            mockSettings.SetupGet(s => s.TransactionKey).Returns("7824m2DxJw3gGx3k");
            mockSettings.SetupGet(s => s.PaymentType).Returns("AUTH_CAPTURE");
            mockSettings.SetupGet(s => s.ServiceMode).Returns((int)ServiceMode.Test);
            mockSettings.SetupGet(s => s.PostUrl).Returns("https://test.authorize.net/gateway/transact.dll");
            return mockSettings;
        }
    }
}
