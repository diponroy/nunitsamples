﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class CountryCodeControllerTest
    {
        protected Mock<ISnapContext> MockDb { get; set; }

        protected CountryCodeController Controller { get; set; }

        [SetUp]
        public void Setup()
        {
            MockDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void Teardown()
        {
            MockDb = null;
            Controller = null;
        }

        protected void InitializeController()
        {
            if (MockDb == null)
            {
                throw new NullReferenceException("MockDb is null.");
            }
            Controller = new CountryCodeController(MockDb.Object);
        }

        [Test]
        public void Get_Success()
        {
            List<TelephoneCountryCode> telephoneCountryCodes = Builder<TelephoneCountryCode>.CreateListOfSize(2).Build().ToList();
            MockDb.Setup(x => x.TelephoneCountryCodes).Returns(telephoneCountryCodes.ToDbSet());
            InitializeController();

            var result = Controller.Get();
            Assert.IsInstanceOf<OkNegotiatedContentResult<ApiResponse<List<CountryCodeModel>>>>(result);
            ApiResponse<List<CountryCodeModel>> contentResult = ((OkNegotiatedContentResult<ApiResponse<List<CountryCodeModel>>>)result).Content;
            Assert.IsTrue(contentResult.Success);

            List<CountryCodeModel> codes = contentResult.Data;
            Assert.AreEqual(2, codes.Count);
            Assert.AreEqual(telephoneCountryCodes[0].TelephoneCountryID, codes[0].Id);
            Assert.AreEqual(telephoneCountryCodes[0].CountryCode, codes[0].Code);
            Assert.AreEqual(telephoneCountryCodes[0].CountryName, codes[0].Name);
            Assert.AreEqual(telephoneCountryCodes[1].TelephoneCountryID, codes[1].Id);
            Assert.AreEqual(telephoneCountryCodes[1].CountryCode, codes[1].Code);
            Assert.AreEqual(telephoneCountryCodes[1].CountryName, codes[1].Name);
        }

        [Test]
        public void Get_Error()
        {
            InitializeController();
            var result = Controller.Get();
            Assert.IsInstanceOf<OkNegotiatedContentResult<ApiResponse<String>>>(result);
            ApiResponse<string> contentResult = ((OkNegotiatedContentResult<ApiResponse<String>>)result).Content;
            Assert.IsFalse(contentResult.Success);
            Assert.AreEqual("Unable to load Country Code", contentResult.Data);
        }
    }
}
