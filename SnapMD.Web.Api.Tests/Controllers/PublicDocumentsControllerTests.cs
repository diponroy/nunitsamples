﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Models;
using DocumentTypes = SnapMD.ConnectedCare.ApiModels.DocumentTypeCode;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class PublicDocumentsControllerTests
    {
        private const int HospitalId = 126;
        private const string DefaultTermsAndConditionsText = "Default Terms And Conditions";
        private const string CustomTermsAndConditionsText = "My Terms And Conditions";

        private readonly List<Data.Entities.DocumentTypes> _documentTypes;
        private readonly List<HospitalDocuments> _hospitalDocumentses;

        public PublicDocumentsControllerTests()
        {
            _documentTypes = new List<Data.Entities.DocumentTypes>
            {
                new Data.Entities.DocumentTypes
                {
                    DocumentType = (short)DocumentTypes.TermsAndConditions,
                    DefaultText = DefaultTermsAndConditionsText
                }
            };

            _hospitalDocumentses = new List<HospitalDocuments>
            {
                new HospitalDocuments
                {
                    DocumentType = (short)DocumentTypes.TermsAndConditions,
                    HospitalId = HospitalId,
                    DocumentText = CustomTermsAndConditionsText
                }
            };
        }

        [Test]
        public void GET_GetDefaultDocumentTextIfCustomDocumentNotExist_ReturnDefaultText()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>()))
                .Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(new List<HospitalDocuments>().ToDbSet());
                // Custom text NOT EXIST!

            var controller = new PublicDocumentsController(context.Object);

            var result = controller.Get((short)DocumentTypes.TermsAndConditions, HospitalId);

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Data.ElementAt(0).DocumentText == DefaultTermsAndConditionsText);
        }

        [Test]
        public void GET_GetCustomDocumentTextIfCustomDocumentExist_ReturnCustomText()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>()))
                .Returns(new UserLogin { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(_hospitalDocumentses.ToDbSet()); // Custom text EXIST!

            var controller = new PublicDocumentsController(context.Object);

            var result = controller.Get((short)DocumentTypes.TermsAndConditions, 126);

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Data.ElementAt(0).DocumentText == CustomTermsAndConditionsText);
        }

        [Test]
        public void GET_InvalidDocumentType_ReturnBadRequest()
        {
            var controller = new PublicDocumentsController(new Mock<ISnapContext>().Object);

            Assert.Throws<ArgumentNullException>(() => controller.Get(4, 25), "Throws exception because there is no 'Request' object in context scope.");
        }
    }
}
