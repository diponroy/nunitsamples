﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class AccountControllerTest
    {
	    protected Mock<ISnapContext> MockedDb { get; set; }

        protected ISnapAuthenticationManager UserManager { get; set; }

        protected TargetController Controller { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;
            UserManager = null;

            TargetController.MockedTokenIdentity = null;
            Controller = null;
        }

        protected void InitializeController(ITokenIdentity mockedTokenIdentity = null)
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }

            TargetController.MockedTokenIdentity = mockedTokenIdentity;
            Controller = new TargetController(UserManager, MockedDb.Object);
            Controller.Request = new HttpRequestMessage(); //for Request.
            Controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }
		
        [Test, Ignore]
        public async void TestLogin()
        {
            // Todo: Need to fully test this, ensure HTTP responses, etc.

            //Make sure to check the app.config for db
            var lr = new LoginRequest
            {
                Email = "danparker2761@yahoo.com",
                Password = "Password@123",
                HospitalId = 1,
                UserTypeId = 1
            };

            var ac = new AccountController(new Mock<ISnapAuthenticationManager>().Object, MockedDb.Object);

            //not complete yet
            IHttpActionResult result = await ac.Token(lr); //can't get the owin token cus the api doesn't start
            
            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void TestLoginV2()
        {
            // Todo: Need to fully test this, ensure HTTP responses, etc.

            //Make sure to check the app.config for db
            var lr = new LoginRequest
            {
                Email = "aaron.lord+patient@snap.md",
                Password = "Password@123",
                HospitalId = 126,
                UserTypeId = 1
            };
            
            var ac = new AccountController(new Mock<ISnapAuthenticationManager>().Object, MockedDb.Object);

            //not complete yet
            Assert.Throws<HttpResponseException>(async ()
                => {
                       var result = await ac.TokenV2(lr); //can't get the owin token cus the api doesn't start
                });
        }

        [Test]
        public void TestGetUserRoleFunctions()
        {
            var mock = new Mock<ISnapContext>();


            //var result = (from m in _context.Users
            //              join r in _context.UserRoles on m.UserId equals r.UserId
            //              join rl in _context.Roles on r.RoleId equals rl.RoleId
            //              join rf in _context.RoleFunctions on r.RoleId equals rf.RoleId
            //              join fnt in _context.SnapFunctions on rf.FunctionId equals fnt.FunctionId
            //              where (r.UserId == userId && r.IsActive == "A" && rl.IsActive == "A" && rf.IsActive == "A" && fnt.IsActive == "A" && rl.OnOff == "A")
            //              group m by new { rf.FunctionId } into n
            //              select n.Key.FunctionId).ToList();

            var u = Builder<User>.CreateListOfSize(20).All().With(x => x.IsActive = "A").Build();
            var ur = Builder<UserRole>.CreateListOfSize(20).All().With(x => x.IsActive = "A").Build();
            var r = Builder<Role>.CreateListOfSize(20).All().With(x => x.IsActive = "A").Build();
            var rf = Builder<RoleFunction>.CreateListOfSize(20).All().With(x => x.IsActive = "A").Build();
            var sf = Builder<SnapFunction>.CreateListOfSize(20).All().With(x => x.IsActive = "A").Build();

            mock.SetupGet(c => c.Users).Returns(u.ToDbSet());
            mock.SetupGet(c => c.UserRoles).Returns(ur.ToDbSet());
            mock.SetupGet(c => c.Roles).Returns(r.ToDbSet());
            mock.SetupGet(c => c.RoleFunctions).Returns(rf.ToDbSet());
            mock.SetupGet(c => c.SnapFunctions).Returns(sf.ToDbSet());

            var ac = new AccountController(null, mock.Object);
            
            IHttpActionResult result = ac.GetUserRoleFunctions();

            // Assert
            Assert.IsNotNull(result);
        }
		
		        /*
         * ChangePassword
         *      changes a loggedin users password, by his Oldpassword to NewPassword
         *      throws error if old password not found
         *      throw error if new password is not valid, see PasswordUtility
         */

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("Password123")]
        public void ChangePassword_Provided_OldPassword_Not_Same_Throws_Error(string providedOldPassword)
        {
            string realOldPasswordHash = "$2a$10$goLKkH9GiAc/VU0fTHMateaZRarGXQZLp9tjFA5frm4tO5.6uP3i."; //this hash is for Password@123
            string newPassword = "Password123123";

            ITokenIdentity tokenIdentity = Builder<TokenIdentity>.CreateNew().Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.UserId = tokenIdentity.UserId)
                .And(x => x.Password = realOldPasswordHash)
                .And(x => x.UpdateDate = null)
                .And(x => x.UpdatedBy = null)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            InitializeController(tokenIdentity);

            var error =
                Assert.Catch<HttpResponseException>(() => Controller.ChangePassword(providedOldPassword, newPassword));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Old password is invalid.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("password@123123")]   //no upper case letter
        public void ChangePassword_Provided_NewPassword_Not_Valid_Throws_Error(string newPassword)
        {
            string oldPassword = "Password@123";
            string oldhashPassword = "$2a$10$goLKkH9GiAc/VU0fTHMateaZRarGXQZLp9tjFA5frm4tO5.6uP3i.";

            ITokenIdentity tokenIdentity = Builder<TokenIdentity>.CreateNew().Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.UserId = tokenIdentity.UserId)
                .And(x => x.Password = oldhashPassword)
                .And(x => x.UpdateDate = null)
                .And(x => x.UpdatedBy = null)
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            InitializeController(tokenIdentity);

            var error = Assert.Catch<HttpResponseException>(() => Controller.ChangePassword(oldPassword, newPassword));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.That(error.Response.Content.ReadAsAsync<string>().Result,
                Is.StringContaining("New password is invalid."));
        }

        [Test]
        public void ChangePassword_Success_Returns_StatusCode_200()
        {
            string oldPassword = "Password@123";
            string oldhashPassword = "$2a$10$goLKkH9GiAc/VU0fTHMateaZRarGXQZLp9tjFA5frm4tO5.6uP3i.";
            string newPassword = "Password123123";

            ITokenIdentity tokenIdentity = Builder<TokenIdentity>.CreateNew().Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.UserId = tokenIdentity.UserId)
                .And(x => x.Password = oldhashPassword)
                .And(x => x.UpdateDate = null)
                .And(x => x.UpdatedBy = null)
                .Build();
            MockedDb.Setup(x => x.Users).Returns(new List<User> { user }.ToDbSet());
            InitializeController(tokenIdentity);

            DateTime utcNow = DateTime.UtcNow;
            IHttpActionResult result = Controller.ChangePassword(oldPassword, newPassword);
            Assert.IsInstanceOf<OkResult>(result);
            Assert.AreNotEqual(user.Password, oldhashPassword);
            Assert.LessOrEqual(utcNow, user.UpdateDate);
            Assert.AreEqual(tokenIdentity.UserId, user.UpdatedBy);
        }
		
        protected class TargetController : AccountController
        {
            public TargetController(ISnapAuthenticationManager userManager,
                ISnapContext context)
                : base(userManager, context)
            {
            }

            public static ITokenIdentity MockedTokenIdentity { get; set; }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    if (MockedTokenIdentity == null)
                    {
                        throw new NullReferenceException(
                            "controller trying to use user's TokenIdentity, need to set MockedTokenIdentity.");
                    }
                    return MockedTokenIdentity;
                }
            }
        }
    }
}