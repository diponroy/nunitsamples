﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Models;


namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class CoUserTokenControllerTest
    {
        protected Mock<ISnapContext> MockDb { get; set; }

        protected CoUserTokenController Controller { get; set; }

        [SetUp]
        public void Setup()
        {
            MockDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void Teardown()
        {
            MockDb = null;
            Controller = null;
        }

        protected void InitializeController()
        {
            if (MockDb == null)
            {
                throw new NullReferenceException("MockDb is null.");
            }
            Controller = new CoUserTokenController(MockDb.Object);
            Controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            Controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        //
        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Get_HospitalStaffToken_Token_NullOrEmpty_Throws_Error(string token)
        {
            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>().ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(new List<UserRolesTemp>().ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>().ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>().ToDbSet());
            InitializeController();

            var error = Assert.Catch<HttpResponseException>(() => Controller.HospitalStaffToken(token, 1));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("token required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(null)]
        [TestCase(0)]
        [TestCase(-1)]
        public void Get_HospitalStaffToken_HospitalId_NullOrLessThanOne_Throws_Error(int? hospitalId)
        {
            const string token = "theCoUserToken";
            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>().ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(new List<UserRolesTemp>().ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>().ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>().ToDbSet());
            InitializeController();

            var error = Assert.Catch<HttpResponseException>(() => Controller.HospitalStaffToken(token, hospitalId));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("hospitalId required.", error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        public void Get_HospitalStaffToken_CoUserToken_NotFound_By_Token_Returns_Null()
        {
            string token = "theCoUserToken";
            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>().ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(new List<UserRolesTemp>().ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>().ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>().ToDbSet());
            InitializeController();

            var result = Controller.HospitalStaffToken(token, 1);
            Assert.IsInstanceOf<ApiResponse<CoUserTokenDetail>>(result);
            Assert.IsNull(result.Data);
        }

        [Test]
        public void Get_HospitalStaffToken_HospitalStaffTemp_NotFound_By_HospitalId_Returns_Null()
        {
            const int hospitalId = 1;
            var coUsersTemp = Builder<CoUsersTemp>.CreateNew()
                .With(x => x.IsActive = "A")
                .Build();
            var rolesTemp = Builder<UserRolesTemp>.CreateListOfSize(2).All()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build().ToList();
            var coUserToken = Builder<CoUserToken>.CreateNew()
                .With(x => x.TokenStatus = "A")
                .And(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build();
            var staffTemp = Builder<HospitalStaffProfileTemp>.CreateNew()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .And(x => x.HospitalId = 100)   //not equal hospitalId
                .Build();

            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>() { coUsersTemp }.ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(rolesTemp.ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>() { coUserToken }.ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>() { staffTemp }.ToDbSet());
            InitializeController();

            var result = Controller.HospitalStaffToken(coUserToken.Token, hospitalId);
            Assert.IsInstanceOf<ApiResponse<CoUserTokenDetail>>(result);
            Assert.IsNull(result.Data);
        }

        [Test]
        [TestCase("A", true)]
        [TestCase("E", false)]
        [TestCase("I", false)]
        [TestCase("", false)]
        [TestCase(null, false)]
        public void Get_HospitalStaffToken_IsValid_Token(string tokenStatus, bool expectedIsValid)
        {
            const int hospitalId = 1;
            var coUsersTemp = Builder<CoUsersTemp>.CreateNew()
                .With(x => x.IsActive = "A")
                .Build();
            var rolesTemp = Builder<UserRolesTemp>.CreateListOfSize(2).All()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build().ToList();
            var coUserToken = Builder<CoUserToken>.CreateNew()
                .With(x => x.TokenStatus = tokenStatus)
                .And(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build();
            var staffTemp = Builder<HospitalStaffProfileTemp>.CreateNew()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .And(x => x.HospitalId = hospitalId)
                .Build();

            MockDb.Setup(x => x.Roles).Returns(new List<Role>().ToDbSet());
            MockDb.Setup(x => x.SnapFunctions).Returns(new List<SnapFunction>().ToDbSet());
            MockDb.Setup(x => x.RoleFunctions).Returns(new List<RoleFunction>().ToDbSet());
            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>() { coUsersTemp }.ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(rolesTemp.ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>() { coUserToken }.ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>() { staffTemp }.ToDbSet());
            InitializeController();

            var result = Controller.HospitalStaffToken(coUserToken.Token, hospitalId);
            Assert.IsInstanceOf<ApiResponse<CoUserTokenDetail>>(result);
            Assert.AreEqual(expectedIsValid, result.Data.IsValid);
        }

        [Test]
        [TestCase("A", "A", "A", "A", true)]
        [TestCase("A", "I", "A", "A", false)]
        [TestCase("A", "A", "I", "A", false)]
        [TestCase("A", "A", "A", "I", false)]
        public void Get_HospitalStaffToken_Success(string functionStatus, string roleStatus, string roleOnOff, string roleFunctionStatus, bool expectedHasPatientFacingPermission)
        {
            const int hospitalId = 1;
            var function = Builder<SnapFunction>.CreateNew()
                .With(x => x.FunctionId = 4)
                .And(x => x.Description = "Patient Facing")
                .And(x => x.IsActive = functionStatus)
                .Build();
            var role = Builder<Role>.CreateNew()
                .With(x => x.IsActive = roleStatus)
                .And(x => x.OnOff = roleOnOff)
                .Build();
            var roleFunction = Builder<RoleFunction>.CreateNew()
                .With(x => x.RoleId = role.RoleId)
                .And(x => x.FunctionId = function.FunctionId)
                .And(x => x.IsActive = roleFunctionStatus)
                .Build();

            var coUsersTemp = Builder<CoUsersTemp>.CreateNew()
                .With(x => x.IsActive = "A")
                .Build();
            var rolesTemp = Builder<UserRolesTemp>.CreateListOfSize(2).All()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build().ToList();
            var coUserToken = Builder<CoUserToken>.CreateNew()
                .With(x => x.TokenStatus = "A")
                .And(x => x.CoUserId = coUsersTemp.CoUserId)
                .Build();
            var staffTemp = Builder<HospitalStaffProfileTemp>.CreateNew()
                .With(x => x.CoUserId = coUsersTemp.CoUserId)
                .And(x => x.HospitalId = hospitalId)
                .Build();

            MockDb.Setup(x => x.Roles).Returns(new List<Role>() { role }.ToDbSet());
            MockDb.Setup(x => x.SnapFunctions).Returns(new List<SnapFunction>() { function }.ToDbSet());
            MockDb.Setup(x => x.RoleFunctions).Returns(new List<RoleFunction>() { roleFunction }.ToDbSet());
            MockDb.Setup(x => x.CoUsersTemps).Returns(new List<CoUsersTemp>() { coUsersTemp }.ToDbSet());
            MockDb.Setup(x => x.UserRolesTemps).Returns(rolesTemp.ToDbSet());
            MockDb.Setup(x => x.CoUserTokens).Returns(new List<CoUserToken>() { coUserToken }.ToDbSet());
            MockDb.Setup(x => x.HospitalStaffProfileTemps).Returns(new List<HospitalStaffProfileTemp>() { staffTemp }.ToDbSet());
            InitializeController();

            var result = Controller.HospitalStaffToken(coUserToken.Token, hospitalId);
            Assert.IsInstanceOf<ApiResponse<CoUserTokenDetail>>(result);
            CoUserTokenDetail data = result.Data;
            Assert.IsTrue(data.IsValid);
            Assert.AreEqual(coUsersTemp.EmailIdTemp, data.Email);
            Assert.AreEqual(expectedHasPatientFacingPermission, data.IsAuthorizedForPatientInteraction);
        }
    }
}
