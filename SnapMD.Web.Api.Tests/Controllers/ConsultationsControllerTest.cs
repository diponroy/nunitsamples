﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class ConsultationsControllerTest
    {
        protected class TargetController : ConsultationsController
        {
            public TargetController(ISnapContext mockContext)
                : base(mockContext)
            {
            }

            public static ITokenIdentity MockedTokenIdentity { get; set; }

            protected override ITokenIdentity AuthUser()
            {
                if (MockedTokenIdentity == null)
                {
                    throw new NullReferenceException(
                        "controller trying to use user's TokenIdentity, need to set MockedTokenIdentity.");
                }
                return MockedTokenIdentity;
            }
        }

        protected Mock<ISnapContext> MockedDb { get; set; }

        protected TargetController Controller { get; set; }

        [SetUp]
        public void SetUp()
        {
            MockedDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            MockedDb = null;

            TargetController.MockedTokenIdentity = null;
            Controller = null;
        }

        protected void InitializeController(ITokenIdentity mockedTokenIdentity)
        {
            if (MockedDb == null)
            {
                throw new NullReferenceException("Mocked Db is null.");
            }

            TargetController.MockedTokenIdentity = mockedTokenIdentity;
            Controller = new TargetController(MockedDb.Object);
            Controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            Controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        /*
         * Patient list to schedule appointment
         *      where patient profile is active
         *      patient profile is not dependent or authorized dependent
         *      for dependent profile the master user need to be active
         *      patient user active/inactive/whatever, no need to check as
         *          dependent may not be a user, 
         *          Admin should be able to schedule appointments for onboared users, where patient entity active, and user entity inactive)
         */

        [Test]
        [TestCase("A", 1)]
        [TestCase("I", 0)]
        [TestCase("", 0)]
        [TestCase(null, 0)]
        public void GetPatientsListForSchedule_Patient_Entity_NeedToBe_Active(string patientEntityStatus, int expectedListItem)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew()
                .Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.IsActive = "A")
                .And(x => x.HospitalId = identity.ProviderId)
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.IsActive = patientEntityStatus)
                .And(x => x.IsDependent = "N")
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            InitializeController(identity);

            var result = Controller.GetPatientsListForSchedule();
            Assert.IsInstanceOf<IEnumerable<UserList>>(result);
            Assert.AreEqual(expectedListItem, result.Count());
        }

        [Test]
        [TestCase("Y", false, 0)]
        [TestCase("", false, 0)]
        [TestCase(null, false, 0)]
        [TestCase("Y", true, 1)]
        [TestCase("", true, 1)]
        [TestCase(null, true, 1)]

        public void GetPatientsListForSchedule_Dependent_Patient_Entity_NeedToBe_Authorized(string isDependent, bool isAuthorized, int expectedListItem)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew()
                .Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.IsActive = "A")
                .And(x => x.HospitalId = identity.ProviderId)
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = null)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.IsActive = "A")
                .And(x => x.IsDependent = isDependent)
                .Build();
            patientProfile.UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>();

            /*authorization*/
            if (isAuthorized)
            {
                var authorization = Builder<UserDependentRelationAuthorization>.CreateNew()
                    .With(x => x.IsAuthorized = "Y")
                    .And(x => x.User = user)
                    .Build();
                patientProfile.UserDependentRelationAuthorizations.Add(authorization);
            }

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            InitializeController(identity);

            var result = Controller.GetPatientsListForSchedule();
            Assert.IsInstanceOf<IEnumerable<UserList>>(result);
            Assert.AreEqual(expectedListItem, result.Count());
        }

        [Test]
        [TestCase("Y", "A", 1)]
        [TestCase("N", "A", 0)]
        [TestCase("Y", "I", 0)]
        [TestCase("", "", 0)]
        [TestCase(null, null, 0)]

        public void GetPatientsListForSchedule_Dependent_Patient_Entity_NeedToBe_Authorized(string isAuthorized, string isMasterUserActive , int expectedListItem)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew()
                .Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.IsActive = isMasterUserActive)
                .And(x => x.HospitalId = identity.ProviderId)
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = null)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.IsActive = "A")
                .And(x => x.IsDependent = "Y")
                .Build();

            /*authorization*/
            patientProfile.UserDependentRelationAuthorizations = new List<UserDependentRelationAuthorization>();
            var authorization = Builder<UserDependentRelationAuthorization>.CreateNew()
                .With(x => x.IsAuthorized = isAuthorized)
                .And(x => x.User = user)
                .Build();
            patientProfile.UserDependentRelationAuthorizations.Add(authorization);

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            InitializeController(identity);

            var result = Controller.GetPatientsListForSchedule();
            Assert.IsInstanceOf<IEnumerable<UserList>>(result);
            Assert.AreEqual(expectedListItem, result.Count());
        }


        [Test]
        [TestCase("A")]
        [TestCase("I")]
        [TestCase("")]
        [TestCase(null)]
        public void GetPatientsListForSchedule_No_Need_To_Check_UserEntity(string userEntityStatus)
        {
            ITokenIdentity identity = Builder<TokenIdentity>.CreateNew()
                .Build();
            User user = Builder<User>.CreateNew()
                .With(x => x.IsActive = userEntityStatus)
                .And(x => x.HospitalId = identity.ProviderId)
                .Build();
            PatientProfile patientProfile = Builder<PatientProfile>.CreateNew()
                .With(x => x.UserId = user.UserId)
                .And(x => x.HospitalId = user.HospitalId)
                .And(x => x.IsActive = "A")
                .And(x => x.IsDependent = "N")
                .Build();

            MockedDb.Setup(x => x.Users).Returns(new List<User>() { user }.ToDbSet());
            MockedDb.Setup(x => x.PatientProfiles).Returns(new List<PatientProfile>() { patientProfile }.ToDbSet());
            InitializeController(identity);

            var result = Controller.GetPatientsListForSchedule();
            Assert.IsInstanceOf<IEnumerable<UserList>>(result);
            Assert.AreEqual(1, result.Count());
        }
    }
}
