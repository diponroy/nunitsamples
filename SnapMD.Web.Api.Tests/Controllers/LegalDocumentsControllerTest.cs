﻿using System;
using System.Collections.Generic;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.ConnectedCare.AuthLibrary.Models;
using SnapMD.Core.Interfaces.Enumerations;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class LegalDocumentsControllerTest
    {
        private const int HospitalId = 1;
        private const string DefaultTermsAndConditionsText = "Default Terms And Conditions";
        private const string CustomTermsAndConditionsText = "My Terms And Conditions";

        private readonly DocumentType TOC = DocumentType.TermsAndConditions;

        private readonly List<DocumentTypes> _documentTypes;
        private readonly List<HospitalDocuments> _hospitalDocumentses; 

        public LegalDocumentsControllerTest()
        {
            _documentTypes = new List<DocumentTypes>
            {
                new DocumentTypes()
                {
                    DocumentType = (short) DocumentType.TermsAndConditions,
                    DefaultText = DefaultTermsAndConditionsText
                }
            };

            _hospitalDocumentses = new List<HospitalDocuments>
            {
                new HospitalDocuments()
                {
                    DocumentType = (short) DocumentType.TermsAndConditions,
                    HospitalId = HospitalId,
                    DocumentText = CustomTermsAndConditionsText
                }
            };
        }

        [Test]
        public void GET_GetDefaultDocumentTextIfCustomDocumentNotExist_ReturnDefaultText()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(new List<HospitalDocuments>().ToDbSet()); // Custom text NOT EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            var result = controller.Get((short)DocumentType.TermsAndConditions) as System.Web.Http.Results.OkNegotiatedContentResult<string>;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Content == DefaultTermsAndConditionsText);
        }

        [Test]
        public void GET_GetCustomDocumentTextIfCustomDocumentExist_ReturnCustomText()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.Customer });

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(_hospitalDocumentses.ToDbSet()); // Custom text EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            var result = controller.Get((short)DocumentType.TermsAndConditions) as System.Web.Http.Results.OkNegotiatedContentResult<string>;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Content == CustomTermsAndConditionsText);
        }

        [Test]
        public void GET_InvalidDocumentType_ReturnBadRequest()
        {
            LegalDocumentsController controller = new LegalDocumentsController(new Mock<ISnapAuthenticationManager>().Object, new Mock<ISnapContext>().Object, (int)UserTypeEnum.Customer);

            var result = controller.Get(25) as System.Web.Http.Results.BadRequestErrorMessageResult; // Document type with code 25 undefined!

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.InvalidDocumentType);
        }

        [Test]
        public void POST_InvalidDocumentType_ReturnBadRequest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(new List<HospitalDocuments>().ToDbSet()); // Custom text NOT EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            JObject o = new JObject(
                new JProperty("documentType", (short)25), // Document type with code 25 undefined!
                new JProperty("documentText", CustomTermsAndConditionsText)
            );

            var result = controller.Post(o) as System.Web.Http.Results.BadRequestErrorMessageResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.InvalidDocumentType);
        }

        [Test]
        public void POST_NewCustomDocument_NewCustomDocumentCreated()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(new List<HospitalDocuments>().ToDbSet()); // Custom text NOT EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);


            JObject o = new JObject(
                new JProperty("documentType", (short)TOC),
                new JProperty("documentText", CustomTermsAndConditionsText)
            );

            var result = controller.Post(o) as System.Web.Http.Results.OkNegotiatedContentResult<string>;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Content == LegalDocumentsController.Messages.DocumentCreated);
            
            //Check that new document added.
            //Assert.That(context.Object.HospitalDocuments.Count() == 1); //Add to Mock not work. 
        }

        [Test]
        public void POST_ExistedCustomDocument_ReturnBadRequest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(_hospitalDocumentses.ToDbSet()); // Custom text EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            JObject o = new JObject(
                new JProperty("documentType", (short)TOC),
                new JProperty("documentText", CustomTermsAndConditionsText)
            );

            var result = controller.Post(o) as System.Web.Http.Results.BadRequestErrorMessageResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.DocumentAlreadyExist);
        }

        [TestCase(UserTypeEnum.Customer)]
        [TestCase(UserTypeEnum.HospitalStaff)]
        public void POST_NewCustomDocumentByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, new Mock<ISnapContext>().Object, 1);

            JObject o = new JObject(
               new JProperty("documentType", (short)TOC),
               new JProperty("documentText", CustomTermsAndConditionsText)
           );

            var result = controller.Post(o) as System.Web.Http.Results.UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }


        [Test]
        public void PUT_InvalidDocumentType_ReturnBadRequest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, new Mock<ISnapContext>().Object, 1);

            var result = controller.Put(25, CustomTermsAndConditionsText) as System.Web.Http.Results.BadRequestErrorMessageResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.InvalidDocumentType);
        }

        [Test]
        public void PUT_NewCustomDocument_ReturnBadRequest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(new List<HospitalDocuments>().ToDbSet()); // Custom text NOT EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);


            var result = controller.Put((short)TOC, CustomTermsAndConditionsText) as System.Web.Http.Results.BadRequestErrorMessageResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.DocumentNotExist);
        }

        [Test]
        public void PUT_ExistedCustomDocument_DocumentUpdated()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(_hospitalDocumentses.ToDbSet()); // Custom text EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            var result = controller.Put((short)TOC, CustomTermsAndConditionsText) as System.Web.Http.Results.OkNegotiatedContentResult<string>;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Content == LegalDocumentsController.Messages.DocumentUpdated);
        }

        [TestCase(UserTypeEnum.Customer)]
        [TestCase(UserTypeEnum.HospitalStaff)]
        public void PUT_UpdateByNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, new Mock<ISnapContext>().Object, 1);

            var result = controller.Put((short)TOC, CustomTermsAndConditionsText) as System.Web.Http.Results.UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }

        [Test]
        public void DELETE_InvalidDocumentType_ReturnBadRequest()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, new Mock<ISnapContext>().Object, 1);

            var result = controller.Delete(25) as System.Web.Http.Results.BadRequestErrorMessageResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Message == LegalDocumentsController.Messages.InvalidDocumentType);
        }

        [Test]
        public void DELETE_ExistedCustomDocument_DocumentUpdated()
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)UserTypeEnum.SnapMDStaff }); //Only snap admin () could Post new documents

            var context = new Mock<ISnapContext>();
            context.SetupGet(x => x.DocumentTypes).Returns(_documentTypes.ToDbSet());
            context.SetupGet(x => x.HospitalDocuments).Returns(_hospitalDocumentses.ToDbSet()); // Custom text EXIST!

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, context.Object, 1);

            var result = controller.Delete((short)TOC) as System.Web.Http.Results.OkNegotiatedContentResult<string>;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
            Assert.That(result.Content == LegalDocumentsController.Messages.DocumentDeleted);
        }

        [TestCase(UserTypeEnum.Customer)]
        [TestCase(UserTypeEnum.HospitalStaff)]
        public void DELETE_UserNotAdmin_ReturnUnauthorized(UserTypeEnum userType)
        {
            var authenticationManager = new Mock<ISnapAuthenticationManager>();
            authenticationManager.Setup(x => x.FindById(It.IsAny<int>())).Returns(new UserLogin() { HospitalId = HospitalId, UserTypeId = (int)userType }); //User without admin permissions

            LegalDocumentsController controller = new LegalDocumentsController(authenticationManager.Object, new Mock<ISnapContext>().Object, 1);

            var result = controller.Delete((short)TOC) as System.Web.Http.Results.UnauthorizedResult;

            //Check thar we have correct result.
            Assert.IsNotNull(result);
        }
    }
}
