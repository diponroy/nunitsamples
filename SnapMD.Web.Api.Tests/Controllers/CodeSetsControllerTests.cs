﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.StatusCodes;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class CodeSetsControllerTests
    {
        protected Mock<ISnapContext> DbContext { get; set; }

        protected CodeSetsController Controller { get; set; }

        [SetUp]
        public void StartUp()
        {
            DbContext = new Mock<ISnapContext>();
        }

        [TearDown]
        public void TearDown()
        {
            DbContext = null;
            Controller = null;
        }

        protected void InitializeController()
        {
            if (DbContext == null)
            {
                throw new NullReferenceException("DbContext");
            }
            Controller = new CodeSetsController(DbContext.Object);
        }

        /*
         * returns contents (codes) to fill dropdown using CodeSet and Code table
         *      For spacific HospitalId, and Fields(CodeSet)
         *      Fields examples: "medicalconditions,medications,medicationallergies,consultprimaryconcerns,consultsecondaryconcerns"
         * 
         *  converts each field from fields to specific CodeSetEnum
         *  can handel multiple fields
         *  if requested filed is secondaryconcerns, returns the codes of primary concetns      
         */

        [Test]
        public void GetV2_Return_Fields()
        {
            int hospitalId = 1;
            int codeListSize = 2;

            /*medicalconditions*/
            CodeSet medicalConditionCodeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)CodeSetEnum.Medical_Conditions)
                    .And(x => x.Description = "Medical Conditions")
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> medicalConditionCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.Description = "Medical Condition Code")
                    .And(x => x.CodeSetId = medicalConditionCodeSet.CodeSetId)
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            /*medications*/
            CodeSet medicationCodeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)CodeSetEnum.Medications)
                    .And(x => x.Description = "Medications")
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> medicationCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = medicationCodeSet.CodeSetId)
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            DbContext.Setup(x => x.CodeSets)
                .Returns(new List<CodeSet> { medicalConditionCodeSet, medicationCodeSet }.ToDbSet());
            DbContext.Setup(x => x.Codes).Returns(medicalConditionCodes.Concat(medicationCodes).ToList().ToDbSet());
            InitializeController();


            ApiResponseV2<CodeSetResponse> result = Controller.GetV2(hospitalId, "medicalconditions");
            Assert.IsInstanceOf<ApiResponseV2<CodeSetResponse>>(result);
            Assert.IsInstanceOf<IEnumerable<CodeSetResponse>>(result.Data);
            Assert.AreEqual(1, result.Data.Count());


            /*selected codeset and codes for medicalcondition only*/
            CodeSetResponse medicalcondition = result.Data.First();
            Assert.AreEqual(medicalConditionCodeSet.Description, medicalcondition.Name);
            Assert.AreEqual(codeListSize, medicalcondition.Codes.Count(x => x.Text.Contains("Medical Condition Code")));

            /*codes*/
            Assert.IsTrue(
                medicalcondition.Codes.First().CodeId == medicalConditionCodes.First().CodeId
                && medicalcondition.Codes.First().Text == medicalConditionCodes.First().Description
                && medicalcondition.Codes.First().DisplayOrder == medicalConditionCodes.First().DisplayOrder
                );
            Assert.IsTrue(
                medicalcondition.Codes.Last().CodeId == medicalConditionCodes.Last().CodeId
                && medicalcondition.Codes.Last().Text == medicalConditionCodes.Last().Description
                && medicalcondition.Codes.Last().DisplayOrder == medicalConditionCodes.Last().DisplayOrder
                );
        }


        [Test]
        [TestCase("medicalconditions", CodeSetEnum.Medical_Conditions, "Medical Conditions")]
        [TestCase("medications", CodeSetEnum.Medications, "Medications")]
        [TestCase("medicationallergies", CodeSetEnum.Medication_Allergies, "Medication Allergies")]
        [TestCase("consultprimaryconcerns", CodeSetEnum.Patients_Primary_Concern, "Patients Primary Concern")]
        public void GetV2_FieldString_To_Enum_Convirsion(string fileldName,
            CodeSetEnum codeSetEnum,
            string codeSetDescription)
        {
            int hospitalId = 1;
            int codeListSize = 2;
            CodeSet codeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)codeSetEnum)
                    .And(x => x.Description = codeSetDescription)
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> codes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = (int)codeSetEnum)
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();


            DbContext.Setup(x => x.CodeSets).Returns(new List<CodeSet> { codeSet }.ToDbSet());
            DbContext.Setup(x => x.Codes).Returns(codes.ToDbSet());
            InitializeController();

            CodeSetResponse result = Controller.GetV2(hospitalId, fileldName).Data.First();
            Assert.AreEqual(codeSet.Description, result.Name);
            Assert.AreEqual(codeListSize, result.Codes.Count());
        }

        [Test]
        public void GetV2_Returns_Codes_For_Multiple_FieldName()
        {
            int hospitalId = 1;
            int codeListSize = 2;

            /*medicalconditions*/
            CodeSet medicalConditionCodeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)CodeSetEnum.Medical_Conditions)
                    .And(x => x.Description = "Medical Conditions")
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> medicalConditionCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = medicalConditionCodeSet.CodeSetId)
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            /*medications*/
            CodeSet medicationCodeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)CodeSetEnum.Medications)
                    .And(x => x.Description = "Medications")
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> medicationCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = medicationCodeSet.CodeSetId)
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            /*mock*/
            DbContext.Setup(x => x.CodeSets)
                .Returns(new List<CodeSet> { medicalConditionCodeSet, medicationCodeSet }.ToDbSet());
            DbContext.Setup(x => x.Codes).Returns(medicalConditionCodes.Concat(medicationCodes).ToList().ToDbSet());
            InitializeController();

            /*result*/
            IEnumerable<CodeSetResponse> result = Controller.GetV2(hospitalId, "medicalconditions, medications").Data;
            Assert.AreEqual(2, result.Count());

            CodeSetResponse medicalcondition = result.First();
            Assert.AreEqual(medicalConditionCodeSet.Description, medicalcondition.Name);
            Assert.AreEqual(codeListSize, medicalcondition.Codes.Count());

            CodeSetResponse medication = result.Last();
            Assert.AreEqual(medicationCodeSet.Description, medication.Name);
            Assert.AreEqual(codeListSize, medication.Codes.Count());
        }

        [Test]
        public void GetV2_Returns_PrimaryConcern_Codes_For_SecondaryConcerns()
        {
            int hospitalId = 1;
            int codeListSize = 2;

            CodeSet codeSet =
                Builder<CodeSet>.CreateNew()
                    .With(x => x.CodeSetId = (int)CodeSetEnum.Patients_Secondary_Concern)
                    .And(x => x.Description = "Secondary Concerns")
                    .And(x => x.IsActive = "A")
                    .Build();
            List<Code> secondaryConcernCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = codeSet.CodeSetId)
                    .And(x => x.Description = "Secondary Concern Code")
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            /*primary codes*/
            List<Code> primaryConcernCodes =
                Builder<Code>.CreateListOfSize(codeListSize)
                    .All()
                    .With(x => x.HospitalId = hospitalId)
                    .And(x => x.CodeSetId = (int)CodeSetEnum.Patients_Primary_Concern)
                    .And(x => x.Description = "Primary Concern Code")
                    .And(x => x.IsActive = "A")
                    .And(x => x.DisplayOrder = 0).Build().ToList();

            /*mock*/
            DbContext.Setup(x => x.CodeSets).Returns(new List<CodeSet> { codeSet }.ToDbSet());
            DbContext.Setup(x => x.Codes).Returns(primaryConcernCodes.Concat(secondaryConcernCodes).ToList().ToDbSet());
            InitializeController();


            CodeSetResponse result = Controller.GetV2(hospitalId, "consultsecondaryconcerns").Data.First();
            Assert.AreEqual(codeSet.Description, result.Name);
            Assert.AreEqual(codeListSize, result.Codes.Count(x => x.Text.Contains("Primary Concern Code")));    /*selected primary concern codes, rather than secondary*/
        }
    }
}
