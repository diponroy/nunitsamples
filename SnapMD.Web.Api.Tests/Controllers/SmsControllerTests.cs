﻿using System;
using System.Linq;
using System.Web.Http;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.ConnectedCare.ApiModels.Sms;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class SmsControllerTests
    {
        private ISnapContext _mockContext;

        [Test]
        public void TestGet()
        {
            // Arrange
            var controller = new SmsController(_mockContext);

            // Act
            ApiResponseV2<EncounterSms> actionResult = controller.Get(2);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, contentResult.Count); 
        }

        [Test, Ignore]
        public void TestPost()
        {
            // Arrange
            const int smsId = 0;
            var controller = new SmsController(_mockContext);
            var toAdd = CreateSmsModel(0, 25);

            // Act
            var actionResult = controller.Post(toAdd);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null.");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null.");
            var afterPostContentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, afterPostContentResult.Count, "size of returned data should be 1 element");
            Assert.Greater(afterPostContentResult[0].SmsId, 0);
        }


        [Test]
        public void TestPut()
        {
            // Arrange
            var controller = new SmsController(_mockContext);

            // Act - verify there are 3 smss
            var actionResult = controller.Get(5);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, contentResult.Count);
            Assert.AreEqual(1, actionResult.Total);

            // Act - update one sms
            var sms = CreateSmsModel(5, 11);
            sms.Status = SmsStatus.Sent;
            sms.Sent = DateTime.UtcNow;
            actionResult = controller.Put(sms);

            // Assert
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(1, actionResult.Data.Count(), "result should return exactly 1 element");
            var singlesmsResult = actionResult.Data.FirstOrDefault();
            Assert.IsNotNull(singlesmsResult, "Returned sms should not be null.");
            Assert.AreEqual(sms.Sent.Value, singlesmsResult.Sent);
        }


        /// Test V2 Delete
        /// </summary>
        [Test]
        public void TestNotFound()
        {
            // Arrange
            var controller = new SmsController(_mockContext);

            // Act
            Assert.Throws<HttpResponseException>(() => controller.Get(11));
        }

        /// <summary>
        /// Test V2 Delete
        /// </summary>
        [Test]
        public void TestDelete()
        {
            // Arrange
            var controller = new SmsController(_mockContext);

            // Act
            var actionResult = controller.Get(4);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, contentResult.Count);

            // Act - do delete
            controller.Delete(contentResult[0].SmsId);
            
            // Act - Get the new count of sms smss for id 4
            Assert.Throws<HttpResponseException>(() => controller.Get(4));

        }


        [SetUp]
        public void Setup()
        {
            var smsRecords = new MockDbSet<SmsConsultation>();

            var mockContext = new Mock<ISnapContext>();
            mockContext.SetupGet(c => c.SmsConsultations).Returns(smsRecords);
            _mockContext = mockContext.Object;

            smsRecords.Add(CreateSms(1, 11));
            smsRecords.Add(CreateSms(2, 24, ConsultationEvents.EndedConsultation, SmsStatus.Pending));
            smsRecords.Add(CreateSms(3, 11, ConsultationEvents.JoinConsultation, SmsStatus.Invalid));
            smsRecords.Add(CreateSms(4, 35, ConsultationEvents.ReviewConsultation, SmsStatus.NoTextAlerts));
            smsRecords.Add(CreateSms(5, 11, ConsultationEvents.StartedConsultation, SmsStatus.Sent));
            _mockContext.SaveChanges();
        }

        private SmsConsultation CreateSms(int id,
            int consultationId, ConsultationEvents eventId = ConsultationEvents.WaitingConsultation, SmsStatus status = SmsStatus.New)
        {
            var sms = new SmsConsultation
            {
                SmsId = id,
                ConsultationId = consultationId,
                Created = DateTime.UtcNow,
                EventId = (int)eventId,
                Status = (int)status
            };
            return sms;
        }

        private EncounterSms CreateSmsModel(int id,
          int consultationId, ConsultationEvents eventId = ConsultationEvents.WaitingConsultation, SmsStatus status = SmsStatus.New)
        {
            var sms = new EncounterSms
            {
                SmsId = id,
                ConsultationId = consultationId,
                Created = DateTime.UtcNow,
                EventId = eventId,
                Status = status
            };
            return sms;
        }
    }
}
