﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Interfaces;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    internal class LocationsControllerTests
    {
        [Test]
        public void TestGet()
        {
            var mock = new Mock<ISnapContext>();
            var mockLocations = Builder<Location>.CreateListOfSize(5).Build();
            var mockOrganizations = Builder<Organization>.CreateListOfSize(5).Build();
            for (var i = 0; i < mockLocations.Count(); i++)
            {
                var item = mockLocations.ElementAt(i);
                item.Organization = mockOrganizations.ElementAt(i);
            }

            mock.Setup(m => m.Set<Location>()).Returns(mockLocations.ToDbSet());
            var controller = new TestController(new LocationsRepository(mock.Object));
            controller.ProviderId = 2;

            ApiResponseV2<Location> actual = null;
            Assert.DoesNotThrow(() => actual = controller.Get());
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Data.Count());
            var result = actual.Data.FirstOrDefault();
            Assert.IsNotNull(result);
            Assert.AreEqual("Name2", result.Name);
        }

        [Test]
        public void TestUpsert()
        {
            var mock = new Mock<ISnapContext>();
            var mockDbSet = Builder<Location>.CreateListOfSize(5).Build();
            var set = mockDbSet.ToDbSet();
            mock.Setup(m => m.Set<Location>()).Returns(set);
            mock.SetupGet(m => m.Locations).Returns(set);
            var controller = new TestController(new LocationsRepository(mock.Object));
            //To Do, have to build a organization and then a location
            //var target = new Location {  Id = 3, OrganizationId };

            /*mock.Setup(m => m.SetAdded(target)).Callback(() => target.Id = mockDbSet.Max(m => m.Id) + 1);
            mock.Setup(m => m.SaveChanges()).Callback(() => mockDbSet.Add(target));
            Assert.DoesNotThrow(() => controller.Post(target));
            Assert.AreEqual(6, target.Id);
            Assert.AreEqual(6, mockDbSet.Count);*/
        }

        private class TestController : LocationsController
        {
            public TestController(ILocationsRepository locationsRepository) : base(locationsRepository)
            {
            }

            public int ProviderId { private get; set; }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    var mock = new Mock<ITokenIdentity>();
                    mock.SetupGet(m => m.ProviderId).Returns(ProviderId);
                    return mock.Object;
                }
            }
        }
    }
}
