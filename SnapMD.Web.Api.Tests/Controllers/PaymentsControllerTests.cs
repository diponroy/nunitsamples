﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AuthorizeNet;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Billing;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Billing;
using SnapMD.Utilities;
using SnapMD.Web.Api.Patients.Controllers;
using System.Security.Claims;
using System.Security.Principal;
using SnapMD.ConnectedCare.ApiModels.Payments;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class PaymentsControllerTests
    {
        [Test, Explicit]
        public void TestGetCustomerWithNoSettings()
        {
            var built = Builder<BillingProfile>.CreateListOfSize(10).Build();

            var mock = new Mock<ISnapContext>();
            mock.SetupGet(c => c.BillingProfiles).Returns(built.ToDbSet());
            mock.SetupGet(c => c.HospitalSettings).Returns(new List<HospitalSetting>().ToDbSet());

            var target = new PaymentsController(mock.Object);
            Assert.Throws<SnapServiceException>(() => { var actual = target.GetProfile(1, 1); });
        }


        [Test, Explicit]
        public void TestNullCustomer()
        {
            var built = Builder<BillingProfile>.CreateListOfSize(10).Build();
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(c => c.BillingProfiles).Returns(built.ToDbSet());
            var settings = MakeFakeHospitalSettings();
            mock.SetupGet(c => c.HospitalSettings).Returns(settings.ToDbSet());
            var target = new PaymentsController(mock.Object);
            var actual = target.GetProfile(11, 1);
            Assert.IsNull(actual);
        }

        private static List<HospitalSetting> MakeFakeHospitalSettings()
        {
            var settings = new List<HospitalSetting>
            {
                new HospitalSetting
                {
                    HospitalId = 1,
                    Key = "AuthorizeNet_LoginID",
                    Value = "867hTNWkm8"
                },
                new HospitalSetting
                {
                    HospitalId = 1,
                    Key = "AuthorizeNet_TransactionKey",
                    Value = "7824m2DxJw3gGx3k"
                },
                new HospitalSetting
                {
                    HospitalId = 1,
                    Key = "AuthorizeNet_PaymentMethod",
                    Value = "AUTH_CAPTURE"
                },
                new HospitalSetting
                {
                    HospitalId = 1,
                    Key = "AuthorizeNet_PostURL",
                    Value = "test"
                }
            };
            return settings;
        }

        [Test]
        public void TestCreateProfile()
        {
            Mock<ISnapContext> mock = BuildMockDB();
            mock.Setup(m => m.Dispose()).Verifiable();

            Mock<IAuthorizeNetSettings> mockSettings = GatewaySettings.MakeSettingsMock();

            string hashCode = String.Format("{0:X}", DateTime.Now.GetHashCode());
            var emailHash = string.Format("aaron.lord+{0}@snap.md", hashCode).ToLower();
            Mock<ITokenIdentity> mockUser = BuildMockUser(emailHash);

            var paymentData = BuildMockPaymentInfo();
            Assert.IsTrue(paymentData.IsValid());

            var target = new PaymentsController(mock.Object);
            var actual = target.RegisterNewProfile(paymentData, mockSettings.Object, mockUser.Object);
            Assert.IsNotNull(actual);
            Assert.Greater(int.Parse(actual["profileId"]), 310900);
            Assert.Greater(int.Parse(actual["paymentProfileId"]), 3109700);

            //mock.Verify(m => m.Dispose(), Times.Once());
        }

        [Test]
        public void TestDuplicateCustomerParsing()
        {
            var email = "aaron.lord+toddg@snap.md";

            Mock<ISnapContext> mock = BuildMockDB();
            Mock<IAuthorizeNetSettings> mockSettings = GatewaySettings.MakeSettingsMock();
            Mock<ITokenIdentity> mockUser = BuildMockUser(email);

            var settings = mockSettings.Object;
            var target = new PaymentsController(mock.Object);
            ICustomerGateway gateway = new CustomerGateway(
               settings.LoginId, settings.TransactionKey, (ServiceMode)settings.ServiceMode);

            // "Error processing request: E00039 - A duplicate record with ID 31824893 already exists."
            Assert.DoesNotThrow(() => target.HandleCustomerRegistration(mockUser.Object, gateway));
        }

        private static Mock<IPrincipal> MakePrincipalMock()
        {
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();

            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim("name", "sameerfairgoogl@gmail.com"));
            claims.Add(new Claim("nameidentifier", "1"));
            claims.Add(new Claim("provider", "0"));

            ClaimsIdentity c = new ClaimsIdentity(claims);

            mockPrincipal.Setup(e => e.IsInRole("Admin")).Returns(true);
            //mockIIdentity.Setup(e=>e.Name).Returns("sameer");
            mockPrincipal.Setup(e => e.Identity).Returns(c);//mockIIdentity.Object);
            //More detail to add later for token

            return mockPrincipal;
        }

        private static PaymentData BuildMockPaymentInfo()
        {
            var paymentData = Builder<PaymentData>.CreateNew().Build();
            paymentData.CardNumber = "4111111111111111";
            paymentData.ExpiryMonth = 12;
            paymentData.ExpiryYear = DateTime.Today.Year;
            paymentData.Cvv = 123.ToString();
            return paymentData;
        }

        private static Mock<ISnapContext> BuildMockDB()
        {
            var mock = new Mock<ISnapContext>();

            IList<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = 1,
                UserName = "automatedtest@snap.md",
                ProfileImage = "asdf",
                Email = "automatedtest@snap.md",
                HospitalId = 1,
                TimeZoneId = 1
            });

            var patients = new List<PatientProfile>
            {
                
            new PatientProfile	
            {
                PatientId = 2,
                UserId = 1,
                PatientName = "PatientName",
                LastName = "PatientLastName",
                IsActive = "A" //important
            }};
            mock.SetupGet(m => m.Users).Returns(userList.ToDbSet());
            mock.SetupGet(x => x.PatientProfiles).Returns(patients.ToDbSet());
            mock.SetupGet(c => c.BillingProfiles).Returns(new List<BillingProfile>{new BillingProfile
            {
                CreateDate = DateTime.UtcNow,
                CreatedBy = 1,
                CustomerProfileId = 1,
                HospitalId = 1,
                UpdateDate = DateTime.UtcNow,
                UpdatedBy = 1,
                UserId = 1
            }}.ToDbSet());

            var settings = MakeFakeHospitalSettings();
            mock.SetupGet(c => c.HospitalSettings).Returns(settings.ToDbSet());




            return mock;
        }

        private static Mock<ITokenIdentity> BuildMockUser(string email)
        {
            var mockUser = new Mock<ITokenIdentity>();

            mockUser.SetupGet(m => m.ProviderId).Returns(1);
            mockUser.SetupGet(m => m.UserId).Returns(1);

            mockUser.SetupGet(m => m.User).Returns(email);
            return mockUser;
        }


        [Test, Explicit]
        public void Payment_GetAuthorizedCustomerProfile()
        {
            //Mock SnapContext to implement Patient Repo
            Mock<ISnapContext> contextMock = BuildMockDB();

            Mock<IAuthorizeNetSettings> AuthorizeNetSettingsMock = GatewaySettings.MakeSettingsMock();
            contextMock = BuildMockDB();

            string hashCode = String.Format("{0:X}", DateTime.Now.GetHashCode());
            var emailHash = string.Format("aaron.lord+{0}@snap.md", hashCode).ToLower();

            Mock<ITokenIdentity> mockUser = BuildMockUser(emailHash);

            var paymentData = BuildMockPaymentInfo();

            Assert.IsTrue(paymentData.IsValid());

            var controller = new PaymentsController(contextMock.Object);
            controller.User = MakePrincipalMock().Object;

            var actual = controller.RegisterNewProfile(paymentData, AuthorizeNetSettingsMock.Object, mockUser.Object);

            var response = controller.GetAuthorizedCustomerProfile(1);

            Assert.IsTrue(response.GetType() == typeof(ApiResponseV2<CimCustomer>));

            Assert.IsTrue((response).Success);
        }


        [Test, Explicit]
        public void Payment_GetPaymentProfiles()
        {
            //Mock SnapContext to implement Patient Repo
            Mock<ISnapContext> contextMock = BuildMockDB();

            Mock<IAuthorizeNetSettings> AuthorizeNetSettingsMock = GatewaySettings.MakeSettingsMock();
            contextMock = BuildMockDB();

            string hashCode = String.Format("{0:X}", DateTime.Now.GetHashCode());
            var emailHash = string.Format("aaron.lord+{0}@snap.md", hashCode).ToLower();

            Mock<ITokenIdentity> mockUser = BuildMockUser(emailHash);

            var paymentData = BuildMockPaymentInfo();

            Assert.IsTrue(paymentData.IsValid());

            var controller = new PaymentsController(contextMock.Object);
            controller.User = MakePrincipalMock().Object;

            var actual = controller.RegisterNewProfile(paymentData, AuthorizeNetSettingsMock.Object, mockUser.Object);

            var response = controller.GetCustomerProfile();
            Assert.IsInstanceOf<ApiResponseV2<CimCustomer>>(response);
        }

    }
}