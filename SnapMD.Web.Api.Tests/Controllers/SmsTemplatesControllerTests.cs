﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class SmsTemplatesControllerTests
    {
        private ISnapContext _mockContext;

        #region Tests
        
        #region GET
        /// <summary>
        /// Test V1 of Get()
        /// </summary>
        [Test]
        public void V1Get()
        {
            // Arrange
            var controller = new SmsTemplatesController(_mockContext);

            // Act
            var actionResult = controller.Get(11);
            var contentResult = actionResult as OkNegotiatedContentResult<SmsTemplate[]>;

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(contentResult, "contentResult should not be null");
            Assert.IsNotNull(contentResult.Content, "contentResult.Content should not be null");
            Assert.AreEqual(3, contentResult.Content.Length, "There are 3 active templates for hospitalid=11");
                // (see Setup() method)
        }

        /// <summary>
        /// Test v2 of the Get(), which returns an ApiResponseV2, instead of the older one using Ok();
        /// </summary>
        [Test]
        public void V2Get()
        {
            // Arrange
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            // Act
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(11);
            
            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(3, contentResult.Count, "There are 3 active templates for hospitalid=11");    // (see Setup() method)
        }

        /// <summary>
        /// Test GetV2 with non-existent hospital
        /// </summary>
        [Test]
        public void V2GetWithNonExistentHospital()
        {
            // Arrange
            const int nonExistentHospitalId = 9999;
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            // Act
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(nonExistentHospitalId);
            
            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(0, contentResult.Count, "There should be 0 templates for non-existent hospital id " + nonExistentHospitalId);
        }

        /// <summary>
        /// Test GetV2, using a negative id
        /// </summary>
        [Test]
        public void V2GetWithNegativeHospitalId()
        {
            // Arrange
            const int invalidHospitalId = -9999;
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);
            TestDelegate td = () => controller.GetV2(invalidHospitalId);

            // Act & Assert
            Assert.Throws<ArgumentException>(td, "GetV2 should throw on invalid hospital id value (negative).");
        }

        #endregion

        #region POST

        /// <summary>
        /// Test AddV1
        /// </summary>
        [Test]
        public void V1Add()
        {
            // Arrange
            const int hospitalId = 68;
            string smsText = "sms template for hospital " + hospitalId;
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);
            SmsTemplate toAdd = new SmsTemplate()
            {
                HospitalId = hospitalId,
                SmsText = smsText,
                Type = "Consultation_SMS",
                IsActive = "Y"
            }; 
            
            // Assert
            IHttpActionResult actionResult = controller.Get(hospitalId);
            var contentResult = actionResult as OkNegotiatedContentResult<SmsTemplate[]>;

            Assert.IsNotNull(contentResult, "contentResult should not be null.");
            Assert.AreEqual(0, contentResult.Content.Length, "There should be no items for hospitalid = " + hospitalId + ".");
            
            // Act
            actionResult = controller.Post(toAdd);
            var afterPostContentResult = actionResult as OkNegotiatedContentResult<SmsTemplate>;

            // Assert
            Assert.IsNotNull(afterPostContentResult, "afterPostContentResult should not be null.");
            Assert.AreEqual(hospitalId, afterPostContentResult.Content.HospitalId, "hospital ids should match.");
            Assert.AreEqual(smsText, afterPostContentResult.Content.SmsText, "SMS text should be match.");
        }

        /// <summary>
        /// Basic test of AddV2
        /// </summary>
        [Test]
        public void V2Add()
        {
            // Arrange
            const int hospitalId = 68;
            string smsText = "sms template for hospital " + hospitalId;
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);
            SmsTemplate toAdd = new SmsTemplate()
            {
                HospitalId = hospitalId,
                SmsText = smsText,
                Type = "Consultation_SMS",
                IsActive = "Y"
            };

            // Assert
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(hospitalId);
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null.");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(0, contentResult.Count, "There should be no items for hospitalid = " + hospitalId + ".");

            // Act
            actionResult = controller.PostV2(toAdd);
            

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null.");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null.");
            var afterPostContentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, afterPostContentResult.Count, "size of returned data should be 1 element");
            Assert.AreEqual(hospitalId, afterPostContentResult[0].HospitalId, "hospital ids should match.");
            Assert.AreEqual(smsText, afterPostContentResult[0].SmsText, "SMS text should be match.");
        }

        /// <summary>
        /// V2 Add with missing information.  Should throw an argument exception
        /// </summary>
        [Test]
        public void V2AddWithMissingInformation()
        {
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);
            SmsTemplate toAdd = new SmsTemplate()
            {
                HospitalId = 10,
                SmsText = string.Empty,
                Type = "Consultation_SMS",
                IsActive = "Y"
            };

            TestDelegate td = () => controller.PostV2(toAdd);
            Assert.Throws<ArgumentException>(td, "Missing SmsText should throw an exception.");
            SmsTemplate toAdd2 = new SmsTemplate()
            {
                HospitalId = 10,
                SmsText = "some string",
                Type = string.Empty,
                IsActive = "Y"
            };

            TestDelegate td2 = () => controller.PostV2(toAdd2);
            Assert.Throws<ArgumentException>(td, "Missing Type field should throw an exception.");
        }

        #endregion

        #region PUT

        /// <summary>
        /// Test V2 PUT to update a template from active to inactive
        /// </summary>
        [Test]
        public void V2PutActiveToInactive()
        {
            // Arrange
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            // Act - verify there are 3 templates
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(11);
            
            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(3, contentResult.Count, "There are 3 active templates for hospitalid=11");    // (see Setup() method)
            
            // Act - update one template
            SmsTemplate template = Clone(contentResult[0]);
            template.IsActive = "N";
            actionResult = controller.PutV2(template.Id, template);
            
            
            // Assert
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            Assert.AreEqual(1, actionResult.Data.Count(), "result should return exactly 1 element");
            var singleTemplateResult = actionResult.Data.FirstOrDefault();
            Assert.IsNotNull(singleTemplateResult, "Returned template should not be null.");
            Assert.AreEqual("N", singleTemplateResult.IsActive);

            // Act - verify that there are 2 templates left
            actionResult = controller.GetV2(11);
            contentResult = actionResult.Data.ToList();

            // Assert
            Assert.AreEqual(2, contentResult.Count, "There should now be only 2 active templates for hospitalid=11");   
        }

        [Test]
        public void V2PutHospitalAndText()
        {
            // Arrange
            const int hospitalId = 24; // this one has only 1 template (see Setup() method)
            const int newHospitalId = 52;
            const string newSmsText = "new sms text here.";
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            // Act - verify there are 3 templates
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(hospitalId);
            
            // Assert
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, contentResult.Count, "There is 1 active template for hospitalid=" + hospitalId);    // (see Setup() method)
            
            SmsTemplate template = Clone(contentResult[0]);
            template.HospitalId = newHospitalId;
            template.SmsText = newSmsText;

            // Act - call Put to update
            actionResult = controller.PutV2(template.Id, template);
            var singleTemplateContentResult = actionResult.Data.First();
            
            // Assert
            Assert.IsNotNull(singleTemplateContentResult, "Returned template should not be null");
            Assert.AreEqual(hospitalId, singleTemplateContentResult.HospitalId, "Hospital id of returned template should still be the same");
            Assert.AreEqual(newSmsText, singleTemplateContentResult.SmsText, "SmsText should have been updated.");

            // Act - query using Get()
            actionResult = controller.GetV2(newHospitalId);
            
            // Assert
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            contentResult = actionResult.Data.ToList();
            Assert.AreEqual(0, contentResult.Count, "No templates should have been transferred");

            // Act - verify the count from the original hospital id
            actionResult = controller.GetV2(hospitalId);

            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            contentResult = actionResult.Data.ToList();
            Assert.AreEqual(1, contentResult.Count, "There should still be one template related to hospital =" + hospitalId);
        }

        #endregion // PUT

        #region DELETE

        /// <summary>
        /// Test V1 Delete
        /// </summary>
        [Test]
        public void V1Delete()
        {
            
        }

        /// <summary>
        /// Test V2 Delete
        /// </summary>
        [Test]
        public void V2Delete()
        {
            // Arrange
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            // Act
            ApiResponseV2<SmsTemplate> actionResult = controller.GetV2(11);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            var contentResult = actionResult.Data.ToList();
            Assert.AreEqual(3, contentResult.Count, "There are 3 active templates for hospitalid=11");    // (see Setup() method)

            // Act - do delete
            ApiResponseV2<int> deleteActionResult = controller.DeleteV2(contentResult[0].Id);

            // Assert
            Assert.IsNotNull(deleteActionResult, "Delete action result should not be null");
            Assert.AreEqual(1, deleteActionResult.Data.Count(), "There should be only 1 element");
            Assert.AreEqual(contentResult[0].Id, deleteActionResult.Data.First(), "The id deleted should match the id returned");

            // Act - Get the new count of sms templates for hospital = 11
            actionResult = controller.GetV2(11);

            // Assert
            Assert.IsNotNull(actionResult, "actionResult should not be null");
            Assert.IsNotNull(actionResult.Data, "actionResult.Data should not be null");
            contentResult = actionResult.Data.ToList();
            Assert.AreEqual(2, contentResult.Count, "There should now be only 2 active templates for hospitalid=11");    // (see Setup() method)
        }

        /// <summary>
        /// Test deletion of non-existent id
        /// </summary>
        [Test]
        public void V2DeleteNonExistentId()
        {
            // Arrange
            const int nonExistentId = 999999;
            SmsTemplatesController controller = new SmsTemplatesController(_mockContext);

            TestDelegate td = () => controller.DeleteV2(nonExistentId);
            
            // Assert
            Assert.Throws<Exception>(td, "Should throw an exception with a non-existent id");
        }

        #endregion // DELETE

        #endregion // TESTS

        #region Helpers
        [SetUp]
        public void Setup()
        {
            var smsRepo = new MockDbSet<SmsTemplate>();

            var mockContext = new Mock<ISnapContext>();
            mockContext.SetupGet(c => c.SmsTemplates).Returns(smsRepo);
            _mockContext = mockContext.Object;


            // Create 5 smstexts.  4 are active, 1 inactive.  
            //   Of the 4 active, 3 belong to hospital 11, and 1 belong to hospital 24
            //   The 1 inactive belong to hospital 35
            //   id         hospitalId          isActive
            //    1                 11                 1
            //    2                 24                 1
            //    3                 11                 1
            //    4                 35                 0
            //    5                 11                 1
         
            smsRepo.Add(CreateSmsTemplate(1, 11, "quick brown fox", "Consultation_SMS", true));
            smsRepo.Add(CreateSmsTemplate(2, 24, "smsText2", "Consultation_SMS", true));
            smsRepo.Add(CreateSmsTemplate(3, 11, "lorem ipsum", "Consultation_SMS", true));
            smsRepo.Add(CreateSmsTemplate(4, 35, "this is sample text", "Consultation_SMS", false));
            smsRepo.Add(CreateSmsTemplate(5, 11, "jumps over the lazy dog", "Consultation_SMS", true));
            _mockContext.SaveChanges();
        }

        private SmsTemplate CreateSmsTemplate(int id, int hospitalId = 0, string smsText = "", string type = "", bool isActive = true)
        {
            var smsTemplate = new SmsTemplate
            {
                Id = id,
                HospitalId = hospitalId,
                SmsText = smsText,
                Type = type,
                IsActive = isActive ? "Y" : "N"
            };
            return smsTemplate;
        }

        /// <summary>
        /// Used for debugging purposes to peak at the contents of the repository
        /// </summary>
        private void PrintRepoContents()
        {
            foreach (var st in _mockContext.SmsTemplates)
            {
                Console.WriteLine(@"id = {0}, active = {1}, hospital = {2}", st.Id, st.IsActive, st.HospitalId);
            }
        }

        private SmsTemplate Clone(SmsTemplate template)
        {
            return new SmsTemplate
            {
                Id = template.Id,
                SmsText = template.SmsText,
                Type = template.Type,
                HospitalId = template.HospitalId,
                IsActive = template.IsActive
            };
        }

        #endregion

    }

}
