﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces.Models;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Consultations;
using SnapMD.Utilities;
using SnapMD.Web.Api.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class ReportsControllerTests
    {
        private ISnapContext _mockContext;

        [Test]
        public void V2TestDetails()
        {
            ReportsController rc = new ReportsController(_mockContext);

            var response = rc.DetailsV2(1, "");

            Assert.AreEqual(1, response.Data.Count());
            var consultationReport = response.Data.First();
            Assert.IsNotNull(consultationReport);
        }

        [SetUp]
        public void Setup()
        {
            var hospitalDbSet = Builder<Hospital>.CreateListOfSize(1).Build().ToDbSet();
            var consultationsDbSet = Builder<Consultation>.CreateListOfSize(1).Build().ToDbSet();
            var patientsDbSet = Builder<PatientProfile>.CreateListOfSize(1).Build().ToDbSet();
            var codesDbSet = Builder<Code>.CreateListOfSize(1).Build().ToDbSet();
            var pmhDbSet = Builder<PatientMedicalHistory>.CreateListOfSize(1).Build().ToDbSet();
            var hspDbSet = Builder<HospitalStaffProfile>.CreateListOfSize(1).Build().ToDbSet();
            var docSpecialtiesDbSet = Builder<DoctorSpeciality>.CreateListOfSize(1).Build().ToDbSet();
            var addressDbSet = Builder<Address>.CreateListOfSize(1).Build().ToDbSet();
            var cptCodes = Builder<CptCode>.CreateListOfSize(12).Build().ToDbSet();
            var cptSoapCodes = Builder<ConsultationSoapNotesCptCode>.CreateListOfSize(12).Build().ToDbSet();
            var consultationMedicalCodes = Builder<ConsultationMedicalCode>.CreateListOfSize(1)
                .All().With(x => x.MedicalCode = new MedicalCode
                {
                    MedicalCodeID = 1,
                    CodeValue = "AA345",
                    ShortDescription = "ShortDescription",
                    MedicalCodingSystem = new MedicalCodingSystem
                    {
                        Name = "ICD-10-DX"
                    }
                    //Other fields were skipped for brevity
                })
                .Build().ToDbSet();
            var users = Builder<User>.CreateListOfSize(50).Build().ToDbSet();
            var tz = Builder<StandardTimeZone>.CreateListOfSize(24).Build().ToDbSet();


            var patientConsultationReportsList = Builder<PatientConsultationReport>.CreateListOfSize(4).Build();
            int codeId = 107;
            foreach (var p in patientConsultationReportsList)
            {
                p.ConsultationId = 1;
                p.CodeId = codeId++;
            }

            var patientConsultationReports = patientConsultationReportsList.ToDbSet();

            var prescriptionInfo = Builder<PrescriptionInfo>.CreateListOfSize(14).Build().ToDbSet();
            
            var result = SetupEncounterMetadata();
            EncounterMetadata metadata = new EncounterMetadata { ConsultationId = 1, EncounterId = 1, Id = Guid.NewGuid(), Metadata = result };
            IList<EncounterMetadata> metadataList = new List<EncounterMetadata>();
            metadataList.Add(metadata);
            var encounterDbSet = metadataList.ToDbSet();
            
            var context = new Mock<ISnapContext>();
            context.SetupGet(a => a.Hospitals).Returns(hospitalDbSet);
            context.SetupGet(a => a.Consultations).Returns(consultationsDbSet);
            context.SetupGet(a => a.PatientProfiles).Returns(patientsDbSet);
            context.SetupGet(a => a.Codes).Returns(codesDbSet);
            context.SetupGet(a => a.PatientMedicalHistories).Returns(pmhDbSet);
            context.SetupGet(a => a.HospitalStaffProfiles).Returns(hspDbSet);
            context.SetupGet(a => a.DoctorSpecialities).Returns(docSpecialtiesDbSet);
            context.SetupGet(a => a.EncounterMetadata).Returns(encounterDbSet);
            context.SetupGet(a => a.Addresses).Returns(addressDbSet);
            context.SetupGet(a => a.PatientConsultationReports).Returns(patientConsultationReports);
            context.SetupGet(a => a.ConsultationSoapNotesCptCodes).Returns(cptSoapCodes);
            context.SetupGet(a => a.CptCodes).Returns(cptCodes);
            context.SetupGet(a => a.ConsultationMedicalCodes).Returns(consultationMedicalCodes);
            context.SetupGet(a => a.PrescriptionInfoes).Returns(prescriptionInfo);
            context.SetupGet(a => a.Users).Returns(users);
            context.SetupGet(a => a.StandardTimeZones).Returns(tz);

            _mockContext = context.Object;
            
        }

        private string SetupEncounterMetadata()
        {
            /** prepare encounter metadata **/
            var medicalHistory = new Mock<IMedicalHistory>();

            medicalHistory.SetupGet(h => h.MedicalCondition1).Returns(1);
            medicalHistory.SetupGet(h => h.MedicalCondition2).Returns(2);
            medicalHistory.SetupGet(h => h.AllergicMedication1).Returns(3);
            medicalHistory.SetupGet(h => h.TakingMedication1).Returns(55);

            var dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "I am a condition");
            dictionary.Add(2, "I am something, too");
            dictionary.Add(3, "Allergic to a med");

            var convertedMetadata = ConvertToMetadata(medicalHistory.Object, new EncounterConcern[0], dictionary);
            convertedMetadata.Wait();
            var result = convertedMetadata.Result.Metadata;
            return result;
        }

        private async Task<EncounterMetadata> ConvertToMetadata(
            IMedicalHistory medicalHistory, EncounterConcern[] concerns, IDictionary<int, string> dictionary)
        {
            var metadata = new EncounterMetadata();
            var adapter = new IntakeQuestionnaireAdapter(medicalHistory, concerns, dictionary, null);
            var x = await adapter.ToXmlAsync(XConversionOptions.ToCamelCase);
            metadata.Metadata = x;
            return metadata;
        }
    }
}
