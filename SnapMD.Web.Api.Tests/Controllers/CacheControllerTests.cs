﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Enumerations;
using SnapMD.Data.Entities.HelperModel;
using SnapMD.Web.Api.Auth;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Patients.Controllers;
using SnapMD.Web.Api.Utilities;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class CacheControllerTest
    {
        [Test]
        public void TestCacheDelete()
        {
            HttpContextUtility.MockCurrent("",
                "http://snap.local/api/v2/caches", "cacheType=userTimeZone");

            var controller = new CacheController();

            Assert.DoesNotThrow(() => controller.Delete(SnapCacheType.UserTimeZone));
        }
        
    }
}
