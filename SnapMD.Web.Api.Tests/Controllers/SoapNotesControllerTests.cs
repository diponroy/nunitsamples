﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Physicians.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class SoapNotesControllerTests
    {
        private ISnapContext _mockContext;

        #region Tests

        [Test]
        public void V2TestInsert()
        {
            var controller = new PhysicianSoapNotesController(_mockContext);

            SoapNoteDetails soapNoteDetails = new SoapNoteDetails
            {
                UserID = 1,
                PatientID = 1,
                ConsultationID = 1,
                Assessment = "test assessment",
                Subjective = "test subjective",
                Objective = "test objective"
            };

            var resp = controller.Insert(soapNoteDetails);
            Assert.IsTrue(resp.Data.First().Success);
        }

        #endregion

        #region Helpers / Setup

        [SetUp]
        public void Setup()
        {
            var consultationDbSet = Builder<Consultation>.CreateListOfSize(1).Build().ToDbSet();
            var patientDbSet = Builder<PatientProfile>.CreateListOfSize(1).Build().ToDbSet();
            var userDbSet = Builder<User>.CreateListOfSize(1).Build().ToDbSet();
            var patientConsultationDbSet = Builder<PatientConsultationReport>.CreateListOfSize(1).Build().ToDbSet();
            var consultationSoapNotesCptCodes = Builder<ConsultationSoapNotesCptCode>.CreateListOfSize(1).Build().ToDbSet();

            var context = new Mock<ISnapContext>();
            context.SetupGet(a => a.PatientProfiles).Returns(patientDbSet);
            context.SetupGet(a => a.Users).Returns(userDbSet);
            context.SetupGet(a => a.Consultations).Returns(consultationDbSet);
            context.SetupGet(a => a.PatientConsultationReports).Returns(patientConsultationDbSet);
            context.SetupGet(a => a.ConsultationSoapNotesCptCodes).Returns(consultationSoapNotesCptCodes);

            _mockContext = context.Object;
        }

        #endregion
    }
}
