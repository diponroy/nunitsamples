﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class PatientMedicalProfileControllerTests
    {
        private ISnapContext _mockContext;

        [Test]
        public void TestSimpleGet()
        {
            PatientMedicalProfileController controller = new PatientMedicalProfileController(_mockContext);
            var patientMedicalHistoryResponse = controller.GetV2(1);

            Assert.IsNotNull(patientMedicalHistoryResponse);
            Assert.IsNotNull(patientMedicalHistoryResponse.Data);
            Assert.AreEqual(1, patientMedicalHistoryResponse.Data.Count());
            Assert.AreEqual(1, patientMedicalHistoryResponse.Data.First().PatientId);
        }

        [SetUp]
        public void Setup()
        {
            var mockContext = new Mock<ISnapContext>();

            var patientHistoryRepo = Builder<PatientMedicalHistory>.CreateListOfSize(1).Build();
            mockContext.SetupGet(a => a.PatientMedicalHistories).Returns(patientHistoryRepo.ToDbSet());

            var patientProfiles = Builder<PatientProfile>.CreateListOfSize(1).All().With(a => a.UserId = 0).Build();    //UserId is set to 0, because SnapUser.UserId is blank
            mockContext.SetupGet(a => a.PatientProfiles).Returns(patientProfiles.ToDbSet());

            var clinicianProfiles = Builder<HospitalStaffProfile>.CreateListOfSize(1).Build();    //UserId is set to 0, because SnapUser.UserId is blank
            mockContext.SetupGet(a => a.HospitalStaffProfiles).Returns(clinicianProfiles.ToDbSet());

            var authorizations = Builder<UserDependentRelationAuthorization>.CreateListOfSize(1).Build();
            mockContext.SetupGet(a => a.UserDependentRelationAuthorizations).Returns(authorizations.ToDbSet());

            _mockContext = mockContext.Object;
        }
    }
}
