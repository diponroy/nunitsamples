﻿using System;
using System.Linq;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Encounters;
using SnapMD.Core.Interfaces;
using SnapMD.Core.Models;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Patients;
using SnapMD.Web.Api.Patients.Controllers;
using System.Collections.Generic;
using System.Web.Http;
using FizzWare.NBuilder;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.ConnectedCare.Sdk.Models;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class ConsultationControllerTests
    {
        private ISnapContext _mockContext;

        #region Tests

        /// <summary>
        /// Simple test of an existing patient
        /// </summary>
        [Test, Ignore("Does not work. Missing ethnicity codes, etc.")]
        public void V2GetAllPatientInfo()
        {
            // Arrange
            var controller = new TestController(_mockContext);
            int existingId = 1;

            // Act
            var result = controller.GetAllPatientinformationV2(existingId);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(1, result.Data.Count());

            var completePatientInfo = result.Data.First();
            Assert.IsNotNull(completePatientInfo.ConsultationInfo);
            Assert.AreEqual(1, completePatientInfo.ConsultationInfo.Id); // based on dummy data
            Assert.IsNotNull(completePatientInfo.IntakeForm);
            Assert.AreEqual(1, completePatientInfo.PatientInformation.PatientId);    // based on dummy data
            Assert.AreEqual("CptCode1", completePatientInfo.SoapNote.CptCode);             // based on dummy data
        }

        /// <summary>
        ///     UpdateIntake simple case
        /// </summary>
        [Test]
        public void V2UpdateIntakeThrowsException()
        {
            // Arrange
            PatientConsultationsController controller = new PatientConsultationsController(_mockContext);
            int consultationId = 1; // exists

            Assert.Throws<HttpResponseException>(() => controller.UpdateIntakeV2(consultationId, null),
                "input parameter must be non-null");
        }

        /// <summary>
        ///     UpdateIntake simple case
        [Test, Ignore("Need to bypass auth requests for testing.")]
        public void V2UpdateIntake()
        {
            // Arrange
            PatientConsultationsController controller = new PatientConsultationsController(_mockContext);
            int consultationId = 1; // exists

            Assert.Throws<ArgumentException>(() => controller.UpdateIntakeV2(consultationId, null)
                , "input parameter must be non-null");

            var facade = new ConsultationResponseFacade(_mockContext);
            var allInfo = facade.GetPatientInfoForConsultation(consultationId, 1);

            string origPrimary = allInfo.ConsultationInfo.PrimaryConcern;
            string origSecondary = allInfo.ConsultationInfo.SecondaryConcern;

            var intake = new IntakeQuestionnaire();
            string newPrimary = "new primary concern";
            string newSecondary = "new secondary concern";

            // assert - make sure they are not equal to begin with
            Assert.AreNotEqual(origPrimary, newPrimary);
            Assert.AreNotEqual(origSecondary, newSecondary);

            // act - update primary and secondary concerns
            intake.Concerns = new List<EncounterConcern>
            {
                new EncounterConcern {IsPrimary = true, CustomCode = new CustomCode(newPrimary)},
                new EncounterConcern {CustomCode = new CustomCode(newSecondary)}
            };

            controller.UpdateIntake(consultationId, intake, 1, 1, 1);

            // requery
            var allInfo2 = controller.GetAllPatientinformationV2(consultationId).Data.First();

            // assert - the queried values are equal to the new values
            Assert.AreEqual(newPrimary, allInfo2.IntakeForm.Concerns[0]);
            Assert.AreEqual(newSecondary, allInfo2.IntakeForm.Concerns[1]);
        }

        #endregion

        #region Helpers

        [SetUp]
        public void Setup()
        {
            _mockContext = new MockSnapContext();

            // Build Hospital records
            _mockContext.Hospitals.Add(CreateHospital(1));
            // Build HospitalStaffProfile records
            _mockContext.HospitalStaffProfiles.Add(CreateHospitalStaffProfile(1, 1, 1));
            // Build PatientHistory records
            _mockContext.PatientMedicalHistories.Add(CreatePatientMedicalHistory(1, 1));
            // Build PatientProfile
            _mockContext.PatientProfiles.Add(CreatePatientProfile(1, 1, 1));
            Builder<FamilyProfile>.CreateListOfSize(10).Build().ForEach(m => _mockContext.FamilyProfiles.Add(m));
            _mockContext.Users = new MockDbSet<User>();
            Builder<User>.CreateListOfSize(10).Build().ForEach(m => _mockContext.Users.Add(m));

            // Build Consultation records
            _mockContext.Consultations.Add(CreateConsultation(1, 1, 1, 1, 1));
            // Build PatientConsultationReport records
            _mockContext.PatientConsultationReports.Add(CreatePatientConsultationReport(1, 1, 107));
            _mockContext.PatientConsultationReports.Add(CreatePatientConsultationReport(1, 1, 108));
            _mockContext.PatientConsultationReports.Add(CreatePatientConsultationReport(1, 1, 109));
            _mockContext.PatientConsultationReports.Add(CreatePatientConsultationReport(1, 1, 110));
            // Build ConsultationSoapNotesCptCode
            _mockContext.ConsultationSoapNotesCptCodes.Add(CreateConsultationSoapNotesCptCode(1, 1));
            // Build FamilyProfile records
            _mockContext.FamilyProfiles.Add(CreateFamilyProfile(1));
            // Build UserDependentRelationAuthorization records
            _mockContext.UserDependentRelationAuthorizations.Add(CreateAuthorization(1));
            // Build EncounterMetadata
            _mockContext.EncounterMetadata.Add(CreateEncounterMetadata(Guid.NewGuid(), 1, 1));
            // Build Codes
            _mockContext.Codes.Add(CreateCode(1, 1));
            
            // Addresses
            Builder<Address>.CreateListOfSize(110).Build().ForEach(m => _mockContext.Addresses.Add(m));
            Builder<PatientAddress>.CreateListOfSize(110).Build().ForEach(m => _mockContext.PatientAddresses.Add(m));
            

            _mockContext.SaveChanges();

            /*
             from p in _context.PatientProfiles
                        join con in _context.Consultations
                        join u in _context.Users 
                        join garProfile in _context.PatientProfiles o
                        join f in _context.FamilyProfiles on p.FamilyGroupId equals f.FamilyGroupId
                        join pa in _context.PatientAddresses o
                        join add in _context.Addresses 
             
             */


        }

        private Code CreateCode(int codeId, int hospitalId)
        {
            Code c = new Code
            {
                HospitalId = hospitalId,
                CodeId = codeId
            };
            return c;
        }

        private EncounterMetadata CreateEncounterMetadata(Guid id, int consultationId, int encounterId)
        {
            EncounterMetadata em = new EncounterMetadata();
            em.Id = id;
            em.ConsultationId = consultationId;
            em.EncounterId = encounterId;
            em.Metadata = @"<intakeQuestionnaire xmlns=""https://snap.md/api/v2/xml/encounters"">
  <medicationAllergies>
    <customCode code=""3"">
      <description>Allergic to a med</description>
    </customCode>
  </medicationAllergies>
  <surgeries />
  <medicalConditions>
    <customCode code=""1"">
      <description>I am a condition</description>
    </customCode>
    <customCode code=""2"">
      <description>I am something, too</description>
    </customCode>
  </medicalConditions>
  <medications />
  <encounterConcerns />
</intakeQuestionnaire>";
            return em;
        }

        private PatientProfile CreatePatientProfile(int patientId, int familyGroupId, int userId)
        {
            PatientProfile p = new PatientProfile
            {
                PatientId = patientId,
                FamilyGroupId = familyGroupId,
                UserId = userId
            };
            return p;
        }

        private Consultation CreateConsultation(int consultationId,
            int patientId,
            int consultantUserId,
            int hospitalId,
            int assignedDoctorId)
        {
            var c = new Consultation
            {
                ConsultationId = consultationId,
                PatientId = patientId,
                ConsultantUserId = consultantUserId,
                HospitalId = hospitalId,
                AssignedDoctorId = assignedDoctorId,
                PrimaryConsern = "primary concern",
                SecondaryConsern = "secondary concern"
            };
            return c;
        }

        private Hospital CreateHospital(int id)
        {
            Hospital h = new Hospital
            {
                HospitalId = id
            };
            return h;
        }

        private HospitalStaffProfile CreateHospitalStaffProfile(int staffId, int hospitalId, int userId)
        {
            HospitalStaffProfile p = new HospitalStaffProfile
            {
                HospitalId = hospitalId,
                StaffId = staffId,
                UserId = userId
            };
            return p;
        }

        private PatientMedicalHistory CreatePatientMedicalHistory(int historyId, int patientId)
        {
            PatientMedicalHistory h = new PatientMedicalHistory
            {
                HistoryId = historyId,
                PatientId =  patientId,
                Code_TakingMedication1 = new Code() { Description = "" },
                Code_TakingMedication2 = new Code() { Description = "" },
                Code_TakingMedication3 = new Code() { Description = "" },
                Code_AllergicMedication1 = new Code() { Description = "" },
                Code_AllergicMedication2 = new Code() { Description = "" },
                Code_AllergicMedication3 = new Code() { Description = "" },
                Code_AllergicMedication4 = new Code() { Description = "" }
            };
            return h;
        }

        private PatientConsultationReport CreatePatientConsultationReport(int patientId, int consultationId, int codeId)
        {
            PatientConsultationReport p = new PatientConsultationReport
            {
                PatientId = patientId,
                ConsultationId = consultationId,
                CodeId = codeId,
                ConsultationDescription = ""
            };
            return p;
        }

        private ConsultationSoapNotesCptCode CreateConsultationSoapNotesCptCode(int id, int consultationId)
        {
            ConsultationSoapNotesCptCode c = new ConsultationSoapNotesCptCode
            {
                Id = id,
                ConsultationId = consultationId
            };
            return c;
        }

        private FamilyProfile CreateFamilyProfile(int id)
        {
            FamilyProfile p = new FamilyProfile
            {
                FamilyGroupId = id
            };
            return p;
        }

        private UserDependentRelationAuthorization CreateAuthorization(int userId)
        {
            UserDependentRelationAuthorization a = new UserDependentRelationAuthorization
            {
                UserId = userId
            };
            return a;
        }

        class TestController : PatientConsultationsController
        {
            public TestController(ISnapContext mockContext) : base(mockContext)
            {
            }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    var mock = new Mock<ITokenIdentity>();
                    mock.SetupGet(m => m.UserId).Returns(1);
                    return mock.Object;
                }
            }
        }

        #endregion

    }
}
