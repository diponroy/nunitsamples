﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Core.Billing;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Data.Entities.Billing;
using SnapMD.Web.Api.Models;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class CopayControllerTests
    {
        [Test, Ignore]
        public void TestCopayFail()
        {
            const int consultationId = 1;
            const double consultationAmount = 100;

            //mocks
            Mock<ISnapContext> mockContext = MakeContextMock(consultationId, consultationAmount);
            Mock<IAuthorizeNetSettings> mockSettings = GatewaySettings.MakeSettingsMock();

            //test
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(r => r.ProfileId = "11111")
                .With(r => r.PaymentProfileId = "11223")
                .With(r => r.ConsultationId = consultationId)
                .With(r => r.Amount = consultationAmount)
                .Build();

            var con = new CopayController(mockContext.Object);
            Assert.Throws<HttpResponseException>(() => con.MakePayment(request, 1, 1, mockSettings.Object),
                "Expected because profile IDs are faked.");
        }

        [Test, Explicit]
        public void TestCopay()
        {
            const int consultationId = 1;
            const double consultationAmount = 100;

            //mocks
            Mock<ISnapContext> mockContext = MakeContextMock(consultationId, consultationAmount);
            Mock<IAuthorizeNetSettings> mockSettings = GatewaySettings.MakeSettingsMock();
            Mock<ITokenIdentity> mockUser = MakeUserMock();
            PaymentData paymentData = MakeCCMock();

            //register mock
            var target = new PaymentsController(mockContext.Object);
            IDictionary<string, string> actual = target.RegisterNewProfile(paymentData,
                mockSettings.Object,
                mockUser.Object);
            string profileId = actual["profileId"];
            string paymentProfileId = actual["paymentProfileId"];

            //test
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(r => r.ProfileId = profileId)
                .With(r => r.PaymentProfileId = paymentProfileId)
                .With(r => r.Email = mockUser.Object.User)
                .With(r => r.ConsultationId = consultationId)
                .With(r => r.Amount = consultationAmount)
                .Build();
            var con = new CopayController(mockContext.Object);
            Assert.DoesNotThrow(() => con.MakePayment(request, 1, 1, mockSettings.Object));
        }

        private static PaymentData MakeCCMock()
        {
            var paymentData = new PaymentData
            {
                CardNumber = "4111111111111111",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year
            };
            return paymentData;
        }

        private static Mock<ITokenIdentity> MakeUserMock()
        {
            var mockUser = new Mock<ITokenIdentity>();
            mockUser.SetupGet(m => m.ProviderId).Returns(1);
            mockUser.SetupGet(m => m.UserId).Returns(1);

            string hashCode = String.Format("{0:X}", DateTime.Now.Ticks.GetHashCode());
            string emailHash = string.Format("aaron.lord+{0}@snap.md", hashCode).ToLower();

            mockUser.SetupGet(m => m.User).Returns(emailHash);
            return mockUser;
        }

        private static Mock<ISnapContext> MakeContextMock(int consultationId, double consultaionAmount)
        {
            var mockContext = new Mock<ISnapContext>();

            var settings = new List<HospitalSetting>
            {
                new HospitalSetting { HospitalId = 1, Key = "AuthorizeNet_LoginID", Value = "867hTNWkm8" },
                new HospitalSetting { HospitalId = 1, Key = "AuthorizeNet_TransactionKey", Value = "7824m2DxJw3gGx3k" }
            };

            mockContext.SetupGet(m => m.HospitalSettings).Returns(settings.ToDbSet());
            mockContext.SetupGet(m => m.ConsultationPaymentsDetails)
                .Returns(new List<ConsultationPaymentsDetail>().ToDbSet());
            mockContext.SetupGet(m => m.Consultations).Returns(new List<Consultation>
            {
                new Consultation
                {
                    ConsultationId = consultationId,
                    ConsultationAmount = consultaionAmount
                }
            }.ToDbSet());
            mockContext.Setup(x => x.Hospitals).Returns(new List<Hospital>().ToDbSet());

            mockContext.SetupGet(c => c.BillingProfiles).Returns(new List<BillingProfile>().ToDbSet());
            return mockContext;
        }

        [Test]
        public void MakePayment_Payment_AlreadyDone_Throws_Exception()
        {
            //data
            Consultation consultation = Builder<Consultation>.CreateNew().Build();
            ConsultationPaymentsDetail details = Builder<ConsultationPaymentsDetail>.CreateNew()
                    .With(x => x.ConsultationId = consultation.ConsultationId)
                    .Build();
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                    .With(x => x.ConsultationId = consultation.ConsultationId)
                    .Build();

            //mock
            Mock<ISnapContext> dbContextMock = new Mock<ISnapContext>();
            dbContextMock.Setup(x => x.Hospitals).Returns(new List<Hospital>().ToDbSet());
            dbContextMock.Setup(x => x.Consultations).Returns(new List<Consultation>() { consultation }.ToDbSet());
            dbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>() { details }.ToDbSet());

            //contoller setup
            var controller = new CopayController(dbContextMock.Object);
            controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //tests
            HttpResponseException error = Assert.Catch<HttpResponseException>(() => controller.MakePayment(request, 1, 1, null));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual(CopayController.PaymentAlreadyMade, error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(11, 10)]
        [TestCase(10, 11)]
        [TestCase(10.01, 10)]
        [TestCase(10, 10.01)]
        public void MakePayment_WithCreditCard_RequestedAmount_Not_Same_As_ConsultationAmount_Throws_Exception(double requiredAmount, double paidAmount)
        {
            //data
            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.ConsultationAmount = requiredAmount)
                .With(x => x.IsHealthPlanApply = "N")
                .Build();
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(x => x.ConsultationId = consultation.ConsultationId)
                .With(x => x.Amount = paidAmount)   //not as consultaion's ConsultationAmount
                .Build();       

            //mock
            Mock<ISnapContext> dbContextMock = new Mock<ISnapContext>();
            dbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>().ToDbSet());
            dbContextMock.Setup(x => x.Consultations).Returns(new List<Consultation>(){consultation}.ToDbSet());
            dbContextMock.Setup(x => x.Hospitals).Returns(new List<Hospital>().ToDbSet());

            //contoller setup
            var controller = new CopayController(dbContextMock.Object);
            controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //tests
            HttpResponseException error = Assert.Catch<HttpResponseException>(() => controller.MakePayment(request, 1, 1, null));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual(String.Format("Payment amount should be {0:c}.", consultation.ConsultationAmount), error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(11, 10)]
        [TestCase(10, 11)]
        [TestCase(10.01, 10)]
        [TestCase(10, 10.01)]
        public void MakePayment_WithCreditCard_ConsultationAmount_Null_At_Consultation_ChecksFrom_Hospital_Setup(double requiredAmount, double paidAmount)
        {
            //data
            Hospital hospital = Builder<Hospital>.CreateNew()
                .With(x => x.ConsultationCharge = requiredAmount)
                .Build();
            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.ConsultationAmount = null)     //no amount set
                .With(x => x.IsHealthPlanApply = "N")
                .Build();
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(x => x.ConsultationId = consultation.ConsultationId)
                .With(x => x.Amount = paidAmount)   //not as hospital's default charge
                .Build();

            //mock
            Mock<ISnapContext> dbContextMock = new Mock<ISnapContext>();
            dbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>().ToDbSet());
            dbContextMock.Setup(x => x.Consultations).Returns(new List<Consultation>() { consultation }.ToDbSet());
            dbContextMock.Setup(x => x.Hospitals).Returns(new List<Hospital>(){hospital}.ToDbSet());

            //contoller setup
            var controller = new CopayController(dbContextMock.Object);
            controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //tests
            HttpResponseException error = Assert.Catch<HttpResponseException>(() => controller.MakePayment(request, 1, hospital.HospitalId, null));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual(String.Format("Payment amount should be {0:c}.", hospital.ConsultationCharge), error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        [TestCase(11, 10)]
        [TestCase(10, 11)]
        [TestCase(10.01, 10)]
        [TestCase(10, 10.01)]
        public void MakePayment_WithHealthPlane_RequestedAmount_Not_Same_As_CopayAmount_Throws_Exception(double requiredAmount, double paidAmount)
        {
            //data
            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.CopayAmount = requiredAmount)
                .With(x => x.IsHealthPlanApply = "Y")
                .Build();
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(x => x.ConsultationId = consultation.ConsultationId)
                .With(x => x.Amount = paidAmount)   //not as consultaion's copayAmount
                .Build();

            //mock
            Mock<ISnapContext> dbContextMock = new Mock<ISnapContext>();
            dbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>().ToDbSet());
            dbContextMock.Setup(x => x.Consultations).Returns(new List<Consultation>() { consultation }.ToDbSet());
            dbContextMock.Setup(x => x.Hospitals).Returns(new List<Hospital>().ToDbSet());

            //contoller setup
            var controller = new CopayController(dbContextMock.Object);
            controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //tests
            HttpResponseException error = Assert.Catch<HttpResponseException>(() => controller.MakePayment(request, 1, 1, null));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual(String.Format("Payment amount should be {0:c}.", consultation.CopayAmount), error.Response.Content.ReadAsAsync<string>().Result);
        }

        [Test]
        public void MakePayment_WithHealthPlane_CopayAmount_Null_Throws_Exception()
        {
            //data
            Consultation consultation = Builder<Consultation>.CreateNew()
                .With(x => x.CopayAmount = null)     //no amount set
                .With(x => x.IsHealthPlanApply = "Y")
                .Build();
            CopayRequest request = Builder<CopayRequest>.CreateNew()
                .With(x => x.ConsultationId = consultation.ConsultationId)
                .With(x => x.Amount = 100)   //not as hospital's default charge
                .Build();

            //mock
            Mock<ISnapContext> dbContextMock = new Mock<ISnapContext>();
            dbContextMock.Setup(x => x.ConsultationPaymentsDetails).Returns(new List<ConsultationPaymentsDetail>().ToDbSet());
            dbContextMock.Setup(x => x.Consultations).Returns(new List<Consultation>() { consultation }.ToDbSet());
            dbContextMock.Setup(x => x.Hospitals).Returns(new List<Hospital>().ToDbSet());

            //contoller setup
            var controller = new CopayController(dbContextMock.Object);
            controller.Request = new HttpRequestMessage();              //for Request.CreateResponse
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //tests
            HttpResponseException error = Assert.Catch<HttpResponseException>(() => controller.MakePayment(request, 1, 1, null));
            Assert.AreEqual(400, (int)error.Response.StatusCode);
            Assert.AreEqual("Health plane not applied successfully, copay amount is null.", error.Response.Content.ReadAsAsync<string>().Result);
        }


    }
}
