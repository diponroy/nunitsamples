﻿using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core.Interfaces;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    internal class ClinicianProfilesControllerTests
    {
        [Test]
        public void TestGet()
        {
            var mock = new Mock<ISnapContext>();
            var mockSet = Builder<HospitalStaffProfile>.CreateListOfSize(5).Build();
           
            mock.Setup(m => m.Set<HospitalStaffProfile>()).Returns(mockSet.ToDbSet());
            var controller = new TestController(mock.Object);
            controller.ProviderId = 2;

            ApiResponseV2<ClinicianProfilesResult> result = null;
            Assert.DoesNotThrow(() => result = controller.Get(2));
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Data.Count());
            var actual = result.Data.FirstOrDefault();
            Assert.IsNotNull(actual);
            Assert.AreEqual("Name2 LastName2", actual.FullName);
        }


        private class TestController : ClinicianProfilesController
        {
            public TestController(ISnapContext context) : base(context)
            {
            }

            public int ProviderId { private get; set; }

            public override ITokenIdentity SnapUser
            {
                get
                {
                    var mock = new Mock<ITokenIdentity>();
                    mock.SetupGet(m => m.ProviderId).Returns(ProviderId);
                    return mock.Object;
                }
            }
        }
    }
}
