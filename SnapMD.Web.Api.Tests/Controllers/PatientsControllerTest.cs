﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Web.Api.Models;
using SnapMD.Web.Api.Patients;
using SnapMD.Web.Api.Patients.Controllers;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class PatientsControllerTest
    {
        [Test]
        public void GetPatientsShouldHaveNewFields()
        {
            var dbSet = Builder<PatientProfile>.CreateListOfSize(10)
                .All().With(c => c.Location = new Location { Name = "Location" + c.PatientId }).Build().ToDbSet();
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(db => db.PatientProfiles).Returns(dbSet);

            var target = new PatientsController(mock.Object);
            IEnumerable<PatientProfile> actual;
            Assert.IsTrue(target.GetAuthorizedProfile(5, out actual));
            Assert.AreEqual(1, actual.Count());
            Assert.AreEqual(5, actual.Single().PatientId);
            Assert.AreEqual("Location5", actual.Single().Location.Name);
        }

        [Test]
        public void GetUserPatientProfile()
        {
            var dbSet = Builder<PatientProfile>.CreateListOfSize(1)
                .All().With(c => c.User = new User { TimeZoneId = 1, UserId = 0 }).With(c => c.UserId = 0).Build().ToDbSet();
            
            var dbSet2 = Builder<PatientAddress>.CreateListOfSize(10)
                .All().With(c => c.Address = new Address{ AddressId = 1, AddressText = "Test Address" }).With(c => c.PatientProfileId = 0).Build().ToDbSet();
   
            var mock = new Mock<ISnapContext>();
            mock.SetupGet(db => db.PatientProfiles).Returns(dbSet);
            mock.SetupGet(db => db.PatientAddresses).Returns(dbSet2);
            mock.SetupGet(db => db.Users).Returns(new List<User>().ToDbSet());
            mock.SetupGet(db => db.StandardTimeZones).Returns(new List<StandardTimeZone>().ToDbSet());

            var target = new PatientsController(mock.Object);

            var result = target.GetUserPatientProfile().Data.First();

            Assert.AreEqual(result.TimeZoneId, 1);
            Assert.AreEqual(result.FullName, "PatientName1 LastName1");
            Assert.AreEqual(result.TimeZone, "UTC");
        }

        
        [Test]
        public void TestInsertDisconnected()
        {
            var mock = Builder<AddPatientProfileRequest>.CreateNew().Build();
            mock.PatientMedicalHistoryData = Builder<PatientMedicalHistory>.CreateNew().Build();
            mock.PatientProfileData = Builder<PatientUpdateRequest>.CreateNew().Build();
            var dbSet1 = Builder<PatientProfile>.CreateListOfSize(10).Build().ToDbSet();
            var context = new Mock<ISnapContext>();
            context.SetupGet(db => db.PatientProfiles).Returns(dbSet1);
            var dbSet2 = Builder<User>.CreateListOfSize(10).Build().ToDbSet();
            context.SetupGet(db => db.Users).Returns(dbSet2);
            var dbSet3 = Builder<PatientMedicalHistory>.CreateListOfSize(10).Build().ToDbSet();
            context.SetupGet(db => db.PatientMedicalHistories).Returns(dbSet3);
            ISnapContext snapContext = context.Object;

            var target = new CoUserRegistration
            {
                CodesRepository = new CodeRepository(snapContext),
                MedicalHistoryRepository = new PatientMedicalHistoryRepository(snapContext),
                MetadataRepository = new PatientMetadataRepository(snapContext)
            };
            target.CreateDependentPatient(mock.PatientProfileData, 1, 2, 1, new PatientRepository(snapContext));
        }

    }
}
