﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Models;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class TimeZoneControllerTest
    {
        protected Mock<ISnapContext> MockDb { get; set; }

        protected TimeZoneController Controller { get; set; }

        [SetUp]
        public void Setup()
        {
            MockDb = new Mock<ISnapContext>();
        }

        [TearDown]
        public void Teardown()
        {
            MockDb = null;
            Controller = null;
        }

        protected void InitializeController()
        {
            if (MockDb == null)
            {
                throw new NullReferenceException("MockDb is null.");
            }
            Controller = new TimeZoneController(MockDb.Object);
        }

        [Test]
        public void Get_Success()
        {
            List<StandardTimeZone> timeZones = Builder<StandardTimeZone>.CreateListOfSize(2).Build().ToList();
            MockDb.Setup(x => x.StandardTimeZones).Returns(timeZones.ToDbSet());
            InitializeController();

            var result = Controller.Get();
            Assert.IsInstanceOf<OkNegotiatedContentResult<ApiResponse<List<TimeZoneModel>>>>(result);
            ApiResponse<List<TimeZoneModel>> contentResult = ((OkNegotiatedContentResult<ApiResponse<List<TimeZoneModel>>>)result).Content;
            Assert.IsTrue(contentResult.Success);

            List<TimeZoneModel> zones = contentResult.Data;
            Assert.AreEqual(2, zones.Count);
            Assert.AreEqual(timeZones[0].TimeZoneId, zones[0].Id);
            Assert.AreEqual(timeZones[0].TimeZoneDescription, zones[0].Name);
            Assert.AreEqual(timeZones[1].TimeZoneId, zones[1].Id);
            Assert.AreEqual(timeZones[1].TimeZoneDescription, zones[1].Name);
        }

        [Test]
        public void Get_Error()
        {
            InitializeController();
            var result = Controller.Get();
            Assert.IsInstanceOf<OkNegotiatedContentResult<ApiResponse<String>>>(result);
            ApiResponse<string> contentResult = ((OkNegotiatedContentResult<ApiResponse<String>>)result).Content;
            Assert.IsFalse(contentResult.Success);
            Assert.AreEqual("Unable to load Time Zone", contentResult.Data);
        }
    }
}
