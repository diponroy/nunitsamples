﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.ConnectedCare.ApiModels;
using SnapMD.Core;
using SnapMD.Core.Interfaces.Repositories;
using SnapMD.Data.Entities;
using SnapMD.Data.Repositories;
using SnapMD.Data.Repositories.Interfaces;
using SnapMD.Web.Api.Admin.Models;
using SnapMD.Web.Api.Controllers;
using SnapMD.Web.Api.Tests.Utilities;

namespace SnapMD.Web.Api.Tests.Controllers
{
    [TestFixture]
    public class HospitalControllerTest
    {
        private Mock<ISnapContext> _mock;
        private HospitalRepository _hospitalRepository;
        private PatientProfileRepository _patientProfileRepository;
        private HospitalSettingsRepository _hospitalSettingsRepository;

        private HospitalController _hospitalcontroller;
        private Mock<IHospitalRepository> _mockHopitalRepo;
        private Mock<IPatientProfileRepository> _mockpatientsRepo;
        private Mock<IHospitalSettingsRepository> _mocksettingsRepo;

        private OperatingHours[] hours;

        [SetUp]
        public void Setup()
        {
            _mock = new Mock<ISnapContext>();
            _hospitalRepository = new HospitalRepository(_mock.Object);
            _patientProfileRepository = new PatientProfileRepository(_mock.Object);
            _hospitalSettingsRepository = new HospitalSettingsRepository(_mock.Object);

            _hospitalcontroller = new HospitalController(_hospitalRepository,
                _patientProfileRepository, _hospitalSettingsRepository);

            _mockHopitalRepo = new Mock<IHospitalRepository>();
            _mockpatientsRepo = new Mock<IPatientProfileRepository>();
            _mocksettingsRepo = new Mock<IHospitalSettingsRepository>();

            hours = new[]
            {
                new OperatingHours()
                {
                    EndDayOfWeek = 1,
                    EndTime = TimeSpan.MaxValue,
                    HospitalId = 1,
                    OperatingHoursId = 1,
                    StartDayOfWeek = 1,
                    StartTime = TimeSpan.MaxValue
                },
                new OperatingHours()
                {
                    EndDayOfWeek = 2,
                    EndTime = TimeSpan.MaxValue,
                    HospitalId = 2,
                    OperatingHoursId = 2,
                    StartDayOfWeek = 2,
                    StartTime = TimeSpan.MaxValue
                },
            };
        }

        [Test]
        public void GetById()
        {
            var hospitals = Builder<Hospital>.CreateListOfSize(20).Build();
            _mock.SetupGet(m => m.Hospitals).Returns(hospitals.ToDbSet());
            _mock.SetupGet(m => m.HospitalSettings).Returns(new List<HospitalSetting>().ToDbSet());

            var actual = _hospitalcontroller.GetRawHospital(1);

            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.HospitalId);
        }

        [Test]
        public void FindHospitalByEmailId()
        {
            var u = new User {Email = "asdf@asdf.com", UserId = 10};
            var patients = Builder<PatientProfile>.CreateListOfSize(15).Build();

            patients.Add(new PatientProfile {User = u, HospitalId = 2});
            var users = Builder<User>.CreateListOfSize(9).Build();
            users.Add(u);
            var hospitals = Builder<Hospital>.CreateListOfSize(20).Build();
            
            _mock.SetupGet(m => m.HospitalSettings).Returns(new List<HospitalSetting>().ToDbSet());
            _mock.SetupGet(m => m.PatientProfiles).Returns(patients.ToDbSet());
            _mock.SetupGet(m => m.Hospitals).Returns(hospitals.ToDbSet());
            _mock.SetupGet(m => m.Users).Returns(users.ToDbSet());

            var result = _hospitalcontroller.Get("asdf@asdf.com");
            var data = result.Data;

            Assert.AreEqual(1, data.Count());
        }

        [Test]
        public void TestGetWithModules()
        {            
            var settings = new List<HospitalSetting>
            {
                new HospitalSetting
                {
                    HospitalId = 2,
                    Id = 4,
                    Key = SnapModules.GetKey(SnapModule.CPTCodes),
                    Value = "True"
                },

                new HospitalSetting
                {
                    HospitalId = 2,
                    Id = 5,
                    Key = SnapModules.GetKey(SnapModule.ClinicianSearch),
                    Value = "False"
                }
            };
            var hospitals = Builder<Hospital>.CreateListOfSize(20).Build();
            _mock.SetupGet(m => m.Hospitals).Returns(hospitals.ToDbSet());
            _mock.SetupGet(m => m.HospitalSettings).Returns(settings.ToDbSet());

            var result = _hospitalcontroller.GetHospital(2);

            Assert.AreEqual(1, result.EnabledModules.Count);
            Assert.AreEqual("CPTCodes", result.EnabledModules[0]);
        }

        [Test]
        public void TestWithSsoSettings()
        {
            var settings = new List<HospitalSetting>
            {
                new HospitalSetting
                {
                    HospitalId = 2,
                    Id = 4,
                    Key = HospitalSettingsKeys.CustomerSso,
                    Value = CustomerSsoType.Optional.ToString()
                },

                new HospitalSetting
                {
                    HospitalId = 2,
                    Id = 5,
                    Key = HospitalSettingsKeys.SsoLoginLinkText,
                    Value = "Sign in with Leopardon account"
                }
            };
			
            var hospitals = Builder<Hospital>.CreateListOfSize(20).Build();
            _mock.SetupGet(m => m.Hospitals).Returns(hospitals.ToDbSet());
            _mock.SetupGet(m => m.HospitalSettings).Returns(settings.ToDbSet());

            var result = _hospitalcontroller.GetHospital(2);

            Assert.AreEqual(CustomerSsoType.Optional, result.CustomerSso);
            Assert.AreEqual("Sign in with Leopardon account", result.CustomerSsoLinkText);
        }

        [Test]
        public void GetWorkingHours_HospitalNotFound_Returns404()
        {   
            _mockHopitalRepo.Setup(x => x.GetOperatingHours(It.IsAny<int>())).Returns(() => null);

            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
                _mocksettingsRepo.Object);

            var answer = _hospitalcontroller.GetOperatingHours(1);

            Assert.IsTrue(answer.Is404());
        }
        
        [Test]
        public void GetWorkingHours_HospitalsFound_ReturnAllhospitals()
        {
            _mockHopitalRepo.Setup(x => x.GetOperatingHours(It.IsAny<int>())).Returns(() => hours);

            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
                _mocksettingsRepo.Object);

            var answer = _hospitalcontroller.GetOperatingHours(1);

            Assert.IsTrue(answer.Is200<IEnumerable<OperatingHourDTO>>());
            Assert.AreEqual(2, ((OkNegotiatedContentResult<IEnumerable<OperatingHourDTO>>)answer).Content.Count());
        }

        [Test]
        public void DeleteWorkingHours_CorrectHours_DeletedSucessful()
        {
            _mockHopitalRepo.Setup(x => x.DeleteOperatingHours(It.IsAny<int>())).Returns(true);
            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
               _mocksettingsRepo.Object);
            
            var answer = _hospitalcontroller.DeleteOperatingHours(1);

            Assert.IsTrue(answer.Is200());
        }

        [Test]
        public void DeleteWorkingHours_HoursNotExist_Returns404()
        {
            _mockHopitalRepo.Setup(x => x.DeleteOperatingHours(It.IsAny<int>())).Returns(false);
            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
               _mocksettingsRepo.Object);

            var answer = _hospitalcontroller.DeleteOperatingHours(1);

            Assert.IsTrue(answer.Is404());
        }

        [Test]
        public void UpdateWorkingHours_HospitalNotExist_Returns404()
        {
            _mockHopitalRepo.Setup(x => x.AddUpdateOperatingHours(It.IsAny<OperatingHours>()))
                .Returns(() => null);
            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
               _mocksettingsRepo.Object);

            var operatingHourDTO = new OperatingHourDTO() {EndDayOfWeek = 1, OperatingHoursId = 10, 
                EndTime = "10:00", HospitalId = 1, StartDayOfWeek = 1, StartTime = "11:00"};
            var answer = _hospitalcontroller.SaveOperatingHours(operatingHourDTO, 1);

            Assert.IsTrue(answer.Is404());
        }

        [Test]
        [ExpectedException(typeof(HttpResponseException))]
        public void UpdateWorkingHours_HospitalWrong_ThrowsException()
        {
            _mockHopitalRepo.Setup(x => x.AddUpdateOperatingHours(It.IsAny<OperatingHours>()))
                .Returns(() => null);
            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
               _mocksettingsRepo.Object);

            var answer = _hospitalcontroller.SaveOperatingHours(new OperatingHourDTO(), 1);

            Assert.IsTrue(answer.Is404());
        }

        [Test]
        [ExpectedException(typeof(HttpResponseException))]
        public void UpdateWorkingHours_HospitalIsNull_ThrowsException()
        {
            _mockHopitalRepo.Setup(x => x.AddUpdateOperatingHours(It.IsAny<OperatingHours>()))
                .Returns(() => null);
            _hospitalcontroller = new HospitalController(_mockHopitalRepo.Object, _mockpatientsRepo.Object,
               _mocksettingsRepo.Object);

            var answer = _hospitalcontroller.SaveOperatingHours(null, 1);

            Assert.IsTrue(answer.Is404());
        }

        [TearDown]
        public void ClenUp()
        {
            _mock = null;
        }
    }
}