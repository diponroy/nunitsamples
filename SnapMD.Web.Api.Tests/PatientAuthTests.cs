﻿using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;
using SnapMD.Data.Entities;
using SnapMD.Web.Api.Areas.Patients;
using SnapMD.Web.Api.Patients;

namespace SnapMD.Web.Api.Tests
{
    [TestFixture]
    public class PatientAuthTests
    {
        [Test]
        public void TestConstructor()
        {
            var context = new Mock<ISnapContext>();
            var patientAuth = new PatientAuthorization(1, context.Object);
            Assert.AreEqual(1, patientAuth.UserId);
        }

        [Test]
        public void TestSelfAuthorization()
        {
            var patientData = Builder<PatientProfile>.CreateListOfSize(2).Build();
            var clinicianData = Builder<HospitalStaffProfile>.CreateListOfSize(1).Build();
            var context = new Mock<ISnapContext>();
            context.SetupGet(c => c.PatientProfiles).Returns(patientData.ToDbSet());
            context.SetupGet(c => c.HospitalStaffProfiles).Returns(clinicianData.ToDbSet());
            var patientAuth = new PatientAuthorization(1, context.Object);
            Assert.IsTrue(patientAuth.IsAuthorized(1));
        }
    }
}
