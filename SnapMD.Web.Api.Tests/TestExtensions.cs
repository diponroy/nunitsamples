﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;

namespace SnapMD.Web.Api.Tests
{
    public static class TestExtensions
    {
        public static IDbSet<T> ToDbSet<T>(this IList<T> list) where T : class
        {
            // Borrowed: http://msdn.microsoft.com/en-us/library/dn314429.aspx#queryTest
            var data = list.AsQueryable();
            var mockSet = new Mock<IDbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            mockSet.Setup(x => x.Add(It.IsAny<T>())).Callback((T item) => { list.Add(item); });
            return mockSet.Object;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }
    }
}