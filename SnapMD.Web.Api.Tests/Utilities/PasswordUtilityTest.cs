﻿using System.Collections.Generic;
using NUnit.Framework;
using SnapMD.ConnectedCare.AuthLibrary;
using SnapMD.Web.Api.Utilities;

namespace SnapMD.Web.Api.Tests.Utilities
{
    [TestFixture]
    public class PasswordUtilityTest
    {
        [Test]
        public void HashPassword_Returns_Encrypted_Password()
        {
            string password = "Password@123";
            string hashPassword = PasswordUtility.HashPassword(password); /*encrypted password*/
            bool isSame = PasswordUtility.Verify(password, hashPassword);
            Assert.IsTrue(isSame);
        }

        [Test]
        [TestCase(null, null, false)]
        [TestCase("", "", false)]
        [TestCase("123", "$2a$10$goLKkH9GiAc/VU0fTHMateaZRarGXQZLp9tjFA5frm4tO5.6uP3i.", false)]
        [TestCase("Password@123", "$2a$10$goLKkH9GiAc/VU0fTHMateaZRarGXQZLp9tjFA5frm4tO5.6uP3i.", true)]
        public void Verify_Checks_If_Password_And_EncryptedPassword_SameOrNot(string password, string hashPassword, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, PasswordUtility.Verify(password, hashPassword));
        }

        /*
         * validation for
         * Length 8-20
         * At least one capital letter
         * At least one lowercase letter
         * At least one number
         * No spaces
         */

        [Test]
        [TestCase("Pas@123", @"Length 8-20: ^.{8,20}$")]
        [TestCase("Pas@123Pas@123Pas@123", @"Length 8-20: ^.{8,20}$")]
        [TestCase("password@123", @"At least one capital letter: [A-Z]")]
        [TestCase("PASSWORD@123", @"At least one lowercase letter: [a-z]")]
        [TestCase("PASSWORD@qwe", @"At least one number: [0-9]")]
        [TestCase("Password@ 123", @"No spaces")]
        [TestCase(" Password@123", @"No spaces")]
        [TestCase("Password@123 ", @"No spaces")]
        [TestCase("Passwor@  123", @"No spaces")]
        [TestCase("  Passwor@123", @"No spaces")]
        [TestCase("Passwor@123  ", @"No spaces")]
        public void Validate_Fail(string password, string errorMessage)
        {
            List<string> errors;
            bool isValid = PasswordUtility.Validate(password, out errors);
            Assert.IsFalse(isValid);
            Assert.AreEqual(1, errors.Count);
            Assert.IsTrue(errors.Contains(errorMessage));
        }

        [Test]
        [TestCase(null, 5)]
        [TestCase("", 5)]
        public void Validate_Fail_Multiple(string password, int errorCount)
        {
            List<string> errors;
            bool isValid = PasswordUtility.Validate(password, out errors);
            Assert.False(isValid);
            Assert.AreEqual(errorCount, errors.Count);
        }

        [Test]
        [TestCase("Password123")]
        [TestCase("Password@123")]
        public void Validate_Success(string password)
        {
            List<string> errors;
            bool isValid = PasswordUtility.Validate(password, out errors);
            Assert.IsTrue(isValid);
            Assert.AreEqual(0, errors.Count);
        }
    }
}
