﻿using System.Web.Http;
using System.Web.Http.Results;
using NUnit.Framework;

namespace SnapMD.Web.Api.Tests.Utilities
{
    public static class Helpers
    {
        /// <summary>
        /// check is responce is 404 (not found)
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool Is404(this IHttpActionResult result)
        {
            return result.GetType() == typeof (NotFoundResult); 
        } 
        
        /// <summary>
        /// check is responce is 200 (OK) with content
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool Is200<T>(this IHttpActionResult result)
        {
            return result.GetType() == typeof (OkNegotiatedContentResult<T>); 
        }
       
        /// <summary>
        /// check is responce is 200 (OK) with content
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool Is200(this IHttpActionResult result)
        {
            return result.GetType() == typeof (OkResult); 
        }
       
    }
}
