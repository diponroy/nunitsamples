﻿using System;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using SnapMD.Utilities;

namespace SnapMD.Web.Api.Tests.Utilities
{
    [TestFixture]
    public class JsonTimeConversionTests
    {
        [Test]
        public void TestTimeZoneConversion()
        {
            var input = new
            {
                SourceDate = new DateTimeOffset(2015, 10, 11, 19, 0, 0, new TimeSpan(-8, 0, 0))
            };

            string actual = JsonConvert.SerializeObject(input, GetJsonSerializerSettings());
            var expected = @"{
  ""$id"": ""1"",
  ""sourceDate"": ""2015-10-11T19:00:00-08:00""
}".Replace("\r\n", "\n");

            Debug.WriteLine(actual);

            Assert.AreEqual(expected, actual.Replace("\r\n", "\n"));
        }

        public class TimeZoneTestItem
        {
            public DateTimeOffset SourceDate { get; set; }
        }

        [Test]
        public void TestTimeZoneConversionWithExtensionUtility()
        {
            var original = new DateTimeOffset(2015, 10, 11, 19, 0, 0, TimeSpan.Zero);

            var input = new[] {  
                new TimeZoneTestItem
                {
                    SourceDate = original
                }
            };

            var converted = input.ConvertTimeZones2(TimeZoneInfo.Utc,
                TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"));

            Assert.AreEqual(original, converted.ElementAt(0).SourceDate);
            Assert.AreNotEqual(original.ToString(), converted.ElementAt(0).SourceDate.ToString());

            string actual = JsonConvert.SerializeObject(converted, GetJsonSerializerSettings());
            var expected = @"[
  {
    ""$id"": ""1"",
    ""sourceDate"": ""2015-10-11T14:00:00-05:00""
  }
]".Replace("\r\n", "\n");

            Debug.WriteLine(actual);

            Assert.AreEqual(expected, actual.Replace("\r\n", "\n"));
        }

        /// <summary>
        /// Matches the serialization settings used by the API project at design time.
        /// </summary>
        /// <returns></returns>
        internal static JsonSerializerSettings GetJsonSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver =
                    new CamelCasePropertyNamesContractResolver(),
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };
        }

    }
}
