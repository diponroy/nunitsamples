﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using NUnit.Framework;
using SnapMD.Web.Api.Utilities;

namespace SnapMD.Web.Api.Tests.Utilities
{
    [TestFixture]
    public class HttpContextUtilityTest
    {
        /*http://stackoverflow.com/questions/20164298/net-how-to-build-a-url*/
        [Test]
        public void Uri()
        {
            var uri = new Uri("https://snap.local.com/api/v2/patients/diponsust@gmail.com/mail?type=resetPassword&hospitalId=1");

            var url = uri.ToString();
            string host = uri.Host.ToLower();
            string domainPrefix = url.Substring(0, url.IndexOf(host));
            string baseUrl = domainPrefix + host;

            Assert.AreEqual("https", uri.Scheme);
            Assert.AreEqual("snap.local.com", uri.Host);
            Assert.AreEqual("https://", domainPrefix);
            Assert.AreEqual("https://snap.local.com", baseUrl);
            Assert.AreEqual("https://snap.local.com", new UriBuilder(uri.Scheme, uri.Host).Uri.ToString().TrimEnd('/'));
        }

        [Test]
        [TestCase("https://www.live.com", "https")]
        [TestCase("http://www.live.com", "http")]
        public void CurrentRequestScheme(string mockReuestUrl, string expectedScheme)
        {
            HttpContextUtility.MockCurrent("", mockReuestUrl, "");
            var scheme = HttpContextUtility.CurrentRequestScheme();
            Assert.AreEqual(expectedScheme, scheme);
        }

        [Test]
        [TestCase("https://www.live.com", "snap.local.com", "https://snap.local.com")]
        [TestCase("http://www.live.com", "snap.local", "http://snap.local")]
        public void UrlFromCurrentRequest(string mockReuestUrl, string hospitalDomainName, string expectedUrl)
        {
            HttpContextUtility.MockCurrent("", mockReuestUrl, "");
            var url = HttpContextUtility.UrlFromCurrentRequest(hospitalDomainName);
            Assert.AreEqual(expectedUrl, url);
        }
    }
}
